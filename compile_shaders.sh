#!/bin/bash
set -e

cd -P -- "$(dirname -- "$0")/src"
compiletype="compact"
checktimestamps="true"
quiet="false"

for arg in "$@"
do
    case "$arg" in
        "--pretty" | "--style=pretty")
            compiletype="pretty"
            ;;
        "--compact" | "--style=compact")
            compiletype="compact"
            ;;
        "--no-check-timestamps" | "--force")
            checktimestamps="false"
            ;;
        "--check-timestamps")
            checktimestamps="true"
            ;;
        "--quiet")
            quiet="true"
            ;;
        "--loud" | "-v" | "--verbose")
            quiet="false"
            ;;
        *)
            echo "Unknown argument $arg."
            exit 1
            ;;
    esac
done

cleanup ()
{
    arr=("$@")
    for file in "${arr[@]}"
    do
        rm "${file}"
    done
}

verboseecho ()
{
    if [[ "$quiet" == "false" ]]
    then
        echo -e "$@"
    fi
}

createShader ()
{
    outputdst="../data/shaders/${2}"

    if [[ "$checktimestamps" == "true" && -f "$outputdst" ]]
    then
        usecached="true"
        for shadersource in "shaders/${1}."*
        do
            if [[ "$shadersource" -nt "$outputdst" ]]
            then
                usecached="false";
            fi
        done
        if [[ "$usecached" == "true" ]]
        then
            verboseecho "Compiling shader ${1}: \033[36mSkipped (cached).\033[0m"
            return;
        fi
    fi

    verboseecho -n "Compiling shader ${1}: "

    compiled=()
    for shader in "shaders/${1}."*
    do
        currfile="$(mktemp shader.XXXXXX)"
        compiled+=("${currfile}")
        glslc -I ../include "${shader}" -o "${currfile}" || (verboseecho "\033[31mFailed.\033[0m"; cleanup "${compiled[@]}"; exit 1)
    done
    cmdline=("--${compiletype}" "--output" "$outputdst")
    for module in "${compiled[@]}"
    do
        cmdline+=("--module" "${module}" "deflated_base64")
    done
    spirv-info "${cmdline[@]}"
    cleanup "${compiled[@]}"
    verboseecho "\033[32mSuccess.\033[0m"
}

createShader "copy-to-swapchain" "copy-to-swapchain.json"
createShader "default" "default.json"
createShader "debug-draw-normals" "debug-draw-normals.json"
createShader "draw-lines" "draw-lines.json"
createShader "flat-shaded" "flat-shaded.json"
createShader "plotter" "plotter.json"
createShader "texturer/drawmesh" "texturer-drawmesh.json"
createShader "texturer/average" "texturer-average.json"
createShader "texturer/qualitymapper" "texturer-qualitymapper.json"
createShader "texturer/colourrenderer" "texturer-colourrenderer.json"
createShader "texturer/colourrenderer" "texturer-colourrenderer.json"
