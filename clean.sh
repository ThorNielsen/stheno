#!/bin/bash
if [[ -d build ]]; then
    # Bad things may happen when running rm with -rf (e.g. if run in the wrong
    # directory then all files are forcibly removed; including read-only files
    # if their parent directory is writable!). Since we only have a single file
    # which may be write-protected, it is probably slightly better to explicitly
    # remove that forcibly and then only running rm -r instead.
    if [[ -f build/install_manifest.txt ]]; then
        rm -f build/install_manifest.txt
    fi
    rm -r build
fi
if [[ -d install ]]; then
    rm -r install
fi
