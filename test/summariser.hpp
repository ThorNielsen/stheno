#ifndef TEST_SUMMARISER_HPP_INCLUDED
#define TEST_SUMMARISER_HPP_INCLUDED

#include "interpreter/error.hpp"

#include <expected>
#include <iterator>
#include <iostream>
#include <map>
#include <source_location>
#include <vector>

struct TestSummariser
{
public:
    TestSummariser() = default;
    TestSummariser(TestSummariser&& other) = default;
    TestSummariser& operator=(TestSummariser&&) = default;

    TestSummariser& operator+=(TestSummariser&& other)
    {
        m_failures.insert(m_failures.end(),
                          std::make_move_iterator(other.m_failures.begin()),
                          std::make_move_iterator(other.m_failures.end()));
        m_successes += other.m_successes;
        return *this;
    }

    ~TestSummariser()
    {
        print();
    }

    void expect(std::expected<bool, std::unique_ptr<interpreter::CompilationError>> result,
                std::source_location location = std::source_location::current())
    {
        if (result.has_value())
        {
            if (*result)
            {
                ++m_successes;
                return;
            }
            m_failures.emplace_back(location, "");
        }
        else
        {
            const auto& error = result.error();
            std::string message;
            if (auto errorAt = error->location())
            {
                message = std::format("Error in {} (line {}, column {}): {}",
                                      error->compilerPass(),
                                      errorAt->rangeBegin.line+1,
                                      errorAt->rangeBegin.character,
                                      error->description());
            }
            else
            {
                message = std::format("Error in {}: {}",
                                      error->compilerPass(),
                                      error->description());
            }
            m_failures.emplace_back(location, std::move(message));
        }
    }

    void expectError(std::expected<bool, std::unique_ptr<interpreter::CompilationError>> result,
                     std::string pass,
                     std::source_location location = std::source_location::current())
    {
        if (result.has_value())
        {
            m_failures.emplace_back(location, "Expected error in " + pass);
        }
        else
        {
            const auto& error = result.error();
            if (error->compilerPass() != pass)
            {
                m_failures.emplace_back(location,
                                        std::format("Expected error in {}, but got error in {}.",
                                                    pass, error->compilerPass()));
            }
            else
            {
                ++m_successes;
                return;
            }
        }
    }

    void expect(bool expr, std::source_location location = std::source_location::current())
    {
        if (!expr) m_failures.emplace_back(location, "");
        else ++m_successes;
    }

    void expectFalse(bool expr, std::source_location location = std::source_location::current())
    {
        if (expr) m_failures.emplace_back(location, "");
        else ++m_successes;
    }

    int returnCode() const noexcept
    {
        return !m_failures.empty();
    }

private:
    void print()
    {
        if (!m_successes && m_failures.empty())
        {
            std::cerr << "\033[36No tests ran.\n";
            return;
        }
        const char* successColour = m_successes ? "\033[32m" : "\033[30m";
        const char* failureColour = m_failures.empty() ? "\033[30m" : "\033[31m";
        std::cerr << std::format("Ran {} tests: {}{} succeded\033[0m, {}{} failed\033[0m.\n",
                                 m_successes + m_failures.size(),
                                 successColour,
                                 m_successes,
                                 failureColour,
                                 m_failures.size());

        if (m_failures.empty()) return;

        std::map
        <
            std::string_view,
            std::map
            <
                std::string_view,
                std::vector
                <
                    std::pair
                    <
                        decltype(std::source_location{}.line()),
                        std::string
                    >
                >
            >
        > failureLocations;

        for (auto& failure : m_failures)
        {
            failureLocations[failure.first.file_name()]
                            [failure.first.function_name()]
                            .emplace_back(failure.first.line(), failure.second);
        }

        for (const auto& [fileName, locationsWithFunction] : failureLocations)
        {
            std::cerr << std::format("\033[31m{}\033[0m:\n", fileName);
            for (const auto& [functionName, failures] : locationsWithFunction)
            {
                std::cerr << std::format("  {} failure{} in \033[31m{}\033[0m:\n",
                                         failures.size(),
                                         failures.size() > 1 ? "s" : "",
                                         functionName);
                for (const auto& [line, message] : failures)
                {
                    std::cerr << std::format("    * Test failed at line {}; full path: {}:{}\n",
                                             line,
                                             fileName,
                                             line);
                    if (!message.empty())
                    {
                        std::cerr << "      Message: " << message << "\n";
                    }
                }
            }
        }
    }

    std::vector<std::pair<std::source_location, std::string>> m_failures;
    std::size_t m_successes = 0;
};

#endif // TEST_SUMMARISER_HPP_INCLUDED

