#include "summariser.hpp"

#include "interpreter/lexer.hpp"

#include <kraken/utility/range.hpp>

using namespace interpreter;

bool checkTokenCount(std::string toParse, std::size_t expectedCount)
{
    return tokenise(toParse)
        .transform([expectedCount](auto tokens)
        {
            if (tokens.empty() || tokens.back().type != TokenType::Eof) return false;
            return 1 + expectedCount == tokens.size();
        })
        .value_or(false);
}

auto checkTokenTypes(std::string toParse, const std::vector<TokenType>& expected)
{
    return tokenise(toParse)
        .transform([&expected](auto tokens) -> bool
    {
        if (tokens.size() != expected.size()) return false;
        for (auto i : kraken::range(tokens.size()))
        {
            if (tokens[i].type != expected[i]) return false;
        }
        return true;
    });
}

int main()
{
    TestSummariser tester;

    tester.expect(checkTokenCount("", 0));
    tester.expect(checkTokenCount("2+2", 3));
    tester.expect(checkTokenCount("2 +       2", 3));
    tester.expect(checkTokenCount("abc xyz zyz,zygy ()((9)))(", 14));
    tester.expectFalse(checkTokenCount("9", 3));
    tester.expect(checkTokenCount("9", 1));

    tester.expect(checkTokenTypes("", {TokenType::Eof}));
    tester.expect(checkTokenTypes("2 +2", {TokenType::Number, TokenType::Plus, TokenType::Number, TokenType::Eof}));
    tester.expect(checkTokenTypes("2+ 2", {TokenType::Number, TokenType::Plus, TokenType::Number, TokenType::Eof}));
    tester.expectError(checkTokenTypes("'", {TokenType::Eof}),
                       "lexer");
    tester.expect(checkTokenTypes("2abc2", {TokenType::Number, TokenType::Identifier, TokenType::Eof}));
    tester.expect(checkTokenTypes("2abc 2", {TokenType::Number, TokenType::Identifier, TokenType::Number, TokenType::Eof}));
    tester.expect(checkTokenTypes("1+7-3*5/2",
                                  {TokenType::Number, TokenType::Plus,
                                   TokenType::Number, TokenType::Dash,
                                   TokenType::Number, TokenType::Star,
                                   TokenType::Number, TokenType::Slash,
                                   TokenType::Number, TokenType::Eof}));

    tester.expect(checkTokenTypes("2^3", {TokenType::Number, TokenType::Caret, TokenType::Number, TokenType::Eof}));

    return tester.returnCode();
}
