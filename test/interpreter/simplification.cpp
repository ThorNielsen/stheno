#include "summariser.hpp"

#include "interpreter/lexer.hpp"
#include "interpreter/parser.hpp"
#include "interpreter/astmanip/simplification.hpp"
#include "interpreter/astmanip/to_string.hpp"

#include <kraken/utility/range.hpp>

using namespace interpreter;

std::string simplify(std::string input)
{
    auto result = tokenise(input)
                  .and_then(parse)
                  .transform(simplifyProgram)
                  .transform([](auto&& pst){ return to_string(pst); })
                      .transform_error([](auto error) { return std::string(error->description()); });
    return result.value_or(result.error_or("This string is not possible to get."));
}

int main()
{
    TestSummariser tester;

    tester.expect(simplify("x+x") == "2*x");
    tester.expect(simplify("diff(x, x)") == "1");
    tester.expect(simplify("gradient(3*x+2*y+w-5*z)") == "[3, 2, 1, -5]");

    return tester.returnCode();
}
