#include "messyrenderer.hpp"

#include <algorithm>
#include <array>
#include <atomic>
#include <cstddef>
#include <cstring>
#include <filesystem>
#include <fstream>
#include <functional>
#include <iostream>
#include <optional>
#include <string>
#include <vector>
#include <vulkan/vulkan.h>

#include <kraken/globaltypes.hpp>
#include <kraken/image/image.hpp>
#include <kraken/math/matrix.hpp>
#include <kraken/utility/json.hpp>
#include <kraken/utility/memory.hpp>
#include <kraken/utility/timer.hpp>

#include "plotter/gpucompiler.hpp"

std::optional<kraken::JSONValue> readJSON(fs::path path)
{
    std::ifstream input(path);
    if (!input.is_open()) return std::nullopt;
    kraken::JSONValue json;
    input >> json;
    if (input.fail() || input.bad()) return std::nullopt;
    return json;
}

MovementState modsToMovementState(kraken::window::KeyModifiers mods)
{
    if (mods.ctrl) return MovementState::Scaling;
    else if (mods.shift) return MovementState::Translating;
    else return MovementState::Rotating;
}

void MessyRenderer::setupWindowCallbacks()
{
    using namespace kraken::window;
    m_resized = false;
    m_window.setMaximiseCallback([this]{ m_resized = true; });
    m_window.setFramebufferResizeCallback([this](U32, U32){ m_resized = true; });
    m_window.setMousePressCallback(
    [this](Mouse mouse, KeyModifiers mods)
    {
        if (mouse == Mouse::Middle
            && m_currMovement == MovementState::None)
        {
            m_currMovement = modsToMovementState(mods);
        }
        else if (mouse == Mouse::Left)
        {
            updateSelectedModel(m_hoveringModel);

            if (auto currVertex = getCurrentVertex())
            {
                m_currentDrawingLine.vertices.push_back(currVertex.value());
                m_currLineLastDepth = m_currHoverDepth;
            }
        }
        else if (mouse == Mouse::Right)
        {
            if (m_hoveringModel.has_value()
                && m_currHoverPoint.has_value())
            {
                m_currDrag = DragMovementInfo{};
                m_currDrag->objectIndex = *m_hoveringModel;
                m_currDrag->startPos = currFrame().cursorPos;
                m_currDrag->movementOrigPos = *m_currHoverPoint;
                std::lock_guard guard(m_modelMutex);
                m_currDrag->origTransform = m_models[*m_hoveringModel].transform.cast<double>();
                m_currDrag->state = modsToMovementState(mods);
            }
        }
    });

    m_window.setMouseReleaseCallback
    (
        [this](Mouse mouse, KeyModifiers)
        {
            auto hasMoved = m_currDrag.has_value()
                         && length(m_currDrag->startPos - currFrame().cursorPos) > 1e-2;

            if (mouse == Mouse::Left)
            {
                auto currVertex = getCurrentVertex();
                if (currVertex.has_value() && !m_window.isPressed(Key::LeftShift))
                {
                    m_currentDrawingLine.vertices.push_back(*currVertex);
                    endCurrentLine();
                }
            }
            if (mouse == Mouse::Middle)
            {
                if (m_currMovement != MovementState::None)
                {
                    m_camera->endTransform();
                }
                if (!hasMoved)
                {
                    // Todo check if we stayed at the original position and if so
                    // recenter the camera to look at the clicked point, if any.
                    std::cerr << "Todo: Recenter camera.\n";
                }
                m_currMovement = MovementState::None;
            }

            if (mouse == Mouse::Right
                && m_currDrag.has_value())
            {
                std::lock_guard guard(m_modelMutex);
                m_models[m_currDrag->objectIndex].transform = m_currDrag->continueDrag(m_camera->worldToCamera(),
                                                                                       m_perspective,
                                                                                       currFrame().cursorPos,
                                                                                       m_window.framebufferSize()).cast<float>();
                m_currDrag.reset();
            }
        }
    );

    m_window.setScrollCallback([this](F64, F64 dy)
    {
        if (m_currMovement == MovementState::None
            || m_currMovement == MovementState::Scaling)
        {
            m_camera->scale(dy * -0.15);
        }
    });

    m_window.setCursorPositionCallback([this](F64 posX, F64 posY)
    {
        dvec2 newPos{posX, posY};
        auto diff = newPos - m_lastCursorPosition;
        m_lastCursorPosition = newPos;
        switch (m_currMovement)
        {
        case MovementState::None:
        default:
            break;
        case MovementState::Translating:
            m_camera->translate({diff.x * -0.001, diff.y * 0.001});
            break;
        case MovementState::Rotating:
            m_camera->rotate(diff*-0.002);
            break;
        case MovementState::Scaling:
            m_camera->scale(diff.y*0.0025);
            break;
        }
        if (m_currDrag.has_value())
        {
            std::lock_guard guard(m_modelMutex);
            m_models[m_currDrag->objectIndex].transform = m_currDrag->continueDrag(m_camera->worldToCamera(),
                                                                                   m_perspective,
                                                                                   currFrame().cursorPos,
                                                                                   m_window.framebufferSize()).cast<float>();
        }
    });

    m_window.setStringDropCallback([this](std::vector<kraken::String> strings)
    {
        for (auto elem : strings)
        {
            auto path = fs::path(elem.str());
            std::error_code err;
            auto status = fs::status(path, err);
            if (err)
            {
                std::cerr << "Failed to determine if "
                          << elem
                          << " is a valid path: " << err.message() << "\n";
                continue;
            }
            if (fs::exists(status))
            {
                if (path.extension() == ".json")
                {
                    m_sceneObjectLoader.loadScene(readJSON(path).value(),
                                                  "data/models");
                }
                else if (path.extension() == ".ply")
                {
                    m_sceneObjectLoader.loadMesh(path);
                }
                else if (path.extension() == ".stl")
                {
                    m_sceneObjectLoader.loadMesh(path);
                }
            }
        }
    });

    auto keyPressRepeatCallback = [this](Key key, Scancode, KeyModifiers mods)
    {
        if (key == Key::N)
        {
            std::cerr << "WARNING: Normal visualisation disabled as it is "
                         "pretty crashy at the moment.\n";
            //m_showNormals = true;
            //return;
        }
        if (key == Key::T)
        {
            m_drawToon = true;
        }

        if (key == Key::Tab && !m_models.empty())
        {
            auto currVal = m_selectedModel.value_or(0);
            if (mods.shift)
            {
                currVal = currVal ? currVal - 1 : m_models.size() - 1;
            }
            else currVal = (currVal + 1) % m_models.size();

            updateSelectedModel(currVal);
        }
        else if (key == Key::Backspace)
        {
            updateSelectedModel(std::nullopt);
        }
        else if (key == Key::Enter)
        {
            endCurrentLine();
            m_currentDrawingLine.vertices.clear();
        }

        // This is absolutely not ideal movement; the idea is to use a much more
        // natural mouse-based relative movement relative to the selected
        // object's local coordinates (its local coordinate space), but this is
        // useful to test the visual part of the operation.
        if (m_selectedModel.has_value())
        {
            auto fwd = m_camera->forward().cast<float>();
            auto up = m_camera->up().cast<float>();
            auto right = cross(fwd, up);
            vec3 direction{0., 0., 0.};
            // In this case we might want to move the object.
            if (key == Key::Left) direction = -right;
            if (key == Key::Right) direction = right;
            if (key == Key::Up) direction = up;
            if (key == Key::Down) direction = -up;

            std::lock_guard guard(m_modelMutex);
            auto& currObject = m_models[*m_selectedModel];
            auto camToObjOriginDist = length(m_camera->position()
                                             - dvec3(currObject.transform.col(3))) * 0.05;

            camToObjOriginDist = std::max(camToObjOriginDist, 0.001);

            direction *= camToObjOriginDist;

            mat4 transform(1.f);
            transform.setCol(3, {direction.x, direction.y, direction.z, 1.});
            currObject.transform = transform * currObject.transform;
        }
    };

    auto keyReleaseCallback = [this](Key key, Scancode, KeyModifiers)
    {
        bool consume = false;
        if (key == Key::N)
        {
            m_showNormals = false;
            consume = true;
        }
        if (key == Key::T)
        {
            m_drawToon = false;
            consume = true;
        }
        if (consume) return;
        if (key == Key::LeftShift)
        {
            endCurrentLine();
        }
    };

    m_window.setKeyPressCallback(keyPressRepeatCallback);
    m_window.setKeyRepeatCallback(keyPressRepeatCallback);
    m_window.setKeyReleaseCallback(keyReleaseCallback);
}

std::optional<DrawableLine::Vertex>
MessyRenderer::getCurrentVertex(std::optional<vec3> replacementPosition)
{
    using kraken::window::Key;
    vec4 colour(0., 0., 0., 1.);
    colour.r = m_window.isPressed(Key::R) || m_window.isPressed(Key::Key1);
    colour.g = m_window.isPressed(Key::G) || m_window.isPressed(Key::Key2);
    colour.b = m_window.isPressed(Key::B) || m_window.isPressed(Key::Key3);

    vec4 position;
    if (!m_currHoverPoint.has_value())
    {
        if (replacementPosition.has_value()) position = vec4(*replacementPosition, 1.);
        else return std::nullopt;
    }
    else position = vec4(m_currHoverPoint.value().cast<float>(), 1.);

    return DrawableLine::Vertex{.positionAndWidth = position, .colour = colour};
}

void MessyRenderer::endCurrentLine()
{
    if (m_currentDrawingLine.vertices.size() > 1)
    {
        m_lines.emplace_back(std::move(m_currentDrawingLine));
    }
    m_currentDrawingLine.vertices.clear();
    m_currLineLastDepth = std::nullopt;
}

void MessyRenderer::performDestruction()
{
    destroySwapchain();

    m_placeholderTexture.destroy();

    m_plotFunctionProvider = nullptr;
    m_lightDataProvider = nullptr;
    m_pickProvider = nullptr;
    m_readbackProvider = nullptr;
    m_transformProvider = nullptr;

    m_multiAllocatedBuffer.destroy();

    m_models.clear();

    m_scCopy.shader.destroy();

    m_ddnShader.destroy();

    for (auto* shader : {&m_defaultMaterial, &m_flatMaterial, &m_lineMaterial, &m_plotMaterial})
    {
        if (shader->pipeline != VK_NULL_HANDLE)
        {
            vkDestroyPipeline(m_device, shader->pipeline, nullptr);
        }
        if (shader->layout != VK_NULL_HANDLE)
        {
            vkDestroyPipelineLayout(m_device, shader->layout, nullptr);
        }
        shader->shader = nullptr;
    }

    for (const auto* elem : {&m_transformAndPickSetLayout,
                             &m_lightSetLayout,
                             &m_materialParameterSetLayout,
                             &m_plotFunctionLayout})
    {
        vkDestroyDescriptorSetLayout(m_device, elem->layout, nullptr);
    }

    if (m_transientCommandPool)
    {
        vkDestroyCommandPool(m_device, m_transientCommandPool, nullptr);
    }

    for (U32 i = 0; i < maxCachedFrames; ++i)
    {
        m_frameData[i].lineMesh.destroy();
        if (m_frameData[i].completionFence)
        {
            vkDestroyFence(m_device, m_frameData[i].completionFence, nullptr);
        }
        if (m_frameData[i].renderFinish)
        {
            vkDestroySemaphore(m_device, m_frameData[i].renderFinish, nullptr);
        }
        if (m_frameData[i].imageAcquire)
        {
            vkDestroySemaphore(m_device, m_frameData[i].imageAcquire, nullptr);
        }
        if (m_frameData[i].commandBuffer)
        {
            vkFreeCommandBuffers(m_device,
                                 m_frameData[i].pool,
                                 1,
                                 &m_frameData[i].commandBuffer);
        }
        if (m_frameData[i].pool)
        {
            vkDestroyCommandPool(m_device, m_frameData[i].pool, nullptr);
        }
    }

    m_samplerCache.destroy();

    m_descAllocator.destroy();

    m_sceneObjectLoader.clear();

    m_resourceLoader.destroy();

    if (m_allocator) vmaDestroyAllocator(m_allocator);

    // Exceedingly ugly, but to avoid waiting for std::cin.
    // To fix when taking input graphically instead.
    {
        using namespace std::chrono_literals;
        if (m_nextFunctionInput.has_value()
            && m_nextFunctionInput->valid()
            && m_nextFunctionInput->wait_for(0ms) != std::future_status::ready)
        {
            std::terminate();
        }
    }
}

MessyRenderer::MessyRenderer(fs::path dataDirectory)
    : m_datadir{fs::canonical(dataDirectory)}
    , m_instance(VulkanInstanceParameters{.name = "Vulkan test program",
                                          .swapchainSupport = true,
                                          .enableValidation = true,
                                          .desiredVulkanVersion = VK_MAKE_VERSION(1, 1, 0)})
    , m_windowOpener{m_window,
                     m_instance,
                     kraken::window::WindowCreationSettings
                     {
                         .title = "Vulkan test program",
                         .width = 1280,
                         .height = 720,
                     },
                     true}
    , m_device(VulkanDeviceParameters{.instance = m_instance,
                                      .surface = m_windowOpener.surface(),
                                      .queue =
                                      {
                                          .requiredFlags = VK_QUEUE_GRAPHICS_BIT,
                                          .priority = 1.f,
                                          .requirePresentSupport = true,
                                      },
                                      .features =
                                      {
                                          .geometryShader = true,
                                          .tessellationShader = true,
                                          .fragmentStoresAndAtomics = true,
                                      }})
    , m_sceneObjectLoader{m_resourceLoader, m_device}
{
    m_showNormals = false;
    m_allocator = VK_NULL_HANDLE;
    m_transientCommandPool = VK_NULL_HANDLE;
    m_scCopy.prevPassesFinished = VK_NULL_HANDLE;

    setupWindowCallbacks();

    m_currMovement = MovementState::None;
    m_lastCursorPosition = m_window.mousePos();

    m_camera = std::make_unique<TurntableCamera>(dvec3{0., 0., 0.},
                                                 5.,
                                                 dvec3{0, 1, 0},
                                                 dvec3{0, 0, 1});

    if (createAllocator() != VK_SUCCESS)
    {
        performDestruction();
        throw std::runtime_error("Failed to create allocator.");
    }

    m_resourceLoader.create(8, dataDirectory,
                            m_device, m_device.queueInfo(),
                            128*1048576, 1048576, m_allocator);

    m_descAllocator.construct(m_device);

    m_samplerCache.create(m_device);

    VkCommandPoolCreateInfo commandPoolCreate{};
    commandPoolCreate.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    commandPoolCreate.queueFamilyIndex = m_device.queueInfo().familyIndex;
    commandPoolCreate.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

    for (U32 i = 0; i < maxCachedFrames; ++i)
    {
        m_frameData[i].cursorPos = m_window.framebufferSize().cast<double>() * .5;
        if (vkCreateCommandPool(m_device, &commandPoolCreate, nullptr, &m_frameData[i].pool) != VK_SUCCESS)
        {
            performDestruction();
            throw std::runtime_error("Failed to create command pool.");
        }
        VkCommandBufferAllocateInfo commandBufferAllocate{};
        commandBufferAllocate.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        commandBufferAllocate.commandPool = m_frameData[i].pool;
        commandBufferAllocate.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        commandBufferAllocate.commandBufferCount = 1;

        if (vkAllocateCommandBuffers(m_device, &commandBufferAllocate, &m_frameData[i].commandBuffer) != VK_SUCCESS)
        {
            performDestruction();
            throw std::runtime_error("Failed to allocate command buffers.");
        }

        VkSemaphoreCreateInfo semaphoreCreate{};
        semaphoreCreate.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        if (vkCreateSemaphore(m_device, &semaphoreCreate, nullptr, &m_frameData[i].imageAcquire) != VK_SUCCESS)
        {
            performDestruction();
            throw std::runtime_error("Failed to create image acquire semaphore.");
        }
        if (vkCreateSemaphore(m_device, &semaphoreCreate, nullptr, &m_frameData[i].renderFinish) != VK_SUCCESS)
        {
            performDestruction();
            throw std::runtime_error("Failed to create render finish semaphore.");
        }
        VkFenceCreateInfo fenceCreate{};
        fenceCreate.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        fenceCreate.flags = VK_FENCE_CREATE_SIGNALED_BIT;
        if (vkCreateFence(m_device, &fenceCreate, nullptr, &m_frameData[i].completionFence) != VK_SUCCESS)
        {
            performDestruction();
            throw std::runtime_error("Failed to create fence.");
        }

        m_frameData[i].lineMesh.resizeVertexBuffers(1);
        m_frameData[i].lineMesh.resizeIndexBuffers(1);
    }

    commandPoolCreate.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
    if (vkCreateCommandPool(m_device, &commandPoolCreate, nullptr, &m_transientCommandPool) != VK_SUCCESS)
    {
        performDestruction();
        throw std::runtime_error("Failed to create transient command pool.");
    }

    m_defaultMaterial.shader = std::make_shared<GraphicsShader>();
    m_flatMaterial.shader = std::make_shared<GraphicsShader>();
    m_lineMaterial.shader = std::make_shared<GraphicsShader>();
    m_plotMaterial.shader = std::make_shared<GraphicsShader>();

    if (!m_defaultMaterial.shader->createFromFile(dataDirectory / "shaders/default.json", m_device))
    {
        performDestruction();
        throw std::runtime_error("Failed to create shader.");
    }

    if (!m_ddnShader.createFromFile(dataDirectory / "shaders/debug-draw-normals.json", m_device))
    {
        performDestruction();
        throw std::runtime_error("Failed to create shader.");
    }

    if (!m_lineMaterial.shader->createFromFile(dataDirectory / "shaders/draw-lines.json", m_device))
    {
        performDestruction();
        throw std::runtime_error("Failed to create shader.");
    }

    if (!m_flatMaterial.shader->createFromFile(dataDirectory / "shaders/flat-shaded.json", m_device))
    {
        performDestruction();
        throw std::runtime_error("Failed to create shader.");
    }

    if (!m_plotMaterial.shader->createFromFile(dataDirectory / "shaders/plotter.json", m_device))
    {
        performDestruction();
        throw std::runtime_error("Failed to create shader.");
    }

    if (!m_scCopy.shader.createFromFile(dataDirectory / "shaders/copy-to-swapchain.json", m_device))
    {
        performDestruction();
        throw std::runtime_error("Failed to create swapchain copy shader.");
    }

    m_sceneObjectLoader.loadScene(readJSON("data/scenes/scene.json").value(),
                                  "data/models");

    m_sceneObjectLoader.loadMesh("meshes/complex-model.ply");

    m_multiAllocatedBuffer.construct(m_allocator,
                                     VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                                     4096);

    m_transformProvider = std::make_unique<GlobalTransformProvider>(m_multiAllocatedBuffer, m_device);
    m_readbackProvider = std::make_unique<ReadbackArrayProvider>(m_device, m_allocator, 32);
    m_pickProvider = std::make_unique<PickDataProvider>(m_multiAllocatedBuffer, m_device);

    m_lightDataProvider = std::make_unique<LightDataProvider>(m_multiAllocatedBuffer, m_device);

    m_plotFunctionProvider = std::make_unique<PlotFunctionBufferProvider>(m_device, m_allocator);

    if (auto program = plotter::compileForGPU("20 - (x*x + y*y)/10"))
    {
        m_plotFunctionProvider->setFunction(std::move(program->code),
                                            std::move(program->constants));
    }

    auto& lights = m_lightDataProvider->lights;

    for (size_t i = 0; i < LightSources::maxDirectionalLights; ++i)
    {
        lights.dirLights[i] = {vec4(0.f), vec4(0.f)};
    }
    for (size_t i = 0; i < LightSources::maxPointLights; ++i)
    {
        lights.pointLights[i] = {vec4(0.f), vec4(0.f)};
    }

    lights.dirLights[0].direction = normalise(vec4{0.7, 0.7, 0.2, 0.});
    lights.dirLights[1].direction = normalise(vec4{-0.7, -0.7, 0.2, 0.});
    lights.dirLights[2].direction = normalise(vec4{0.1, 0.1, -0.5, 0.});
    lights.dirLights[0].colour = {1., 1., 1., 0.4};
    lights.dirLights[1].colour = {1., 1., 1., 0.3};
    lights.dirLights[2].colour = {1., 1., 1., 0.1};

    {
        using namespace inputlayout;

        auto storageBuffer = DescriptorType(VK_DESCRIPTOR_TYPE_STORAGE_BUFFER);
        auto uniformBuffer = DescriptorType(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER);
        auto vertexShader = StageFlags(VK_SHADER_STAGE_VERTEX_BIT);
        auto geometryShader = StageFlags(VK_SHADER_STAGE_GEOMETRY_BIT);
        auto fragmentShader = StageFlags(VK_SHADER_STAGE_FRAGMENT_BIT);

        m_transformAndPickSetLayout.bindings = getCreateInfo<VkDescriptorSetLayoutBinding>
        (
            DescriptorCount(1)
            * (fragmentShader
               * (Binding(1) * storageBuffer | Binding(2) * uniformBuffer)
               | vertexShader * geometryShader * uniformBuffer * Binding(0))
        );

        m_lightSetLayout.bindings = getCreateInfo<VkDescriptorSetLayoutBinding>
        (
            Binding(0)
            * DescriptorType(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER)
            * DescriptorCount(1)
            * StageFlags(VK_SHADER_STAGE_FRAGMENT_BIT)
        );

        m_materialParameterSetLayout.bindings = getCreateInfo<VkDescriptorSetLayoutBinding>
        (
            DescriptorCount(1)
            * StageFlags(VK_SHADER_STAGE_FRAGMENT_BIT)
            * (DescriptorType(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER)
               * (Binding(0) | Binding(1))
               | DescriptorType(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER)
               * Binding(2))
        );
        m_plotFunctionLayout.bindings = getCreateInfo<VkDescriptorSetLayoutBinding>
        (
            DescriptorCount(1)
            * DescriptorType(VK_DESCRIPTOR_TYPE_STORAGE_BUFFER)
            * StageFlags(VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT)
            * (Binding(0) | Binding(1))
        );

        Format vec4Format(VK_FORMAT_R32G32B32A32_SFLOAT);
        auto vec3Format = Format(VK_FORMAT_R32G32B32_SFLOAT);
        Format vec2Format(VK_FORMAT_R32G32_SFLOAT);

        m_defaultSpec.attributes = getCreateInfo<VkVertexInputAttributeDescription>
        (
            Binding(0) * (vec3Format * (Location(0) * Offset(0)
                                        | Location(1) * Offset(sizeof(vec3)))
                          | vec2Format * Location(2) * Offset(2 * sizeof(vec3)))
        );
        m_defaultSpec.bindings = getCreateInfo<VkVertexInputBindingDescription>
        (
            Binding(0) * Stride(2*sizeof(vec3) + sizeof(vec2)) * InputRate(VK_VERTEX_INPUT_RATE_VERTEX)
        );

        m_lineSpec.attributes = getCreateInfo<VkVertexInputAttributeDescription>
        (
            Binding(0) * vec4Format * (Location(0) * Offset(0) | Location(1) * Offset(sizeof(vec4)))
        );
        m_lineSpec.bindings = getCreateInfo<VkVertexInputBindingDescription>
        (
            Binding(0) * Stride(2*sizeof(vec4)) * InputRate(VK_VERTEX_INPUT_RATE_VERTEX)
        );
    }

    if (compileLayout(m_transformAndPickSetLayout, m_device) != VK_SUCCESS
        || compileLayout(m_lightSetLayout, m_device) != VK_SUCCESS
        || compileLayout(m_materialParameterSetLayout, m_device) != VK_SUCCESS
        || compileLayout(m_plotFunctionLayout, m_device) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to compile shader layout.");
    }

    for (size_t k = 0; k < maxCachedFrames; ++k)
    {
        std::array<VkDescriptorSetLayout, 2> allocLayouts;
        allocLayouts[0] = m_transformAndPickSetLayout;
        allocLayouts[1] = m_lightSetLayout;
        if (!m_descAllocator.allocate(m_transformLightSets[k],
                                      allocLayouts.data(),
                                      allocLayouts.size()))
        {
            throw std::logic_error("Failed to allocate descriptor sets.");
        }
        m_transformProvider->write(m_transformLightSets[k][0], k);
        m_readbackProvider->write(m_transformLightSets[k][0], k);
        m_pickProvider->write(m_transformLightSets[k][0], k);
        m_lightDataProvider->write(m_transformLightSets[k][1], k);

        if (!m_descAllocator.allocate(&m_plotFunctionSets[k],
                                      &m_plotFunctionLayout.layout,
                                      1))
        {
            throw std::logic_error("Failed to allocate plot descriptor set layout.");
        }
        m_plotFunctionProvider->write(m_plotFunctionSets[k], k);
    }

    for (size_t k = 0; k < maxCachedFrames; ++k)
    {
        m_transformProvider->update(k);
        m_readbackProvider->update(k);
        m_pickProvider->update(k);
        m_lightDataProvider->update(k);
        m_plotFunctionProvider->update(k);
    }

    m_flatSpec.attributes =
    {
        {
            .location = 0,
            .binding = 0,
            .format = VK_FORMAT_R32G32B32_SFLOAT,
            .offset = 0,
        }
    };
    m_flatSpec.bindings =
    {
        {
            .binding = 0,
            .stride = sizeof(vec3),
            .inputRate = VK_VERTEX_INPUT_RATE_VERTEX
        }
    };

    VkDescriptorSetLayout defaultLayouts[] =
    {
        m_transformAndPickSetLayout.layout,
        m_lightSetLayout.layout,
        m_materialParameterSetLayout.layout,
    };

    VkPushConstantRange pushConstant;
    pushConstant.size = sizeof(mat4) + sizeof(U32);
    pushConstant.offset = 0;
    pushConstant.stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_GEOMETRY_BIT;

    VkPipelineLayoutCreateInfo pipelineCreate
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .setLayoutCount = 3,
        .pSetLayouts = defaultLayouts,
        .pushConstantRangeCount = 1,
        .pPushConstantRanges = &pushConstant,
    };

    if (vkCreatePipelineLayout(m_device, &pipelineCreate, nullptr, &m_defaultMaterial.layout)
        != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create default material pipeline layout.");
    }

    pipelineCreate.setLayoutCount = 1;

    if (vkCreatePipelineLayout(m_device, &pipelineCreate, nullptr, &m_flatMaterial.layout)
        != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create flat material pipeline layout.");
    }

    pipelineCreate.pushConstantRangeCount = 0;
    pipelineCreate.pPushConstantRanges = nullptr;

    if (vkCreatePipelineLayout(m_device, &pipelineCreate, nullptr, &m_lineMaterial.layout)
        != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create line material layout.");
    }


    pushConstant =
    {
        .stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_GEOMETRY_BIT,
        .offset = 0,
        .size = sizeof(mat4) + sizeof(vec4),
    };

    pipelineCreate.pushConstantRangeCount = 1;
    pipelineCreate.pPushConstantRanges = &pushConstant;
    pipelineCreate.setLayoutCount = 1;
    pipelineCreate.pSetLayouts = &m_plotFunctionLayout.layout;

    if (vkCreatePipelineLayout(m_device, &pipelineCreate, nullptr, &m_plotMaterial.layout)
        != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create line material layout.");
    }

    m_placeholderTexture = createPlaceholderTexture({1,1,0});

    recreateSwapchain();
}

MessyRenderer::~MessyRenderer()
{
    vkDeviceWaitIdle(m_device);

    performDestruction();
}

VkResult MessyRenderer::immediateExecute(const std::function<void(VkCommandBuffer)>& func)
{
    VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandBufferCount = 1;
    allocInfo.commandPool = m_transientCommandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;

    VkCommandBuffer commandBuf;
    vkAllocateCommandBuffers(m_device, &allocInfo, &commandBuf);

    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    vkBeginCommandBuffer(commandBuf, &beginInfo);

    func(commandBuf);

    vkEndCommandBuffer(commandBuf);
    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuf;

    VkFenceCreateInfo fenceCreate{};
    fenceCreate.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

    VkFence fence;
    auto result = vkCreateFence(m_device, &fenceCreate, nullptr, &fence);
    if (result != VK_SUCCESS) return result;

    result = vkQueueSubmit(m_device.queue(), 1, &submitInfo, fence);
    result != VK_SUCCESS || vkWaitForFences(m_device, 1, &fence, VK_FALSE, 0xffffffffffffffff);
    vkDestroyFence(m_device, fence, nullptr);
    vkFreeCommandBuffers(m_device, m_transientCommandPool, 1, &commandBuf);
    return result;
}

VkResult MessyRenderer::createAllocator()
{
    VmaAllocatorCreateInfo allocatorInfo{};
    allocatorInfo.physicalDevice = m_device;
    allocatorInfo.device = m_device;
    allocatorInfo.instance = m_instance;

    auto result = vmaCreateAllocator(&allocatorInfo, &m_allocator);
    if (result != VK_SUCCESS) return result;

    return VK_SUCCESS;
}

void MessyRenderer::createSwapchain()
{
    SwapchainCreateInfo sci{};
    sci.surface = m_windowOpener.surface();
    sci.physicalDevice = m_device;
    sci.device = m_device;
    sci.maxCachedFrames = 3;

    VkSurfaceCapabilitiesKHR surfaceCaps;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(m_device, m_windowOpener.surface(), &surfaceCaps);
    sci.swapchain.extent = surfaceCaps.currentExtent;
    if (sci.swapchain.extent.width == 0xffffffffu)
    {
        sci.swapchain.extent.width = m_window.framebufferSize().x;
    }
    if (sci.swapchain.extent.height == 0xffffffffu)
    {
        sci.swapchain.extent.height = m_window.framebufferSize().y;
    }
    sci.swapchain.minImageCount = 3;
    sci.swapchain.clipped = VK_TRUE;
    sci.swapchain.desiredPresentMode = VK_PRESENT_MODE_MAILBOX_KHR;
    sci.swapchain.desiredFormat.format = VK_FORMAT_B8G8R8A8_SRGB;
    sci.swapchain.desiredFormat.colorSpace = VK_COLORSPACE_SRGB_NONLINEAR_KHR;

    m_perspective.setAspect(m_window.framebufferSize().x,
                            m_window.framebufferSize().y);

    if (m_swapchain.create(sci, m_allocator, false) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create swap chain.");
    }

    m_framegraph.create(m_device, m_allocator);

    m_currFrame = 0;
}

void MessyRenderer::destroySwapchain()
{
    if (m_scCopy.prevPassesFinished != VK_NULL_HANDLE)
    {
        vkDestroyEvent(m_device, m_scCopy.prevPassesFinished, nullptr);
        m_scCopy.prevPassesFinished = VK_NULL_HANDLE;
    }

    m_scCopy.pipeline.destroy();
    m_scCopy.descSetLayout.destroy();

    m_framegraph.destroy();
    m_swapchain.destroy();
}

void MessyRenderer::recreateSwapchain()
{
    vkDeviceWaitIdle(m_device);

    destroySwapchain();

    createSwapchain();
    constructFrameGraph();
    createSwapchainCopyStuff();

    auto* pass = m_framegraph.pass("default");
    if (!pass) throw std::logic_error("No pass found.");

    if (m_defaultMaterial.pipeline)
    {
        vkDestroyPipeline(m_device, m_defaultMaterial.pipeline, nullptr);
    }
    if (m_flatMaterial.pipeline)
    {
        vkDestroyPipeline(m_device, m_flatMaterial.pipeline, nullptr);
    }
    if (m_lineMaterial.pipeline)
    {
        vkDestroyPipeline(m_device, m_lineMaterial.pipeline, nullptr);
    }
    if (m_plotMaterial.pipeline)
    {
        vkDestroyPipeline(m_device, m_plotMaterial.pipeline, nullptr);
    }

    GraphicsPipelineInfo info;

    info.fillInfo(pass->layout().width,
                  pass->layout().height,
                  VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
                  VK_POLYGON_MODE_FILL,
                  VK_CULL_MODE_BACK_BIT,
                  VK_FRONT_FACE_COUNTER_CLOCKWISE,
                  VK_SAMPLE_COUNT_1_BIT);

    info.colourBlendStates.push_back({});

    auto& colourBlendState2 = info.colourBlendStates.back();
    colourBlendState2.blendEnable = VK_FALSE;
    colourBlendState2.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    colourBlendState2.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    colourBlendState2.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    colourBlendState2.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    colourBlendState2.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;

    info.vertexInputCreate.vertexAttributeDescriptionCount = m_defaultSpec.attributes.size();
    info.vertexInputCreate.pVertexAttributeDescriptions = m_defaultSpec.attributes.data();
    info.vertexInputCreate.vertexBindingDescriptionCount = m_defaultSpec.bindings.size();
    info.vertexInputCreate.pVertexBindingDescriptions = m_defaultSpec.bindings.data();

    auto graphicsCreate = info.getGraphicsPipelineCreateInfo();
    graphicsCreate.pTessellationState = nullptr;
    graphicsCreate.pDynamicState = nullptr;
    graphicsCreate.stageCount = m_defaultMaterial.shader->stageCount();
    graphicsCreate.pStages = m_defaultMaterial.shader->pStages();
    graphicsCreate.layout = m_defaultMaterial.layout;
    graphicsCreate.renderPass = pass->vulkanPass().pass;
    graphicsCreate.subpass = pass->vulkanPass().subpass;

    if (vkCreateGraphicsPipelines(m_device, VK_NULL_HANDLE, 1, &graphicsCreate, nullptr, &m_defaultMaterial.pipeline) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create default graphics pipeline.");
    }

    info.vertexInputCreate.vertexAttributeDescriptionCount = m_flatSpec.attributes.size();
    info.vertexInputCreate.pVertexAttributeDescriptions = m_flatSpec.attributes.data();
    info.vertexInputCreate.vertexBindingDescriptionCount = m_flatSpec.bindings.size();
    info.vertexInputCreate.pVertexBindingDescriptions = m_flatSpec.bindings.data();

    graphicsCreate.stageCount = m_flatMaterial.shader->stageCount();
    graphicsCreate.pStages = m_flatMaterial.shader->pStages();
    graphicsCreate.layout = m_flatMaterial.layout;

    if (vkCreateGraphicsPipelines(m_device, VK_NULL_HANDLE, 1, &graphicsCreate, nullptr, &m_flatMaterial.pipeline) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create flat graphics pipeline.");
    }

    graphicsCreate.stageCount = m_lineMaterial.shader->stageCount();
    graphicsCreate.pStages = m_lineMaterial.shader->pStages();
    graphicsCreate.layout = m_lineMaterial.layout;

    info.vertexInputCreate.vertexAttributeDescriptionCount = m_lineSpec.attributes.size();
    info.vertexInputCreate.pVertexAttributeDescriptions = m_lineSpec.attributes.data();
    info.vertexInputCreate.vertexBindingDescriptionCount = m_lineSpec.bindings.size();
    info.vertexInputCreate.pVertexBindingDescriptions = m_lineSpec.bindings.data();

    info.inputAssemblyCreate.topology = VK_PRIMITIVE_TOPOLOGY_LINE_LIST;

    if (vkCreateGraphicsPipelines(m_device, VK_NULL_HANDLE, 1, &graphicsCreate, nullptr, &m_lineMaterial.pipeline) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create flat graphics pipeline.");
    }

    graphicsCreate.stageCount = m_plotMaterial.shader->stageCount();
    graphicsCreate.pStages = m_plotMaterial.shader->pStages();
    graphicsCreate.layout = m_plotMaterial.layout;

    info.vertexInputCreate.vertexAttributeDescriptionCount = 0;
    info.vertexInputCreate.pVertexAttributeDescriptions = nullptr;
    info.vertexInputCreate.vertexBindingDescriptionCount = 0;
    info.vertexInputCreate.pVertexAttributeDescriptions = nullptr;

    graphicsCreate.pTessellationState = &info.tesselationCreate;
    info.rasterizerCreate.cullMode = VK_CULL_MODE_NONE;

    info.tesselationCreate.patchControlPoints = 4;

    info.inputAssemblyCreate.topology = VK_PRIMITIVE_TOPOLOGY_PATCH_LIST;

    if (vkCreateGraphicsPipelines(m_device, VK_NULL_HANDLE, 1, &graphicsCreate, nullptr, &m_plotMaterial.pipeline) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create plot graphics pipeline.");
    }

    recordCommands();
}

////////////////////////////////////////////////////////////////////////////////
/// SNIP
////////////////////////////////////////////////////////////////////////////////

void MessyRenderer::notifyLoadStatusUpdated(size_t modelIndex)
{
    std::unique_lock lock(m_modelMutex);

    if (m_models[modelIndex].mesh)
    {
        invalidateDescriptors(modelIndex, true);
    }
}

void MessyRenderer::constructFrameGraph()
{
    PassLayout layout;
    layout.width = m_swapchain.extent().width;
    layout.height = m_swapchain.extent().height;
    layout.layers = 1;
    layout.samples = VK_SAMPLE_COUNT_1_BIT;

    auto& defaultPass = m_framegraph.addPass("default", layout, VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT);

    layout.width = m_swapchain.extent().width;
    layout.height = m_swapchain.extent().height;

    VkClearValue clearVals[4];
    clearVals[0].color = {{0.3f, 0.7f, 0.8f, 1.f}};
    clearVals[1].depthStencil.depth = 1.f;
    clearVals[2].color = {{0, 0, 0, 0}};

    defaultPass.addOutput("fsColourOut",
                          VK_FORMAT_R32G32B32A32_SFLOAT,
                          clearVals[0])
               .addOutput("depth",
                          VK_FORMAT_D32_SFLOAT,
                          clearVals[1])
               .addOutput("infoOut",
                          VK_FORMAT_R32G32B32A32_UINT,
                          clearVals[2]);

    VkSampler sampler;
    if (m_samplerCache.getSampler(&sampler,
                                  VK_FILTER_NEAREST,
                                  VK_FILTER_NEAREST,
                                  VK_SAMPLER_MIPMAP_MODE_NEAREST,
                                  VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT,
                                  0.f) != VK_SUCCESS)
    {
        throw std::logic_error("Failed to create a nearest-nearest sampler.");
    }

    auto& swapchainCopyPass = m_framegraph.addPass("swapchaincopy",
                                                   layout,
                                                   VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT);
    swapchainCopyPass.addInputAttachment("colourIn", VK_FORMAT_R32G32B32A32_SFLOAT)
                     .addInputImage("infoIn", VK_FORMAT_R32G32B32A32_UINT, sampler)
                     .addOutput("colourOut",
                                m_swapchain,
                                clearVals[0]);

    m_scCopy.pass = &swapchainCopyPass;

    m_framegraph.connect(defaultPass["fsColourOut"], swapchainCopyPass["colourIn"]);
    m_framegraph.connect(defaultPass["infoOut"], swapchainCopyPass["infoIn"]);

    std::vector<VkImageView> swapchainViews;
    for (U32 i = 0; i < m_swapchain.imageCount(); ++i)
    {
        swapchainViews.push_back(m_swapchain.view(i));
    }

    m_framegraph.compile(swapchainViews.data(), swapchainViews.size());
}

Pipeline MessyRenderer::createSwapchainCopyPipeline(U32 width, U32 height,
                                                    VkRenderPass renderpass,
                                                    U32 subpass) const
{
    GraphicsPipelineInfo gpi;
    gpi.fillInfo(width, height, VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
                 VK_POLYGON_MODE_FILL, VK_CULL_MODE_NONE,
                 VK_FRONT_FACE_COUNTER_CLOCKWISE, VK_SAMPLE_COUNT_1_BIT);

    gpi.depthStencilCreate.depthTestEnable = VK_FALSE;
    gpi.depthStencilCreate.stencilTestEnable = VK_FALSE;

    gpi.vertexInputCreate.vertexAttributeDescriptionCount = 0;
    gpi.vertexInputCreate.pVertexAttributeDescriptions = nullptr;
    gpi.vertexInputCreate.vertexBindingDescriptionCount = 0;
    gpi.vertexInputCreate.pVertexAttributeDescriptions = nullptr;

    Pipeline pipeline;
    pipeline.device = m_device;

    auto setLayout = m_scCopy.descSetLayout.layout();

    VkPushConstantRange pushConstant;
    pushConstant.size = sizeof(uvec2) + sizeof(U32);
    pushConstant.offset = 0;
    pushConstant.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    VkPipelineLayoutCreateInfo pipelineLayoutCreate{};
    pipelineLayoutCreate.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutCreate.setLayoutCount = 1;
    pipelineLayoutCreate.pSetLayouts = &setLayout;
    pipelineLayoutCreate.pushConstantRangeCount = 1;
    pipelineLayoutCreate.pPushConstantRanges = &pushConstant;

    if (vkCreatePipelineLayout(m_device, &pipelineLayoutCreate, nullptr, &pipeline.layout) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create swapchain copy pipeline layout.");
    }
    auto& blendStates = gpi.colourBlendStates;
    blendStates.resize(1);
    blendStates[0] =
    {
        VK_FALSE,
        VK_BLEND_FACTOR_ZERO,
        VK_BLEND_FACTOR_ONE,
        VK_BLEND_OP_ADD,
        VK_BLEND_FACTOR_ZERO,
        VK_BLEND_FACTOR_ONE,
        VK_BLEND_OP_ADD,
        VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT
    };

    auto graphicsCreate = gpi.getGraphicsPipelineCreateInfo();
    graphicsCreate.pTessellationState = nullptr;
    graphicsCreate.pDynamicState = nullptr;
    graphicsCreate.stageCount = m_scCopy.shader.stageCount();
    graphicsCreate.pStages = m_scCopy.shader.pStages();
    graphicsCreate.layout = pipeline.layout;
    graphicsCreate.renderPass = renderpass;
    graphicsCreate.subpass = subpass;

    if (vkCreateGraphicsPipelines(m_device, VK_NULL_HANDLE, 1, &graphicsCreate, nullptr, &pipeline.pipeline) != VK_SUCCESS)
    {
        pipeline.destroy();
        throw std::runtime_error("Failed to create swapchain copy pipeline.");
    }

    return pipeline;
}

void MessyRenderer::createSwapchainCopyStuff()
{
    m_scCopy.descSetLayout.construct(m_device, nullptr);

    m_scCopy.shader.info().appendLayoutInfo(m_scCopy.descSetLayout.layoutInfo(), 0);
    if (m_scCopy.descSetLayout.compileLayout() != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create SCC descriptor set layout.");
    }
    m_scCopy.descSet.allocateCopies(maxCachedFrames);
    if (!createDescriptorSets(m_scCopy.descSet.sets(),
                              maxCachedFrames,
                              &m_scCopy.descSetLayout,
                              m_descAllocator))
    {
        throw std::runtime_error("Failed to create SCC descriptor sets.");
    }

    if (!m_scCopy.pass)
    {
        throw std::logic_error("Bug: Pass 'swapchaincopy' not constructed yet!");
    }

    m_scCopy.pipeline = createSwapchainCopyPipeline(m_swapchain.extent().width,
                                                    m_swapchain.extent().height,
                                                    m_scCopy.pass->vulkanPass().pass,
                                                    m_scCopy.pass->vulkanPass().subpass);

    VkEventCreateInfo evtCreate{};
    evtCreate.sType = VK_STRUCTURE_TYPE_EVENT_CREATE_INFO;
    if (vkCreateEvent(m_device, &evtCreate, nullptr, &m_scCopy.prevPassesFinished) != VK_SUCCESS)
    {
        throw std::logic_error("Failed to create event.");
    }

    if (vkSetEvent(m_device, m_scCopy.prevPassesFinished) != VK_SUCCESS)
    {
        throw std::logic_error("Failed to set event.");
    }
}

void MessyRenderer::updateDescriptors(U32 frameIndex, Model& model)
{
    if (!model.materialProvider) return;

    enum class HighlightState : U32
    {
        None = 0,
        Hovered,
        Selected,
    } highlightState{HighlightState::None};

    if (m_selectedModel.has_value()
        && size_t(std::distance(m_models.data(), &model)) == *m_selectedModel)
    {
        highlightState = HighlightState::Selected;
    }
    else if (m_hoveringModel.has_value()
             && size_t(std::distance(m_models.data(), &model)) == *m_hoveringModel)
    {
        highlightState = HighlightState::Hovered;
    }

    auto& tint = model.materialProvider->tint;
    switch (highlightState)
    {
    case HighlightState::None:     tint = {0., 0., 0., 0.}; break;
    case HighlightState::Hovered:  tint = {1., 1., 1., .3}; break;
    case HighlightState::Selected: tint = {1., 1., 1., .8}; break;
    }

    model.materialProvider->write(model.materialSets[4*frameIndex+2], frameIndex);
    model.materialProvider->update(frameIndex);
}

void MessyRenderer::updateHoveredModel(std::optional<size_t> newHover)
{
    std::swap(m_hoveringModel, newHover);
    if (m_hoveringModel.has_value()) invalidateDescriptors(*m_hoveringModel);
    if (newHover.has_value()) invalidateDescriptors(*newHover);
}

void MessyRenderer::updateSelectedModel(std::optional<size_t> newSelected)
{
    std::swap(m_selectedModel, newSelected);
    if (m_selectedModel.has_value()) invalidateDescriptors(*m_selectedModel);
    if (newSelected.has_value()) invalidateDescriptors(*newSelected);
}


void MessyRenderer::recordCommands()
{
    m_framegraph.pass("default")->setCallback([this](VkCommandBuffer commandBuf)
    {
        size_t setOffset = m_currFrame * 4;
        for (size_t objectIndex = 0; objectIndex < m_models.size(); ++objectIndex)
        {
            const auto& model = m_models[objectIndex];
            if (!model.mesh) continue;
            if (!model.material) continue;
            vkCmdBindPipeline(commandBuf, VK_PIPELINE_BIND_POINT_GRAPHICS, model.material->pipeline);
            U32 numSets = 0;
            // Max set count = 4
            for (; numSets < 4; ++numSets)
            {
                if (model.materialSets[numSets+setOffset] == VK_NULL_HANDLE)
                {
                    break;
                }
            }
            vkCmdBindDescriptorSets(commandBuf,
                                    VK_PIPELINE_BIND_POINT_GRAPHICS,
                                    model.material->layout,
                                    0,
                                    numSets,
                                    model.materialSets.data()+setOffset,
                                    0,
                                    nullptr);

            struct PerObjectPushInfo
            {
                mat4 transform;
                U32 objectIndex;
            };
            // Fucking ugly test for attribute count for flat material; this
            // should be refactored ASAP.
            /// TODO: Actually refactor this.
            // auto isFlat = model.mesh->attributeCount() == 1;

            PerObjectPushInfo popi;
            popi.transform = model.transform;
            popi.objectIndex = objectIndex+1;

            vkCmdPushConstants(commandBuf,
                               model.material->layout,
                               VK_SHADER_STAGE_GEOMETRY_BIT | VK_SHADER_STAGE_VERTEX_BIT,
                               0,
                               sizeof(popi.transform)+sizeof(popi.objectIndex),
                               &popi);

            VkDeviceSize offsets[] = {0};
            vkCmdBindVertexBuffers(commandBuf, 0, 1, &model.mesh->vertexBuffer(0).deviceBuffer.buffer, offsets);

            if (model.mesh->indexBufferCount())
            {
                vkCmdBindIndexBuffer(commandBuf, model.mesh->indexBuffer(0).deviceBuffer.buffer, 0,
                                     model.mesh->indexBuffer(0).stride == 4 ? VK_INDEX_TYPE_UINT32 : VK_INDEX_TYPE_UINT16);

                vkCmdDrawIndexed(commandBuf, model.mesh->indexBuffer(0).count, 1, 0, 0, 0);
            }
            else
            {
                vkCmdDraw(commandBuf, model.mesh->vertexBuffer(0).count, 1, 0, 0);
            }

            if (m_showNormals)
            {
                throw std::logic_error("Not shrimplemented, but layouts should be compatible.");
            }
        }

        const auto& lineMesh = currFrame().lineMesh;
        if (lineMesh.vertexBuffer(0).deviceAllocationSize)
        {
            vkCmdBindPipeline(commandBuf, VK_PIPELINE_BIND_POINT_GRAPHICS, m_lineMaterial.pipeline);
            vkCmdBindDescriptorSets(commandBuf,
                                    VK_PIPELINE_BIND_POINT_GRAPHICS,
                                    m_lineMaterial.layout,
                                    0,
                                    1,
                                    &m_transformLightSets[m_currFrame][0],
                                    0,
                                    nullptr);
            VkDeviceSize offsets[] = {0};
            vkCmdBindVertexBuffers(commandBuf, 0, 1, &lineMesh.vertexBuffer(0).deviceBuffer.buffer, offsets);
            vkCmdBindIndexBuffer(commandBuf, lineMesh.indexBuffer(0).deviceBuffer.buffer, 0, VK_INDEX_TYPE_UINT32);
            vkCmdDrawIndexed(commandBuf, m_lineElemDrawCount, 1, 0, 0, 0);
        }

        vkCmdBindPipeline(commandBuf, VK_PIPELINE_BIND_POINT_GRAPHICS, m_plotMaterial.pipeline);

        struct PlotInfo
        {
            mat4 worldToClip;
            vec4 bounds;
        }
        plotInfo
        {
            .worldToClip = (m_perspective.transform() * m_camera->worldToCamera()).cast<float>(),
            .bounds = {-8, 8, -10, 10},
        };

        vkCmdPushConstants(commandBuf,
                           m_plotMaterial.layout,
                           VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_GEOMETRY_BIT,
                           0,
                           sizeof(PlotInfo),
                           &plotInfo);
        vkCmdBindDescriptorSets(commandBuf,
                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                m_plotMaterial.layout,
                                0, 1,
                                &m_plotFunctionSets[m_currFrame],
                                0,
                                nullptr);
        vkCmdDraw(commandBuf, 4, 1, 0, 0);
    });

    m_scCopy.pass->setCallback([this](VkCommandBuffer commandBuf)
    {
        vkCmdBindPipeline(commandBuf, VK_PIPELINE_BIND_POINT_GRAPHICS, m_scCopy.pipeline.pipeline);

        uvec3 mousePosAndControl = {(U32)m_lastCursorPosition.x,
                                    (U32)m_lastCursorPosition.y,
                                     m_drawToon ? 0x3u : 0x1u};

        vkCmdPushConstants(commandBuf,
                           m_scCopy.pipeline.layout,
                           VK_SHADER_STAGE_FRAGMENT_BIT,
                           0,
                           sizeof(mousePosAndControl),
                           &mousePosAndControl);

        vkCmdBindDescriptorSets(commandBuf,
                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                m_scCopy.pipeline.layout,
                                0,
                                1,
                                &m_scCopy.descSet.sets()[m_currFrame],
                                0,
                                nullptr);
        vkCmdDraw(commandBuf, 3, 1, 0, 0);
    });
}

void MessyRenderer::updateUniforms(U32 imageIndex)
{
    // Should go directly model-to-camera...
    auto worldToCamera = m_camera->worldToCamera().cast<float>();
    auto cameraToClip = m_perspective.transform().cast<float>();
    m_transformProvider->worldToCamera = worldToCamera;
    m_transformProvider->cameraToClip = cameraToClip;
    m_transformProvider->update(imageIndex);

    m_pickProvider->center = m_frameData[imageIndex].cursorPos.cast<U32>();
    m_pickProvider->radius = 0;
    m_pickProvider->update(imageIndex);

    auto& lights = m_lightDataProvider->lights;
    lights.pointLights[0].position = worldToCamera * vec4{1., 1., 1., 1.};
    lights.pointLights[0].colour = {1., 0.2, 0.1, 3};
    lights.pointLights[1].position = worldToCamera * vec4{-1.5, 1., -1.5, 1.};
    lights.pointLights[1].colour = {0., 0.2, 1., 3};

    m_lightDataProvider->update(imageIndex);

    m_plotFunctionProvider->update(imageIndex);

    std::vector<VkWriteDescriptorSet> descSetWrite;
    U32 count = 0;
    m_scCopy.pass->writeDescriptorSet(m_scCopy.descSet.sets()[imageIndex], &count, nullptr);
    descSetWrite.resize(count);
    m_scCopy.pass->writeDescriptorSet(m_scCopy.descSet.sets()[imageIndex], &count, descSetWrite.data());
    if (count)
    {
        vkUpdateDescriptorSets(m_device, count, descSetWrite.data(), 0, nullptr);
    }
}

void MessyRenderer::updateLines(U32 imageIndex)
{
    std::vector<DrawableLine::Vertex> vertices;
    std::vector<U32> vertexIndices;

    auto appendLine = [&vertices, &vertexIndices](const auto& lineVertices)
    {
        if (lineVertices.size() <= 1) return;
        for (bool firstIteration = true; const auto& vertex : lineVertices)
        {
            if (!firstIteration)
            {
                vertexIndices.push_back(vertices.size()-1);
                vertexIndices.push_back(vertices.size());
            }
            else firstIteration = false;
            vertices.push_back(vertex);
        }
    };

    for (const auto& line : m_lines)
    {
        appendLine(line.vertices);
    }
    if (m_currentDrawingLine.vertices.size() > 1)
    {
        appendLine(m_currentDrawingLine.vertices);
    }

    if (!m_currentDrawingLine.vertices.empty())
    {
        // Use *m_currLastLineDepth to set the depth correctly.
        auto viewportToCS = m_perspective.viewportToCameraSpace(m_perspective.near(),
                                                                m_window.framebufferSize().y);

        auto hoverPosInWorldSpace = rigidInverse(m_camera->worldToCamera())
                                  * dvec4(viewportToCS * dvec3(currFrame().cursorPos, 1.), 1.);

        auto currColour = getCurrentVertex(vec3(0.)).value().colour;
        currColour = currColour * .5 + vec4(0.5, 0.5, 0.5, 0.5);

        std::array<DrawableLine::Vertex, 2> adHocLine;
        adHocLine[0].positionAndWidth = m_currentDrawingLine.vertices.back().positionAndWidth;
        adHocLine[1].positionAndWidth = vec4(vec3(hoverPosInWorldSpace.cast<float>()), 1.);
        adHocLine[0].colour = currColour;
        adHocLine[1].colour = currColour;
        appendLine(adHocLine);
    }

    if (vertices.empty()) return;

    m_lineElemDrawCount = vertexIndices.size();

    auto& vertexBuffer = m_frameData[imageIndex].lineMesh.vertexBuffer(0);
    auto& indexBuffer = m_frameData[imageIndex].lineMesh.indexBuffer(0);

    auto resizeBufferToFit = [this, &vertexBuffer](auto& buffer, size_t requiredBytes)
    {
        if (buffer.deviceAllocationSize >= requiredBytes) return;

        auto toAllocate = std::max(2*buffer.deviceAllocationSize, requiredBytes);
        MeshBuffer newBuffer;
        VkBufferCreateInfo bufCreate{};
        bufCreate.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        bufCreate.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        bufCreate.usage = &buffer == &vertexBuffer
                        ? VK_BUFFER_USAGE_VERTEX_BUFFER_BIT
                        : VK_BUFFER_USAGE_INDEX_BUFFER_BIT
                        ;
        bufCreate.size = toAllocate;
        VmaAllocationCreateInfo allocCreate{};
        allocCreate.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
        auto result = vmaCreateBuffer(m_allocator,
                                      &bufCreate,
                                      &allocCreate,
                                      &newBuffer.deviceBuffer.buffer,
                                      &newBuffer.deviceBuffer.allocation,
                                      nullptr);
        if (result != VK_SUCCESS)
        {
            throw std::logic_error("Failed to create buffer for line drawing.");
        }
        vmaSetAllocationName(m_allocator,
                             newBuffer.deviceBuffer.allocation,
                             "Buffer from line creation.");
        newBuffer.allocator = m_allocator;
        newBuffer.deviceAllocationSize = toAllocate;

        buffer = std::move(newBuffer);
    };

    resizeBufferToFit(vertexBuffer, vertices.size() * sizeof(vertices[0]));
    resizeBufferToFit(indexBuffer, vertexIndices.size() * sizeof(vertexIndices[0]));

    {
        MemoryMapGuard mapper(m_allocator, vertexBuffer.deviceBuffer.allocation);
        std::memcpy(mapper, vertices.data(), vertices.size() * sizeof(vertices[0]));
    }
    {
        MemoryMapGuard mapper(m_allocator, indexBuffer.deviceBuffer.allocation);
        std::memcpy(mapper, vertexIndices.data(), vertexIndices.size() * sizeof(vertexIndices[0]));
    }
}

Image MessyRenderer::createPlaceholderTexture(VkExtent3D extent, VkFormat format)
{
    auto imgCreate = simpleImageCreateInfo(format, extent,
                                           VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT);
    U32 dim = 1;
    if (extent.height > 0) dim = 2;
    if (extent.depth > 0) dim = 3;
    Image img;
    if (img.allocate(imgCreate, dim, m_allocator, m_device, true) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create placeholder texture.");
    }
    if (m_samplerCache.getSampler(&img.sampler,
                                  VK_FILTER_NEAREST,
                                  VK_FILTER_NEAREST,
                                  VK_SAMPLER_MIPMAP_MODE_NEAREST,
                                  VK_SAMPLER_ADDRESS_MODE_REPEAT,
                                  0.f) != VK_SUCCESS)
    {
        img.destroy();
        throw std::runtime_error("Failed to acquire sampler for placeholder "
                                 "texture.");
    }

    VkImageMemoryBarrier barrier{};
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.srcAccessMask = 0;
    barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;

    barrier.image = img.image;
    barrier.subresourceRange = img.defaultSubresourceRange();

    immediateExecute([barrier](VkCommandBuffer commandBuf)
    {
        vkCmdPipelineBarrier(commandBuf, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                             VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0,
                             nullptr, 0, nullptr, 1, &barrier);
        VkClearColorValue cv;
        cv.float32[0] = 1;
        cv.float32[1] = 0;
        cv.float32[2] = 1;
        cv.float32[3] = 1;

        auto nextBarrier = barrier;
        nextBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        nextBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
        nextBarrier.oldLayout = nextBarrier.newLayout;
        nextBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        vkCmdClearColorImage(commandBuf, barrier.image,
                             VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                             &cv, 1, &barrier.subresourceRange);
        vkCmdPipelineBarrier(commandBuf, VK_PIPELINE_STAGE_TRANSFER_BIT,
                             VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0,
                             nullptr, 0, nullptr, 1, &nextBarrier);
    });

    img.layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    return img;
}

void MessyRenderer::invalidateDescriptors(size_t modelIndex, bool hasExternalSync)
{
    if (!hasExternalSync)
    {
        std::lock_guard guard(m_modelMutex);
        invalidateDescriptors(modelIndex, true);
        return;
    }
    // Warning: Slow search for now.
    for (auto& elem : m_invalidatedDescriptors)
    {
        if (elem.modelIndex == modelIndex)
        {
            elem.invalidated = maxCachedFrameMask;
            return;
        }
    }
    m_invalidatedDescriptors.push_back({modelIndex, maxCachedFrameMask});
}

void MessyRenderer::updateLoadedResources()
{
    m_resourceLoader.executeVulkanCommands();
    std::lock_guard guard(m_modelMutex);

    decltype(m_sceneObjectLoader.nextModifiedObject()) nextModified;
    while ((nextModified = m_sceneObjectLoader.nextModifiedObject()).has_value())
    {
        bool justCreated = false;
        if ((justCreated = !m_sceneIDToModelIndex.contains(*nextModified)))
        {
            m_sceneIDToModelIndex[*nextModified] = m_models.size();
            m_models.emplace_back();
        }

        auto& currModel = m_models[m_sceneIDToModelIndex[*nextModified]];
        auto obj = m_sceneObjectLoader.get(*nextModified);

        currModel.mesh = obj.mesh.get();
        if (justCreated)
        {
            currModel.transform = obj.transform;
            for (auto& set : currModel.materialSets) set = VK_NULL_HANDLE;
            for (size_t frame = 0; frame < maxCachedFrames; ++frame)
            {
                auto offset = 4*frame;
                currModel.materialSets[offset+0] = m_transformLightSets[frame][0];
                currModel.materialSets[offset+1] = m_transformLightSets[frame][1];
            }
        }
        if (currModel.mesh && !currModel.material)
        {
            if (currModel.mesh->attributeCount() == 1)
            {
                currModel.material = &m_flatMaterial;
                for (size_t frame = 0; frame < maxCachedFrames; ++frame)
                {
                    // Only one set in total.
                    currModel.materialSets[4*frame+1] = VK_NULL_HANDLE;
                }
            }
            else
            {
                currModel.materialProvider = std::make_unique<DefaultMaterialParameterProvider>
                    (m_multiAllocatedBuffer, m_device, m_placeholderTexture);

                currModel.material = &m_defaultMaterial;

                VkDescriptorSet results[2];
                VkDescriptorSetLayout layouts[2] =
                {
                    m_materialParameterSetLayout,
                    m_materialParameterSetLayout,
                };

                if (!m_descAllocator.allocate(results, layouts, 2))
                {
                    throw std::logic_error("Failed to allocate descriptor sets.");
                }

                /// TODO: Actually make this use number of cached frames.
                currModel.materialSets[2+0*4] = results[0];
                currModel.materialSets[2+1*4] = results[1];

                currModel.materialProvider->write(results[0], 0);
                currModel.materialProvider->write(results[1], 1);
            }
        }

        if (currModel.materialProvider && obj.colour)
        {
            obj.colour->destroyOnDestruction = true;
            currModel.materialProvider->colourTexture = obj.colour;
        }
        if (currModel.materialProvider && obj.normal)
        {
            obj.normal->destroyOnDestruction = true;
            currModel.materialProvider->normalTexture = obj.normal;
        }
        invalidateDescriptors(m_sceneIDToModelIndex[*nextModified], true);
    }

    const U32 currFrameMask = 1 << m_currFrame;
    const U32 negFrameMask = ~currFrameMask;
    for (size_t i = 0; i < m_invalidatedDescriptors.size();)
    {
        auto& currDesc = m_invalidatedDescriptors[i];
        if (currDesc.invalidated & currFrameMask)
        {
            updateDescriptors(m_currFrame, m_models[currDesc.modelIndex]);
            currDesc.invalidated &= negFrameMask;
        }
        if (currDesc.invalidated) ++i;
        else
        {
            std::swap(currDesc, m_invalidatedDescriptors.back());
            m_invalidatedDescriptors.pop_back();
        }
    }
}

bool MessyRenderer::run()
{
    vkWaitForFences(m_device, 1, &currFrame().completionFence, VK_TRUE, 0xffffffffffffffffull);
    vkResetFences(m_device, 1, &currFrame().completionFence);
    if (vkResetCommandBuffer(currFrame().commandBuffer, 0) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to reset command buffer.");
    }

    auto readback = m_readbackProvider->readback(m_currFrame);
    std::sort(readback.begin(), readback.end(),
    [](const ReadbackArrayElement& a, const ReadbackArrayElement& b)
    {
        return a.position.w < b.position.w;
    });

    if (!readback.empty())
    {
        // We can now get the pointed-to position
        updateHoveredModel(readback.begin()->ids.x-1);
        auto hoverPoint = readback.begin()->position.cast<double>();
        auto camDepth = m_perspective.ndcToCameraDepth(hoverPoint.w);
        auto pt = m_perspective.viewportToCameraSpace(camDepth, m_window.framebufferSize().y);
        dvec3 cursorPos{currFrame().cursorPos.x+.5, currFrame().cursorPos.y+.5, 1.};
        cursorPos.z = 1.;
        dvec4 toTransform(pt * cursorPos);
        toTransform.w = 1.;
        m_currHoverPoint = dvec3(inverse(m_camera->worldToCamera()) * toTransform);
        m_currHoverDepth = camDepth;

        // We are currently also storing the camera space point, but that is
        // no longer necessary, and we could thus make the data significantly
        // more compact.
    }
    else
    {
        updateHoveredModel(std::nullopt);
        m_currHoverPoint = std::nullopt;
        m_currHoverDepth = std::nullopt;
    }

    bool requestNextInput = !m_nextFunctionInput.has_value();

    if (m_nextFunctionInput.has_value() && m_nextFunctionInput->valid())
    {
        using namespace std::chrono_literals;
        if (m_nextFunctionInput->wait_for(0ms) == std::future_status::ready)
        {
            auto result = m_nextFunctionInput->get();
            if (result != "e" && result != "q" && result != "exit" && result != "stop")
            {
                requestNextInput = true;

                if (auto compiled = plotter::compileForGPU(result))
                {
                    m_plotFunctionProvider->setFunction(std::move(compiled->code),
                                                        std::move(compiled->constants));
                }
            }
        }
    }

    if (requestNextInput)
    {
        m_nextFunctionInput = std::async
        ([]()
        {
            std::string s;
            std::getline(std::cin, s);
            return s;
        });
    }

    // Can be uncommented to see exactly what values we get.
    /*
    if (!readback.empty())
    {
        std::cout << "Got " << readback.size() << " picked objects.\n";
        for (size_t i = 0; i < readback.size(); ++i)
        {
            std::cout << readback[i].position << ":" << readback[i].ids << "\n";
        }
        std::cout << std::flush;
    }*/

    currFrame().cursorPos = m_lastCursorPosition;

    U32 imageIndex;
    auto result = m_swapchain.acquireNextImage(imageIndex, currFrame().imageAcquire);

    if (result != VK_SUCCESS)
    {
        if (result == VK_ERROR_OUT_OF_DATE_KHR)
        {
            recreateSwapchain();
            return true;
        }
        else if (result != VK_SUBOPTIMAL_KHR)
        {
            throw std::runtime_error("Something went wrong when trying to acquire image.");
        }
    }

    updateLines(m_currFrame);

    m_resourceLoader.executeVulkanCommands();

    while (vkGetEventStatus(m_device, m_scCopy.prevPassesFinished) == VK_EVENT_RESET)
        ;

    auto& commandBuffer = currFrame().commandBuffer;
    if (vkResetCommandBuffer(commandBuffer, 0) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to reset command buffer.");
    }

    VkCommandBufferBeginInfo commandBegin{};
    commandBegin.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    if (vkBeginCommandBuffer(commandBuffer, &commandBegin) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to begin command buffer.");
    }
    // Scope reduction.
    decltype(m_enqueuedFunctions) enqueued;
    {
        std::lock_guard lg(m_functionEnqueueMutex);
        std::swap(enqueued, m_enqueuedFunctions);
    }
    for (const auto& func : enqueued)
    {
        func(commandBuffer);
    }

    m_sceneObjectLoader.recordVulkanCommands(commandBuffer, m_samplerCache);

    updateLoadedResources();

    updateUniforms(m_currFrame);

    vkCmdResetEvent(commandBuffer, m_scCopy.prevPassesFinished, VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT);

    m_framegraph.execute(commandBuffer, imageIndex);

    vkCmdSetEvent(commandBuffer, m_scCopy.prevPassesFinished, VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT);
    if (vkEndCommandBuffer(commandBuffer) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to end command buffer.");
    }

    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

    VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = &currFrame().imageAcquire;
    submitInfo.pWaitDstStageMask = waitStages;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &currFrame().commandBuffer;
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = &currFrame().renderFinish;

    if (vkQueueSubmit(m_device.queue(), 1, &submitInfo, currFrame().completionFence) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to submit to queue.");
    }

    result = m_swapchain.present(m_device.queue(), 1, &currFrame().renderFinish);

    if (result != VK_SUCCESS || m_resized)
    {
        if (result == VK_ERROR_OUT_OF_DATE_KHR
            || result == VK_SUBOPTIMAL_KHR
            || m_resized)
        {
            m_resized = false;
            recreateSwapchain();
        }
        else
        {
            throw std::runtime_error("Something went wrong when trying to present image.");
        }
    }

    advanceToNextFrame();
    m_window.waitEvents(0.005f);

    return !(m_window.isPressed(kraken::window::Key::Escape) || m_window.closeRequested());
}

void runMessyRenderer(fs::path dataPath)
{
    MessyRenderer vkprog(dataPath);
    while (vkprog.run())
        ;
}
