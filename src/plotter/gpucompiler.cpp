#include "plotter/gpucompiler.hpp"

#include "interpreter/astmanip/simplification.hpp"
#include "plotter/gpuopcodes.h"
#include "interpreter/lexer.hpp"
#include "interpreter/parser.hpp"

#include <iostream>
#include <map>

namespace plotter
{

struct GPUCompilationContext
{
    GPUProgram* program;
    std::map<F32, U32> constantIndices;

    const interpreter::ASTContext* astContext;

    std::string_view toString(interpreter::ASTContext::StringID id)
    {
        return astContext->getString(id);
    }

    U32 indexOfConstant(F32 value)
    {
        auto it = constantIndices.find(value);
        if (it != constantIndices.end()) return it->second;
        U32 index = program->constants.size();
        constantIndices.emplace(value, index);
        program->constants.push_back(value);
        return index;
    }

    void emitCode(U32 code)
    {
        program->code.push_back(code);
    }
};

void compile(const interpreter::Expression& expr, GPUCompilationContext& context);

void compile(const interpreter::BinaryExpression& binExp, GPUCompilationContext& context)
{
    using BinOp = interpreter::BinaryOperationType;
    compile(*binExp.left, context);
    compile(*binExp.right, context);
    switch (binExp.op)
    {
    case BinOp::Addition:
        context.emitCode(PLOT_OP_ADD);
        break;
    case BinOp::Subtraction:
        context.emitCode(PLOT_OP_SUB);
        break;
    case BinOp::Multiplication:
        context.emitCode(PLOT_OP_MUL);
        break;
    case BinOp::Division:
        context.emitCode(PLOT_OP_DIV);
        break;
    case BinOp::Modulo:
        context.emitCode(PLOT_OP_MOD);
        break;
    case BinOp::Exponentiation:
        throw std::logic_error("Exponentiation not supported on GPU yet.");
    default:
        throw std::runtime_error("Unknown binary expression token '"
                                 + std::to_string((int)binExp.op)
                                 + "'.");
    }
}

void compile(const interpreter::CallExpression& callExp, GPUCompilationContext& context)
{
    if (context.toString(callExp.toCall) == "diff")
    {
        throw std::logic_error("Cannot perform differentiation on GPU; this "
                               "must be done in advance.");
    }
    struct FunctionInfo
    {
        U32 opcode;
        U32 argCount;
    };

    static const std::map<std::string, FunctionInfo, std::less<>> functions =
    {
        {"abs", {PLOT_FUNCTION_ABS, 1}},
        {"sin", {PLOT_FUNCTION_SIN, 1}},
        {"cos", {PLOT_FUNCTION_COS, 1}},
        {"tan", {PLOT_FUNCTION_TAN, 1}},
        {"sign", {PLOT_FUNCTION_SIGN, 1}},
    };

    auto it = functions.find(context.toString(callExp.toCall));
    if (it == functions.end())
    {
        throw std::runtime_error("Unknown function '"
                                 + (std::string)context.toString(callExp.toCall)
                                 + "'.");
    }

    if (callExp.arguments.size() != it->second.argCount)
    {
        throw std::runtime_error(std::format("Expected {} arguments to function {}, but got {}.",
                                             it->second.argCount,
                                             context.toString(callExp.toCall),
                                             callExp.arguments.size()));
    }

    for (const auto& arg : callExp.arguments)
    {
        compile(*arg, context);
    }
    if (callExp.arguments.size() != 1)
    {
        throw std::logic_error("Only unary functions implemented.");
    }
    context.emitCode(PLOT_OP_UNARY_FUNCTION);
    context.emitCode(it->second.opcode);
}

void compile(const interpreter::IdentifierExpression& identifier, GPUCompilationContext& context)
{
    if (context.toString(identifier.name) == "x")
    {
        context.emitCode(PLOT_OP_PUSH_X);
    }
    else if (context.toString(identifier.name) == "y")
    {
        context.emitCode(PLOT_OP_PUSH_Y);
    }
    else throw std::runtime_error(std::format("Unkown identifier '{}'.",
                                              context.toString(identifier.name)));
}

void compile(const interpreter::NumberExpression& number, GPUCompilationContext& context)
{
    context.emitCode(PLOT_OP_PUSH_CONSTANT);
    context.emitCode(context.indexOfConstant(number.value));
}

void compile(const interpreter::UnaryExpression& unary, GPUCompilationContext& context)
{
    using UnOp = interpreter::UnaryOperationType;
    compile(*unary.right, context);
    if (unary.op == UnOp::UnaryPlus)
    {
        context.emitCode(PLOT_OP_NEGATE);
    }
    else throw std::runtime_error("Unkown unary operation.");
}

void compile(const interpreter::Expression& expr, GPUCompilationContext& context)
{
    switch (expr.type())
    {
    case interpreter::ExpressionType::Binary:
        compile(static_cast<const interpreter::BinaryExpression&>(expr), context);
        break;
    case interpreter::ExpressionType::Call:
        compile(static_cast<const interpreter::CallExpression&>(expr), context);
        break;
    case interpreter::ExpressionType::Identifier:
        compile(static_cast<const interpreter::IdentifierExpression&>(expr), context);
        break;
    case interpreter::ExpressionType::Number:
        compile(static_cast<const interpreter::NumberExpression&>(expr), context);
        break;
    case interpreter::ExpressionType::Unary:
        compile(static_cast<const interpreter::UnaryExpression&>(expr), context);
        break;
    default:
        throw std::logic_error("Invalid / unimplemented expression type.");
    }
}

std::optional<GPUProgram> compileForGPU(const interpreter::Expression& expr)
{
    try
    {
        GPUProgram program;
        GPUCompilationContext context;
        context.program = &program;
        compile(expr, context);
        program.code.push_back(PLOT_OP_RETURN);

        return program;
    }
    catch (std::exception& except)
    {
        std::cerr << "Failed to compile program: " << except.what() << "\n";
        return std::nullopt;
    }
}

std::optional<GPUProgram> compileProgramForGPU(const interpreter::ProgramSyntaxTree& tree)
{
    try
    {
        GPUProgram program;
        GPUCompilationContext context;
        context.program = &program;
        context.astContext = &tree.context;
        compile(*tree.expression, context);
        program.code.push_back(PLOT_OP_RETURN);

        return program;
    }
    catch (std::exception& except)
    {
        std::cerr << "Failed to compile program: " << except.what() << "\n";
        return std::nullopt;
    }
}

std::optional<GPUProgram> compileForGPU(const std::string& source)
{
    return interpreter::tokenise(source)
           .and_then(interpreter::parse)
           .transform(interpreter::simplifyProgram)
           .transform(compileProgramForGPU)
           .value_or(std::nullopt);
}

} // namespace plotter
