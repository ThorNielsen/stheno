#include "plotter/gpuvm.hpp"
#include "plotter/gpucompiler.hpp"
#include "plotter/gpuopcodes.h"

#include <array>
#include <cmath>

namespace plotter
{

float evalUnaryFunction(std::size_t functionID, float arg)
{
    switch (functionID)
    {
    case PLOT_FUNCTION_ABS:
        return std::abs(arg);
    case PLOT_FUNCTION_SIN:
        return std::sin(arg);
    case PLOT_FUNCTION_COS:
        return std::cos(arg);
    case PLOT_FUNCTION_TAN:
        return std::tan(arg);
    case PLOT_FUNCTION_SIGN:
        if (arg > 0.f) return 1.f;
        if (arg < 0.f) return -1.f;
        return 0.f;
    default:
        ;
    }

    return arg; // Fallback is identity.
}

std::optional<float> run(const GPUProgram& program, float x, float y)
{
    // Hardcoded size, but to translate directly from shader.
    std::array<float, 64> stack;
    stack[0] = 0.f;

    std::size_t codePos = 0;
    std::size_t stackTop = 0;

    float right;
    while (true)
    {
        switch (program.code[codePos++])
        {
        case PLOT_OP_RETURN:
            return stack[stackTop];
        case PLOT_OP_PUSH_X:
            stack[++stackTop] = x;
            break;
        case PLOT_OP_PUSH_Y:
            stack[++stackTop] = y;
            break;
        case PLOT_OP_PUSH_CONSTANT:
            stack[++stackTop] = program.constants[program.code[codePos++]];
            break;
        case PLOT_OP_UNARY_FUNCTION:
            stack[stackTop] = evalUnaryFunction(program.code[codePos++], stack[stackTop]);
            break;
        case PLOT_OP_ADD:
            right = stack[stackTop--];
            stack[stackTop] += right;
            break;
        case PLOT_OP_SUB:
            right = stack[stackTop--];
            stack[stackTop] -= right;
            break;
        case PLOT_OP_MUL:
            right = stack[stackTop--];
            stack[stackTop] *= right;
            break;
        case PLOT_OP_DIV:
            right = stack[stackTop--];
            stack[stackTop] /= right;
            break;
        case PLOT_OP_MOD:
            right = stack[stackTop--];
            stack[stackTop] = std::fmod(stack[stackTop], right);
            break;
        case PLOT_OP_NEGATE:
            stack[stackTop] = -stack[stackTop];
            break;
        default:
            return std::nullopt;
        }
    }
    return std::nullopt;
}

} // namespace plotter
