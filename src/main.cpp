#include <filesystem>
#include <iostream>
#include <string>
#include <vector>

#include "vulkan/common.hpp"

namespace fs = std::filesystem;

void runMessyRenderer(fs::path dataPath);
int texturerMain(int argc, char* argv[]);

int main(int argc, char* argv[])
{
    if (argc > 1 && (std::string(argv[1]) == "texturer"))
    {
        std::vector<char*> modifiedArgv;

        modifiedArgv.push_back(argv[0]);

        for (int i = 2; i < argc; ++i)
        {
            modifiedArgv.push_back(argv[i]);
        }

        return texturerMain(int(modifiedArgv.size()), modifiedArgv.data());
    }
    else
    {
        std::vector<fs::path> searchDirectories;
        fs::path currExecutable = *argv;
        searchDirectories.push_back(currExecutable.remove_filename());
        searchDirectories.push_back(fs::current_path());
        if (fs::current_path().filename() == "build")
        {
            searchDirectories.push_back(fs::current_path() / "..");
        }
        if (argc >= 1)
        {
            searchDirectories.emplace_back(argv[0]);
            if (fs::canonical(searchDirectories.back().parent_path()).filename() == "build")
            {
                searchDirectories.push_back(searchDirectories.back() / "..");
            }
        }
        auto dataPath = findDirectory("data", searchDirectories);
        if (dataPath.empty())
        {
            std::cerr << "Error, couldn't find data directory.\n";
            return 13;
        }
        std::cout << "Found data path " << dataPath << "\n";
        runMessyRenderer(dataPath);
    }
}
