#version 450

// This will output multiple normals:
// * Input / stored vertex normals given per-vertex.
// * Computed normal for this triangle; one at centroid, and one at each vertex
//   for 4 instances of it in total.

layout (triangles) in;
layout (line_strip, max_vertices = 20) out;

layout (location = 0) in vec3 vsPositionOut[];
layout (location = 1) in vec3 vsNormalOut[];

layout (location = 0) out vec3 gsColourOut;

layout (set = 0, binding = 0) uniform Inputs
{
    layout(row_major) mat4 worldToCamera;
    layout(row_major) mat4 cameraToClip;
} inputs;

layout (push_constant) uniform ObjectInfo
{
    layout(row_major) mat4 modelToWorld;
} objInfo;


// Expects inputs in clip space
void addLineTransformed(mat4 transform, vec3 begin, vec3 end, vec3 colour)
{
    gl_Position = transform * vec4(begin, 1);
    gsColourOut = colour;
    EmitVertex();
    gl_Position = transform * vec4(end, 1);
    gsColourOut = colour;
    EmitVertex();
    EndPrimitive();
}

void main()
{
    mat4 modelToClip = inputs.cameraToClip * inputs.worldToCamera * objInfo.modelToWorld;

    vec3 vert0 = vsPositionOut[0];
    vec3 vert1 = vsPositionOut[1];
    vec3 vert2 = vsPositionOut[2];

    vec3 midpoint = (1./3.) * (vert0 + vert1 + vert2);

    vec3 computedNormal = normalize(cross(vert1-vert0, vert2-vert0));

    {
        vec4 tr0 = modelToClip * vec4(vert0, 1.);
        vec4 tr1 = modelToClip * vec4(vert1, 1.);
        vec4 tr2 = modelToClip * vec4(vert2, 1.);
        vec3 t0 = tr0.xyz/tr0.w;
        vec3 t1 = tr1.xyz/tr1.w;
        vec3 t2 = tr2.xyz/tr2.w;
        if (cross(t1-t0, t2-t0).z > 0) return;
    }

    vec3 midpointComputedColour = vec3(1, 1, 0);
    vec3 vertexComputedColour = vec3(.7, 0, 0);
    vec3 vertexStoredColour = vec3(0, 1, 1);
    vec3 triangleLineColour = vec3(0, 0, 0);

    addLineTransformed(modelToClip,
                       midpoint,
                       midpoint + 0.02 * computedNormal,
                       midpointComputedColour);

    for (uint i = 0u; i < 3u; ++i)
    {
        addLineTransformed(modelToClip,
                           vsPositionOut[i],
                           vsPositionOut[i] + 0.025 * computedNormal,
                           vertexComputedColour);
        addLineTransformed(modelToClip,
                           vsPositionOut[i],
                           vsPositionOut[i] + 0.05 * vsNormalOut[i],
                           vertexStoredColour);

        addLineTransformed(modelToClip,
                           vsPositionOut[i],
                           vsPositionOut[(i+1u)%3u],
                           triangleLineColour);
    }
}

