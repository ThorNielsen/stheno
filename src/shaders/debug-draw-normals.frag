#version 450

layout (location = 0) in vec3 gsColourOut;

layout (location = 0) out vec4 fsColourOut;

void main()
{
    fsColourOut = vec4(gsColourOut, 1);
}

