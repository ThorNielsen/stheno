#version 450

#include "plotter/gpuopcodes.h"

layout (quads, equal_spacing, ccw) in;

layout (std140, set = 0, binding = 0) buffer Code
{
    uvec4 data[];
} code;

layout (std140, set = 0, binding = 1) buffer Constants
{
    vec4 data[];
} constants;

uint getCode(uint offset)
{
    return code.data[offset >> 2][offset & 3];
}

float getConstant(uint offset)
{
    return constants.data[offset >> 2][offset & 3];
}

float evalUnaryFunction(uint functionID, float arg)
{
    switch (functionID)
    {
    case PLOT_FUNCTION_ABS:
        return abs(arg);
    case PLOT_FUNCTION_SIN:
        return sin(arg);
    case PLOT_FUNCTION_COS:
        return cos(arg);
    case PLOT_FUNCTION_TAN:
        return tan(arg);
    case PLOT_FUNCTION_SIGN:
        return sign(arg);
    }

    return arg; // Fallback is identity.
}

float evalFunction(float x, float y)
{
    float stack[64]; // Somewhat small, but functions shouldn't be that complex.

    uint stackTop = 0u;
    uint codePos = 0u;
    float right;
    while (true)
    {
        switch (getCode(codePos++))
        {
        case PLOT_OP_RETURN:
            return stack[stackTop];
        case PLOT_OP_PUSH_X:
            stack[++stackTop] = x;
            break;
        case PLOT_OP_PUSH_Y:
            stack[++stackTop] = y;
            break;
        case PLOT_OP_PUSH_CONSTANT:
            stack[++stackTop] = getConstant(getCode(codePos++));
            break;
        case PLOT_OP_UNARY_FUNCTION:
            stack[stackTop] = evalUnaryFunction(getCode(codePos++), stack[stackTop]);
            break;
        case PLOT_OP_ADD:
            right = stack[stackTop--];
            stack[stackTop] += right;
            break;
        case PLOT_OP_SUB:
            right = stack[stackTop--];
            stack[stackTop] -= right;
            break;
        case PLOT_OP_MUL:
            right = stack[stackTop--];
            stack[stackTop] *= right;
            break;
        case PLOT_OP_DIV:
            right = stack[stackTop--];
            stack[stackTop] /= right;
            break;
        case PLOT_OP_MOD:
            right = stack[stackTop--];
            stack[stackTop] = mod(stack[stackTop], right);
            break;
        case PLOT_OP_NEGATE:
            stack[stackTop] = -stack[stackTop];
            break;
        }
    }
    // Should never happen.
    return 0.;
}

float function(float x, float y)
{
    return evalFunction(x, y);
}

void main()
{
    vec4 leftPos = mix(gl_in[0].gl_Position, gl_in[1].gl_Position, gl_TessCoord.x);
    vec4 rightPos = mix(gl_in[2].gl_Position, gl_in[3].gl_Position, gl_TessCoord.x);
    vec4 position = mix(leftPos, rightPos, gl_TessCoord.y);

    position.y = function(position.x, position.z);

    gl_Position = position;
}
