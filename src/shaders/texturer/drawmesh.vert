#version 450

layout (location = 0) in vec3 positionIn;
layout (location = 1) in vec3 normalIn;
layout (location = 2) in vec2 texcoordIn;
layout (location = 3) in vec3 tangentIn;

layout (location = 0) out vec3 positionOut;
layout (location = 1) out vec3 normalOut;
layout (location = 2) out vec3 tangentOut;

layout (push_constant, row_major) uniform TransformData
{
    mat4 uvToPos;
} transformData;

void main()
{
    gl_Position = transformData.uvToPos * vec4(texcoordIn, 0.5, 1.);
    positionOut = positionIn;
    normalOut = normalIn;
    tangentOut = tangentIn;
}
