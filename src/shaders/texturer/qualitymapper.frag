#version 450

layout (input_attachment_index = 0, set = 0, binding = 0) uniform subpassInput positionIn;
layout (input_attachment_index = 1, set = 0, binding = 1) uniform subpassInput normalIn;

// Contents of stats:
// [0] sum_i(quality_i)
// [1] sum_i(quality_i^2)
// [2] sum_i 1            [That is, 1 for every image which pixel is inside of]
// [3] max_i(quality_i)
layout (location = 0) out vec4 stats;

layout (set = 0, binding = 2) uniform sampler2D normalDepthTex;
layout (set = 0, binding = 3) uniform sampler2D confidenceTex;
layout (set = 0, binding = 4) uniform sampler2D qualityTex;

#define INPUTS_BINDING_NUMBER 5
#include "quality.common"

void main()
{
    vec3 worldPos = subpassLoad(positionIn).xyz;
    vec3 worldNormal = subpassLoad(normalIn).xyz;

    if (dot(worldPos, worldPos) <= 1e-10 || !isInsideImage(worldPos))
    {
        discard;
    }

    float quality = getQuality(worldPos, worldNormal);

    if (quality < 0) discard;
    stats = vec4(quality, quality*quality, 1, quality);
}
