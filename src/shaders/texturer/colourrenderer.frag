#version 450

layout (input_attachment_index = 0, set = 0, binding = 0) uniform subpassInput positionIn;
layout (input_attachment_index = 1, set = 0, binding = 1) uniform subpassInput normalIn;
layout (input_attachment_index = 2, set = 0, binding = 2) uniform subpassInput tangentIn;
layout (input_attachment_index = 3, set = 0, binding = 3) uniform subpassInput statsIn;

layout (location = 0) out vec4 colour;
layout (location = 1) out vec4 normal; // .w stores the displacement map, i.e.
// the difference in length from model to dmap points.

layout (set = 0, binding = 4) uniform sampler2D normalDepthTex;
layout (set = 0, binding = 5) uniform sampler2D confidenceTex;
layout (set = 0, binding = 6) uniform sampler2D qualityTex;
layout (set = 0, binding = 7) uniform sampler2D colourTex;

#define INPUTS_BINDING_NUMBER 8
#include "quality.common"

float qualityMagic(float quality, vec4 stats)
{
    if (quality <= 0) discard;

    // Sample not rejected, so stats.b is nonzero now (it contains the count).
    float mean = stats.r / stats.b;
    float maximum = stats.a;

    if (inputs.discardLessThanMean > 0 && quality < mean) discard;

    if (inputs.discardLessThanMean > 1)
    {
        quality -= mean;
        if (maximum - mean <= 1e-5) quality += 1.;
    }

    return transformQuality(quality, stats);
}

mat3 getWorldToTangentMap(vec3 worldNormal, vec3 worldTangent)
{
    float sgn = dot(worldTangent, worldTangent) > 1.5 ? -1. : 1.;
    vec3 bitangent = sgn * cross(worldNormal, worldTangent);

    mat3 tbn = mat3(normalize(worldTangent), bitangent, worldNormal);
    mat3 tbnInverse = transpose(tbn); // A real orthonormal matrix's inverse is
                                      // itself transposed.
    return tbnInverse;
}

vec3 reproject(vec2 imgPos)
{
    return imagePosToWorldPos(imgPos, texture(normalDepthTex, imgPos).w);
}

void main()
{
    vec3 worldPos = subpassLoad(positionIn).xyz;
    vec3 worldNormal = subpassLoad(normalIn).xyz;
    vec3 worldTangent = subpassLoad(tangentIn).xyz;

    // Contents of stats:
    // [0] sum_i(quality_i)
    // [1] sum_i(quality_i^2)
    // [2] sum_i 1
    // [3] max_i(quality_i)
    vec4 stats = subpassLoad(statsIn);

    if (stats.z <= 0.5) discard;

    if (dot(worldPos, worldPos) <= 1e-10 || !isInsideImage(worldPos))
    {
        discard;
    }
    vec2 imagePos = worldPosToImagePos(worldPos);
    float quality = getQuality(worldPos, worldNormal);
    quality = qualityMagic(quality, stats);

    /*
    vec2 imgPosFromNormal = findClosestImageCoordinates(worldPos, worldNormal);
    vec3 reprojectedWorldPos = reproject(imgPosFromNormal); /*/
    vec2 imgPosFromWorldPos = worldPosToImagePos(worldPos);
    vec3 reprojectedWorldPos = reproject(imgPosFromWorldPos); //*/

    float d = dot(reprojectedWorldPos - worldPos, worldNormal);

    colour = vec4(texture(colourTex, imagePos).rgb * quality, quality);

    mat3 worldToTangent = getWorldToTangentMap(worldNormal, worldTangent);
    vec3 wsNormal = cameraNormalToWorldSpace(texture(normalDepthTex, imagePos).xyz) * quality;
    normal = vec4(worldToTangent * wsNormal, d);
}
