#version 450

layout (location = 0) in vec3 positionIn;
layout (location = 1) in vec3 normalIn;
layout (location = 2) in vec3 tangentIn;

layout (location = 0) out vec4 positionOut;
layout (location = 1) out vec4 normalOut;
layout (location = 2) out vec4 tangentOut;

void main()
{
    positionOut = vec4(positionIn, 1.0);
    normalOut = vec4(normalIn, 1.0);
    tangentOut = vec4(tangentIn, 1.0);
}
