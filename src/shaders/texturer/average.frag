#version 450

layout (input_attachment_index = 0, set = 0, binding = 0) uniform subpassInput positionIn;
layout (input_attachment_index = 1, set = 0, binding = 1) uniform subpassInput normalIn;
layout (input_attachment_index = 2, set = 0, binding = 2) uniform subpassInput tangentIn;

layout (location = 0) out vec4 positionOut;
layout (location = 1) out vec4 normalOut;
layout (location = 2) out vec4 tangentOut;

vec4 computeAverage(vec4 v)
{
    return v.w > 0 ? v / v.w : vec4(0., 0., 0., 0.);
}

vec4 normalisedAverageOrZero(vec4 v)
{
    vec4 w = computeAverage(v);
    if (dot(w, w) > 1e-8)
    {
        return normalize(w);
    }
    else
    {
        return vec4(0., 0., 0., 0.);
    }
}

void main()
{
    positionOut = computeAverage(subpassLoad(positionIn));
    normalOut = normalisedAverageOrZero(subpassLoad(normalIn));
    tangentOut = computeAverage(subpassLoad(tangentIn));
}
