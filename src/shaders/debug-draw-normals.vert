#version 450

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;

layout (location = 0) out vec3 vsPositionOut;
layout (location = 1) out vec3 vsNormalOut;

void main()
{
    vsPositionOut = position;
    vsNormalOut = normal;
}

