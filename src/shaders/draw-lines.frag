#version 450

layout (location = 0) in vec4 vsColourOut;

layout (location = 0) out vec4 fsColourOut;

// For layout compatibility ONLY.

struct ReadbackArrayElement
{
    vec4 position;
    uvec4 ids; // .z, .w are fragment coordinates.
};

layout (std140, set = 0, binding = 1) buffer ReadbackInfo
{
    uint maxElemCount;
    uint nextPos;
    uint padding0;
    uint padding1;
    ReadbackArrayElement readbacks[];
} readbackInfo;

layout (set = 0, binding = 2) uniform PickingInfo
{
    uint centerX;
    uint centerY;
    uint radiusSqr;
    uint padding;
} pickInfo;


void main()
{
    fsColourOut = vsColourOut;
}

