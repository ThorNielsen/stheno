#version 450

layout (push_constant) uniform Inputs
{
    layout(row_major) mat4 modelToClip;
    vec4 bounds; // = vec4(xMin, xMax, zMin, zMax)
} inputs;

void main()
{
    vec4 range = inputs.bounds;
    gl_Position = vec4(vec4(range[0], range[0], range[1], range[1])[gl_VertexIndex & 3],
                       0.,
                       vec4(range[2], range[3], range[2], range[3])[gl_VertexIndex & 3],
                       1.);
}
