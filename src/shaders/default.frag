#version 450

layout (location = 0) in vec2 vsTexcoordOut;
layout (location = 1) in vec3 vsNormalOut;
layout (location = 2) in vec3 vsCSPositionOut;
// Currently these are unused but should be used to implement hover selection by
// outputting to a separate render pass and reading back the position at the
// mouse (and/or other selected hoved points) to the CPU; maybe by using a
// storage buffer if possible, or by making a dummy framebuffer and reading that
// with ACCESS_HOST_READ_BIT | LAYOUT_GENERAL.
layout (location = 3) flat in uvec2 vsDrawIndicesOut;

struct PointLight
{
    vec4 position; // .xyz = position, .w unused
    vec4 colour; // .rgb = colour, .a = intensity∈[0, ∞)
};

struct DirectionalLight
{
    vec4 direction; // .xyz = direction, .w unused
    vec4 colour; // .rgb = colour, .a = intensity∈[0, 1]
};

const uint maxDirectionalLights = 3;
const uint maxPointLights = 5;

struct ReadbackArrayElement
{
    vec4 position;
    uvec4 ids; // .z, .w are fragment coordinates.
};

layout (std140, set = 0, binding = 1) buffer ReadbackInfo
{
    uint maxElemCount;
    uint nextPos;
    uint padding0;
    uint padding1;
    ReadbackArrayElement readbacks[];
} readbackInfo;

layout (set = 0, binding = 2) uniform PickingInfo
{
    uint centerX;
    uint centerY;
    uint radiusSqr;
    uint padding;
} pickInfo;

layout (set = 1, binding = 0) uniform Lights
{
    DirectionalLight directional[maxDirectionalLights];
    PointLight points[maxPointLights];
} lights;

layout (set = 2, binding = 0) uniform sampler2D coltex;
layout (set = 2, binding = 1) uniform sampler2D nmltex;
layout (set = 2, binding = 2) uniform MaterialProperties
{
    vec4 tint; // .rgb = colour, .a = intensity
} materialProperties;

layout (location = 0) out vec4 fsColourOut;
layout (location = 1) out uvec4 infoOut;

vec3 getLightColour(vec3 currPosition, vec3 currNormal)
{
    vec3 acc = vec3(0., 0., 0.);
    for (uint i = 0; i < maxDirectionalLights; ++i)
    {
        acc += clamp(dot(currNormal, lights.directional[i].direction.xyz), 0., 1.)
             * lights.directional[i].colour.rgb
             * lights.directional[i].colour.a;
    }
    for (uint i = 0; i < maxPointLights; ++i)
    {
        vec3 dir = currPosition - lights.points[i].position.xyz;
        float scaleFac = clamp(dot(normalize(dir), currNormal), 0., 1.)
                       / max(1., dot(dir, dir));
        acc += lights.points[i].colour.rgb
             * lights.points[i].colour.a
             * scaleFac;
    }
    return acc;
}

void main()
{
    vec4 colour = vec4(mix(texture(coltex, vsTexcoordOut).rgb,
                           materialProperties.tint.rgb,
                           materialProperties.tint.a),
                       1.0);
    vec3 normal = texture(nmltex, vsTexcoordOut).xyz;
    fsColourOut = vec4(getLightColour(vsCSPositionOut, normal), 1.) * colour;

    uvec2 posDiff = uvec2(gl_FragCoord.x, gl_FragCoord.y)
                  - uvec2(pickInfo.centerX, pickInfo.centerY);
    if (dot(posDiff, posDiff) <= pickInfo.radiusSqr)
    {
        uint currPos = atomicAdd(readbackInfo.nextPos, 1);
        if (currPos < readbackInfo.maxElemCount)
        {
            readbackInfo.readbacks[currPos].position = vec4(vsCSPositionOut, gl_FragCoord.z);
            readbackInfo.readbacks[currPos].ids = uvec4(vsDrawIndicesOut, gl_FragCoord.x, gl_FragCoord.y);
        }
    }
    infoOut = uvec4((vsDrawIndicesOut.x << 1) | uint(vsNormalOut.z < 0),
                    vsDrawIndicesOut.y,
                    floatBitsToUint(vsNormalOut.x),
                    floatBitsToUint(vsNormalOut.y));
}

