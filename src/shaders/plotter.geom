#version 450

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

layout (location = 0) out vec3 normalOut;

layout (push_constant) uniform Inputs
{
    layout(row_major) mat4 modelToClip;
    vec4 bounds[4];
} inputs;

void main()
{
    vec3 computedNormal = normalize(cross(gl_in[1].gl_Position.xyz-gl_in[0].gl_Position.xyz,
                                          gl_in[2].gl_Position.xyz-gl_in[0].gl_Position.xyz));

    for (uint i = 0u; i < 3u; ++i)
    {
        gl_Position = inputs.modelToClip * vec4(gl_in[i].gl_Position.xyz, 1.);
        normalOut = (inputs.modelToClip * vec4(computedNormal, 0.)).xyz;
        EmitVertex();
    }
    EndPrimitive();
}
