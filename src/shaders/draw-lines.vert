#version 450

layout (location = 0) in vec4 positionAndWidth;
layout (location = 1) in vec4 colour;

layout (location = 0) out vec4 vsColourOut;

layout (set = 0, binding = 0) uniform Inputs
{
    layout(row_major) mat4 worldToCamera;
    layout(row_major) mat4 cameraToClip;
} inputs;

void main()
{
    gl_Position = inputs.cameraToClip * inputs.worldToCamera * vec4(positionAndWidth.xyz, 1.);
    vsColourOut = colour;
}

