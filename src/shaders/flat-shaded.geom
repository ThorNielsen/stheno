#version 450

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

layout (location = 0) in vec3 positionIn[];
layout (location = 1) flat in uint vertexIndexIn[];

layout (location = 0) out vec3 normalOut;
layout (location = 1) out vec3 csPositionOut;
layout (location = 2) flat out uvec2 drawIndicesOut;

layout (set = 0, binding = 0) uniform Inputs
{
    layout(row_major) mat4 worldToCamera;
    layout(row_major) mat4 cameraToClip;
} inputs;

layout (push_constant) uniform ObjectInfo
{
    layout(row_major) mat4 modelToWorld;
    uint objectIndex;
} objInfo;

void main()
{
    mat4 toCamera = inputs.worldToCamera * objInfo.modelToWorld;
    mat4 transform = inputs.cameraToClip * toCamera;

    vec3 computedNormal = normalize(cross(positionIn[1]-positionIn[0],
                                          positionIn[2]-positionIn[0]));

    for (uint i = 0u; i < 3u; ++i)
    {
        gl_Position = transform * vec4(positionIn[i], 1.);
        normalOut = (transform * vec4(computedNormal, 0.)).xyz;
        csPositionOut = (toCamera * vec4(positionIn[i], 1.)).xyz;
        drawIndicesOut = uvec2(objInfo.objectIndex, vertexIndexIn[i]);
        EmitVertex();
    }
    EndPrimitive();
}

