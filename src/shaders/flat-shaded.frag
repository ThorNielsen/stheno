#version 450

layout (location = 0) in vec3 normal;
layout (location = 1) in vec3 csPositionIn;
layout (location = 2) flat in uvec2 drawIndicesIn;

layout (location = 0) out vec4 colour;

struct PointLight
{
    vec4 position; // .xyz = position, .w unused
    vec4 colour; // .rgb = colour, .a = intensity∈[0, ∞)
};

struct DirectionalLight
{
    vec4 direction; // .xyz = direction, .w unused
    vec4 colour; // .rgb = colour, .a = intensity∈[0, 1]
};

const uint maxDirectionalLights = 3;
const uint maxPointLights = 5;

struct ReadbackArrayElement
{
    vec4 position;
    uvec4 ids; // .z, .w are fragment coordinates.
};

layout (std140, set = 0, binding = 1) buffer ReadbackInfo
{
    uint maxElemCount;
    uint nextPos;
    uint padding0;
    uint padding1;
    ReadbackArrayElement readbacks[];
} readbackInfo;

layout (set = 0, binding = 2) uniform PickingInfo
{
    uint centerX;
    uint centerY;
    uint radiusSqr;
    uint padding;
} pickInfo;

layout (set = 1, binding = 0) uniform Lights
{
    DirectionalLight directional[maxDirectionalLights];
    PointLight points[maxPointLights];
} lights;

void main()
{
    colour = vec4(abs(normal), 1);
    uvec2 posDiff = uvec2(gl_FragCoord.x, gl_FragCoord.y)
                  - uvec2(pickInfo.centerX, pickInfo.centerY);
    if (dot(posDiff, posDiff) <= pickInfo.radiusSqr)
    {
        uint currPos = atomicAdd(readbackInfo.nextPos, 1);
        if (currPos < readbackInfo.maxElemCount)
        {
            readbackInfo.readbacks[currPos].position = vec4(csPositionIn, gl_FragCoord.z);
            readbackInfo.readbacks[currPos].ids = uvec4(drawIndicesIn, gl_FragCoord.x, gl_FragCoord.y);
        }
    }
}

