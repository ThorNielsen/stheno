#version 450

layout (location = 0) in vec3 position;

layout (location = 0) out vec3 positionOut;
layout (location = 1) flat out uint vertexIndexOut;

void main()
{
    positionOut = position;
    vertexIndexOut = gl_VertexIndex;
}
