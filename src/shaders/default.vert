#version 450

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texcoord;

layout (location = 0) out vec2 vsTexcoordOut;
layout (location = 1) out vec3 vsNormalOut;
layout (location = 2) out vec3 vsCSPositionOut; // Camera space position.
// .x = Object index (into "global" object list)
// .y = Vertex index (note that for indexed rendering this is the index into
//                    the *indices*, and NOT the vertices, so this will uniquely
//                    identify on a triangle level, but not vertex level (as we
//                    are using flat interpolation, so only one is selected
//                    meaning only one vertex index will be given)).
layout (location = 3) flat out uvec2 vsDrawIndicesOut;

layout (set = 0, binding = 0) uniform Inputs
{
    layout(row_major) mat4 worldToCamera;
    layout(row_major) mat4 cameraToClip;
} inputs;

layout (push_constant) uniform ObjectInfo
{
    layout(row_major) mat4 modelToWorld;
    uint objectIndex;
} objInfo;

void main()
{
    vec4 csPos = inputs.worldToCamera
                * objInfo.modelToWorld
                * vec4(position, 1.0);
    gl_Position = inputs.cameraToClip * csPos;
    vsTexcoordOut = texcoord;
    vsNormalOut = (inputs.worldToCamera
                * objInfo.modelToWorld * vec4(normal, 0.)).xyz;
    vsCSPositionOut = csPos.xyz;
    vsDrawIndicesOut = uvec2(objInfo.objectIndex, gl_VertexIndex);
}

