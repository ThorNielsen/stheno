#version 450

layout (input_attachment_index = 0, set = 0, binding = 0) uniform subpassInput colourIn;

layout (set = 0, binding = 1) uniform usampler2D infoIn;

layout (location = 0) out vec4 colourOut;

layout (push_constant) uniform Inputs
{
    uvec2 mousePos;
    uint controlBits;
} inputs;

bool shouldDrawOutline()
{
    return (inputs.controlBits & 0x1u) != 0u;
}

bool shouldDrawToon()
{
    return (inputs.controlBits & 0x2u) != 0u;
}

vec3 getNormal(uvec4 packedInfo)
{
    vec3 normal = vec3(uintBitsToFloat(packedInfo.z), uintBitsToFloat(packedInfo.w), 0.);
    normal.z = sqrt(1 - dot(normal, normal));
    if ((packedInfo.x & 1) == 1) normal.z = -normal.z;
    return normal;
}

uint getObjectID(uvec4 packedInfo)
{
    return packedInfo.x >> 1u;
}

// Right now this ignores colour, but is *should* bias the cel colour with it.
vec4 computeCelColour(vec3 normal, vec4 colour)
{
    const vec3 baseToonColour = vec3(0.6, 0.8, 0.2);

    float intensity = clamp(dot(normal, normalize(vec3(-0.3, -0.1, -0.8))), 0, 1);

    if (intensity < 0.4) intensity = 0.4;
    else if (intensity < 0.7) intensity = 0.6;
    else intensity = 0.9;
    //else intensity = smoothstep((intensity-0.7) / 0.3, 0.7, 1.);

    colour.rgb = baseToonColour * intensity;

    return colour;
}

// Normal-based; main component of toon shading.
float innerStrokeIntensity(ivec2 currPos, vec3 currNormal)
{
    float deviation = 0.;
    for (int dy = -2; dy <= 2; ++dy)
    {
        for (int dx = -2; dx <= 2; ++dx)
        {
            ivec2 coordOffset = ivec2(dx, dy);
            float sqOffset = dot(coordOffset, coordOffset);
            if (sqOffset <= 5)
            {
                vec3 normalAtPoint = getNormal(texelFetch(infoIn, currPos+coordOffset, 0));
                deviation += (1-max(dot(normalAtPoint, currNormal), 0))
                           / max(1, float(sqOffset));
            }
        }
    }
    return 1-smoothstep(0, 1, clamp(deviation*10, 0, 1));
}

// ID-based, required for toon shading to avoid problems if two different
// objects with normals in the same direction are in front of each other.
float outerStrokeIntensity(ivec2 currPos, uint currID)
{
    bool foundCurr = false;
    float deviation = 0.;
    for (int dy = -2; dy <= 2; ++dy)
    {
        for (int dx = -2; dx <= 2; ++dx)
        {
            ivec2 coordOffset = ivec2(dx, dy);
            float sqOffset = dot(coordOffset, coordOffset);
            if (sqOffset <= 7)
            {
                uint idAtPoint = getObjectID(texelFetch(infoIn, currPos+coordOffset, 0));
                if (idAtPoint != currID)
                {
                    deviation += 1 / max(1, float(sqOffset));
                }
                else foundCurr = true;
            }
        }
    }
    if (!foundCurr) return 1;
    return 1-smoothstep(0, 1, clamp(deviation*0.15, 0, 1));
}

void main()
{
    uvec4 mouseHoverInfo = texelFetch(infoIn, ivec2(inputs.mousePos), 0);
    uint mouseHoverObjectID = mouseHoverInfo.x >> 1u;

    colourOut = subpassLoad(colourIn);

    uvec4 currInfo = texelFetch(infoIn, ivec2(gl_FragCoord.xy), 0);
    vec3 normal = getNormal(currInfo);

    // We only want to apply effects to the current hovered object.
    if (mouseHoverObjectID != 0)
    {
        if (getObjectID(currInfo) == mouseHoverObjectID)
        {
            if (shouldDrawToon())
            {
                colourOut = computeCelColour(normal, colourOut)
                          * innerStrokeIntensity(ivec2(gl_FragCoord.xy), normal)
                          * outerStrokeIntensity(ivec2(gl_FragCoord.xy), getObjectID(currInfo))
                          ;
            }
        }
        else
        {
            if (shouldDrawOutline())
            {
                colourOut *= outerStrokeIntensity(ivec2(gl_FragCoord.xy), mouseHoverObjectID);
            }
        }
    }
}
