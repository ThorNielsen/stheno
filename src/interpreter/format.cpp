#include "interpreter/format.hpp"

#include <stdexcept>

namespace interpreter
{

std::string to_string(TokenType tokenType)
{
    switch (tokenType)
    {
    case TokenType::LeftParenthesis:  return "LeftParenthesis";
    case TokenType::RightParenthesis: return "RightParenthesis";
    case TokenType::LeftSquareBracket: return "LeftSquareBracket";
    case TokenType::RightSquareBracket: return "RightSquareBracket";
    case TokenType::Plus: return "Plus";
    case TokenType::Dash: return "Dash";
    case TokenType::Star: return "Star";
    case TokenType::Slash: return "Slash";
    case TokenType::Percent: return "Percent";
    case TokenType::Caret: return "Caret";
    case TokenType::Comma: return "Comma";
    case TokenType::Identifier: return "Identifier";
    case TokenType::Number: return "Number";
    case TokenType::Eof: return "Eof";
    default:
        throw std::domain_error("Invalid enum.");
    }
}

std::string to_string(UnaryOperationType op)
{
    switch (op)
    {
    case UnaryOperationType::UnaryPlus:  return "+";
    case UnaryOperationType::UnaryMinus: return "-";
    default:
        throw std::domain_error("Invalid enum.");
    }
}

std::string to_string(BinaryOperationType op)
{
    switch (op)
    {
    case BinaryOperationType::Addition:        return "+";
    case BinaryOperationType::Subtraction:     return "-";
    case BinaryOperationType::Multiplication:  return "*";
    case BinaryOperationType::Division:        return "/";
    case BinaryOperationType::Modulo:          return "%";
    case BinaryOperationType::Exponentiation:  return "^";
    default:
        throw std::domain_error("Invalid enum.");
    }
}

} // namespace interpreter
