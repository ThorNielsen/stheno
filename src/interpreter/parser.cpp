#include "interpreter/parser.hpp"

#include "interpreter/format.hpp"

#include <kraken/utility/memory.hpp>

#include <optional>

namespace interpreter
{

// Todo: Add “next” part to this, or otherwise change the interface so that one
// can easily specify a combination of tokens that make up an error without
// purely encoding it as a human-readable string.
class SingleTokenParsingError : public detail::NoexceptCompilationError
{
public:
    // Observe that we copy the token to extend the lifetime of the string.
    SingleTokenParsingError(const Token& token,
                            std::string description)
        : m_description{description}
        , m_tokenContent{token.value}
        , m_token{m_tokenContent, token.type, token.location}
    {}

    std::optional<SourceCodeRange> location() const noexcept override
    {
        return SourceCodeRange
        {
            m_token.location,
            {
                m_token.location.line,
                m_token.location.character + m_token.value.size()
            }
        };
    }

    std::string_view getDescription() const override
    {
        return m_description;
    }

    std::string_view compilerPass() const noexcept override
    {
        using std::string_view_literals::operator""sv;
        return "parser"sv;
    }

private:
    std::string m_description;
    std::string m_tokenContent;
    Token m_token;
};

class MissingTokenParsingError : public CompilationError
{
public:
    // Observe that we copy the token to extend the lifetime of the string.
    MissingTokenParsingError(std::string which,
                             SourceCodeLocation location)
        : m_formatted{which}
        , m_formattedView{m_formatted}
        , m_range{location, location} // Expected at one position
    {}

    std::optional<SourceCodeRange> location() const noexcept override
    {
        return m_range;
    }

    std::string_view description() const noexcept override
    {
        return m_formattedView;
    }

    std::string_view compilerPass() const noexcept override
    {
        using std::string_view_literals::operator""sv;
        return "parser"sv;
    }

private:
    std::string m_formatted;
    std::string_view m_formattedView;
    SourceCodeRange m_range;
};

std::expected<double, std::errc> toDouble(const Token& token)
{
    double value = 0.;
    auto val = std::from_chars(token.value.begin(), token.value.end(), value);
    if (val.ec != std::errc{})
    {
        return std::unexpected(val.ec);
    }
    if (val.ptr != token.value.end())
    {
        return std::unexpected(std::errc::argument_out_of_domain);
    }
    return value;
}

using ExpressionOrError =
std::expected
<
    std::unique_ptr<Expression>,
    std::unique_ptr<CompilationError>
>;

class Parser
{
public:
    Parser(const std::vector<Token>& tokens,
           ASTContext& context)
        : m_tokens{tokens}
        , m_context{context}
        , m_position{0}
    {
        try
        {
            m_result = expression();
        }
        catch (AbnormalTerminationTag&)
        {
            // No cleanup is necessary; the tag purely gives us a longjmp of
            // sorts.
        }
    }

    ExpressionOrError result()
    {
        return std::move(m_result);
    }

    [[nodiscard]] bool success() const noexcept
    {
        return !!m_result;
    }

private:
    class AbnormalTerminationTag {};

    std::unique_ptr<Expression> expression()
    {
        return term();
    }

    std::unique_ptr<Expression> term()
    {
        auto left = factor();
        while (auto token = extract(TokenType::Plus, TokenType::Dash))
        {
            auto right = factor();
            auto op = token->type == TokenType::Plus
                    ? BinaryOperationType::Addition
                    : BinaryOperationType::Subtraction
                    ;

            left = std::make_unique<BinaryExpression>(op,
                                                      std::move(left),
                                                      std::move(right));
        }
        return left;
    }

    std::unique_ptr<Expression> factor()
    {
        auto left = exponentiation();
        while (auto token = extract(TokenType::Star, TokenType::Slash))
        {
            auto right = exponentiation();
            auto op = token->type == TokenType::Star
                    ? BinaryOperationType::Multiplication
                    : BinaryOperationType::Division
                    ;
            left = std::make_unique<BinaryExpression>(op,
                                                      std::move(left),
                                                      std::move(right));

        }
        return left;
    }

    std::unique_ptr<Expression> exponentiation()
    {
        auto left = unary();
        while (auto token = extract(TokenType::Caret))
        {
            auto right = unary();
            left = std::make_unique<BinaryExpression>(BinaryOperationType::Exponentiation,
                                                      std::move(left),
                                                      std::move(right));

        }
        return left;
    }

    std::unique_ptr<Expression> unary()
    {
        if (auto token = extract(TokenType::Dash))
        {
            auto right = unary();
            auto op = token->type == TokenType::Plus
                    ? UnaryOperationType::UnaryPlus
                    : UnaryOperationType::UnaryMinus
                    ;
            return std::make_unique<UnaryExpression>(op,
                                                     std::move(right));
        }
        return call();
    }

    std::unique_ptr<Expression> call()
    {
        auto left = primary();
        if (auto token = extract(TokenType::LeftParenthesis))
        {
            if (left->type() != ExpressionType::Identifier)
            {
                // Should we maybe return the entire left subtree here?
                terminate(*token, "Can only call static identified functions.");
            }
            std::vector<std::unique_ptr<Expression>> args;
            if (!isCurrentToken(TokenType::RightParenthesis))
            {
                do
                {
                    args.emplace_back(expression());
                } while (extract(TokenType::Comma));
            }
            if (!extract(TokenType::RightParenthesis))
            {
                terminate(*token, "Expected ')' to end function call.");
            }
            return std::make_unique<CallExpression>(kraken::downcast<IdentifierExpression>(std::move(left))->name,
                                                    std::move(args));
        }
        else return left;
    }

    // Parses a general comma-separated group, as for function call arguments,
    // array elements, etc.
    std::vector<std::unique_ptr<Expression>>
    parseGrouping(const Token& startToken, TokenType expectedEndToken)
    {
        std::vector<std::unique_ptr<Expression>> elements;
        if (!isCurrentToken(expectedEndToken))
        {
            do
            {
                elements.emplace_back(expression());
            } while (extract(TokenType::Comma));
        }
        if (!extract(expectedEndToken))
        {
            terminate(startToken, std::format("Expected a closing {}.",
                                              expectedEndToken));
        }
        return elements;
    }

    std::unique_ptr<Expression> primary()
    {
        if (auto token = extract(TokenType::Number))
        {
            return std::make_unique<NumberExpression>(extractNumber(*token));
        }
        else if (auto token = extract(TokenType::Identifier))
        {
            return std::make_unique<IdentifierExpression>(m_context.getID(token->value));
        }
        else if (auto token = extract(TokenType::LeftParenthesis))
        {
            auto elements = parseGrouping(*token, TokenType::RightParenthesis);
            if (elements.size() == 0)
            {
                terminate(*token, "Unexpectedly empty expression within parentheses.");
            }
            else if (elements.size() == 1) return std::move(elements.front());
            else terminate("Tuples not yet supported.");
        }
        else if (auto token = extract(TokenType::LeftSquareBracket))
        {
            return std::make_unique<ArrayExpression>(parseGrouping(*token,
                                                                   TokenType::RightSquareBracket));
        }
        else terminate("Missing primary expression.");
    }

    double extractNumber(const Token& token)
    {
        auto num = toDouble(token);
        if (!num)
        {
            const char* parseFailReason = "unknown";
            if (num.error() == std::errc::invalid_argument)
            {
                parseFailReason = "value is not a number";
            }
            else if (num.error() == std::errc::result_out_of_range)
            {
                parseFailReason = "number too large to represent";
            }
            else if (num.error() == std::errc::argument_out_of_domain)
            {
                parseFailReason = "internal problem: token not fully consumed";
            }
            terminate(token,
                      std::format("Couldn't convert {} to a number ({}).",
                                  token.value,
                                  parseFailReason));
        }
        return *num;
    }

    Token nextToken()
    {
        return m_tokens[m_position++];
    }

    std::optional<Token> extract(TokenType type)
    {
        if (!isCurrentToken(type)) return {};
        return m_tokens[m_position++];
    }

    // Should maybe be templated in number of args, but we currently don't
    // require that so this is much simpler.
    std::optional<Token> extract(TokenType first, TokenType second)
    {
        if (atEnd()) return {};
        const auto& current = m_tokens[m_position];
        if (current.type == first || current.type == second)
        {
            ++m_position;
            return current;
        }
        return {};
    }

    bool isCurrentToken(TokenType type)
    {
        if (atEnd()) return false;
        return m_tokens[m_position].type == type;
    }

    [[nodiscard]] bool atEnd() const noexcept { return m_position >= m_tokens.size(); }

    SourceCodeLocation estimateNextLocation() noexcept
    {
        if (m_tokens.empty()) return {0, 0};
        if (!atEnd()) return m_tokens[m_position].location;
        auto location = m_tokens.back().location;
        location.character += m_tokens.back().value.size();
        return location;
    }


    [[noreturn]] void terminate(std::string message)
    {
        m_result = std::unexpected(std::make_unique<MissingTokenParsingError>(message, estimateNextLocation()));
        throw AbnormalTerminationTag{};
    }

    [[noreturn]] void terminate(Token token, std::string message)
    {
        m_result = std::unexpected(std::make_unique<SingleTokenParsingError>(token, message));
        throw AbnormalTerminationTag{};
    }

    const std::vector<Token>& m_tokens;
    ASTContext& m_context;
    std::size_t m_position;

    ExpressionOrError m_result;
};

std::expected
<
    ProgramSyntaxTree,
    std::unique_ptr<CompilationError>
>
parse(const std::vector<Token>& tokens)
{
    try
    {
        ASTContext context;
        Parser parser(tokens, context);
        return parser.result().transform([&context](auto&& tree)
        {
            return ProgramSyntaxTree{std::move(tree), std::move(context)};
        });
    }
    catch (const std::exception& except)
    {
        using std::string_view_literals::operator""sv;
        return std::unexpected(std::make_unique<UncaughtExceptionError>(except, "parser"sv));
    }

    return std::unexpected(nullptr);
}

} // namespace interpreter
