#include "interpreter/lexer.hpp"

#include <format>
#include <optional>

namespace interpreter
{

class LexingError : public detail::NoexceptCompilationError
{
public:
    LexingError(U64 line, // Line the error starts on.
                U64 charOffset, // Offset into the line.
                U64 length, // Token length
                std::string description)
        : m_description{description}
        , m_range{SourceCodeLocation{line, charOffset},
                  SourceCodeLocation{line, charOffset+length}}
    {}

    std::optional<SourceCodeRange> location() const noexcept override
    {
        return m_range;
    }

    std::string_view getDescription() const override
    {
        return m_description;
    }

    std::string_view compilerPass() const noexcept override
    {
        using std::string_view_literals::operator""sv;
        return "lexer"sv;
    }

private:
    std::string m_description;
    SourceCodeRange m_range;
};

class Lexer
{
public:
    Lexer(const std::string& data)
        : m_data{data}
        , m_lastNewline{0, 0} // We start at line 0, column 0.
        , m_pos{0}
    {}

    char peek() const noexcept { return atEnd() ? '\0' : m_data[m_pos]; }
    bool atEnd() const noexcept { return m_pos >= m_data.size(); }
    void advance() noexcept { ++m_pos; }

    std::optional<Token> nextToken()
    {
        while (std::isspace(peek()))
        {
            if (peek() == '\n')
            {
                ++m_lastNewline.line;
                m_lastNewline.character = m_pos; // We reuse the storage to be
                                                 // able to easily convert a
                                                 // position on this line into
                                                 // a relative offset.
            }
            advance();
        }

        m_currTokenStart = m_pos;
        if (atEnd()) return makeToken(TokenType::Eof);

        switch (auto currChar = peek())
        {
        case '\0':
            return makeError("Unexpected null byte.", 1);
        case '(':
            advance();
            return makeToken(TokenType::LeftParenthesis);
        case ')':
            advance();
            return makeToken(TokenType::RightParenthesis);
        case '[':
            advance();
            return makeToken(TokenType::LeftSquareBracket);
        case ']':
            advance();
            return makeToken(TokenType::RightSquareBracket);
        case '+':
            advance();
            return makeToken(TokenType::Plus);
        case '-':
            advance();
            return makeToken(TokenType::Dash);
        case '*':
            advance();
            return makeToken(TokenType::Star);
        case '/':
            advance();
            return makeToken(TokenType::Slash);
        case '%':
            advance();
            return makeToken(TokenType::Percent);
        case '^':
            advance();
            return makeToken(TokenType::Caret);
        case ',':
            advance();
            return makeToken(TokenType::Comma);
        default:
            if (isDigit(currChar) || currChar == '.')
            {
                return scanNumber();
            }
            else if (isLetter(peek()))
            {
                return scanIdentifier();
            }
            return makeError(std::format("Unexpected character '{}' ({:#04x}).",
                                         currChar,
                                         U32(currChar)), 1);
        }
    }

    // Returns nullptr if there was no error, or it has already been acquired.
    std::unique_ptr<CompilationError> acquireError() noexcept
    {
        return std::move(m_error);
    }

private:
    bool isLetter(char c) const noexcept
    {
        return ('a' <= c && c <= 'z')
            || ('A' <= c && c <= 'Z');
    }
    bool isDigit(char c) const noexcept
    {
        return '0' <= c && c <= '9';
    }

    Token scanNumber()
    {
        while (isDigit(peek())) advance();
        if (peek() == '.') advance();
        while (isDigit(peek())) advance();
        return makeToken(TokenType::Number);
    }
    Token scanIdentifier()
    {
        while (isLetter(peek()) || isDigit(peek())) advance();
        return makeToken(TokenType::Identifier);
    }

    Token makeToken(TokenType type)
    {
        Token tok;
        tok.type = type;
        tok.value = std::string_view(m_data.begin() + m_currTokenStart,
                                     m_data.begin() + m_pos);
        tok.location =
        {
            .line = m_lastNewline.line,
            .character = m_currTokenStart - m_lastNewline.character,
        };
        return tok;
    }

    // Returns nullopt so that it can be returned instead of throwing an
    // exception.
    std::nullopt_t makeError(std::string error, U64 length)
    {
        m_error = std::make_unique<LexingError>
        (
            m_lastNewline.line,
            m_currTokenStart - m_lastNewline.character,
            length,
            std::move(error)
        );
        return std::nullopt;
    }

    std::unique_ptr<CompilationError> m_error;
    const std::string& m_data;
    SourceCodeLocation m_lastNewline;
    std::size_t m_pos;
    std::size_t m_currTokenStart;
};

std::expected
<
    std::vector<Token>,
    std::unique_ptr<CompilationError>
>
tokenise(const std::string& expression)
{
    try
    {
        std::vector<Token> tokens;
        Lexer lexer(expression);

        while (auto token = lexer.nextToken())
        {
            tokens.push_back(*token);
            if (token->type == TokenType::Eof) return tokens;
        }

        return std::unexpected(lexer.acquireError());
    }
    catch (const std::exception& except)
    {
        using std::string_view_literals::operator""sv;
        return std::unexpected(std::make_unique<UncaughtExceptionError>(except, "lexer"sv));
    }
}

} // namespace interpreter
