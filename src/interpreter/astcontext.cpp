#include "interpreter/astcontext.hpp"

namespace interpreter
{

namespace
{

// Makes a transaction where an ID is acquired temporarily from the stack /
// fallback, and *if* the transaction is not committed, this will undo it.
// Observe that nothing may modify the stack until this transaction has been
// committed, and in particular that no other transaction is allowed to run
// concurrently on the same stack.
template <typename IDType>
class AcquireNextIDTransaction
{
public:
    AcquireNextIDTransaction(std::stack<IDType>& stack, IDType fallback)
        : m_stack{stack}
    {
        m_id = stack.empty() ? fallback : m_stack.top();
    }

    [[nodiscard]] operator IDType() const noexcept { return m_id; }
    [[nodiscard]] IDType id() const noexcept { return m_id; }

    void commit()
    {
        if (m_stack.empty()) return;
        if (m_stack.top() == *this) m_stack.pop();
    }

private:
    std::stack<IDType>& m_stack;
    IDType m_id;
};

} // end anonymous namespace

auto ASTContext::getString(StringID id) const -> std::string_view
{
    auto it = m_strings.find(id);
    if (it != m_strings.end()) return it->second;
    return {};
}

auto ASTContext::getID(std::string_view view) -> StringID
{
    auto it = m_viewToID.find(view);
    if (it != m_viewToID.end()) return it->second;

    AcquireNextIDTransaction<uint32_t> newID(m_freeIDs, StringID(m_strings.size()));
    auto newStringIt = m_strings.emplace(newID, view).first;
    try
    {
        m_viewToID.emplace(view, newID);
    }
    catch (...)
    {
        m_strings.erase(newStringIt);
        throw;
    }
    newID.commit();

    return StringID(newID);
}

void ASTContext::erase(StringID id)
{
    m_freeIDs.emplace(id);

    auto view = getString(id);
    m_viewToID.erase(view);
    m_strings.erase(id);
}

auto ASTContext::nextID() noexcept -> StringID
{
    if (m_freeIDs.empty()) return StringID(m_strings.size());
    auto elem = m_freeIDs.top();
    m_freeIDs.pop();
    return StringID(elem);
}

} // namespace interpreter
