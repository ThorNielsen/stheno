#include "interpreter/astoperators.hpp"

namespace interpreter
{

[[nodiscard]] std::unique_ptr<Expression>
operator+(std::unique_ptr<Expression> left,
          std::unique_ptr<Expression> right)
{
    return std::make_unique<BinaryExpression>(BinaryOperationType::Addition,
                                              std::move(left),
                                              std::move(right));
}

[[nodiscard]] std::unique_ptr<Expression>
operator-(std::unique_ptr<Expression> left,
          std::unique_ptr<Expression> right)
{
    return std::make_unique<BinaryExpression>(BinaryOperationType::Subtraction,
                                              std::move(left),
                                              std::move(right));
}

[[nodiscard]] std::unique_ptr<Expression>
operator*(std::unique_ptr<Expression> left,
          std::unique_ptr<Expression> right)
{
    return std::make_unique<BinaryExpression>(BinaryOperationType::Multiplication,
                                              std::move(left),
                                              std::move(right));
}

[[nodiscard]] std::unique_ptr<Expression>
operator/(std::unique_ptr<Expression> left,
          std::unique_ptr<Expression> right)
{
    return std::make_unique<BinaryExpression>(BinaryOperationType::Division,
                                              std::move(left),
                                              std::move(right));
}

[[nodiscard]] std::unique_ptr<Expression>
operator%(std::unique_ptr<Expression> left,
          std::unique_ptr<Expression> right)
{
    return std::make_unique<BinaryExpression>(BinaryOperationType::Modulo,
                                              std::move(left),
                                              std::move(right));
}

[[nodiscard]] std::unique_ptr<Expression>
operator*(double left,
          std::unique_ptr<Expression> right)
{
    return std::make_unique<BinaryExpression>(BinaryOperationType::Multiplication,
                                              std::make_unique<NumberExpression>(left),
                                              std::move(right));
}

[[nodiscard]] std::unique_ptr<Expression>
operator/(double left,
          std::unique_ptr<Expression> right)
{
    return std::make_unique<BinaryExpression>(BinaryOperationType::Division,
                                              std::make_unique<NumberExpression>(left),
                                              std::move(right));
}

[[nodiscard]] std::unique_ptr<Expression>
operator-(std::unique_ptr<Expression> right)
{
    return std::make_unique<UnaryExpression>(UnaryOperationType::UnaryMinus, std::move(right));
}

} // namespace interpreter
