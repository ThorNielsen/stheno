#include "interpreter/error.hpp"

namespace interpreter
{

UncaughtExceptionError::UncaughtExceptionError(const std::exception& exception,
                                               std::string_view pass) noexcept
    : m_pass{pass}
{
    try
    {
        m_description = std::make_unique<std::string>(exception.what());
    }
    catch (...)
    {
        ; // Will be handled later.
    }
}

std::optional<SourceCodeRange>
UncaughtExceptionError::location() const noexcept
{
    return std::nullopt;
}

std::string_view UncaughtExceptionError::description() const noexcept
{
    using std::string_view_literals::operator""sv;
    if (m_description) try
    {
        return *m_description;
    }
    catch (...)
    {
        return "Failed to return description (constructor threw)."sv;
    }

    return "No description available due to exception in descriptor fetch."sv;
}

std::string_view UncaughtExceptionError::compilerPass() const noexcept
{
    using std::string_view_literals::operator""sv;
    return "unknown"sv;
}

namespace detail
{

std::optional<SourceCodeRange> NoexceptCompilationError::location() const noexcept
{
    try
    {
        return getLocation();
    }
    catch (...)
    {
        return std::nullopt;
    }
}

std::string_view NoexceptCompilationError::description() const noexcept
{
    try
    {
        return getDescription();
    }
    catch (...)
    {
        using std::string_view_literals::operator""sv;
        return "Failed to get error description due to exception."sv;
    }
}

std::string_view NoexceptCompilationError::compilerPass() const noexcept
{
    try
    {
        return getCompilerPass();
    }
    catch (...)
    {
        using std::string_view_literals::operator""sv;
        return "(failed to get compiler pass due to exception)"sv;
    }
}

std::optional<SourceCodeRange> NoexceptCompilationError::getLocation() const
{
    return std::nullopt;
}

std::string_view NoexceptCompilationError::getDescription() const
{
    using std::string_view_literals::operator""sv;
    return "getDescription() not overridden, so no error can be returned."sv;
}

std::string_view NoexceptCompilationError::getCompilerPass() const
{
    using std::string_view_literals::operator""sv;
    return "(getCompilerPass() not overridden, so no pass can be identified)"sv;
}

} // namespace detail

InternalCompilerError::InternalCompilerError(std::string_view pass,
                                             std::string&& description,
                                             std::optional<SourceCodeRange> location) noexcept
    : m_location{location}
    , m_description{std::move(description)}
    , m_pass{pass}
{}

[[nodiscard]] std::optional<SourceCodeRange>
InternalCompilerError::location() const noexcept
{
    return m_location;
}

[[nodiscard]] std::string_view InternalCompilerError::description() const noexcept
{
    return m_description;
}

[[nodiscard]] std::string_view
InternalCompilerError::compilerPass() const noexcept
{
    return m_pass;
}

} // namespace interpreter
