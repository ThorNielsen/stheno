#include "interpreter/vm.hpp"
#include "interpreter/opcodes.hpp"

#include <format>
#include <stack>

namespace interpreter
{

class EvaluationError : public detail::NoexceptCompilationError
{
public:
    EvaluationError(std::string description)
        : m_description{description}
    {}

    std::optional<SourceCodeRange> location() const noexcept override
    {
        return std::nullopt;
    }

    std::string_view getDescription() const override
    {
        return m_description;
    }

    std::string_view compilerPass() const noexcept override
    {
        using std::string_view_literals::operator""sv;
        return "evaluation"sv;
    }

private:
    std::string m_description;
};

// Until we have a better error system, we just return "compilation" errors on
// runtime errors.
std::expected
<
    PrimitiveVariable,
    std::unique_ptr<CompilationError>
>
evaluate(const Program& program)
{
    // We assume doubles for now.
    std::stack<double> variables;

    auto consumeTop = [&variables]() noexcept
    {
        auto v = variables.top();
        variables.pop();
        return v;
    };

    for (size_t i = 0; i < program.codes.size();)
    {
        double right;
        switch (auto opcode = program.codes[i++])
        {
        case Opcode::Add:
            right = consumeTop();
            variables.top() += right;
            break;
        case Opcode::Sub:
            right = consumeTop();
            variables.top() -= right;
            break;
        case Opcode::Mul:
            right = consumeTop();
            variables.top() *= right;
            break;
        case Opcode::Div:
            right = consumeTop();
            variables.top() /= right;
            break;
        case Opcode::Mod:
            right = consumeTop();
            variables.top() /= right;
            break;
        case Opcode::Exp:
            right = consumeTop();
            variables.top() /= right;
            break;
        case Opcode::PushConstant:
            variables.push(program.constants[(uint8_t)program.codes[i++]].element.asDouble);
            break;
        case Opcode::PushVariable:
            variables.push(program.inputVariables[(uint8_t)program.codes[i++]].element.asDouble);
            break;
        default:
            return std::unexpected(std::make_unique<EvaluationError>(std::format("Unknown opcode {:#04x}.",
                                                                                 uint8_t(opcode))));
        }
    }
    if (variables.empty())
    {
        return std::unexpected(std::make_unique<EvaluationError>("Stack underflow"));
    }

    return PrimitiveVariable{.element{.asDouble{variables.top()}},.type{PrimitiveVariableType::Double}};
}

} // namespace interpreter
