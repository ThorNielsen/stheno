#include "interpreter/evaluator.hpp"

#include "interpreter/format.hpp"

#include <cmath>
#include <format>

namespace interpreter
{

/// WARNING: We just assume doubles only, since that is what is currently
///          supported when parsing.
using EvalResult = std::expected<double, RuntimeError>;

EvalResult evaluate(const Expression& expression, const EvaluationContext& context);

EvalResult evaluate(const ArrayExpression&, const EvaluationContext&)
{
    return std::unexpected(RuntimeError("Array expression evaluation not yet implemented."));
}

EvalResult evaluate(const BinaryExpression& expression, const EvaluationContext& context)
{
    auto left = evaluate(*expression.left, context);
    if (!left) return left;
    auto right = evaluate(*expression.right, context);
    if (!right) return right;

    switch (expression.op)
    {
    case BinaryOperationType::Addition: return *left + *right;
    case BinaryOperationType::Subtraction: return *left - *right;
    case BinaryOperationType::Multiplication: return *left * *right;
    case BinaryOperationType::Division: return *left / *right;
    case BinaryOperationType::Modulo: return std::fmod(*left, *right);
    case BinaryOperationType::Exponentiation: return std::pow(*left, *right);
    default: return std::unexpected(RuntimeError{std::format("Unknown operation '{}'.",
                                                             expression.op)});
    }
}

EvalResult evaluate(const CallExpression& expression, const EvaluationContext& context)
{
    auto toCall = (*context.astContext)[expression.toCall];

    if (expression.arguments.size() == 1)
    {
        auto arg = evaluate(*expression.arguments[0], context);
        if (!arg) return arg;

        // Todo: Move these out somewhere.
        if (toCall == "sin") return std::sin(*arg);
        if (toCall == "cos") return std::cos(*arg);
        if (toCall == "tan") return std::tan(*arg);
        if (toCall == "asin" || toCall == "arcsin") return std::asin(*arg);
        if (toCall == "acos" || toCall == "arccos") return std::acos(*arg);
        if (toCall == "atan" || toCall == "arctan") return std::atan(*arg);
        if (toCall == "abs") return std::abs(*arg);
        if (toCall == "sgn" || toCall == "")
        {
            return *arg ? (*arg > 0 ? 1 : -1) : 0;
        }
    }
    return std::unexpected(RuntimeError{std::format("Unknown/unevaluable function {}.", toCall)});
}

EvalResult evaluate(const IdentifierExpression& expression, const EvaluationContext& context)
{
    auto toFind = std::string(context.astContext->getString(expression.name));
    auto it = context.bindings.find(toFind);
    if (it == context.bindings.end())
    {
        return std::unexpected(RuntimeError{std::format("Failed to find {} in bindings.", toFind)});
    }
    if (it->second.type != PrimitiveVariableType::Double)
    {
        return std::unexpected(RuntimeError("Evaluation currently only supports doubles."));
    }
    return it->second.element.asDouble;
}

EvalResult evaluate(const NumberExpression& expression, const EvaluationContext&)
{
    return expression.value;
}

EvalResult evaluate(const UnaryExpression& expression, const EvaluationContext& context)
{
    auto result = evaluate(*expression.right, context);
    if (expression.op == UnaryOperationType::UnaryMinus)
    {
        result.transform([](auto x){return -x;});
    }
    return result;
}

EvalResult evaluate(const Expression& expression, const EvaluationContext& context)
{
    switch (expression.type())
    {
    case ExpressionType::Array: return evaluate(static_cast<const ArrayExpression&>(expression), context);
    case ExpressionType::Binary: return evaluate(static_cast<const BinaryExpression&>(expression), context);
    case ExpressionType::Call: return evaluate(static_cast<const CallExpression&>(expression), context);
    case ExpressionType::Identifier: return evaluate(static_cast<const IdentifierExpression&>(expression), context);
    case ExpressionType::Number: return evaluate(static_cast<const NumberExpression&>(expression), context);
    case ExpressionType::Unary: return evaluate(static_cast<const UnaryExpression&>(expression), context);
    default:
        return std::unexpected(RuntimeError("Unexpected expression type."));
    }
}


EvaulationResultOrRuntimeError
evaluateExpression(const Expression& expression, const EvaluationContext& context)
{
    return evaluate(expression, context)
        .transform([](double value) noexcept -> PrimitiveVariable
    {
        return {.element{.asDouble=value},.type{PrimitiveVariableType::Double}};
    });
}

} // namespace interpreter
