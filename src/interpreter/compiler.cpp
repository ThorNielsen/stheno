#include "interpreter/compiler.hpp"

#include "interpreter/ast.hpp"
#include "interpreter/astcontext.hpp"

namespace interpreter
{

struct CompilationContext
{
    Program program;

    void emitOpcode(Opcode code)
    {
        program.codes.push_back(code);
    }

    void emitConstant(double constant)
    {
        emitOpcode(Opcode::PushConstant);
        emitOpcode(static_cast<Opcode>(program.constants.size()));
        program.constants.push_back(PrimitiveVariable{.element{.asDouble{constant}},.type{PrimitiveVariableType::Double}});
    }
    void emitVariable(ASTContext::StringID id)
    {
        emitOpcode(Opcode::PushVariable);
        emitOpcode(static_cast<Opcode>(id.id));
        while (program.inputVariables.size() <= id)
        {
            program.inputVariables.emplace_back();
        }
    }
};

// The top-level class should be renamed.
class ActualCompilationError : public detail::NoexceptCompilationError
{
public:
    ActualCompilationError(std::string description)
        : m_description{description}
    {}

    std::optional<SourceCodeRange> location() const noexcept override
    {
        return std::nullopt;
    }

    std::string_view getDescription() const override
    {
        return m_description;
    }

    std::string_view compilerPass() const noexcept override
    {
        using std::string_view_literals::operator""sv;
        return "compilation"sv;
    }

private:
    std::string m_description;
};

[[noreturn]] void terminateCompilation(std::string message)
{
    throw std::make_unique<ActualCompilationError>(message);
}

void compile(const Expression& expr, CompilationContext& context);

void compile(const BinaryExpression& expr, CompilationContext& context)
{
    compile(*expr.left, context);
    compile(*expr.right, context);
    if (expr.op == BinaryOperationType::Addition) context.emitOpcode(Opcode::Add);
    if (expr.op == BinaryOperationType::Subtraction) context.emitOpcode(Opcode::Sub);
    if (expr.op == BinaryOperationType::Multiplication) context.emitOpcode(Opcode::Mul);
    if (expr.op == BinaryOperationType::Division) context.emitOpcode(Opcode::Div);
    if (expr.op == BinaryOperationType::Modulo) context.emitOpcode(Opcode::Mod);
    if (expr.op == BinaryOperationType::Exponentiation) context.emitOpcode(Opcode::Exp);
}

void compile(const IdentifierExpression& expr, CompilationContext& context)
{
    context.emitVariable(expr.name);
}

void compile(const NumberExpression& expr, CompilationContext& context)
{
    context.emitConstant(expr.value);
}


void compile(const UnaryExpression& expr, CompilationContext& context)
{
    compile(*expr.right, context);
    if (expr.op == UnaryOperationType::UnaryPlus);
    else if (expr.op == UnaryOperationType::UnaryMinus)
    {
        context.emitConstant(-1.);
        context.emitOpcode(Opcode::Mul);
    }
    else terminateCompilation("Unrecognised unary operation type.");
}

void compile(const Expression& expr, CompilationContext& context)
{
    switch (expr.type())
    {
    case ExpressionType::Binary:
        compile(static_cast<const BinaryExpression&>(expr), context);
        break;
    case ExpressionType::Identifier:
        compile(static_cast<const IdentifierExpression&>(expr), context);
        break;
    case ExpressionType::Number:
        compile(static_cast<const NumberExpression&>(expr), context);
        break;
    case ExpressionType::Unary:
        compile(static_cast<const UnaryExpression&>(expr), context);
        break;
    default:
        throw std::logic_error(std::format("{}(): Invalid / unimplemented expression type.",
                                           __func__));
    }
}
std::expected
<
    CompiledProgram,
    std::unique_ptr<CompilationError>
>
compile(const Expression& expression)
{
    try
    {
        CompilationContext context;
        compile(expression, context);
        return CompiledProgram
        {
            .program{std::move(context.program)},
            .inputNames{}
        };
    }
    catch (std::unique_ptr<CompilationError>& ace)
    {
        return std::unexpected(std::move(ace));
    }
}

} // namespace interpreter
