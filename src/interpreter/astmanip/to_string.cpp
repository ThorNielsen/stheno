#include "interpreter/astmanip/to_string.hpp"
#include "interpreter/format.hpp"
#include "interpreter/programsyntaxtree.hpp"

#include <cmath>
#include <sstream>

namespace interpreter
{

[[nodiscard]] size_t precedence(const Expression& expr)
{
    switch (expr.type())
    {
    case ExpressionType::Binary:
        switch (expr.as<BinaryExpression>().op)
        {
        case BinaryOperationType::Addition:
        case BinaryOperationType::Subtraction:
            return 0;
        case BinaryOperationType::Multiplication:
        case BinaryOperationType::Division:
        case BinaryOperationType::Modulo:
            return 1;
        case BinaryOperationType::Exponentiation:
            return 2;
        default:
            return std::numeric_limits<size_t>::max();
        }
    case ExpressionType::Unary:
        return 3;
    case ExpressionType::Call:
        return 4;
    case ExpressionType::Identifier:
    case ExpressionType::Number:
    case ExpressionType::Array:
        return 5;
    default:
        return std::numeric_limits<size_t>::max();
    }
}

void to_string(const Expression& expr,
               const ASTContext& context,
               std::stringstream& stream);

void to_string(const ArrayExpression& expr,
               const ASTContext& context,
               std::stringstream& stream)
{
    stream << "[";
    for (std::size_t index = 0; index < expr.elements.size(); ++index)
    {
        to_string(*expr.elements[index], context, stream);
        if (index + 1 < expr.elements.size())
        {
            stream << ", ";
        }
    }
    stream << "]";
}

void to_string(const BinaryExpression& expr,
               const ASTContext& context,
               std::stringstream& stream)
{
    auto parenthesiseLeft = precedence(*expr.left) < precedence(expr);
    auto parenthesiseRight = precedence(*expr.right) < precedence(expr);
    if (parenthesiseLeft) stream << '(';
    to_string(*expr.left, context, stream);
    if (parenthesiseLeft) stream << ')';

    stream << to_string(expr.op);

    if (parenthesiseRight) stream << '(';
    to_string(*expr.right, context, stream);
    if (parenthesiseRight) stream << ')';
}

void to_string(const CallExpression& expr,
               const ASTContext& context,
               std::stringstream& stream)
{
    stream << context.getString(expr.toCall);
    stream << "(";
    for (std::size_t index = 0; index < expr.arguments.size(); ++index)
    {
        to_string(*expr.arguments[index], context, stream);
        if (index + 1 < expr.arguments.size())
        {
            stream << ", ";
        }
    }
    stream << ")";
}

void to_string(const IdentifierExpression& expr,
               const ASTContext& context,
               std::stringstream& stream)
{
    stream << context.getString(expr.name);
}

void to_string(const NumberExpression& expr,
               const ASTContext&,
               std::stringstream& stream)
{
    stream << expr.value;
}

void to_string(const UnaryExpression& expr,
               const ASTContext& context,
               std::stringstream& stream)
{
    bool parenthesise = precedence(*expr.right) < precedence(expr);
    stream << to_string(expr.op);
    if (parenthesise) stream << '(';
    to_string(*expr.right, context, stream);
    if (parenthesise) stream << ')';
}

void to_string(const Expression& expr,
               const ASTContext& context,
               std::stringstream& stream)
{
    switch (expr.type())
    {
    case ExpressionType::Array:
        to_string(static_cast<const ArrayExpression&>(expr), context, stream);
        break;
    case ExpressionType::Binary:
        to_string(static_cast<const BinaryExpression&>(expr), context, stream);
        break;
    case ExpressionType::Call:
        to_string(static_cast<const CallExpression&>(expr), context, stream);
        break;
    case ExpressionType::Identifier:
        to_string(static_cast<const IdentifierExpression&>(expr), context, stream);
        break;
    case ExpressionType::Number:
        to_string(static_cast<const NumberExpression&>(expr), context, stream);
        break;
    case ExpressionType::Unary:
        to_string(static_cast<const UnaryExpression&>(expr), context, stream);
        break;
    default:
        throw std::logic_error(std::format("{}(): Invalid / unimplemented expression type.",
                                           __func__));
    }
}

std::string to_string(const Expression& expr,
                      const ASTContext& context)
{
    std::stringstream stream;
    to_string(expr, context, stream);
    return stream.str();
}

std::string to_string(const ProgramSyntaxTree& pst)
{
    return to_string(*pst.expression, pst.context);
}

} // namespace interpreter
