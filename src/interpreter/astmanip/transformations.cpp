#include "interpreter/astmanip/transformations.hpp"

#include "interpreter/astmanip/differentiation.hpp"
#include "interpreter/astmanip/simplification.hpp"

namespace interpreter
{

void findIdentifiers(const Expression& expr,
                     std::set<ASTContext::StringID>& addTo)
{
    switch (expr.type())
    {
    case ExpressionType::Array:
        for (const auto& elem : expr.as<ArrayExpression>().elements)
        {
            findIdentifiers(*elem, addTo);
        }
        break;
    case ExpressionType::Binary:
        findIdentifiers(*expr.as<BinaryExpression>().left, addTo);
        findIdentifiers(*expr.as<BinaryExpression>().right, addTo);
        break;
    case ExpressionType::Call:
        // We *don't* add the function name as an identifier YET.
        // Because e.g. metafunctions might also be seen as identifiers, then,
        // which would be a bit weird.
        for (const auto& elem : expr.as<CallExpression>().arguments)
        {
            findIdentifiers(*elem, addTo);
        }
        break;
    case ExpressionType::Identifier:
        addTo.insert(expr.as<IdentifierExpression>().name);
        break;
    case ExpressionType::Number:
        break;
    case ExpressionType::Unary:
        findIdentifiers(*expr.as<UnaryExpression>().right, addTo);
        break;
    }
}

std::set<ASTContext::StringID> findIdentifiers(const Expression& expr)
{
    std::set<ASTContext::StringID> identifiers;
    findIdentifiers(expr, identifiers);
    return identifiers;
}

std::unique_ptr<Expression>
applyMetafunction(std::unique_ptr<CallExpression> expr,
                  ASTContext& context)
{
    if (context.getString(expr->toCall) == "diff")
    {
        if (expr->arguments.size() != 2)
        {
            throw std::runtime_error("Differentiation must have two arguments: "
                                     "The expression and the variable(s).");
        }
        if (expr->arguments[1]->type() == ExpressionType::Array)
        {
            for (auto&& arg : expr->arguments[1]->as<ArrayExpression>().elements)
            {
                if (arg->type() != ExpressionType::Identifier)
                {
                    throw std::runtime_error("Expected array to consist of identifiers only.");
                }
                arg = simplify(computeDerivative(simplify(expr->arguments[0]->copy(), context),
                                                 context,
                                                 arg->as<IdentifierExpression>().name).value(),
                               context);
            }
        }
        else if (expr->arguments[1]->type() == ExpressionType::Identifier)
        {
            expr->arguments[1] = simplify(computeDerivative(simplify(std::move(expr->arguments[0]), context),
                                                            context,
                                                            expr->arguments[1]->as<IdentifierExpression>().name).value(),
                                          context);
        }
        else throw std::runtime_error("Expected differentiation to be wrt. identifier (optionally array of identifiers).");
        return std::move(expr->arguments[1]);
    }
    else if (context.getString(expr->toCall) == "identifiers")
    {
        // One could conceivably make an array of arrays if there are more
        // arguments, but right now we just return *all* identifiers merged.
        std::vector<std::unique_ptr<Expression>> values;
        for (auto id : findIdentifiers(*expr))
        {
            values.emplace_back(std::make_unique<IdentifierExpression>(id));
        }
        return std::make_unique<ArrayExpression>(std::move(values));
    }
    else if (context.getString(expr->toCall) == "gradient")
    {
        auto identifiers = findIdentifiers(*expr);
        for (auto& arg : expr->arguments)
        {
            std::vector<std::unique_ptr<Expression>> values;
            values.reserve(identifiers.size());
            for (auto id : identifiers)
            {
                values.push_back(simplify(computeDerivative(simplify(arg->copy(),
                                                                     context),
                                                            context,
                                                            id).value(),
                                          context));
            }
            arg = std::make_unique<ArrayExpression>(std::move(values));
        }
        if (expr->arguments.size() == 1) return std::move(expr->arguments[0]);
        else return std::make_unique<ArrayExpression>(std::move(expr->arguments));
    }
    return expr;
}

} // namespace interpreter
