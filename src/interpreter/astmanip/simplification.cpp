#include "interpreter/astmanip/simplification.hpp"

#include "interpreter/astmanip/transformations.hpp"
#include "interpreter/astoperators.hpp"
#include "interpreter/format.hpp"

#include <cmath>

namespace interpreter
{

std::unique_ptr<Expression>
simplify(std::unique_ptr<ArrayExpression> expr,
         ASTContext& context)
{
    for (auto&& elem : expr->elements)
    {
        elem = simplify(std::move(elem), context);
    }
    return expr;
}

std::unique_ptr<Expression>
simplify(std::unique_ptr<BinaryExpression> expr,
         ASTContext& context)
{
    const double dummyVal = 666.;
    expr->left = simplify(std::move(expr->left), context);
    expr->right = simplify(std::move(expr->right), context);

    std::optional<double> leftVal, rightVal;

    if (expr->left->type() == ExpressionType::Number)
    {
        leftVal = expr->left->as<NumberExpression>().value;
    }
    if (expr->right->type() == ExpressionType::Number)
    {
        rightVal = expr->right->as<NumberExpression>().value;
    }

    if (leftVal && rightVal)
    {
        double result = *leftVal;
        switch (expr->op)
        {
        case BinaryOperationType::Addition: result += *rightVal; break;
        case BinaryOperationType::Subtraction: result -= *rightVal; break;
        case BinaryOperationType::Multiplication: result *= *rightVal; break;
        case BinaryOperationType::Division: result /= *rightVal; break;
        case BinaryOperationType::Modulo: result = std::fmod(result, *rightVal); break;
        case BinaryOperationType::Exponentiation: result = std::pow(result, *rightVal); break;
        default:
            throw std::logic_error(std::format("Invalid token type '{}'.", expr->op));
        }
        return std::make_unique<NumberExpression>(result);
    }

    if (expr->op == BinaryOperationType::Addition)
    {
        if (leftVal.value_or(dummyVal) == 0.) return std::move(expr->right);
        if (rightVal.value_or(dummyVal) == 0.) return std::move(expr->left);

        if (expr->left->isEqual(*expr->right))
        {
            return 2 * std::move(expr->left);
        }
    }
    if (expr->op == BinaryOperationType::Subtraction)
    {
        if (leftVal.value_or(dummyVal) == 0.)
        {
            return -std::move(expr->right);
        }
        if (rightVal.value_or(dummyVal) == 0.)
        {
            return std::move(expr->left);
        }
    }
    if (expr->op == BinaryOperationType::Multiplication)
    {
        if (leftVal.value_or(dummyVal) == 0.
            || rightVal.value_or(dummyVal) == 0.)
        {
            return std::make_unique<NumberExpression>(0.);
        }
        if (leftVal.value_or(dummyVal) == 1.) return std::move(expr->right);
        if (rightVal.value_or(dummyVal) == 1.) return std::move(expr->left);
        if (leftVal.value_or(dummyVal) == -1.) return -std::move(expr->right);
        if (rightVal.value_or(dummyVal) == -1.) return -std::move(expr->left);
    }
    if (expr->op == BinaryOperationType::Division)
    {
        if (rightVal.value_or(dummyVal) == 1.) return std::move(expr->left);
        if (rightVal.value_or(dummyVal) == -1.) return -std::move(expr->left);
    }
    if (expr->op == BinaryOperationType::Exponentiation)
    {
        if (rightVal.value_or(dummyVal) == 1.) return std::move(expr->left);
    }

    return expr;
}

std::unique_ptr<Expression>
simplify(std::unique_ptr<CallExpression> expr,
         ASTContext& context)
{
    auto metaFunction = applyMetafunction(std::move(expr), context);
    if (metaFunction->type() != ExpressionType::Call) return metaFunction;
    else expr = downcastNode<CallExpression>(std::move(metaFunction));
    for (auto& arg : expr->arguments)
    {
        arg = simplify(std::move(arg), context);
    }
    return expr;
}

std::unique_ptr<Expression>
simplify(std::unique_ptr<IdentifierExpression> expr,
         ASTContext&)
{
    return expr;
}

std::unique_ptr<Expression>
simplify(std::unique_ptr<NumberExpression> expr,
         ASTContext&)
{
    return expr;
}

std::unique_ptr<Expression>
simplify(std::unique_ptr<UnaryExpression> expr,
         ASTContext& context)
{
    return std::make_unique<UnaryExpression>(expr->op,
                                             simplify(std::move(expr->right),
                                                      context));
}

std::unique_ptr<Expression>
simplify(std::unique_ptr<Expression> expr,
         ASTContext& context)
{
    switch (expr->type())
    {
    case ExpressionType::Array:
        return simplify(downcastNode<ArrayExpression>(std::move(expr)), context);
    case ExpressionType::Binary:
        return simplify(downcastNode<BinaryExpression>(std::move(expr)), context);
    case ExpressionType::Call:
        return simplify(downcastNode<CallExpression>(std::move(expr)), context);
    case ExpressionType::Identifier:
        return simplify(downcastNode<IdentifierExpression>(std::move(expr)), context);
    case ExpressionType::Number:
        return simplify(downcastNode<NumberExpression>(std::move(expr)), context);
    case ExpressionType::Unary:
        return simplify(downcastNode<UnaryExpression>(std::move(expr)), context);
    default:
        throw std::logic_error(std::format("{}(): Invalid / unimplemented expression type.",
                                           __func__));
    }
}

ProgramSyntaxTree simplify(ProgramSyntaxTree&& pst)
{
    pst.expression = simplify(std::move(pst.expression), pst.context);
    return pst;
}

} // namespace interpreter
