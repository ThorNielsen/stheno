#include "interpreter/astmanip/differentiation.hpp"
#include "interpreter/astmanip/simplification.hpp"
#include "interpreter/astmanip/transformations.hpp"
#include "interpreter/astoperators.hpp"
#include "interpreter/evaluator.hpp"
#include "interpreter/format.hpp"

#include <cmath>
#include <sstream>

namespace interpreter
{

namespace detail
{

class MathError : public CompilationError
{
public:
    MathError(std::string_view pass,
              std::string&& description,
              std::optional<SourceCodeRange> location = std::nullopt) noexcept
        : m_location{location}
        , m_description{std::move(description)}
        , m_pass{pass}
    {}

    [[nodiscard]] std::optional<SourceCodeRange> location() const noexcept override
    {
        return m_location;
    }

    [[nodiscard]] std::string_view description() const noexcept override
    {
        return m_description;
    }

    [[nodiscard]] std::string_view compilerPass() const noexcept override
    {
        return m_pass;
    }

private:
    std::optional<SourceCodeRange> m_location;
    std::string m_description;
    std::string_view m_pass;
};

static auto mathError(std::string msg)
{
    return std::unexpected(std::make_unique<MathError>(msg, "differentiation"));
}

static auto unimplementedError(std::string msg)
{
    return std::unexpected(std::make_unique<InternalCompilerError>(msg, "differentiation"));
}

using ExpressionOrValue =
std::variant
<
    std::unique_ptr<Expression>,
    double // TODO: Support different value types like uint and *maybe* vector?
>;

[[nodiscard]] auto* getExpression(ExpressionOrValue& expr) noexcept
{
    return std::get_if<std::unique_ptr<Expression>>(&expr);
}

[[nodiscard]] auto* getValue(ExpressionOrValue& expr) noexcept
{
    return std::get_if<double>(&expr);
}

[[nodiscard]] constexpr bool isExpression(ExpressionOrValue& eov) noexcept
{
    return eov.index() == 0;
}

[[nodiscard]] constexpr bool isNumber(ExpressionOrValue& eov) noexcept
{
    return eov.index() == 1;
}

// Assumes the type has already been checked.
[[nodiscard]] auto asExpression(ExpressionOrValue&& expr) noexcept
{
    return std::move(*std::get_if<std::unique_ptr<Expression>>(&expr));
}

[[nodiscard]] auto asNumber(const ExpressionOrValue& num) noexcept
{
    return *std::get_if<double>(&num);
}

// Assumes not valueless
[[nodiscard]] std::unique_ptr<Expression> asNode(ExpressionOrValue&& eov) noexcept
{
    if (isNumber(eov)) return std::make_unique<NumberExpression>(asNumber(eov));
    return asExpression(std::move(eov));
}

// Returns ownership the argument of the expression
[[nodiscard]] std::unique_ptr<Expression>
extractUnaryMinusArgument(std::unique_ptr<Expression>& expr) noexcept
{
    if (expr->type() == ExpressionType::Unary
        && expr->as<UnaryExpression>().op == UnaryOperationType::UnaryMinus)
    {
        return std::move(expr->as<UnaryExpression>().right);
    }
    return nullptr;
}

// As above, but assumes ExpressionOrValue contains an expression.
[[nodiscard]] std::unique_ptr<Expression>
extractUnaryMinusArgument(ExpressionOrValue& expr) noexcept
{
    return extractUnaryMinusArgument(*getExpression(expr));
}

enum class TypeCombination
{
    ExprExpr     = 0b00,
    ExprNumber   = 0b01,
    NumberExpr   = 0b10,
    NumberNumber = 0b11,
};

TypeCombination typeCombination(const ExpressionOrValue& a,
                                const ExpressionOrValue& b)
{
    return static_cast<TypeCombination>((a.index() << 1) | b.index());
}


// Returns {inner expression, number of unary minuses}
[[nodiscard]] std::pair<std::unique_ptr<Expression>, std::size_t>
leftTrimUnaryPlusMinus(std::unique_ptr<UnaryExpression> unary) noexcept
{
    std::size_t unaryMinusCount = 0;
    while (unary->right->type() == ExpressionType::Unary)
    {
        if (unary->op != UnaryOperationType::UnaryPlus
            && unary->op != UnaryOperationType::UnaryMinus)
        {
            return {std::move(unary), unaryMinusCount};
        }
        unaryMinusCount += unary->op == UnaryOperationType::UnaryMinus;
        unary = downcastNode<UnaryExpression>(std::move(unary->right));
    }

    return {std::move(unary->right),
            unaryMinusCount += unary->op == UnaryOperationType::UnaryMinus};
}

[[nodiscard]] std::pair<std::unique_ptr<Expression>, std::size_t>
leftTrimUnaryPlusMinus(std::unique_ptr<Expression> unary) noexcept
{
    if (unary->type() == ExpressionType::Unary)
    {
        return leftTrimUnaryPlusMinus(downcastNode<UnaryExpression>(std::move(unary)));
    }
    return {std::move(unary), 0};
}

[[nodiscard]] ExpressionOrValue operator-(ExpressionOrValue exprVal)
{
    if (isNumber(exprVal)) return -asNumber(exprVal);
    auto [expr, unaryMinusCount] = leftTrimUnaryPlusMinus(asExpression(std::move(exprVal)));
    if (unaryMinusCount & 1) return std::make_unique<UnaryExpression>(UnaryOperationType::UnaryMinus,
                                                                      std::move(expr));
    return std::move(expr);
}

[[nodiscard]] ExpressionOrValue operator+(ExpressionOrValue left, ExpressionOrValue right)
{
    switch (typeCombination(left, right))
    {
    case TypeCombination::ExprExpr:
        return asExpression(std::move(left)) + asExpression(std::move(right));

    case TypeCombination::ExprNumber:
        if (asNumber(right) == 0) return left;
        return asExpression(std::move(left)) + asNode(std::move(right));

    case TypeCombination::NumberExpr:
        if (asNumber(left) == 0) return right;
        return asNode(std::move(left)) + asExpression(std::move(right));

    case TypeCombination::NumberNumber:
        return asNumber(left) + asNumber(right);
    }

    return 0.; // Should be unreachable.
}

[[nodiscard]] ExpressionOrValue operator-(ExpressionOrValue left, ExpressionOrValue right)
{
    if (isExpression(right))
    {
        if (auto negated = extractUnaryMinusArgument(right))
        {
            return std::move(left) + ExpressionOrValue(std::move(negated));
        }
    }
    switch (typeCombination(left, right))
    {
    case TypeCombination::ExprExpr:
        return asExpression(std::move(left)) - asExpression(std::move(right));

    case TypeCombination::ExprNumber:
        if (asNumber(right) == 0) return -std::move(right);
        return asExpression(std::move(left)) - asNode(std::move(right));

    case TypeCombination::NumberExpr:
        if (asNumber(left) == 0) return left;
        return asNode(std::move(left)) - asExpression(std::move(right));

    case TypeCombination::NumberNumber:
        return asNumber(left) - asNumber(right);
    }

    return 0.; // Should be unreachable.
}

[[nodiscard]] ExpressionOrValue operator*(ExpressionOrValue left, ExpressionOrValue right)
{
    switch (typeCombination(left, right))
    {
    case TypeCombination::ExprExpr:
        return asExpression(std::move(left)) * asExpression(std::move(right));

    case TypeCombination::ExprNumber:
        {
            auto num = asNumber(right);
            if (num == 1) return left;
            if (num == 0) return 0.;
            if (num == -1) return -std::move(left);
            return std::move(left) * num;
        }
        if (asNumber(left) == 0) return -asExpression(std::move(right));
        return asExpression(std::move(left)) - asNode(std::move(right));

    case TypeCombination::NumberExpr:
        {
            auto num = asNumber(left);
            if (num == 1) return right;
            if (num == 0) return 0.;
            if (num == -1) return -std::move(right);
            if (num < 0) return - ((-num) * std::move(right));
            return num * std::move(right);
        }

    case TypeCombination::NumberNumber:
        return asNumber(left) * asNumber(right);
    }

    return 0.; // Should be unreachable.
}

[[nodiscard]] Expected<ExpressionOrValue> operator/(ExpressionOrValue left, ExpressionOrValue right)
{
    switch (typeCombination(left, right))
    {
    case TypeCombination::ExprExpr:
        return asExpression(std::move(left)) / asExpression(std::move(right));

    case TypeCombination::ExprNumber:
        {
            auto num = asNumber(right);
            if (num == 1) return left;
            if (num == 0) return mathError("Division by zero.");
            if (num == -1) return -std::move(left);
            // Not necessarily strictly equivalent, but we are simplifying
            // expressions here.
            return (1. / num) * std::move(left);
        }

    case TypeCombination::NumberExpr:
        {
            auto num = asNumber(left);
            if (num == 0) return 0.;
            if (num < 0) return - ((-num) / asExpression(std::move(right)));
            return num / std::move(right);
        }

    case TypeCombination::NumberNumber:
        if (asNumber(right) == 0) return mathError("Division by zero.");
        return asNumber(left) * asNumber(right);
    }

    return 0.; // Should be unreachable.
}

[[nodiscard]] std::optional<double> evaluateExpressionIfConstant(const Expression& expr)
{
    EvaluationContext emptyContext;
    ASTContext dummy;
    emptyContext.astContext = &dummy;
    auto eval = evaluateExpression(expr, emptyContext);
    if (eval) return eval->element.asDouble;
    return std::nullopt;
}

[[nodiscard]] ExpressionOrValue expressionOrValue(std::unique_ptr<Expression>&& expr)
{
    if (auto number = evaluateExpressionIfConstant(*expr)) return *number;
    return expr;
}

Expected<ExpressionOrValue>
derivative(Expected<ExpressionOrValue> expr,
           ASTContext& context,
           ASTContext::StringID diffWRT);

Expected<ExpressionOrValue>
derivative(std::unique_ptr<ArrayExpression> arrayExpr,
           ASTContext& context,
           ASTContext::StringID diffWRT)
{
    for (auto& elem : arrayExpr->elements)
    {
        auto diff = derivative(std::move(elem), context, diffWRT);
        if (diff)
        {
            if (isNumber(*diff)) elem = std::make_unique<NumberExpression>(asNumber(*diff));
            else elem = asExpression(std::move(*diff));
        }
        else return diff;
    }
    return arrayExpr;
}

Expected<ExpressionOrValue>
derivative(std::unique_ptr<BinaryExpression> expr,
           ASTContext& context,
           ASTContext::StringID diffWRT)
{
    if (expr->op == BinaryOperationType::Addition
        || expr->op == BinaryOperationType::Subtraction)
    {
        auto leftDeriv = derivative(std::move(expr->left), context, diffWRT);
        if (!leftDeriv) return leftDeriv;
        auto rightDeriv = derivative(std::move(expr->right), context, diffWRT);
        if (!rightDeriv) return rightDeriv;

        if (expr->op == BinaryOperationType::Addition)
        {
            return std::move(*leftDeriv) + std::move(*rightDeriv);
        }
        else return std::move(*leftDeriv) - std::move(*rightDeriv);
    }

    auto leftDeriv = derivative(expr->left->copy(), context, diffWRT);
    if (!leftDeriv) return leftDeriv;
    auto rightDeriv = derivative(expr->right->copy(), context, diffWRT);
    if (!rightDeriv) return rightDeriv;

    if (expr->op == BinaryOperationType::Multiplication)
    {
        return std::move(*leftDeriv) * expressionOrValue(std::move(expr->right))
             + expressionOrValue(std::move(expr->left)) * std::move(*rightDeriv);
    }
    if (expr->op == BinaryOperationType::Division)
    {
        auto left = std::move(*leftDeriv) * expressionOrValue(expr->right->copy());
        auto right = expressionOrValue(expr->left->copy()) * std::move(*rightDeriv);
        return (std::move(left) - std::move(right))
             * std::make_unique<BinaryExpression>(BinaryOperationType::Exponentiation,
                                                  std::move(expr->right), // Not reduced
                                                  std::make_unique<NumberExpression>(-2));
    }
    if (expr->op == BinaryOperationType::Modulo)
    {
        return unimplementedError("Cannot find derivative of modulo expression "
                                  "until control flow is implemented.");
    }
    if (expr->op == BinaryOperationType::Exponentiation)
    {
        auto left = expressionOrValue(std::move(expr->left));
        auto right = expressionOrValue(std::move(expr->right));
        switch (typeCombination(left, right))
        {
        case TypeCombination::ExprExpr:
            return unimplementedError("Cannot compute general derivative yet: "
                                      "logarithm not yet added to interpreter.");

        case TypeCombination::ExprNumber:
            {
                auto exponent = asNumber(right);
                if (exponent == 0.) return 1.;
                if (exponent == 1.)
                {
                    return derivative(asExpression(std::move(left)), context, diffWRT);
                }
                if (exponent == -1.)
                {
                    return unimplementedError("Cannot compute derivative of 1/x"
                                              "-style expressions because the "
                                              "logarithm has yet to be added.");
                }
                auto leftDeriv = derivative((*getExpression(left))->copy(), context, diffWRT);
                if (!leftDeriv) return leftDeriv;

                auto prod = exponent * asExpression(std::move(left));
                if (!prod) return prod;

                auto resultNode = std::make_unique<BinaryExpression>(BinaryOperationType::Exponentiation,
                                                                     std::move(prod),
                                                                     std::make_unique<NumberExpression>(exponent - 1.));
                if (isNumber(*leftDeriv))
                {
                    return asNumber(*leftDeriv) * std::move(resultNode);
                }
                else return asExpression(std::move(*leftDeriv)) * std::move(resultNode);
            }

        case TypeCombination::NumberExpr:
            {
                auto base = asNumber(left);
                if (base == 0 || base == 1.) return 0.;
                if (base < 0.)
                {
                    return mathError("No real derivative exists of x ↦ c^x, for"
                                     " c < 0.");
                }

                auto rightDeriv = derivative((*getExpression(right))->copy(),
                                             context,
                                             diffWRT);
                if (!rightDeriv) return rightDeriv;
                auto result = std::make_unique<BinaryExpression>(expr->op,
                                                                 std::make_unique<NumberExpression>(base),
                                                                 asExpression(std::move(right)));

                return (std::log(base) * std::move(*rightDeriv))
                     * std::move(result);
            }

        case TypeCombination::NumberNumber:
            return 0.;
        }
    }
    throw std::runtime_error(std::format("Cannot compute derivative of binary operator '{}'.",
                                         expr->op));
}

Expected<ExpressionOrValue>
derivative(std::unique_ptr<CallExpression> call,
           ASTContext& context,
           ASTContext::StringID diffWRT)
{
    auto metaFunction = applyMetafunction(std::move(call), context);
    if (metaFunction->type() != ExpressionType::Call)
    {
        return derivative(std::move(metaFunction), context, diffWRT);
    }
    else call = downcastNode<CallExpression>(std::move(metaFunction));
    auto funcName = context.getString(call->toCall);
    if (funcName == "diff")
    {
        if (call->arguments.size() != 2)
        {
            throw std::runtime_error("Differentiation must have two arguments: "
                                     "The expression and the variable(s).");
        }
        if (call->arguments[1]->type() == ExpressionType::Array)
        {
            for (auto&& arg : call->arguments[1]->as<ArrayExpression>().elements)
            {
                if (arg->type() != ExpressionType::Identifier)
                {
                    throw std::runtime_error("Expected array to consist of identifiers only.");
                }
                auto deriv = derivative(call->arguments[0]->copy(), context, arg->as<IdentifierExpression>().name);
                if (!deriv) return deriv;
                arg = asNode(std::move(*deriv));
            }
        }
        else if (call->arguments[1]->type() == ExpressionType::Identifier)
        {
            auto deriv = derivative(std::move(call->arguments[0]),
                                    context,
                                    call->arguments[1]->as<IdentifierExpression>().name);
            if (!deriv) return deriv;
            call->arguments[1] = asNode(std::move(*deriv));
        }
        else throw std::runtime_error("Expected differentiation to be wrt. identifier (optionally array of identifiers).");
        return std::move(call->arguments[1]);
    }
    if (funcName == "sign")
    {
        return std::make_unique<NumberExpression>(0);
    }

    if (call->arguments.size() != 1)
    {
        return unimplementedError("Only unary built-in functions are supported for derivatives for the moment.");
    }

    auto argDeriv = computeDerivative(call->arguments[0]->copy(), context, diffWRT);
    if (!argDeriv) return argDeriv;

    if (funcName == "abs")
    {
        call->toCall = context.getID("sign");
        return std::move(call) * std::move(*argDeriv);
    }
    else if (funcName == "sin")
    {
        call->toCall = context.getID("cos");
        return std::move(call) * std::move(*argDeriv);
    }
    else if (funcName == "cos")
    {
        call->toCall = context.getID("sin");
        return - (std::move(call) * std::move(*argDeriv));
    }
    else if (funcName == "tan")
    {
        auto tanSquared = std::make_unique<BinaryExpression>(BinaryOperationType::Exponentiation,
                                                             std::move(call),
                                                             std::make_unique<NumberExpression>(2));

        auto onePlusTanSquared = std::make_unique<BinaryExpression>(BinaryOperationType::Addition,
                                                                    std::make_unique<NumberExpression>(1.),
                                                                    std::move(tanSquared));

        return std::make_unique<BinaryExpression>(BinaryOperationType::Multiplication,
                                                  std::move(onePlusTanSquared),
                                                  std::move(*argDeriv));
    }

    return unimplementedError(std::format("Cannot take derivative of {}: Not yet defined.", funcName));
}

Expected<ExpressionOrValue>
derivative(std::unique_ptr<UnaryExpression> unary,
           ASTContext& context,
           ASTContext::StringID diffWRT)
{
    auto [expr, unaryMinusCount] = leftTrimUnaryPlusMinus(std::move(unary));
    if (expr->type() == ExpressionType::Unary)
    {
        return unimplementedError("Encountered unhandled unary operation.");
    }

    auto diff = derivative(std::move(expr), context, diffWRT);
    if (!diff) return diff;
    return (unaryMinusCount & 1) ? -std::move(*diff) : std::move(diff);
}

Expected<ExpressionOrValue>
derivative(Expected<ExpressionOrValue> expr,
           ASTContext& context,
           ASTContext::StringID diffWRT)
{
    if (expr)
    {
        if (isExpression(*expr))
        {
            auto astNode = asExpression(std::move(*expr));
            switch (astNode->type())
            {
            case ExpressionType::Array:
                return derivative(downcastNode<ArrayExpression>(std::move(astNode)), context, diffWRT);
            case ExpressionType::Binary:
                return derivative(downcastNode<BinaryExpression>(std::move(astNode)), context, diffWRT);
            case ExpressionType::Call:
                return derivative(downcastNode<CallExpression>(std::move(astNode)), context, diffWRT);
            case ExpressionType::Identifier:
                return astNode->as<IdentifierExpression>().name == diffWRT ? 1. : 0.;
            case ExpressionType::Number:
                return 0.;
            case ExpressionType::Unary:
                return derivative(downcastNode<UnaryExpression>(std::move(astNode)), context, diffWRT);
            default:
                return unimplementedError("Encountered unknown/unhandled node type.");
            }
        }
        else if (isNumber(*expr))
        {
            // The derivative of a value is just zero.
            return 0.;
        }
    }
    return std::unexpected(std::move(expr.error()));
}

} // namespace detail

ExpectedPointer<Expression>
computeDerivative(std::unique_ptr<Expression> expr,
                  ASTContext& context,
                  ASTContext::StringID diffWRT)
{
    auto result = detail::derivative(std::move(expr), context, diffWRT);
    if (!result) return std::unexpected(std::move(result.error()));
    else if (detail::isExpression(*result)) return detail::asExpression(std::move(*result));
    else if (detail::isNumber(*result)) return std::make_unique<NumberExpression>(detail::asNumber(*result));
    else throw std::logic_error("Should be impossible: All cases covered above.");
}

} // namespace interpreter
