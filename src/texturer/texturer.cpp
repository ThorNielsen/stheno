#include "texturer/texturer.hpp"

#include <boost/program_options.hpp>
#include <cstring>
#include <fstream>

#include "vulkan/allocator.hpp"
#include "vulkan/device.hpp"
#include "vulkan/framegraph.hpp"

#include <kraken/graphics/export.hpp>

static VKAPI_ATTR VkBool32 VKAPI_CALL
debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT severity,
              VkDebugUtilsMessageTypeFlagsEXT,
              const VkDebugUtilsMessengerCallbackDataEXT* callbackData,
              void*)
{
    if (severity > VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
    {
        std::cerr << callbackData->pMessage << "\n";
        return VK_FALSE;
    }
    return VK_FALSE;
}

std::string progressbar(float progress, size_t maxlen,
                        std::string before = "", std::string after = "",
                        bool useColours = false)
{
    constexpr const char* progressSymbol[] =
    {
        " ", "▏", "▎", "▍", "▌", "▋", "▊", "▉", "█"
    };

    constexpr const char* colour[] =
    {
        "\033[31m", // red
        "\033[33m", // yellow
        "\033[32m"  // green
    };
    size_t percentage = std::max(0.f, progress * 100.f + 0.5f);
    if (maxlen >= 9) maxlen -= 9;
    if (before.size() && maxlen >= 2)
    {
        maxlen -= (maxlen < before.size() + 2) ? maxlen : before.size() + 2;
    }
    if (after.size() && maxlen >= 1)
    {
        maxlen -= (maxlen < after.size() + 1) ? maxlen : after.size() + 1;
    }
    if (!maxlen) ++maxlen;
    size_t len = progress * 8 * maxlen;
    size_t filled = len / 8;

    if (filled >= maxlen)
    {
        --filled;
    }

    std::ostringstream s;
    if (before.size())
    {
        s << before << ": ";
    }
    s << "[";
    if (useColours)
    {
        s << colour[std::max(std::min((int)(progress*3.0f), 2), 0)];
    }
    for (size_t i = 0; i < filled; ++i)
    {
        s << progressSymbol[8];
    }
    s << ((percentage == 100) ? progressSymbol[8] : progressSymbol[len%8]);

    for (size_t i = filled+1; i < maxlen; ++i)
    {
        s << " ";
    }
    if (useColours)
    {
        s << "\033[0m";
    }
    s << "] (" << std::setw(3) << percentage << "%)";
    if (after.size())
    {
        s << " " << after;
    }
    return s.str();
}


using namespace kraken::math;
namespace fs = std::filesystem;

VkResult Texturer::createInstance(std::string appname,
                                  bool enableValidation)
{
    VkApplicationInfo appinfo{};
    appinfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appinfo.apiVersion = VK_MAKE_VERSION(1, 0, 0);
    appinfo.pApplicationName = appname.c_str();
    appinfo.applicationVersion = VK_MAKE_VERSION(0, 0, 0);
    appinfo.engineVersion = VK_MAKE_VERSION(0, 0, 0);

    std::vector<VkValidationFeatureEnableEXT> enabledValidationFeatures =
    {
        VK_VALIDATION_FEATURE_ENABLE_BEST_PRACTICES_EXT,
    };

    VkValidationFeaturesEXT validationFeatures{};
    validationFeatures.sType = VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT;
    validationFeatures.enabledValidationFeatureCount = enabledValidationFeatures.size();
    validationFeatures.pEnabledValidationFeatures = enabledValidationFeatures.data();

    VkInstanceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appinfo;

    if (enableValidation)
    {
        createInfo.pNext = &validationFeatures;

        m_wantedLayers.push_back("VK_LAYER_LUNARG_standard_validation");
        m_wantedLayers.push_back("VK_LAYER_KHRONOS_validation");
    }

    m_enabledLayers = getSupportedVulkanLayers(m_wantedLayers);
    addDestructor(0, [this](){ m_enabledLayers.clear();
                               m_wantedLayers.clear(); });

    createInfo.ppEnabledLayerNames = m_enabledLayers.data();
    createInfo.enabledLayerCount = m_enabledLayers.size();

    std::vector<std::string> requiredExtensions;

    std::vector<std::string> optionalInstanceExtensions;
    if (enableValidation) optionalInstanceExtensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);

    auto instanceExtensions = getSupportedVulkanInstanceExtensions(requiredExtensions, optionalInstanceExtensions);
    createInfo.ppEnabledExtensionNames = instanceExtensions.data();
    createInfo.enabledExtensionCount = instanceExtensions.size();


    VkResult result = vkCreateInstance(&createInfo, nullptr, &m_instance);
    if (result != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create Vulkan instance.");
    }
    addDestructor(0, [this](){ vkDestroyInstance(m_instance, nullptr); });

    if (enableValidation)
    {
        for (size_t i = 0; i < createInfo.enabledExtensionCount; ++i)
        {
            if (!strcmp(createInfo.ppEnabledExtensionNames[i], VK_EXT_DEBUG_UTILS_EXTENSION_NAME))
            {
                VkDebugUtilsMessengerCreateInfoEXT debugCreate{};
                debugCreate.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
                debugCreate.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT
                                            | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT
                                            /*| VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT
                                            | VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT*/;
                debugCreate.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT
                                        | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT
                                        | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT;
                debugCreate.pfnUserCallback = debugCallback;
                debugCreate.pUserData = nullptr;

                auto CreateDebugUtilsMessengerEXT = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(m_instance, "vkCreateDebugUtilsMessengerEXT");
                auto DestroyDebugUtilsMessengerEXT = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(m_instance, "vkDestroyDebugUtilsMessengerEXT");

                if (!CreateDebugUtilsMessengerEXT)
                {
                    throw std::runtime_error("Failed to load required function pointer for debugging.");
                }

                CreateDebugUtilsMessengerEXT(m_instance, &debugCreate, nullptr, &m_debugUtils);
                addDestructor(0, [this, DestroyDebugUtilsMessengerEXT]()
                                 {
                                     if (DestroyDebugUtilsMessengerEXT)
                                     {
                                         DestroyDebugUtilsMessengerEXT(m_instance, m_debugUtils, nullptr);
                                     }
                                 });
            }
        }
    }
    return VK_SUCCESS;
}

VkResult Texturer::createDevice()
{
    m_physicalDevice = selectBestPhysicalDevice(m_instance).value();

    VkPhysicalDeviceProperties props;
    vkGetPhysicalDeviceProperties(m_physicalDevice, &props);
    m_deviceLimits = props.limits;

    m_queueInfo.resize(1);
    std::vector<QueueSelectionInfo> selectionInfo(m_queueInfo.size());
    selectionInfo.back().requiredFlags = VK_QUEUE_GRAPHICS_BIT;
    selectionInfo.back().priority = 1.f;
    selectionInfo.back().requirePresentSupport = false;
    auto queueCreateInfos = selectDeviceQueues(m_physicalDevice,
                                               VK_NULL_HANDLE,
                                               selectionInfo.size(),
                                               selectionInfo.data(),
                                               m_queueInfo.data());

    if (m_queueInfo[0].familyIndex == 0xffffffff)
    {
        throw std::runtime_error("Failed to construct main graphics queue.");
    }

    VkPhysicalDeviceFeatures features{};
    VkDeviceCreateInfo deviceCreate{};
    deviceCreate.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    deviceCreate.pQueueCreateInfos = queueCreateInfos.first.data();
    deviceCreate.queueCreateInfoCount = queueCreateInfos.first.size();
    deviceCreate.pEnabledFeatures = &features;
    deviceCreate.ppEnabledLayerNames = m_enabledLayers.data();
    deviceCreate.enabledLayerCount = m_enabledLayers.size();

    auto result = vkCreateDevice(m_physicalDevice, &deviceCreate, nullptr, &m_device);
    if (result != VK_SUCCESS) return result;
    addDestructor(0, [this](){ vkDestroyDevice(m_device, nullptr); });

    vkGetDeviceQueue(m_device, m_queueInfo[0].familyIndex, m_queueInfo[0].queueIndex, &m_graphicsQueue);
    return VK_SUCCESS;
}

VkResult Texturer::createAllocator()
{
    VmaAllocatorCreateInfo allocatorInfo{};
    allocatorInfo.physicalDevice = m_physicalDevice;
    allocatorInfo.device = m_device;
    allocatorInfo.instance = m_instance;

    auto result = vmaCreateAllocator(&allocatorInfo, &m_allocator);
    if (result != VK_SUCCESS) return result;
    addDestructor(0, [this](){ vmaDestroyAllocator(m_allocator); });

    return VK_SUCCESS;
}

Texturer::Texturer()
{
    m_debugUtils = VK_NULL_HANDLE;
    m_allocator = VK_NULL_HANDLE;
    m_graphicsQueue = VK_NULL_HANDLE;
    m_device = VK_NULL_HANDLE;
    m_physicalDevice = VK_NULL_HANDLE;
    m_instance = VK_NULL_HANDLE;
}

Texturer::~Texturer()
{
    if (m_device != VK_NULL_HANDLE)
    {
        vkDeviceWaitIdle(m_device);
    }

    m_mesh.destroy();

    performDestruction();
}

BufferAllocation Texturer::createBuffer(VkDeviceSize size,
                                        VkBufferUsageFlags bufFlags,
                                        VmaMemoryUsage memUsage)
{
    VkBufferCreateInfo bufCreate{};
    bufCreate.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufCreate.size = size;
    bufCreate.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    bufCreate.usage = bufFlags;

    VmaAllocationCreateInfo vmaInfo{};
    vmaInfo.usage = memUsage;

    BufferAllocation buf;

    if (vmaCreateBuffer(m_allocator, &bufCreate, &vmaInfo,
                        &buf.buffer, &buf.allocation, nullptr) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create buffer.");
    }
    return buf;
}

void Texturer::copyBuffer(VkBuffer source, VkBuffer dest, VkDeviceSize size,
                          VkDeviceSize srcOffset, VkDeviceSize dstOffset)
{
    VkBufferCopy bufCopy{};
    bufCopy.srcOffset = srcOffset;
    bufCopy.dstOffset = dstOffset;
    bufCopy.size = size;
    immediateExecute([&](VkCommandBuffer commandBuf)
    {
        vkCmdCopyBuffer(commandBuf, source, dest, 1, &bufCopy);
    });
}

VkPhysicalDevice Texturer::selectPhysicalDevice(VkInstance instance)
{
    return selectBestPhysicalDevice(instance).value();
}

VkResult Texturer::immediateExecute(const std::function<void(VkCommandBuffer)>& func)
{
    VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandBufferCount = 1;
    allocInfo.commandPool = m_transientCommandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;

    VkCommandBuffer commandBuf;
    vkAllocateCommandBuffers(m_device, &allocInfo, &commandBuf);

    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    vkBeginCommandBuffer(commandBuf, &beginInfo);

    func(commandBuf);

    vkEndCommandBuffer(commandBuf);
    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuf;

    VkFenceCreateInfo fenceCreate{};
    fenceCreate.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

    VkFence fence;
    auto result = vkCreateFence(m_device, &fenceCreate, nullptr, &fence);
    if (result != VK_SUCCESS) return result;

    result = vkQueueSubmit(m_graphicsQueue, 1, &submitInfo, fence);
    result != VK_SUCCESS || vkWaitForFences(m_device, 1, &fence, VK_FALSE, 0xffffffffffffffff);
    vkDestroyFence(m_device, fence, nullptr);
    vkFreeCommandBuffers(m_device, m_transientCommandPool, 1, &commandBuf);
    return result;
}

Pipeline Texturer::createMeshPipeline(U32 width,
                                      U32 height,
                                      const Mesh* mesh,
                                      const GraphicsShader& shader,
                                      VkRenderPass pass,
                                      U32 subpass,
                                      const VkDescriptorSetLayout* setLayouts,
                                      U32 setLayoutCount,
                                      bool blend)
{
    GraphicsPipelineInfo pipelineInfo;
    pipelineInfo.fillInfo(width, height, VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
                          VK_POLYGON_MODE_FILL, VK_CULL_MODE_NONE,
                          VK_FRONT_FACE_COUNTER_CLOCKWISE, VK_SAMPLE_COUNT_1_BIT);

    auto& depthStencil = pipelineInfo.depthStencilCreate;
    depthStencil.depthTestEnable = VK_FALSE;
    depthStencil.stencilTestEnable = VK_FALSE;

    pipelineInfo.vertexInputCreate.vertexAttributeDescriptionCount = 0;
    pipelineInfo.vertexInputCreate.pVertexAttributeDescriptions = nullptr;
    pipelineInfo.vertexInputCreate.vertexBindingDescriptionCount = 0;
    pipelineInfo.vertexInputCreate.pVertexBindingDescriptions = nullptr;

    // Scope must be outside of next if-statement.
    std::vector<VkVertexInputAttributeDescription> attrDescs;

    if (mesh)
    {
        attrDescs = getAttributeDescriptions(mesh->attributes(),
                                             mesh->attributeCount(),
                                             shader.info());

        pipelineInfo.vertexInputCreate.vertexAttributeDescriptionCount = attrDescs.size();
        pipelineInfo.vertexInputCreate.pVertexAttributeDescriptions = attrDescs.data();
        pipelineInfo.vertexInputCreate.vertexBindingDescriptionCount = mesh->bindingCount();
        pipelineInfo.vertexInputCreate.pVertexBindingDescriptions = mesh->bindings();
    }

    Pipeline pipeline;
    pipeline.device = m_device;

    VkPipelineLayoutCreateInfo pipelineLayoutCreate{};
    pipelineLayoutCreate.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutCreate.setLayoutCount = setLayoutCount;
    pipelineLayoutCreate.pSetLayouts = setLayouts;

    VkPushConstantRange pcr{};

    // If we render a mesh we also need a push constant to define the
    // transformation.
    if (mesh)
    {
        pcr.offset = 0;
        pcr.size = sizeof(mat4);
        pcr.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
        pipelineLayoutCreate.pushConstantRangeCount = 1;
        pipelineLayoutCreate.pPushConstantRanges = &pcr;
    }

    if (vkCreatePipelineLayout(m_device, &pipelineLayoutCreate, nullptr, &pipeline.layout) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create mesh pipeline layout.");
    }
    auto& blendStates = pipelineInfo.colourBlendStates;
    blendStates.resize(3);

    // This ugly addition of an auxiliary variable along with the if-statement
    // instead of a simple ternary statement is to squelch a very persistent
    // compiler warning.
    VkBool32 blendEnable = VK_FALSE;
    if (blend) blendEnable = VK_TRUE;

    blendStates[0] =
    {
        blendEnable,
        VK_BLEND_FACTOR_ONE,
        VK_BLEND_FACTOR_ONE,
        VK_BLEND_OP_ADD,
        VK_BLEND_FACTOR_ONE,
        VK_BLEND_FACTOR_ONE,
        VK_BLEND_OP_ADD,
        VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT
    };
    blendStates[1] = blendStates[0];
    blendStates[2] = blendStates[1];

    auto graphicsCreate = pipelineInfo.getGraphicsPipelineCreateInfo();
    graphicsCreate.pTessellationState = nullptr;
    graphicsCreate.pDynamicState = nullptr;
    graphicsCreate.stageCount = shader.stageCount();
    graphicsCreate.pStages = shader.pStages();
    graphicsCreate.layout = pipeline.layout;
    graphicsCreate.renderPass = pass;
    graphicsCreate.subpass = subpass;

    if (vkCreateGraphicsPipelines(m_device, VK_NULL_HANDLE, 1, &graphicsCreate, nullptr, &pipeline.pipeline) != VK_SUCCESS)
    {
        pipeline.destroy();
        throw std::runtime_error("Failed to create mesh pipeline.");
    }

    return pipeline;
}

Pipeline Texturer::createQualityMapperPipeline(U32 width,
                                               U32 height,
                                               const GraphicsShader& shader,
                                               VkRenderPass pass,
                                               U32 subpass,
                                               const VkDescriptorSetLayout* setLayouts,
                                               U32 setLayoutCount,
                                               U32 passCount,
                                               VkBlendOp alphaOp)
{
    GraphicsPipelineInfo pipelineInfo;
    pipelineInfo.fillInfo(width, height, VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
                          VK_POLYGON_MODE_FILL, VK_CULL_MODE_NONE,
                          VK_FRONT_FACE_COUNTER_CLOCKWISE, VK_SAMPLE_COUNT_1_BIT);

    auto& depthStencil = pipelineInfo.depthStencilCreate;
    depthStencil.depthTestEnable = VK_FALSE;
    depthStencil.stencilTestEnable = VK_FALSE;

    pipelineInfo.vertexInputCreate.vertexAttributeDescriptionCount = 0;
    pipelineInfo.vertexInputCreate.pVertexAttributeDescriptions = nullptr;
    pipelineInfo.vertexInputCreate.vertexBindingDescriptionCount = 0;
    pipelineInfo.vertexInputCreate.pVertexBindingDescriptions = nullptr;

    Pipeline pipeline;
    pipeline.device = m_device;

    VkPipelineLayoutCreateInfo pipelineLayoutCreate{};
    pipelineLayoutCreate.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutCreate.setLayoutCount = setLayoutCount;
    pipelineLayoutCreate.pSetLayouts = setLayouts;

    if (vkCreatePipelineLayout(m_device, &pipelineLayoutCreate, nullptr, &pipeline.layout) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create quality mapper pipeline layout.");
    }
    auto& blendStates = pipelineInfo.colourBlendStates;
    blendStates.resize(passCount);
    blendStates[0] =
    {
        VK_TRUE,
        VK_BLEND_FACTOR_ONE,
        VK_BLEND_FACTOR_ONE,
        VK_BLEND_OP_ADD,
        VK_BLEND_FACTOR_ONE,
        VK_BLEND_FACTOR_ONE,
        alphaOp,
        VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT
    };

    for (U32 i = 1; i < passCount; ++i)
    {
        blendStates[i] = blendStates[0];
    }

    auto graphicsCreate = pipelineInfo.getGraphicsPipelineCreateInfo();
    graphicsCreate.pTessellationState = nullptr;
    graphicsCreate.pDynamicState = nullptr;
    graphicsCreate.stageCount = shader.stageCount();
    graphicsCreate.pStages = shader.pStages();
    graphicsCreate.layout = pipeline.layout;
    graphicsCreate.renderPass = pass;
    graphicsCreate.subpass = subpass;

    if (vkCreateGraphicsPipelines(m_device, VK_NULL_HANDLE, 1, &graphicsCreate, nullptr, &pipeline.pipeline) != VK_SUCCESS)
    {
        pipeline.destroy();
        throw std::runtime_error("Failed to create mesh pipeline.");
    }

    return pipeline;
}

std::vector<Texturer::DMAPLoadInfo> Texturer::getDepthmapPaths(fs::path dmapDirectory)
{
    std::vector<Texturer::DMAPLoadInfo> dmaps;
    for (const auto& entry : fs::directory_iterator{dmapDirectory})
    {
        if (entry.path().extension() != ".dmap") continue;
        std::string stem = entry.path().stem().string();
        if (stem.substr(0, 5) != "depth") continue;
        bool onlyDigits = true;
        for (size_t i = 5; i < stem.size(); ++i)
        {
            if (!std::isdigit(stem[i]))
            {
                onlyDigits = false;
                break;
            }
        }
        if (!onlyDigits) continue;
        if (entry.is_directory()) continue; // It must be a file, but since we
                                            // can have symlinks to files, we
                                            // choose just to disregard
                                            // directories. If there's a symlink
                                            // to a directory, tough.

        dmaps.push_back({entry, nullptr});
    }
    return dmaps;
}

bool Texturer::acquireReadyVulkanDMAP(std::vector<ResourceLoader::ResourceID>& loadIDs,
                                      VulkanDMAP* dmapOut)
{
    if (!dmapOut) return false;

    auto finishID = ResourceLoader::invalidResource;

    if (m_currentTransferID != ResourceLoader::invalidResource)
    {
        auto status = m_resourceLoader.getStatus(m_currentTransferID);
        if (status == ResourceLoader::Status::WaitingForSubresources
            || status == ResourceLoader::Status::Good)
        {
            return false;
        }
        else if (status != ResourceLoader::Status::Ready)
        {
            throw std::runtime_error("Failed to transfer required DMAP.");
        }
        finishID = m_currentTransferID;
        m_currentTransferID = ResourceLoader::invalidResource;
    }

    for (size_t i = 0; i < loadIDs.size(); ++i)
    {
        auto id = loadIDs[i];
        auto status = m_resourceLoader.getStatus(id);
        if (status == ResourceLoader::Status::WaitingForSubresources)
        {
            if (m_currentTransferID != ResourceLoader::invalidResource)
            {
                throw std::logic_error("Something is completely wrong!");
            }
            std::swap(loadIDs[i], loadIDs.back());
            loadIDs.pop_back();
            m_currentTransferID = id;
            if (g_showResourceLoaderDebugOutput)
            {
                std::stringstream ss; ss << "ASK-UNBLOCK(" << id << ")"; std::cerr << ss.str();
            }
            m_resourceLoader.unblockResource(id, ResourceLoader::ResourceType::VulkanResource);
            break;
        }
        else if (status == ResourceLoader::Status::Ready)
        {
            throw std::logic_error("A resource was ready before being unblocked.");
        }
        else if (status != ResourceLoader::Status::Good)
        {
            throw std::runtime_error("Failed to load some resource.");
        }
    }

    if (finishID != ResourceLoader::invalidResource)
    {
        const auto& subresources = m_resourceLoader.getSubresourceList(finishID);
        if (subresources.empty())
        {
            throw std::logic_error("Empty subresource list.");
        }
        if (subresources.size() != 3 && subresources.size() != 4)
        {
            throw std::logic_error("Subresources wrong size.");
        }

        VulkanDMAP dmap;
        Image* subimages[4] = {&dmap.normalDepthMap, &dmap.confidenceMap, &dmap.qualityMap, &dmap.colourImage};

        for (U32 i = 0; i < subresources.size(); ++i)
        {
            auto subid = subresources[i];
            ResourceLoader::VulkanResource vulkanRes;
            if (!m_resourceLoader.acquireVulkanResource(subid, &vulkanRes))
            {
                throw std::runtime_error("Failed to acquire vulkan image.");
            }
            if (vulkanRes.image.image == VK_NULL_HANDLE)
            {
                throw std::logic_error("No vulkan image (expected one).");
            }
            *subimages[i] = vulkanRes.image;
        }

        DMAPContent content;
        {
            ResourceLoader::HostResource hostRes;
            m_resourceLoader.acquireHostResource(finishID, &hostRes);
            content = std::move(*static_cast<DMAPContent*>(hostRes.resource));
            hostRes.destroy();
        }

        dmap.colourImageFilename = content.filename;

        dmap.camera = content.camera;
        dmap.rotation = content.rotation;
        dmap.position = content.position;

        dmap.focusInfo = content.focusInfo;

        dmap.width = content.qualityMap.width();
        dmap.height = content.qualityMap.height();

        if (content.normalDepthMap.width() != dmap.width
            || content.normalDepthMap.height() != dmap.height
            || content.confidenceMap.width() != dmap.width
            || content.confidenceMap.height() != dmap.height)
        {
            throw std::logic_error("All DMAP images (normal/depth + "
                                   "confidence + quality) must have same "
                                   "dimensions.");
        }

        auto origIndex = m_loadIDsToIndices[finishID];
        m_dmapLoadInfo[origIndex].content = std::make_shared<DMAPContent>();
        *m_dmapLoadInfo[origIndex].content.get() = std::move(content);

        if (m_cachedActiveDMAPs.find(origIndex) == m_cachedActiveDMAPs.end())
        {
            throw std::logic_error("Bug: Found index was not marked as cached "
                                   "+ active.");
        }
        m_cachedActiveDMAPs.erase(origIndex);
        m_cachedInactiveDMAPs.insert(origIndex);

        *dmapOut = dmap;
        return true;
    }

    return false;
}

VulkanDMAP Texturer::acquireVulkanDMAP(std::vector<ResourceLoader::ResourceID>& loadIDs)
{
    if (!loadIDs.size() && m_currentTransferID == ResourceLoader::invalidResource)
    {
        throw std::logic_error("No load IDs given and no current transfer.");
    }

    VulkanDMAP dmap;

    U32 deadlockGuard = 0;
    U32 deadlockHitCount = 0;

    while (!acquireReadyVulkanDMAP(loadIDs, &dmap))
    {
        // Try acquire again -- there is a slim but nonzero chance that between
        // the above call and that to .isIdle() all resources have finished
        // parsing.
        if (m_resourceLoader.isIdle()) ++deadlockGuard;
        else deadlockGuard = 0;

        if (deadlockGuard >= 10000000)
        {
            m_resourceLoader.panicIfIdle(deadlockHitCount >= 3);
            if (!m_resourceLoader.isIdle()) continue;
            if (deadlockHitCount < 3)
            {
                ++deadlockHitCount;
                deadlockGuard = 0;
                continue;
            }
            throw std::logic_error("DEADLOCK DETECTED! ResourceLoader is idle, "
                                   "yet no DMAP can be acquired.");
        }

        m_resourceLoader.executeVulkanCommands(0xffffffffffffffff);
    }

    return dmap;
}

void Texturer::pushAllDMAPIndices()
{
    m_dmapIndicesToLoad.clear();
    std::vector<U32> alreadyLoaded;
    for (U32 i = 0; i < m_dmapLoadInfo.size(); ++i)
    {
        if (m_dmapLoadInfo[i].content.get())
        {
            alreadyLoaded.push_back(i);
        }
        else m_dmapIndicesToLoad.push_back(i);
    }
    for (auto i : alreadyLoaded) m_dmapIndicesToLoad.push_back(i);
}

bool Texturer::isDMAPCacheFull() const
{
    return m_cachedInactiveDMAPs.size() + m_cachedActiveDMAPs.size() >= m_maxLoadCapacity;
}

bool Texturer::tryPurgeInactiveDMAP()
{
    if (m_cachedInactiveDMAPs.empty()) return false;
    auto index = *m_cachedInactiveDMAPs.begin();
    m_cachedInactiveDMAPs.erase(index);
    m_dmapLoadInfo[index].content = nullptr;
    return true;
}

bool Texturer::canLoadNextDMAPIndex(U32 index) const
{
    if (!isDMAPCacheFull()) return true;
    if (m_cachedInactiveDMAPs.find(index) != m_cachedInactiveDMAPs.end()) return true;
    if (m_cachedActiveDMAPs.find(index) != m_cachedActiveDMAPs.end())
    {
        throw std::logic_error("BUG: Trying to load DMAP which is already loading.");
    }
    return false;
}

void Texturer::enqueueDMAPLoads()
{
    while (!m_dmapIndicesToLoad.empty()
           && (canLoadNextDMAPIndex(m_dmapIndicesToLoad.back())
               || tryPurgeInactiveDMAP()))
    {
        auto nextID = m_dmapIndicesToLoad.back();
        m_dmapIndicesToLoad.pop_back();

        m_cachedActiveDMAPs.insert(nextID);

        auto loadOpts = m_dmapLoadOpts;

        if (m_dmapLoadInfo[nextID].content.get())
        {
            m_cachedInactiveDMAPs.erase(nextID);
            auto transferSrc = DMAPParser::createDataSourceFromPreparsedDMAP(m_dmapLoadInfo[nextID].content);
            loadOpts.parseDataSource = transferSrc.dataSource;
        }

        auto id = m_resourceLoader.load("image/dmap",
                                        m_dmapLoadInfo[nextID].path,
                                        loadOpts);
        m_dmapLoadIDs.push_back(id);
        m_loadIDsToIndices[id] = nextID;
    }
}

void Texturer::clearImage(VkImage image,
                          VkImageLayout initialLayout,
                          VkImageLayout finalLayout,
                          VkAccessFlags srcAccess,
                          VkAccessFlags dstAccess,
                          VkClearColorValue ccv)
{
    immediateExecute([image, initialLayout, finalLayout, srcAccess, dstAccess, ccv]
    (VkCommandBuffer commandBuf)
    {
        VkImageMemoryBarrier barrier{};
        barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        barrier.srcAccessMask = srcAccess;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.oldLayout = initialLayout;
        barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;

        barrier.image = image;
        barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        barrier.subresourceRange.baseArrayLayer = 0;
        barrier.subresourceRange.layerCount = 1;
        barrier.subresourceRange.baseMipLevel = 0;
        barrier.subresourceRange.levelCount = 1;

        vkCmdPipelineBarrier(commandBuf, VK_PIPELINE_STAGE_TRANSFER_BIT,
                             VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0,
                             nullptr, 0, nullptr, 1, &barrier);

        vkCmdClearColorImage(commandBuf,
                             image,
                             VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                             &ccv,
                             1,
                             &barrier.subresourceRange);

        barrier.oldLayout = barrier.newLayout;
        barrier.srcAccessMask = barrier.dstAccessMask;
        barrier.newLayout = finalLayout;
        barrier.dstAccessMask = dstAccess;

        vkCmdPipelineBarrier(commandBuf, VK_PIPELINE_STAGE_TRANSFER_BIT,
                             VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, 0, 0,
                             nullptr, 0, nullptr, 1, &barrier);
    });
}

void Texturer::updateQualityMapperInputs(QualityMapperInputs& qmInputs,
                                         const VulkanDMAP& dmap,
                                         VmaAllocation alloc)
{
    dmat3 dimensionNormaliser({1./dmap.width, 0.,             0.,
                               0.,            1./dmap.height, 0.,
                               0.,            0.,             1.});
    /// WARNING: Even though dimensionNormaliser is diagonal, it does NOT
    /// commute with camRot, and multiplication MUST be in the defined order.
    mat3 camRot = prec_cast<F32>(dimensionNormaliser * dmap.camera * dmap.rotation);
    if (std::abs(det(camRot)) <= 1e-7f)
    {
        throw std::logic_error("Camera * rotation matrix not invertible.");
    }
    mat3 invCamRot = inverse(camRot);
    qmInputs.camRot = mat4(camRot);
    qmInputs.invCamRot = mat4(invCamRot);
    qmInputs.rotation = mat4(prec_cast<F32>(dmap.rotation));
    qmInputs.inverseRotation = mat4(prec_cast<F32>(inverse(dmap.rotation)));
    auto lowpPos = prec_cast<F32>(dmap.position);
    qmInputs.position = {lowpPos.x, lowpPos.y, lowpPos.z, 0.f};

    qmInputs.focusMean = dmap.focusInfo.depthMean;
    qmInputs.focusSD = dmap.focusInfo.depthSD;

    qmInputs.inputDepthTextureWidth = dmap.width;
    qmInputs.inputDepthTextureHeight = dmap.height;

    void* mem;
    vmaMapMemory(m_allocator, alloc, &mem);
    std::memcpy(mem, &qmInputs, sizeof(qmInputs));
    vmaUnmapMemory(m_allocator, alloc);
}


U32 Texturer::writeBarriers(VkImageMemoryBarrier* barriers,
                            U32 maxBarrierCount,
                            const VulkanDMAP& dmap) const
{
    VkImageMemoryBarrier barrier{};
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
    barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    if (maxBarrierCount <= 0) return maxBarrierCount;

    barrier.image = dmap.normalDepthMap.image;
    barrier.subresourceRange = dmap.normalDepthMap.defaultSubresourceRange();
    barriers[0] = barrier;

    if (maxBarrierCount <= 1) return maxBarrierCount;

    barrier.image = dmap.confidenceMap.image;
    barrier.subresourceRange = dmap.confidenceMap.defaultSubresourceRange();
    barriers[1] = barrier;

    if (maxBarrierCount <= 2) return maxBarrierCount;

    barrier.image = dmap.qualityMap.image;
    barrier.subresourceRange = dmap.qualityMap.defaultSubresourceRange();
    barriers[2] = barrier;

    if (maxBarrierCount <= 3
        || dmap.colourImage.image == VK_NULL_HANDLE) return maxBarrierCount;

    barrier.image = dmap.colourImage.image;
    barrier.subresourceRange = dmap.colourImage.defaultSubresourceRange();
    barriers[3] = barrier;
    return 4;
}

void normaliseByAlpha(FloatImage& img)
{
    for (U32 y = 0; y < img.height(); ++y)
    {
        for (U32 x = 0; x < img.width(); ++x)
        {
            vec4 col = img.getPixel(x, y);
            if (std::abs(col.a) <= 1e-6) col.a = 0.f;
            else col.a = 1.f / col.a;
            img.setPixel(x, y, col*col.a);
        }
    }
}

void normaliseByImage(FloatImage& img, const FloatImage& normFactor, float epsilon = 1e-5f)
{
    for (U32 y = 0; y < img.height(); ++y)
    {
        for (U32 x = 0; x < img.width(); ++x)
        {
            vec4 col = img.getPixel(x, y);
            float normaliser = normFactor.getPixel(x, y).r;
            if (std::abs(normaliser) <= epsilon) normaliser = 0.f;
            else normaliser = 1.f / normaliser;
            img.setPixel(x, y, col*normaliser);
        }
    }
}

// This expands all pixels with strictly positive alpha channel to cover all
// pixels with non-positive alpha channel.
void expandByAlpha(FloatImage& img)
{
    struct PointInfo
    {
        U32 distSqr; // |(xFrom, yFrom)-(x,y)|^2;
        Vector<2, U16> from; // origin of this point
        Vector<2, U16> loc; // Coordinate of current point.
        bool operator<(const PointInfo& other) const
        {
            // Yes, wrong way. To be used by priority queue comparison.
            return distSqr > other.distSqr;
        }
    };

    if (img.width() >= 65536 || img.height() >= 65536)
    {
        throw std::logic_error("Image dimensions are too great (>2^16).");
    }

    std::priority_queue<PointInfo> ptQueue;
    for (U16 y = 0; y < img.height(); ++y)
    {
        for (U16 x = 0; x < img.width(); ++x)
        {
            if (img.getPixel(x, y).a > 0.f) ptQueue.push({0, {x, y}, {x, y}});
        }
    }

    while (!ptQueue.empty())
    {
        auto info = ptQueue.top();
        ptQueue.pop();

        for (int yOffset = -1; yOffset <= 1; ++yOffset)
        {
            for (int xOffset = -1; xOffset <= 1; ++xOffset)
            {
                if (!xOffset && !yOffset) continue;
                int currX = (int)info.loc.x + xOffset;
                int currY = (int)info.loc.y + yOffset;
                if (currX < 0 || currY < 0 || (U32)currX >= img.width() || (U32)currY >= img.height()) continue;

                auto x = (U16)currX;
                auto y = (U16)currY;

                if (img.getPixel(x, y).a > 0.f) continue;
                img.setPixel(x, y, img.getPixel(info.from.x, info.from.y));

                auto xDiff = (U32)info.from.x - (U32)currX;
                auto yDiff = (U32)info.from.y - (U32)currY;
                U32 distSqr = xDiff*xDiff + yDiff*yDiff;
                ptQueue.push({distSqr, info.from, {x, y}});
            }
        }
    }
}

// This expands all pixels with strictly positive alpha channel to cover all
// pixels with non-positive alpha channel. This modifies and destroys all
// information available in the image "alpha".
void expandByAlpha(kraken::image::Image& dstImg,
                   kraken::image::Image& alpha)
{
    if (dstImg.width() != alpha.width() || dstImg.height() != alpha.height())
    {
        throw std::logic_error("Alpha and destination images does not have "
                               "identical dimensions.");
    }
    struct PointInfo
    {
        U32 distSqr; // |(xFrom, yFrom)-(x,y)|^2;
        Vector<2, U16> from; // origin of this point
        Vector<2, U16> loc; // Coordinate of current point.
        bool operator<(const PointInfo& other) const
        {
            // Yes, wrong way. To be used by priority queue comparison.
            return distSqr > other.distSqr;
        }
    };

    if (dstImg.width() >= 65536 || dstImg.height() >= 65536)
    {
        throw std::logic_error("Image dimensions are too great (>2^16).");
    }

    std::priority_queue<PointInfo> ptQueue;
    for (U16 y = 0; y < alpha.height(); ++y)
    {
        for (U16 x = 0; x < alpha.width(); ++x)
        {
            if (alpha.getPixel(x, y)) ptQueue.push({0, {x, y}, {x, y}});
        }
    }

    while (!ptQueue.empty())
    {
        auto info = ptQueue.top();
        ptQueue.pop();

        for (int yOffset = -1; yOffset <= 1; ++yOffset)
        {
            for (int xOffset = -1; xOffset <= 1; ++xOffset)
            {
                if (!xOffset && !yOffset) continue;
                int currX = (int)info.loc.x + xOffset;
                int currY = (int)info.loc.y + yOffset;
                if (currX < 0 || currY < 0 || (U32)currX >= dstImg.width() || (U32)currY >= dstImg.height()) continue;

                auto x = (U16)currX;
                auto y = (U16)currY;

                if (alpha.getPixel(x, y)) continue;
                dstImg.setPixel(x, y, dstImg.getPixel(info.from.x, info.from.y));
                alpha.setPixel(x, y, 1);

                auto xDiff = (U32)info.from.x - (U32)currX;
                auto yDiff = (U32)info.from.y - (U32)currY;
                U32 distSqr = xDiff*xDiff + yDiff*yDiff;
                ptQueue.push({distSqr, info.from, {x, y}});
            }
        }
    }
}

VkPhysicalDeviceLimits Texturer::getDeviceLimits(VkPhysicalDevice device)
{
    VkPhysicalDeviceProperties props;
    vkGetPhysicalDeviceProperties(device, &props);
    return props.limits;
}

void Texturer::ensureMeshLoaded()
{
    if (m_meshLoadID == ResourceLoader::invalidResource) return;

    while (m_resourceLoader.getStatus(m_meshLoadID) == ResourceLoader::Status::Good)
        m_resourceLoader.executeVulkanCommands();
    if (m_resourceLoader.getStatus(m_meshLoadID) != ResourceLoader::Status::Ready)
    {
        throw std::runtime_error("Failed to load mesh.");
    }

    ResourceLoader::HostResource hostRes;
    ResourceLoader::VulkanResource vkres;
    m_resourceLoader.acquireVulkanResource(m_meshLoadID, &vkres);
    m_resourceLoader.acquireHostResource(m_meshLoadID, &hostRes);
    m_mesh = std::move(*static_cast<Mesh*>(hostRes.resource));

    m_meshLoadID = ResourceLoader::invalidResource;
}

void Texturer::init(const TexturerOptions& texOpts)
{
    if (m_device != VK_NULL_HANDLE) return; // Already initialised
    if (createInstance("Texturer", texOpts.debugLevel > 0) != VK_SUCCESS
        || createDevice() != VK_SUCCESS
        || createAllocator() != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to initialise Vulkan.");
    }

    if (texOpts.debugLevel > 1) g_showResourceLoaderDebugOutput = true;
    if (texOpts.debugLevel > 2) g_showResourceLoaderExtraDebugOutput = true;

    m_resourceLoader.create(texOpts.maxThreads, texOpts.dmapDirectory,
                            m_device, m_queueInfo[0],
                            128*1048576, 1048576, m_allocator);

    ResourceLoader::Options opts;
    opts.priority = 0.f;
    opts.type = ResourceLoader::ResourceType::VulkanResource
                | ResourceLoader::ResourceType::HostResource;
    opts.pathRelativeToLoaderPath = false;

    opts.parseOptions["generateTangents"] = true;

    m_meshLoadID = m_resourceLoader.load("model/ply", texOpts.meshFile, opts);

    m_resourceLoader.addResourceSupport<DMAPParser>("image/dmap");

    opts.type = ResourceLoader::ResourceType::HostResource
                | ResourceLoader::ResourceType::VulkanResource;
    opts.vulkanOptions["image"]["usageFlags"] =
    {
        "VK_IMAGE_USAGE_SAMPLED_BIT",
    };
    opts.parseOptions["loadColourImage"] = false;
    opts.parseOptions["qualityEstimator"]["focusHighpassSigma"] = texOpts.focusHighpassSigma;
    opts.parseOptions["qualityEstimator"]["focusLowerCutoff"] = texOpts.focusLowerCutoff;
    opts.parseOptions["qualityEstimator"]["focusCutoffMeanIterations"] = texOpts.focusCutoffMeanIterations;
    opts.parseOptions["qualityEstimator"]["maxAngle"] = texOpts.maxAngle;
    opts.parseOptions["qualityEstimator"]["minConfidence"] = texOpts.minConfidence;
    opts.parseOptions["qualityEstimator"]["blurSigma"] = texOpts.blurSigma;
    opts.parseOptions["qualityEstimator"]["expandRadius"] = texOpts.expandRadius;
    opts.parseOptions["qualityEstimator"]["expandSigma"] = texOpts.expandSigma;
    opts.parseOptions["qualityEstimator"]["expandMinConfidence"] = texOpts.expandMinConfidence;

    opts.blockTypes = ResourceLoader::ResourceType::VulkanResource;

    m_dmapLoadInfo = getDepthmapPaths(texOpts.dmapDirectory);
    m_dmapLoadOpts = opts;
    m_dmapLoadOpts.parseOptions["debugDump"] = texOpts.dumpImages;
    m_dmapLoadOpts.pathRelativeToLoaderPath = false;
    m_maxLoadCapacity = texOpts.maxLoadCapacity;

    m_samplerCache.create(m_device);
    m_descAllocator.construct(m_device);

    addDestructor(0, [this](){ m_descAllocator.destroy();
                               m_samplerCache.destroy();
                               m_resourceLoader.destroy(); });


    VkCommandPoolCreateInfo commandPoolCreate{};
    commandPoolCreate.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    commandPoolCreate.queueFamilyIndex = m_queueInfo[0].familyIndex;
    commandPoolCreate.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    commandPoolCreate.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
    if (vkCreateCommandPool(m_device, &commandPoolCreate, nullptr, &m_transientCommandPool) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create transient command pool.");
    }
    addDestructor(0, [this](){ vkDestroyCommandPool(m_device, m_transientCommandPool, nullptr); });
}

void Texturer::drawMeshToTexture(const TexturerOptions& texOpts,
                                 Image& positionOut,
                                 Image& normalOut,
                                 Image& tangentOut)
{
    U32 width = texOpts.width;
    U32 height = texOpts.height;

    FrameGraph graph;
    graph.create(m_device, m_allocator);

    PassLayout passLayout;
    passLayout.width = width;
    passLayout.height = height;
    passLayout.layers = 1;
    passLayout.samples = VK_SAMPLE_COUNT_1_BIT;

    auto& meshToTexturePass = graph.addPass("meshToTexture", passLayout, VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT);
    auto& textureAverager = graph.addPass("textureAverager", passLayout, VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT);

    VkClearValue floatClear;
    floatClear.color = {0.f, 0.f, 0.f, 0.f};

    meshToTexturePass.addOutput("position", VK_FORMAT_R32G32B32A32_SFLOAT, floatClear);
    meshToTexturePass.addOutput("normal", VK_FORMAT_R32G32B32A32_SFLOAT, floatClear);
    meshToTexturePass.addOutput("tangent", VK_FORMAT_R32G32B32A32_SFLOAT, floatClear);

    textureAverager.addInputAttachment("position", VK_FORMAT_R32G32B32A32_SFLOAT);
    textureAverager.addInputAttachment("normal", VK_FORMAT_R32G32B32A32_SFLOAT);
    textureAverager.addInputAttachment("tangent", VK_FORMAT_R32G32B32A32_SFLOAT);

    textureAverager.addOutput("position", VK_FORMAT_R32G32B32A32_SFLOAT, {}, VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT);
    textureAverager.addOutput("normal", VK_FORMAT_R32G32B32A32_SFLOAT, {}, VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT);
    textureAverager.addOutput("tangent", VK_FORMAT_R32G32B32A32_SFLOAT, {}, VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT);

    graph.connectToInput(meshToTexturePass["position"], textureAverager["position"]);
    graph.connectToInput(meshToTexturePass["normal"], textureAverager["normal"]);
    graph.connectToInput(meshToTexturePass["tangent"], textureAverager["tangent"]);

    graph.markOutputUsed(textureAverager["position"], VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_SRC_BIT);
    graph.markOutputUsed(textureAverager["normal"], VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_SRC_BIT);
    graph.markOutputUsed(textureAverager["tangent"], VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_SRC_BIT);

    graph.compile(nullptr, 0, texOpts.debugLevel > 1);

    GraphicsShader meshRenderShader;
    if (!meshRenderShader.createFromFile(texOpts.internalDataDirectory / "shaders/texturer-drawmesh.json", m_device))
    {
        throw std::runtime_error("Failed to create mesh render shader.");
    }

    GraphicsShader meshAverageShader;
    if (!meshAverageShader.createFromFile(texOpts.internalDataDirectory / "shaders/texturer-average.json", m_device))
    {
        throw std::runtime_error("Failed to create mesh averaging shader.");
    }


    VkDescriptorSetLayout meshDescriptorLayout = VK_NULL_HANDLE;
    VkDescriptorSet meshDescriptorSet = VK_NULL_HANDLE;
    DescriptorSetLayoutInfo meshDescriptorSetInfo;

    meshAverageShader.info().appendLayoutInfo(meshDescriptorSetInfo, 0);

    auto meshLayoutCreate = meshDescriptorSetInfo.descriptorSetLayoutCreateInfo();

    if (vkCreateDescriptorSetLayout(m_device, &meshLayoutCreate, nullptr, &meshDescriptorLayout) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create mesh descriptor set layout.");
    }
    addDestructor(2, [this, meshDescriptorLayout](){ vkDestroyDescriptorSetLayout(m_device, meshDescriptorLayout, nullptr); });

    if (!m_descAllocator.allocate(&meshDescriptorSet, &meshDescriptorLayout, 1))
    {
        throw std::runtime_error("Failed to create mesh descriptor set.");
    }

    {
        std::vector<VkWriteDescriptorSet> descSetWrite;
        U32 count = 0;
        textureAverager.writeDescriptorSet(meshDescriptorSet, &count, nullptr);
        descSetWrite.resize(count);
        textureAverager.writeDescriptorSet(meshDescriptorSet, &count, descSetWrite.data());

        if (count)
        {
            vkUpdateDescriptorSets(m_device, count, descSetWrite.data(), 0, nullptr);
        }
    }

    ensureMeshLoaded();

    auto meshPipeline = createMeshPipeline(width,
                                           height,
                                           &m_mesh,
                                           meshRenderShader,
                                           meshToTexturePass.vulkanPass().pass,
                                           meshToTexturePass.vulkanPass().subpass,
                                           nullptr,
                                           0,
                                           true);

    addDestructor(2, [&meshPipeline]
    {
        meshPipeline.destroy();
    });

    auto averagePipeline = createMeshPipeline(width,
                                              height,
                                              nullptr,
                                              meshAverageShader,
                                              textureAverager.vulkanPass().pass,
                                              textureAverager.vulkanPass().subpass,
                                              &meshDescriptorLayout,
                                              1,
                                              false);
    addDestructor(2, [&averagePipeline]
    {
        averagePipeline.destroy();
    });

    mat4 meshRenderTransform(1.f);
    {
        // Transformation matrix mapping the area [x0, x1] × [y0, y1] into the
        // total render area.
        float x0 = texOpts.bottomLeft.x;
        float y0 = texOpts.bottomLeft.y;
        float x1 = texOpts.topRight.x;
        float y1 = texOpts.topRight.y;

        meshRenderTransform(0, 0) = 2.f / (x1 - x0);
        meshRenderTransform(1, 1) = 2.f / (y1 - y0);
        meshRenderTransform(0, 3) = (x0 + x1) / (x0 - x1);
        meshRenderTransform(1, 3) = (y0 + y1) / (y0 - y1);
    }

    meshToTexturePass.setCallback([this, meshPipeline, &meshRenderTransform](VkCommandBuffer cmdbuf)
    {
        vkCmdBindPipeline(cmdbuf, VK_PIPELINE_BIND_POINT_GRAPHICS, meshPipeline.pipeline);

        vkCmdPushConstants(cmdbuf,
                           meshPipeline.layout,
                           VK_SHADER_STAGE_VERTEX_BIT,
                           0,
                           sizeof(meshRenderTransform),
                           meshRenderTransform.data());

        VkDeviceSize offsets[] = {0};
        vkCmdBindVertexBuffers(cmdbuf,
                               0,
                               1,
                               &m_mesh.vertexBuffer(0).deviceBuffer.buffer,
                               offsets);
        vkCmdBindIndexBuffer(cmdbuf,
                             m_mesh.indexBuffer(0).deviceBuffer.buffer,
                             0,
                             m_mesh.indexBuffer(0).stride == 4 ? VK_INDEX_TYPE_UINT32 : VK_INDEX_TYPE_UINT16);

        vkCmdDrawIndexed(cmdbuf, m_mesh.indexBuffer(0).count, 1, 0, 0, 0);
    });
    textureAverager.setCallback([averagePipeline, meshDescriptorSet](VkCommandBuffer cmdbuf)
    {
        vkCmdBindPipeline(cmdbuf, VK_PIPELINE_BIND_POINT_GRAPHICS, averagePipeline.pipeline);
        vkCmdBindDescriptorSets(cmdbuf, VK_PIPELINE_BIND_POINT_GRAPHICS, averagePipeline.layout, 0, 1, &meshDescriptorSet, 0, nullptr);
        vkCmdDraw(cmdbuf, 3, 1, 0, 0);
    });

    VkCommandBuffer commandBuf;
    VkCommandBufferAllocateInfo commandAlloc{};
    commandAlloc.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    commandAlloc.commandBufferCount = 1;
    commandAlloc.commandPool = m_transientCommandPool;

    if (vkAllocateCommandBuffers(m_device, &commandAlloc, &commandBuf) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to allocate command buffer.");
    }
    VkCommandBufferBeginInfo commandBegin{};
    commandBegin.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;


    vkBeginCommandBuffer(commandBuf, &commandBegin);
    graph.execute(commandBuf);
    vkEndCommandBuffer(commandBuf);


    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuf;

    VkFence fence;
    VkFenceCreateInfo fenceCreate{};
    fenceCreate.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    if (vkCreateFence(m_device, &fenceCreate, nullptr, &fence) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create fence.");
    }
    addDestructor(2, [this, fence](){ vkDestroyFence(m_device, fence, nullptr); });

    if (vkQueueSubmit(m_graphicsQueue, 1, &submitInfo, fence) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to submit to queue.");
    }

    vkWaitForFences(m_device, 1, &fence, VK_TRUE, 0xffffffffffffffff);

    performDestruction(2);


    auto attachInfo = graph.outputAttachmentInfo(textureAverager["position"]);
    positionOut = copyImage(attachInfo.image,
                            texOpts.width, texOpts.height,
                            VK_IMAGE_ASPECT_COLOR_BIT,
                            VK_ACCESS_SHADER_READ_BIT,
                            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                            texOpts.debugLevel > 0 ? VK_IMAGE_USAGE_TRANSFER_SRC_BIT : 0,
                            true);

    attachInfo = graph.outputAttachmentInfo(textureAverager["normal"]);
    normalOut = copyImage(attachInfo.image,
                          texOpts.width, texOpts.height,
                          VK_IMAGE_ASPECT_COLOR_BIT,
                          VK_ACCESS_SHADER_READ_BIT,
                          VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                          texOpts.debugLevel > 0 ? VK_IMAGE_USAGE_TRANSFER_SRC_BIT : 0,
                          true);

    attachInfo = graph.outputAttachmentInfo(textureAverager["tangent"]);
    tangentOut = copyImage(attachInfo.image,
                           texOpts.width, texOpts.height,
                           VK_IMAGE_ASPECT_COLOR_BIT,
                           VK_ACCESS_SHADER_READ_BIT,
                           VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                           texOpts.debugLevel > 0 ? VK_IMAGE_USAGE_TRANSFER_SRC_BIT : 0,
                           true);
}

void Texturer::transitionAttachment(VkImage image,
                                    VkImageLayout currentLayout,
                                    VkImageLayout finalLayout,
                                    VkPipelineStageFlags srcStage,
                                    VkImageAspectFlags aspect,
                                    fs::path path,
                                    U32 width,
                                    U32 height,
                                    U32 channels)
{
    BufferAllocation imgOutBuf;

    bool writeOut = !path.empty() && width && height && channels;

    if (!writeOut && currentLayout == finalLayout) return;

    if (writeOut)
    {
        imgOutBuf = createBuffer(width * height * channels * 4,
                                 VK_BUFFER_USAGE_TRANSFER_DST_BIT,
                                 VMA_MEMORY_USAGE_GPU_TO_CPU);
        addDestructor(3, [&imgOutBuf, this](){ imgOutBuf.destroy(m_allocator); });
    }

    immediateExecute([image, currentLayout, finalLayout, srcStage, aspect,
                      width, height, writeOut, &imgOutBuf]
                     (VkCommandBuffer commandBuf)
    {
        VkImageMemoryBarrier barrier{};
        barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        barrier.srcAccessMask = VK_ACCESS_MEMORY_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT;
        barrier.oldLayout = currentLayout;
        barrier.newLayout = finalLayout;

        barrier.image = image;
        barrier.subresourceRange.aspectMask = aspect;
        barrier.subresourceRange.baseArrayLayer = 0;
        barrier.subresourceRange.layerCount = 1;
        barrier.subresourceRange.baseMipLevel = 0;
        barrier.subresourceRange.levelCount = 1;

        if (!writeOut)
        {
            vkCmdPipelineBarrier(commandBuf, srcStage,
                                 VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0,
                                 nullptr, 0, nullptr, 1, &barrier);
            return;
        }

        if (currentLayout != VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
        {
            barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;

            vkCmdPipelineBarrier(commandBuf, srcStage,
                                 VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0,
                                 nullptr, 0, nullptr, 1, &barrier);
        }
        VkBufferImageCopy copy{};
        copy.imageExtent = {width, height, 1};
        copy.imageOffset = {0, 0, 0};
        copy.imageSubresource.aspectMask = aspect;
        copy.imageSubresource.baseArrayLayer = 0;
        copy.imageSubresource.layerCount = 1;
        copy.imageSubresource.mipLevel = 0;
        vkCmdCopyImageToBuffer(commandBuf, image,
                               VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                               imgOutBuf.buffer, 1, &copy);

        if (finalLayout != VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
        {
            barrier.oldLayout = barrier.newLayout;
            barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
            barrier.newLayout = finalLayout;
            vkCmdPipelineBarrier(commandBuf, VK_PIPELINE_STAGE_TRANSFER_BIT,
                                 VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0,
                                 nullptr, 0, nullptr, 1, &barrier);
        }
    });
    if (writeOut)
    {
        if (channels != 4) throw std::logic_error("Non-4-channel data not yet supported for debug write-out.");

        void* imgOutData;
        vmaMapMemory(m_allocator, imgOutBuf.allocation, &imgOutData);
        writeImage(path, static_cast<float*>(imgOutData), width, height);
        vmaUnmapMemory(m_allocator, imgOutBuf.allocation);

        performDestruction(3);
    }
}

Image Texturer::copyImage(VkImage sourceImage,
                          U32 width, U32 height,
                          VkImageAspectFlags aspect,
                          VkAccessFlags access,
                          VkImageLayout newLayout,
                          VkImageUsageFlags extraFlags,
                          bool addToDestructionQueue)
{
    bool successful = false;

    VkImageCreateInfo imgCopyCreate =
        simpleImageCreateInfo(VK_FORMAT_R32G32B32A32_SFLOAT,
                              {width, height, 0},
                              extraFlags | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT);

    Image dstImage;
    dstImage.allocate(imgCopyCreate, 2, m_allocator, m_device);

    // Enqueue destruction in case of an exception.
    addDestructor(3, [&dstImage, &successful, this]()
    {
        if (!successful) dstImage.destroy(m_allocator, m_device);
    });

    VkImageCopy imgCopy{};
    imgCopy.srcOffset = {0, 0, 0};
    imgCopy.dstOffset = {0, 0, 0};
    imgCopy.srcSubresource.aspectMask = aspect;
    imgCopy.srcSubresource.baseArrayLayer = 0;
    imgCopy.srcSubresource.layerCount = 1;
    imgCopy.srcSubresource.mipLevel = 0;
    imgCopy.dstSubresource = imgCopy.srcSubresource;
    imgCopy.extent = {width, height, 1};


    VkImage src = sourceImage;
    VkImage dst = dstImage.image;

    immediateExecute([imgCopy, src, dst, aspect, access, newLayout]
                     (VkCommandBuffer commandBuf)
    {
        VkImageMemoryBarrier barrier{};
        barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;

        barrier.image = dst;
        barrier.subresourceRange.aspectMask = aspect;
        barrier.subresourceRange.baseArrayLayer = 0;
        barrier.subresourceRange.layerCount = 1;
        barrier.subresourceRange.baseMipLevel = 0;
        barrier.subresourceRange.levelCount = 1;

        vkCmdPipelineBarrier(commandBuf, VK_PIPELINE_STAGE_TRANSFER_BIT,
                             VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr,
                             0, nullptr, 1, &barrier);

        vkCmdCopyImage(commandBuf,
                       src, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                       dst, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                       1, &imgCopy);

        barrier.srcAccessMask = barrier.dstAccessMask;
        barrier.dstAccessMask = access;
        barrier.oldLayout = barrier.newLayout;
        barrier.newLayout = newLayout;

        vkCmdPipelineBarrier(commandBuf, VK_PIPELINE_STAGE_TRANSFER_BIT,
                             VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, nullptr,
                             0, nullptr, 1, &barrier);
    });

    successful = true;
    performDestruction(3);
    if (addToDestructionQueue)
    {
        addDestructor(1, [dstImage, this]()
        {
            Image constDestroyHack = dstImage;
            constDestroyHack.destroy(m_allocator, m_device);
        });
    }
    return dstImage;
}

QualityMapperInputs Texturer::extractQualityMapperInputs(const TexturerOptions& texOpts) const
{
    QualityMapperInputs qmInputs;
    qmInputs.qualityWeight = texOpts.qualityWeight;
    qmInputs.normalWeight = texOpts.normalWeight;
    qmInputs.distanceWeight = texOpts.distanceWeight;
    qmInputs.squaredDistanceWeight = texOpts.squaredDistanceWeight;

    qmInputs.cosAngleGoodCutoff = texOpts.cosineGoodCutoff;
    qmInputs.cosAngleBadCutoff = texOpts.cosineBadCutoff;
    qmInputs.cosAngleDiscard = texOpts.cosineDiscard;

    qmInputs.focusSDNearCutoff = texOpts.focusSDNearCutoff;
    qmInputs.focusSDFarCutoff = texOpts.focusSDFarCutoff;
    qmInputs.focusSDFarDiscard = texOpts.focusSDFarDiscard;
    qmInputs.focusWeight = texOpts.focusWeight;

    qmInputs.discardLessThanMean = texOpts.qualityDiscardParameter;
    qmInputs.qualityTransform = texOpts.qualityTransformFunction;
    qmInputs.qualityTransformParameter0 = texOpts.qualityTransformPar0;
    qmInputs.qualityTransformParameter1 = texOpts.qualityTransformPar1;

    return qmInputs;
}

void Texturer::runQualityMapper(const TexturerOptions& texOpts,
                                const Image& position,
                                const Image& normal,
                                Image& statsOut)
{
    auto qmInputs = extractQualityMapperInputs(texOpts);

    FrameGraph qualityMapperGraph;
    qualityMapperGraph.create(m_device, m_allocator);

    PassLayout passLayout;
    passLayout.width = texOpts.width;
    passLayout.height = texOpts.height;
    passLayout.layers = 1;
    passLayout.samples = VK_SAMPLE_COUNT_1_BIT;

    auto& confidencePass = qualityMapperGraph.addPass("confidence", passLayout, VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT);

    confidencePass.addInputAttachment("position", VK_FORMAT_R32G32B32A32_SFLOAT)
                  .addInputAttachment("normal", VK_FORMAT_R32G32B32A32_SFLOAT);

    confidencePass.addOutput("stats", VK_FORMAT_R32G32B32A32_SFLOAT, {}, VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT);
    confidencePass.markExternallyCleared(confidencePass["stats"],
                                         VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);

    // We cannot just set this to TRANSFER_SRC_OPTIMAL since we will render
    // using the graph several times. Instead, we will need to perform a
    // transition later on.
    qualityMapperGraph.markOutputUsed(confidencePass["stats"], VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_SRC_BIT);

    ExternalImageInfo imgInfo;
    imgInfo.initialLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    imgInfo.finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    imgInfo.view = position.view;
    qualityMapperGraph.markExternal(confidencePass["position"].asInput(), imgInfo);

    imgInfo.view = normal.view;
    qualityMapperGraph.markExternal(confidencePass["normal"].asInput(), imgInfo);

    qualityMapperGraph.compile(nullptr, 0, texOpts.debugLevel > 1);

    auto qmInputBuf = createBuffer(sizeof(QualityMapperInputs),
                                   VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                                   VMA_MEMORY_USAGE_CPU_TO_GPU);

    addDestructor(2, [&qmInputBuf, this]()
    {
        qmInputBuf.destroy(m_allocator);
    });

    GraphicsShader qualityMapperShader;
    if (!qualityMapperShader.createFromFile(texOpts.internalDataDirectory / "shaders/texturer-qualitymapper.json", m_device))
    {
        throw std::runtime_error("Failed to create quality mapper shader.");
    }

    VkDescriptorSetLayout qmDescriptorLayout = VK_NULL_HANDLE;
    VkDescriptorSet qmDescriptorSet = VK_NULL_HANDLE;

    DescriptorSetLayoutInfo qmDescriptorSetInfo;

    qualityMapperShader.info().appendLayoutInfo(qmDescriptorSetInfo, 0);

    auto qmLayoutCreate = qmDescriptorSetInfo.descriptorSetLayoutCreateInfo();

    if (vkCreateDescriptorSetLayout(m_device, &qmLayoutCreate, nullptr, &qmDescriptorLayout) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create descriptor set layout.");
    }
    addDestructor(2, [this, qmDescriptorLayout](){ vkDestroyDescriptorSetLayout(m_device, qmDescriptorLayout, nullptr); });

    if (!m_descAllocator.allocate(&qmDescriptorSet, &qmDescriptorLayout, 1))
    {
        throw std::runtime_error("Failed to create descriptor set.");
    }

    auto qmPipeline = createQualityMapperPipeline(texOpts.width,
                                                  texOpts.height,
                                                  qualityMapperShader,
                                                  confidencePass.vulkanPass().pass,
                                                  confidencePass.vulkanPass().subpass,
                                                  &qmDescriptorLayout,
                                                  1);
    addDestructor(2, [&qmPipeline]()
    {
        qmPipeline.destroy();
    });

    confidencePass.setCallback([qmPipeline, qmDescriptorSet](VkCommandBuffer cmdbuf)
    {
        vkCmdBindPipeline(cmdbuf, VK_PIPELINE_BIND_POINT_GRAPHICS, qmPipeline.pipeline);
        vkCmdBindDescriptorSets(cmdbuf, VK_PIPELINE_BIND_POINT_GRAPHICS, qmPipeline.layout, 0, 1, &qmDescriptorSet, 0, nullptr);
        vkCmdDraw(cmdbuf, 3, 1, 0, 0);
    });

    VkSampler linearlinearSampler = VK_NULL_HANDLE;
    if (m_samplerCache.getSampler(&linearlinearSampler,
                                  VK_FILTER_LINEAR,
                                  VK_FILTER_LINEAR,
                                  VK_SAMPLER_MIPMAP_MODE_NEAREST,
                                  VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT,
                                  0.f) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to acquire sampler.");
    }



    {
        std::vector<VkWriteDescriptorSet> descSetWrite;
        U32 count = 0;
        confidencePass.writeDescriptorSet(qmDescriptorSet, &count, nullptr);
        descSetWrite.resize(count);
        confidencePass.writeDescriptorSet(qmDescriptorSet, &count, descSetWrite.data());
        if (count)
        {
            vkUpdateDescriptorSets(m_device, count, descSetWrite.data(), 0, nullptr);
        }
    }

    {
        auto attachmentInfo = qualityMapperGraph.outputAttachmentInfo(confidencePass["stats"]);
        VkClearColorValue ccv;
        ccv.float32[0] = 0;
        ccv.float32[1] = 0;
        ccv.float32[2] = 0;
        ccv.float32[3] = 0;
        clearImage(attachmentInfo.image,
                   VK_IMAGE_LAYOUT_UNDEFINED,
                   VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                   0,
                   VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
                   ccv);
    }


    U32 qmRenderCount = 0;
    while (!m_dmapLoadIDs.empty() || m_currentTransferID != ResourceLoader::invalidResource)
    {
        if (texOpts.debugLevel > 1)
        {
            std::cerr << "Quality mapping image " << ++qmRenderCount << "/" << m_dmapLoadInfo.size() << ": ";
        }
        else
        {
            std::cout << "\r"
                      << progressbar(++qmRenderCount / (F32)m_dmapLoadInfo.size(),
                                     80, "Quality mapping image", "", true)
                      << std::flush;
        }

        enqueueDMAPLoads();

        auto image = acquireVulkanDMAP(m_dmapLoadIDs);


        VkDescriptorImageInfo dinfo[3];
        dinfo[0].imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        dinfo[0].imageView = image.normalDepthMap.view;
        dinfo[0].sampler = linearlinearSampler;

        dinfo[1] = dinfo[0];
        dinfo[1].imageView = image.confidenceMap.view;

        dinfo[2] = dinfo[0];
        dinfo[2].imageView = image.qualityMap.view;

        std::vector<VkWriteDescriptorSet> descSetWrite;
        for (U32 i = 0; i < 3; ++i)
        {
            descSetWrite.push_back(qmDescriptorSetInfo.writeDescriptorSetInfo(2+i, qmDescriptorSet));
            descSetWrite.back().pImageInfo = &dinfo[i];
        }
        descSetWrite.push_back(qmDescriptorSetInfo.writeDescriptorSetInfo(5, qmDescriptorSet));

        VkDescriptorBufferInfo bufinfo{};
        bufinfo.buffer = qmInputBuf.buffer;
        bufinfo.offset = 0;
        bufinfo.range = sizeof(QualityMapperInputs);
        descSetWrite.back().pBufferInfo = &bufinfo;

        vkUpdateDescriptorSets(m_device, descSetWrite.size(), descSetWrite.data(),
                               0, nullptr);

        updateQualityMapperInputs(qmInputs, image, qmInputBuf.allocation);

        VkImageMemoryBarrier layoutTransitionBarriers[3];
        U32 barrierCount = writeBarriers(layoutTransitionBarriers, 3, image);

        immediateExecute([&qualityMapperGraph, barrierCount, &layoutTransitionBarriers]
        (VkCommandBuffer commandBuf)
        {
            vkCmdPipelineBarrier(commandBuf, VK_PIPELINE_STAGE_TRANSFER_BIT,
                                 VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
                                 0, nullptr, 0, nullptr,
                                 barrierCount, layoutTransitionBarriers);
            qualityMapperGraph.execute(commandBuf);
        });

        image.normalDepthMap.destroy(m_allocator, m_device);
        image.confidenceMap.destroy(m_allocator, m_device);
        image.qualityMap.destroy(m_allocator, m_device);
        image.colourImage.destroy(m_allocator, m_device);

        if (texOpts.debugLevel > 1) std::cerr << "Done!\n";
    }
    if (texOpts.debugLevel <= 1) std::cout << std::endl;

    auto attachInfo = qualityMapperGraph.outputAttachmentInfo(confidencePass["stats"]);
    transitionAttachment(attachInfo.image,
                         attachInfo.finalLayout,
                         VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
    statsOut = copyImage(attachInfo.image,
                         texOpts.width, texOpts.height,
                         VK_IMAGE_ASPECT_COLOR_BIT,
                         VK_ACCESS_SHADER_READ_BIT,
                         VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                         texOpts.debugLevel > 0 ? VK_IMAGE_USAGE_TRANSFER_SRC_BIT : 0,
                         true);

    performDestruction(2);
}

void Texturer::runColourRenderer(const TexturerOptions& texOpts,
                                 const Image& position,
                                 const Image& normal,
                                 const Image& tangent,
                                 const Image& stats,
                                 Image& colourOut,
                                 Image& normalOut)
{
    auto qmInputs = extractQualityMapperInputs(texOpts);

    FrameGraph colourRenderGraph(m_device, m_allocator);

    PassLayout passLayout;
    passLayout.width = texOpts.width;
    passLayout.height = texOpts.height;
    passLayout.layers = 1;
    passLayout.samples = VK_SAMPLE_COUNT_1_BIT;

    auto& colourPass = colourRenderGraph.addPass("colourRenderer", passLayout, VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT);

    colourPass.addInputAttachment("position", VK_FORMAT_R32G32B32A32_SFLOAT)
              .addInputAttachment("normal", VK_FORMAT_R32G32B32A32_SFLOAT)
              .addInputAttachment("tangent", VK_FORMAT_R32G32B32A32_SFLOAT)
              .addInputAttachment("stats", VK_FORMAT_R32G32B32A32_SFLOAT)
              .addOutput("colour", VK_FORMAT_R32G32B32A32_SFLOAT, {}, VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT)
              .addOutput("normal", VK_FORMAT_R32G32B32A32_SFLOAT, {}, VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT);

    colourPass.markExternallyCleared(colourPass["colour"],
                                     VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
    colourRenderGraph.markOutputUsed(colourPass["colour"], VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_SRC_BIT);
    colourPass.markExternallyCleared(colourPass["normal"],
                                     VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
    colourRenderGraph.markOutputUsed(colourPass["normal"], VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_SRC_BIT);

    ExternalImageInfo imgInfo;
    imgInfo.initialLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    imgInfo.finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    imgInfo.view = stats.view;

    colourRenderGraph.markExternal(colourPass["stats"].asInput(), imgInfo);

    imgInfo.view = position.view;
    colourRenderGraph.markExternal(colourPass["position"].asInput(), imgInfo);

    imgInfo.view = normal.view;
    colourRenderGraph.markExternal(colourPass["normal"].asInput(), imgInfo);

    imgInfo.view = tangent.view;
    colourRenderGraph.markExternal(colourPass["tangent"].asInput(), imgInfo);

    colourRenderGraph.compile(nullptr, 0, texOpts.debugLevel > 1);


    auto qmInputBuf = createBuffer(sizeof(QualityMapperInputs),
                                   VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                                   VMA_MEMORY_USAGE_CPU_TO_GPU);

    addDestructor(2, [&qmInputBuf, this]()
    {
        qmInputBuf.destroy(m_allocator);
    });

    GraphicsShader colourRenderShader;
    if (!colourRenderShader.createFromFile(texOpts.internalDataDirectory / "shaders/texturer-colourrenderer.json", m_device))
    {
        throw std::runtime_error("Failed to create colour render shader.");
    }

    VkDescriptorSetLayout crDescriptorLayout = VK_NULL_HANDLE;
    VkDescriptorSet crDescriptorSet = VK_NULL_HANDLE;

    DescriptorSetLayoutInfo crDescriptorSetInfo;

    colourRenderShader.info().appendLayoutInfo(crDescriptorSetInfo, 0);

    auto crLayoutCreate = crDescriptorSetInfo.descriptorSetLayoutCreateInfo();

    if (vkCreateDescriptorSetLayout(m_device, &crLayoutCreate, nullptr, &crDescriptorLayout) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create descriptor set layout.");
    }
    addDestructor(2, [this, crDescriptorLayout]{ vkDestroyDescriptorSetLayout(m_device, crDescriptorLayout, nullptr); });

    if (!m_descAllocator.allocate(&crDescriptorSet, &crDescriptorLayout, 1))
    {
        throw std::runtime_error("Failed to create descriptor set.");
    }

    auto crPipeline = createQualityMapperPipeline(texOpts.width,
                                                  texOpts.height,
                                                  colourRenderShader,
                                                  colourPass.vulkanPass().pass,
                                                  colourPass.vulkanPass().subpass,
                                                  &crDescriptorLayout,
                                                  1,
                                                  2,
                                                  VK_BLEND_OP_ADD);

    addDestructor(2, [&crPipeline]{ crPipeline.destroy(); });

    colourPass.setCallback([crPipeline, crDescriptorSet](VkCommandBuffer commandBuf)
    {
        vkCmdBindPipeline(commandBuf, VK_PIPELINE_BIND_POINT_GRAPHICS, crPipeline.pipeline);
        vkCmdBindDescriptorSets(commandBuf, VK_PIPELINE_BIND_POINT_GRAPHICS, crPipeline.layout, 0, 1, &crDescriptorSet, 0, nullptr);
        vkCmdDraw(commandBuf, 3, 1, 0, 0);
    });

    {
        std::vector<VkWriteDescriptorSet> descSetWrite;
        U32 count = 0;
        colourPass.writeDescriptorSet(crDescriptorSet, &count, nullptr);
        descSetWrite.resize(count);
        colourPass.writeDescriptorSet(crDescriptorSet, &count, descSetWrite.data());
        if (count)
        {
            vkUpdateDescriptorSets(m_device, count, descSetWrite.data(), 0, nullptr);
        }
        auto attachmentInfo = colourRenderGraph.outputAttachmentInfo(colourPass["colour"]);
        VkClearColorValue ccv;
        ccv.float32[0] = 0;
        ccv.float32[1] = 0;
        ccv.float32[2] = 0;
        ccv.float32[3] = 0;
        clearImage(attachmentInfo.image,
                   VK_IMAGE_LAYOUT_UNDEFINED,
                   VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                   0,
                   VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
                   ccv);
        attachmentInfo = colourRenderGraph.outputAttachmentInfo(colourPass["normal"]);
        clearImage(attachmentInfo.image,
                   VK_IMAGE_LAYOUT_UNDEFINED,
                   VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                   0,
                   VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
                   ccv);
    }

    VkSampler linearlinearSampler = VK_NULL_HANDLE;
    if (m_samplerCache.getSampler(&linearlinearSampler,
                                  VK_FILTER_LINEAR,
                                  VK_FILTER_LINEAR,
                                  VK_SAMPLER_MIPMAP_MODE_NEAREST,
                                  VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT,
                                  0.f) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to acquire sampler.");
    }

    m_dmapLoadOpts.parseOptions["loadColourImage"] = true;
    pushAllDMAPIndices();

    U32 crRenderCount = 0;
    enqueueDMAPLoads();
    while (!m_dmapLoadIDs.empty() || m_currentTransferID != ResourceLoader::invalidResource)
    {
        if (texOpts.debugLevel > 1)
        {
            std::cerr << "Colour mapping image " << ++crRenderCount << "/" << m_dmapLoadInfo.size() << ": ";
        }
        else
        {
            std::cout << "\r"
                      << progressbar(++crRenderCount / (F32)m_dmapLoadInfo.size(),
                                     80, "Colour  mapping image", "", true)
                      << std::flush;
        }

        enqueueDMAPLoads();

        auto image = acquireVulkanDMAP(m_dmapLoadIDs);

        VkDescriptorImageInfo dinfo[4];
        dinfo[0].imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        dinfo[0].imageView = image.normalDepthMap.view;
        dinfo[0].sampler = linearlinearSampler;

        dinfo[1] = dinfo[0];
        dinfo[1].imageView = image.confidenceMap.view;

        dinfo[2] = dinfo[0];
        dinfo[2].imageView = image.qualityMap.view;

        dinfo[3] = dinfo[0];
        dinfo[3].imageView = image.colourImage.view;

        std::vector<VkWriteDescriptorSet> descSetWrite;
        for (U32 i = 0; i < 4; ++i)
        {
            descSetWrite.push_back(crDescriptorSetInfo.writeDescriptorSetInfo(4+i, crDescriptorSet));
            descSetWrite.back().pImageInfo = &dinfo[i];
        }
        descSetWrite.push_back(crDescriptorSetInfo.writeDescriptorSetInfo(8, crDescriptorSet));

        VkDescriptorBufferInfo bufinfo{};
        bufinfo.buffer = qmInputBuf.buffer;
        bufinfo.offset = 0;
        bufinfo.range = sizeof(QualityMapperInputs);
        descSetWrite.back().pBufferInfo = &bufinfo;

        vkUpdateDescriptorSets(m_device, descSetWrite.size(), descSetWrite.data(),
                               0, nullptr);

        updateQualityMapperInputs(qmInputs, image, qmInputBuf.allocation);

        VkImageMemoryBarrier layoutTransitionBarriers[4];
        U32 barrierCount = writeBarriers(layoutTransitionBarriers, 4, image);

        immediateExecute([&colourRenderGraph, barrierCount, &layoutTransitionBarriers]
        (VkCommandBuffer commandBuf)
        {
            vkCmdPipelineBarrier(commandBuf, VK_PIPELINE_STAGE_TRANSFER_BIT,
                                 VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
                                 0, nullptr, 0, nullptr,
                                 barrierCount, layoutTransitionBarriers);
            colourRenderGraph.execute(commandBuf);
        });

        image.normalDepthMap.destroy(m_allocator, m_device);
        image.confidenceMap.destroy(m_allocator, m_device);
        image.qualityMap.destroy(m_allocator, m_device);
        image.colourImage.destroy(m_allocator, m_device);

        if (texOpts.debugLevel > 1) std::cerr << "Done!\n";
    }
    if (texOpts.debugLevel <= 1) std::cout << std::endl;
    auto attachInfo = colourRenderGraph.outputAttachmentInfo(colourPass["colour"]);
    transitionAttachment(attachInfo.image,
                         attachInfo.finalLayout,
                         VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
    colourOut = copyImage(attachInfo.image,
                          texOpts.width, texOpts.height,
                          VK_IMAGE_ASPECT_COLOR_BIT,
                          VK_ACCESS_SHADER_READ_BIT,
                          VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                          VK_IMAGE_USAGE_TRANSFER_SRC_BIT,
                          false);
    attachInfo = colourRenderGraph.outputAttachmentInfo(colourPass["normal"]);
    transitionAttachment(attachInfo.image,
                         attachInfo.finalLayout,
                         VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
    normalOut = copyImage(attachInfo.image,
                          texOpts.width, texOpts.height,
                          VK_IMAGE_ASPECT_COLOR_BIT,
                          VK_ACCESS_SHADER_READ_BIT,
                          VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                          VK_IMAGE_USAGE_TRANSFER_SRC_BIT,
                          false);
    performDestruction(2);
}

void copySubtexture(const kraken::image::Image& src,
                    kraken::image::Image& dst,
                    uvec2 offset)
{
    if (dst.width() < src.width() + offset.x
        || dst.height() < src.height() + offset.y)
    {
        throw std::logic_error("Invalid subtexture coordinates.");
    }
    if (src.colourType() != dst.colourType())
    {
        throw std::logic_error("Source and destination for subtexture copy have"
                               " different colour types.");
    }
    for (U32 y = 0; y < src.height(); ++y)
    {
        for (U32 x = 0; x < src.width(); ++x)
        {
            dst.setPixel(x+offset.x, y+offset.y, src.getPixel(x, y));
        }
    }
}

void copySubtexture(const FloatImage& src,
                    FloatImage& dst,
                    uvec2 offset)
{
    if (dst.width() < src.width() + offset.x
        || dst.height() < src.height() + offset.y)
    {
        throw std::logic_error("Invalid subtexture coordinates.");
    }
    if (src.channels() != dst.channels())
    {
        throw std::logic_error("Source and destination for subtexture copy have"
                               " different number of channels.");
    }
    for (U32 y = 0; y < src.height(); ++y)
    {
        for (U32 x = 0; x < src.width(); ++x)
        {
            dst.setPixel(x+offset.x, y+offset.y, src.getPixel(x, y));
        }
    }
}

void Texturer::expandTexturesByAlpha(TexturerOutput& output) const
{
    auto alphaCopy = output.alphaImage;
    expandByAlpha(output.colourImage, alphaCopy);

    alphaCopy = output.alphaImage;
    expandByAlpha(output.normalImage, alphaCopy);

    /*
    alphaCopy = output.alphaImage;
    expandByAlpha(output.displacementImage, alphaCopy);*/
}

TexturerOutput Texturer::run(TexturerOptions opts,
                             U32 xTiles,
                             U32 yTiles)
{
    init(opts);

    if (!xTiles) xTiles = 1;
    if (!yTiles) yTiles = 1;

    U32 maxWidth = m_deviceLimits.maxFramebufferWidth;
    U32 maxHeight = m_deviceLimits.maxFramebufferHeight;

    if (maxWidth < 1024 || maxHeight < 1024)
    {
        throw std::logic_error("Vulkan implementation is highly non-conformant "
                               "and does not allow framebuffers of size "
                               "1024x1024 or larger.");
    }

    U32 tileWidth = opts.width / xTiles;
    U32 tileHeight = opts.height / yTiles;

    if (!tileWidth || !tileHeight)
    {
        throw std::logic_error("Tile subdivision is too fine -- average tile "
                               "size is less than 1 pixel along at least one "
                               "axis.");
    }

    if (tileWidth > maxWidth)
    {
        tileWidth = maxWidth;
        xTiles = opts.width / tileWidth + (opts.width % tileWidth != 0);
    }
    if (tileHeight > maxHeight)
    {
        tileHeight = maxHeight;
        xTiles = opts.height / tileHeight + (opts.height % tileHeight != 0);
    }

    TexturerOutput output;

    //FloatImage displacementImage(opts.width, opts.height, 1);

    if (xTiles == 1 && yTiles == 1)
    {
        auto tileOutput = renderTile(opts, true);
        output.colourImage = std::move(tileOutput.colourImage);
        output.normalImage = std::move(tileOutput.normalImage);
        output.alphaImage = std::move(tileOutput.alphaImage);
        //displacementImage = std::move(tileOutput.displacementImage);
    }
    else
    {
        output.colourImage.resize(opts.width, opts.height, opts.colourFormat);
        output.normalImage.resize(opts.width, opts.height, opts.normalFormat);
        output.alphaImage.resize(opts.width, opts.height, opts.alphaFormat);

        vec2 increment = opts.topRight - opts.bottomLeft;

        increment.x = (increment.x * static_cast<F32>(tileWidth)) / static_cast<F32>(opts.width);
        increment.y = (increment.y * static_cast<F32>(tileHeight)) / static_cast<F32>(opts.height);

        auto tileOpts = opts;

        tileOpts.width = tileWidth;
        tileOpts.height = tileHeight;
        for (U32 y = 0; y < yTiles; ++y)
        {
            for (U32 x = 0; x < xTiles; ++x)
            {
                std::cout << "Rendering tile ("
                          << x << ", " << y << ")... ("
                          << 1 + y*xTiles + x << " of " << xTiles * yTiles
                          << ")." << std::endl;

                tileOpts.bottomLeft.x = opts.bottomLeft.x+x*increment.x;
                tileOpts.bottomLeft.y = opts.bottomLeft.y+y*increment.y;

                tileOpts.topRight.x = opts.bottomLeft.x+(x+1)*increment.x;
                tileOpts.topRight.y = opts.bottomLeft.y+(y+1)*increment.y;

                auto tileResult = renderTile(tileOpts, false);

                uvec2 offset{x * tileWidth, y * tileHeight};

                copySubtexture(tileResult.colourImage, output.colourImage, offset);
                copySubtexture(tileResult.normalImage, output.normalImage, offset);
                //copySubtexture(tileResult.displacementImage, displacementImage, offset);
                copySubtexture(tileResult.alphaImage, output.alphaImage, offset);
            }
        }
    }

    /*
    // Handle displacement image.
    F32 minDisplacement = std::numeric_limits<F32>::max();
    F32 maxDisplacement = std::numeric_limits<F32>::min();

    for (U32 y = 0; y < opts.height; ++y)
    {
        for (U32 x = 0; x < opts.width; ++x)
        {
            F32 d = displacementImage.getPixel(x, y).r;
            minDisplacement = std::min(minDisplacement, d);
            maxDisplacement = std::max(maxDisplacement, d);
        }
    }


    auto displacementScale = maxDisplacement - minDisplacement;
    if (displacementScale > 1e-8f) displacementScale = 1./displacementScale;
    else displacementScale = 0.f;

    output.metadata["displacement"]["min"] = minDisplacement;
    output.metadata["displacement"]["max"] = maxDisplacement;

    output.displacementImage.resize(opts.width, opts.height, opts.displacementFormat);
    output.displacementImage.fill([&displacementImage, minDisplacement, displacementScale](U32 x, U32 y)
    {
        F32 d = displacementImage.getPixel(x, y).r;
        d = (d-minDisplacement) * displacementScale;
        return vec4{d, d, d, d};
    });*/

    if (opts.expandTextures) expandTexturesByAlpha(output);
    return output;
}

TileOutput Texturer::renderTile(TexturerOptions texOpts, bool isOnlyTile)
{
    pushAllDMAPIndices();

    enqueueDMAPLoads();

    Image position, normal, tangent, stats;
    drawMeshToTexture(texOpts, position, normal, tangent);

    runQualityMapper(texOpts, position, normal, stats);

    Image colourOut, normalOut;
    runColourRenderer(texOpts, position, normal, tangent, stats, colourOut, normalOut);

    performDestruction(1);

    m_descAllocator.resetPools();

    addDestructor(1, [this, colourOut, normalOut]()
    {
        auto hack = colourOut;
        hack.destroy(m_allocator, m_device);
        hack = normalOut;
        hack.destroy(m_allocator, m_device);
    });


    auto imgOutBuf = createBuffer(texOpts.width * texOpts.height * 4 * sizeof(F32),
                                  VK_BUFFER_USAGE_TRANSFER_DST_BIT,
                                  VMA_MEMORY_USAGE_GPU_TO_CPU);
    addDestructor(2, [&imgOutBuf, this](){ imgOutBuf.destroy(m_allocator); });

    VkBufferImageCopy imgCopy{};
    imgCopy.imageExtent = {texOpts.width, texOpts.height, 1};
    imgCopy.imageOffset = {0, 0, 0};
    imgCopy.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    imgCopy.imageSubresource.baseArrayLayer = 0;
    imgCopy.imageSubresource.layerCount = 1;
    imgCopy.imageSubresource.mipLevel = 0;


    FloatImage sampleCount(texOpts.width, texOpts.height, 1);

    TileOutput output;
    FloatImage floatImg(texOpts.width, texOpts.height, 4);

    // Copy colour image
    immediateExecute([imgCopy, colourOut, imgOutBuf](VkCommandBuffer commandBuf)
    {
        vkCmdCopyImageToBuffer(commandBuf, colourOut.image,
                               VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                               imgOutBuf.buffer, 1, &imgCopy);
    });

    void* imgOutData;
    vmaMapMemory(m_allocator, imgOutBuf.allocation, &imgOutData);
    std::memcpy(floatImg.data(), imgOutData,
                texOpts.width * texOpts.height * 4 * sizeof(F32));
    vmaUnmapMemory(m_allocator, imgOutBuf.allocation);

    for (U32 y = 0; y < floatImg.height(); ++y)
    {
        for (U32 x = 0; x < floatImg.width(); ++x)
        {
            sampleCount.setPixel(x, y, floatImg.getPixel(x, y).a);
        }
    }

    normaliseByImage(floatImg, sampleCount);

    // Copy alpha image (after normalisation; to ensure that fragments discarded
    // during normalisation (i.e. those with *very* low total quality) are not
    // counted as being in the image).
    U64 alphaMask = 0xffffffffffffffff;
    if (texOpts.alphaFormat.bitDepth < 64)
    {
        alphaMask = static_cast<U64>(1) << static_cast<U64>(texOpts.alphaFormat.bitDepth);
        alphaMask -= 1;
    }
    output.alphaImage.resize(texOpts.width, texOpts.height, texOpts.alphaFormat);
    output.alphaImage.fill([&floatImg, alphaMask](U32 x, U32 y)
    {
        return alphaMask * (floatImg.getPixel(x, y).a > 0.f);
    });

    output.colourImage.resize(texOpts.width, texOpts.height, texOpts.colourFormat);
    output.colourImage.fill([&floatImg](U32 x, U32 y)
    {
        return floatImg.getPixel(x, y);
    });

    // Copy normal image
    immediateExecute([imgCopy, normalOut, imgOutBuf](VkCommandBuffer commandBuf)
    {
        vkCmdCopyImageToBuffer(commandBuf, normalOut.image,
                               VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                               imgOutBuf.buffer, 1, &imgCopy);
    });

    vmaMapMemory(m_allocator, imgOutBuf.allocation, &imgOutData);
    std::memcpy(floatImg.data(), imgOutData,
                texOpts.width * texOpts.height * 4 * sizeof(F32));
    vmaUnmapMemory(m_allocator, imgOutBuf.allocation);

    normaliseByImage(floatImg, sampleCount);

    //output.displacementImage.resize(floatImg.width(), floatImg.height(), 1);

    output.normalImage.resize(texOpts.width, texOpts.height, texOpts.normalFormat);
    output.normalImage.fill([&floatImg, &output](U32 x, U32 y)
    {
        auto n = floatImg.getPixel(x, y);

        // Hack: Set pixels of displacement image while filling the normal image
        // -- that gives us iteration for free!
        //output.displacementImage.setPixel(x, y, n.w);

        vec3 normal{n.x, n.y, n.z};
        if (dot(normal, normal) >= 1e-8f)
        {
            normal = normalise(normal);
            return 0.5f*vec4{normal.x, normal.y, normal.z, 1.f} + 0.5f*vec4{1.f, 1.f, 1.f, 1.f};
        }
        else
        {
            return vec4{0.f, 0.f, 0.f, 0.f};
        }
    });
    performDestruction(1);

    if (texOpts.relaxIterations)
    {
        --texOpts.relaxIterations;
        texOpts.cosineGoodCutoff *= texOpts.cosineGoodRelaxFactor;
        texOpts.cosineBadCutoff *= texOpts.cosineBadRelaxFactor;
        texOpts.cosineDiscard *= texOpts.cosineDiscardRelaxFactor;
        texOpts.normalWeight *= texOpts.normalWeightRelaxFactor;
        auto newOutput = renderTile(texOpts, isOnlyTile);
        for (size_t y = 0; y < output.alphaImage.height(); ++y)
        {
            for (size_t x = 0; x < output.alphaImage.width(); ++x)
            {
                if (!output.alphaImage.getPixel(x, y))
                {
                    output.alphaImage.setPixel(x, y, newOutput.alphaImage.getPixel(x, y));
                    output.normalImage.setPixel(x, y, newOutput.normalImage.getPixel(x, y));
                    output.colourImage.setPixel(x, y, newOutput.colourImage.getPixel(x, y));
                    //output.displacementImage.setPixel(x, y, newOutput.displacementImage.getPixel(x, y));
                }
            }
        }
    }
    return output;
}

bool isValidBitDepth(U8 bits)
{
    return bits == 1 || bits == 2 || bits == 4 || bits == 8 || bits == 16;
}

void assertValidBitDepthCombination(U8 bits, U8 channels,
                                    U8 minChannels = 1, U8 maxChannels = 4,
                                    std::string imageName = "Images")
{
    if (!isValidBitDepth(bits))
    {
        throw std::runtime_error("Bit depth must be either 1, 2, 4, 8 or 16.");
    }
    if (channels == 3 && bits < 8)
    {
        throw std::runtime_error("Triple-channel images must have bit depth >= 8.");
    }
    if (channels < minChannels || channels > 4)
    {
        throw std::runtime_error(imageName + " must have between "
                                 + std::to_string(minChannels)
                                 + " and "
                                 + std::to_string(maxChannels)
                                 + " channels.");
    }
}

bool isValidExportFormat(std::string format)
{
    return format == "png" || format == "pnm" || format == "pgm" || format == "pbm" || format == "bmp";
}

void assertValidExportFormat(const std::string& format)
{
    if (!isValidExportFormat(format))
    {
        throw std::runtime_error("'" + format
                                 + "' is not a valid export format. "
                                 "Valid choices are 'png', 'pnm' and 'bmp'.");
    }
}

bool writeImage(const kraken::image::Image& image,
                fs::path path,
                std::string format)
{
    std::ofstream output(path);
    if (!output.is_open()) return false;
    if (!image.write(output, format)) return false;
    return true;
}

// Adds number of errors to errorCountOut.
// This only writes something if path is non-empty; if errormsg != "", then it
// also writes errormsg to stderr in case of an error.
void writeImageWorkerMain(const kraken::image::Image& image,
                          fs::path path,
                          std::string format,
                          std::string errormsg,
                          std::atomic<U32>& errorCountOut)
{
    if (path.empty()) return;
    if (!writeImage(image, path, format))
    {
        std::cerr << errormsg;
        ++errorCountOut;
    }
}

void streamPNGWriter(const kraken::image::Image& image,
                     std::ostream& output,
                     std::atomic<U32>& errorCountOut)
{
    if (!image.write(output, "png"))
    {
        ++errorCountOut;
    }
}

// Todo: Fix behaviour when running on big-endian systems.
void writeGLTFWorkerMain(const Mesh& mesh,
                         const TexturerOutput& output,
                         fs::path path)
{
    struct Vertex
    {
        vec3 position;
        vec3 normal;
        vec2 texcoord;
    };
    static_assert(sizeof(Vertex) == 4*(3+3+2), "Vertex is not tightly packed.");
    static_assert(sizeof(Vertex[2]) == 2*sizeof(Vertex), "Vertex has padding.");

    std::string meshName = "Mesh";
    std::string sceneName = "Scene";
    if (path.empty()) return;

    std::stringstream colImgStream, normalImgStream;
    std::atomic<U32> writeErrCount{0};
    std::vector<std::thread> imageWriters;
    imageWriters.emplace_back(streamPNGWriter, std::cref(output.colourImage), std::ref(colImgStream), std::ref(writeErrCount));
    imageWriters.emplace_back(streamPNGWriter, std::cref(output.normalImage), std::ref(normalImgStream), std::ref(writeErrCount));

    //U32 attrCount = mesh.attributeDescription()->size();

    U64 stride = sizeof(F32)*(3+3+2);
    U64 attributeCount = mesh.vertexBuffer(0).count;
    U64 indexCount = mesh.indexBuffer(0).count;
    U64 indexStride = mesh.indexBuffer(0).stride;


    if (attributeCount*3*sizeof(F32) > 0xffffffffull)
    {
        throw std::domain_error("Mesh contains too many attributes to export "
                                "to a single glb container.");
    }
    if (indexCount*indexStride > 0xffffffffull)
    {
        throw std::domain_error("Mesh contains too many indices to export "
                                "to a single glb container.");
    }

    bool canInterleave = attributeCount*stride <= 0xffffffffull;

    kraken::JSONValue posAccessor;
    posAccessor["bufferView"] = 0;
    posAccessor["componentType"] = 5126; // FLOAT
    posAccessor["count"] = attributeCount;
    posAccessor["type"] = "VEC3";

    auto normalAccessor = posAccessor;
    normalAccessor["bufferView"] = 1;

    auto texcoordAccessor = posAccessor;
    texcoordAccessor["bufferView"] = 2;
    texcoordAccessor["type"] = "VEC2";

    kraken::JSONValue indexAccessor;
    indexAccessor["bufferView"] = 3;
    indexAccessor["componentType"] = indexStride == 4 ? 5125 : (indexStride == 2 ? 5123 : 5121);
    indexAccessor["count"] = indexCount;
    indexAccessor["type"] = "SCALAR";

    kraken::JSONValue posBufferView;
    posBufferView["buffer"] = 0;
    posBufferView["byteLength"] = attributeCount * stride;
    posBufferView["byteStride"] = sizeof(Vertex);

    auto normalBufferView = posBufferView;
    auto texcoordBufferView = posBufferView;

    if (canInterleave)
    {
        normalBufferView["byteOffset"] = sizeof(F32)*3;
        normalBufferView["byteLength"] = (U64)normalBufferView["byteLength"] - (U64)normalBufferView["byteOffset"];
        texcoordBufferView["byteOffset"] = sizeof(F32)*(3+3);
        texcoordBufferView["byteLength"] = (U64)texcoordBufferView["byteLength"] - (U64)texcoordBufferView["byteOffset"];
    }
    else
    {
        throw std::logic_error("Non-interleaved glb export not yet implemented.");/*
        normalBufferView["buffer"] = 1;
        texcoordBufferView["buffer"] = 2;
        posBufferView["byteLength"] = attributeCount * 3 * sizeof(F32);
        normalBufferView["byteLength"] = attributeCount * 3 * sizeof(F32);
        texcoordBufferView["byteLength"] = attributeCount * 2 * sizeof(F32);*/
    }

    kraken::JSONValue indexBufferView;
    indexBufferView["buffer"] = 0;
    indexBufferView["byteLength"] = indexCount * indexStride;
    indexBufferView["byteOffset"] = posBufferView["byteLength"];

    kraken::JSONValue gltf;
    gltf["asset"]["version"] = "2.0";
    gltf["scene"] = 0;
    gltf["scenes"][0]["name"] = sceneName;
    gltf["scenes"][0]["nodes"][0] = 0;
    gltf["nodes"][0]["mesh"] = 0;
    gltf["nodes"][0]["name"] = meshName;

    gltf["meshes"][0]["name"] = meshName;
    auto& meshPrimitives = gltf["meshes"][0]["primitives"][0];
    meshPrimitives["attributes"]["POSITION"] = 0;
    meshPrimitives["attributes"]["NORMAL"] = 1;
    meshPrimitives["attributes"]["TEXCOORD_0"] = 2;
    meshPrimitives["indices"] = 3;
    meshPrimitives["material"] = 0;

    gltf["accessors"] = kraken::JSONValue::CreateArray({posAccessor, normalAccessor, texcoordAccessor, indexAccessor});

    gltf["bufferViews"] = kraken::JSONValue::CreateArray({posBufferView, normalBufferView, texcoordBufferView, indexBufferView});

    U32 bufferedElementCount = 128*1024;
    U32 countMask = bufferedElementCount - 1;

    // Hard-coded since attribute names are actually not set yet.
    U32 positionOffset = 0, normalOffset = 12, texcoordOffset = 24;
    /*
    for (U32 i = 0; i < attrCount; ++i)
    {
        if (mesh.attributeNames()->at(i) == "position")
        {
            positionOffset = mesh.attributeDescription()->at(i).offset;
        }
        else if (mesh.attributeNames()->at(i) == "normal")
        {
            normalOffset = mesh.attributeDescription()->at(i).offset;
        }
        else if (mesh.attributeNames()->at(i) == "texcoord")
        {
            texcoordOffset = mesh.attributeDescription()->at(i).offset;
        }
    }*/

    const U8* srcbuf = reinterpret_cast<const U8*>(mesh.vertexBuffer(0).buffer);
    auto srcStride = mesh.vertexBuffer(0).stride;

    vec3 minPos, maxPos;

    for (U64 currVertex = 0; currVertex < attributeCount; ++currVertex)
    {
        vec3 pos;
        std::memcpy(&pos, srcbuf + currVertex * srcStride + positionOffset, sizeof(pos));
        if (!currVertex) minPos = (maxPos = pos);
        minPos.x = std::min(minPos.x, pos.x);
        minPos.y = std::min(minPos.y, pos.y);
        minPos.z = std::min(minPos.z, pos.z);
        maxPos.x = std::max(maxPos.x, pos.x);
        maxPos.y = std::max(maxPos.y, pos.y);
        maxPos.z = std::max(maxPos.z, pos.z);
    }

    gltf["accessors"][0]["min"] = kraken::JSONValue::CreateArray({minPos.x, minPos.y, minPos.z});
    gltf["accessors"][0]["max"] = kraken::JSONValue::CreateArray({maxPos.x, maxPos.y, maxPos.z});

    for (auto& thread : imageWriters)
    {
        thread.join();
    }

    if (writeErrCount)
    {
        throw std::logic_error("An error occured while compressing images.");
    }

    gltf["materials"][0]["pbrMetallicRoughness"]["roughnessFactor"] = 1.f;
    gltf["materials"][0]["pbrMetallicRoughness"]["baseColorTexture"]["index"] = 0;
    gltf["materials"][0]["normalTexture"]["index"] = 1;

    gltf["samplers"][0]["magFilter"] = 9729; // LINEAR
    gltf["samplers"][0]["minFilter"] = 9987; // LINEAR_MIPMAP_LINEAR

    gltf["images"][0]["bufferView"] = 4;
    gltf["images"][0]["mimeType"] = "image/png";
    gltf["images"][0]["name"] = "colour";

    gltf["images"][1]["bufferView"] = 5;
    gltf["images"][1]["mimeType"] = "image/png";
    gltf["images"][1]["name"] = "normal";

    gltf["textures"][0]["sampler"] = 0;
    gltf["textures"][0]["source"] = 0;
    gltf["textures"][1]["sampler"] = 0;
    gltf["textures"][1]["source"] = 1;

    gltf["bufferViews"][4]["buffer"] = 0;
    gltf["bufferViews"][4]["byteLength"] = colImgStream.str().size();
    gltf["bufferViews"][4]["byteOffset"] = sizeof(Vertex) * attributeCount + indexCount * indexStride;

    gltf["bufferViews"][5]["buffer"] = 0;
    gltf["bufferViews"][5]["byteLength"] = normalImgStream.str().size();
    gltf["bufferViews"][5]["byteOffset"] = (U64)gltf["bufferViews"][4]["byteOffset"] + (U64)gltf["bufferViews"][4]["byteLength"];

    U64 totalSize = (U64)gltf["bufferViews"][5]["byteOffset"] + (U64)gltf["bufferViews"][5]["byteLength"];

    gltf["buffers"][0]["byteLength"] = totalSize;
    struct
    {
        U32 magic = 0x46546C67;
        U32 version = 2;
        U32 length = 0;
    } glbHeader;

    struct
    {
        U32 chunkLength;
        U32 chunkType;
    } jsonChunkHeader, binChunkHeader;

    std::stringstream jsonPrinter;

    kraken::compactPrint(jsonPrinter, gltf);

    auto serialisedJSON = jsonPrinter.str();
    while (serialisedJSON.size() % 4)
    {
        serialisedJSON.push_back(' ');
    }

    jsonChunkHeader.chunkLength = serialisedJSON.size();
    jsonChunkHeader.chunkType = 0x4e4f534a; // "JSON"

    binChunkHeader.chunkLength = totalSize;
    binChunkHeader.chunkType = 0x004e4942; // "BIN\0"

    U32 binPadding = 0;

    if (binChunkHeader.chunkLength % 4)
    {
        binPadding = 4 - (binChunkHeader.chunkLength % 4);
    }

    binChunkHeader.chunkLength += binPadding;

    glbHeader.length = jsonChunkHeader.chunkLength
                       + binChunkHeader.chunkLength
                       + 12 // glb header size
                       + 8*2; // chunk header sizes

    std::ofstream outputFile(path);
    if (!outputFile.is_open())
    {
        throw std::runtime_error("Failed to open " + path.string() + " for reading.");
    }
    outputFile.write(reinterpret_cast<const char*>(&glbHeader), sizeof(glbHeader));
    outputFile.write(reinterpret_cast<const char*>(&jsonChunkHeader), sizeof(jsonChunkHeader));
    outputFile.write(reinterpret_cast<const char*>(serialisedJSON.data()), serialisedJSON.size());

    std::unique_ptr<Vertex[]> tmpbuf = std::make_unique<Vertex[]>(bufferedElementCount);

    outputFile.write(reinterpret_cast<const char*>(&binChunkHeader), sizeof(binChunkHeader));
    U64 currVertex = 0;
    for (; currVertex < attributeCount; ++currVertex)
    {
        Vertex curr;
        std::memcpy(&curr.position, srcbuf + currVertex * srcStride + positionOffset, sizeof(curr.position));
        std::memcpy(&curr.normal, srcbuf + currVertex * srcStride + normalOffset, sizeof(curr.normal));
        std::memcpy(&curr.texcoord, srcbuf + currVertex * srcStride + texcoordOffset, sizeof(curr.texcoord));
        tmpbuf[currVertex&countMask] = curr;

        if (currVertex == countMask)
        {
            outputFile.write(reinterpret_cast<const char*>(tmpbuf.get()), bufferedElementCount * sizeof(Vertex));
        }
    }
    if (currVertex & countMask)
    {
        outputFile.write(reinterpret_cast<const char*>(tmpbuf.get()), (currVertex & countMask) * sizeof(Vertex));
    }

    // Note: Vertices are aligned on 4-byte boundaries already.
    static_assert(sizeof(Vertex) % 4 == 0);

    outputFile.write(reinterpret_cast<const char*>(mesh.indexBuffer(0).buffer), indexCount * indexStride);

    outputFile.write(colImgStream.str().data(), colImgStream.str().size());
    outputFile.write(normalImgStream.str().data(), normalImgStream.str().size());

    if (binPadding)
    {
        char zerobuf[4] = {0, 0, 0, 0};
        outputFile.write(zerobuf, binPadding);
    }
}

std::ostream& writeOptions(std::ostream& ost,
                           const boost::program_options::variables_map vm,
                           std::string ignore = "")
{
    for (const auto& value : vm)
    {
        if (!vm.count(value.first)) continue;
        if (value.first == ignore) continue;
        ost << value.first << "=";
        bool written = false;

#define TRY_WRITE_TYPE(type)\
        do{if (!written)\
        {\
            try\
            {\
                ost << value.second.as<type>();\
                written = true;\
            }\
            catch (const boost::bad_any_cast&){}\
        }}while(false)

        TRY_WRITE_TYPE(fs::path);
        TRY_WRITE_TYPE(U32);
        TRY_WRITE_TYPE(std::string);
        TRY_WRITE_TYPE(F32);
#undef TRY_WRITE_TYPE

        ost << "\n";
    }
    return ost;
}

int texturerMain(int argc, char* argv[])
{
    namespace boost_opts = boost::program_options;

    TexturerOptions opts;

    U32 generalBitDepth = 0;
    U32 colourBitDepth = 0;
    U32 normalBitDepth = 0;
    U32 displacementBitDepth = 0;
    U32 alphaBitDepth = 0;

    U32 colourChannels = 0;
    U32 normalChannels = 0;

    opts.displacementFormat.channels = 1;
    opts.alphaFormat.channels = 1;

    std::string generalFormat;

    U32 xTiles, yTiles;

    fs::path optionInPath;
    fs::path optionDumpPath;

    boost_opts::options_description generalOptions("General options");

    generalOptions.add_options()
            ("help,h", "Print this list of available options.")
            ("mesh,mesh-file,m", boost_opts::value<fs::path>(&opts.meshFile), "UV-unwrapped mesh to use as basis for texturing.")
            ("dmap-path,p", boost_opts::value<fs::path>(&opts.dmapDirectory)->default_value("."), "Location of DMAP images.")
            ("config-file,conf", boost_opts::value<fs::path>(&optionInPath)->default_value(""), "Config file to load options from (will be overridden by program arguments).")
            ("config-dump", boost_opts::value<fs::path>(&optionDumpPath)->default_value(""), "Config file to write all options to (useful for dumping all used options for reproducability).")
            ("max-threads,threads,t", boost_opts::value<U32>(&opts.maxThreads)->default_value(std::thread::hardware_concurrency()), "Maximum number of threads to use for parsing (does not limit the threads used for other tasks, such as transfer operations).")
            ("max-load-capacity", boost_opts::value<U32>(&opts.maxLoadCapacity)->default_value(std::thread::hardware_concurrency()), "Maximal number of images to allow in memory simultaneously (depth + normal + confidence + quality + colour map from the same image together counts as one image for this option). Does not affect thread count but may limit concurrent work.")
            ("x-tiles", boost_opts::value<U32>(&xTiles)->default_value(1), "Number of tiles to render along the x axis.")
            ("y-tiles", boost_opts::value<U32>(&yTiles)->default_value(1), "Number of tiles to render along the y axis.")
        ;

    boost_opts::options_description exportOptions("Export options");
    exportOptions.add_options()
            ("gltf", boost_opts::value<fs::path>(&opts.gltfDest)->default_value(""), "Filename to export a single glTF file containing input model along with all generated data (unset to suppress export).")
            ("width,x", boost_opts::value<U32>(&opts.width)->default_value(2048), "Texture width.")
            ("height,y", boost_opts::value<U32>(&opts.height)->default_value(2048), "Texture height.")
            ("subregion-x-start,subregion-u-start,x0,u0", boost_opts::value<F32>(&opts.bottomLeft.x)->default_value(0.f), "Restrict texturing area to UV coordinates with x-/u-component greater than this.")
            ("subregion-x-end,subregion-u-end,x1,u1", boost_opts::value<F32>(&opts.topRight.x)->default_value(1.f), "Restrict texturing area to UV coordinates with x-/u-component less than this.")
            ("subregion-y-start,subregion-y-start,y0,v0", boost_opts::value<F32>(&opts.bottomLeft.y)->default_value(0.f), "Restrict texturing area to UV coordinates with y-/v-component greater than this.")
            ("subregion-y-end,subregion-y-end,y1,v1", boost_opts::value<F32>(&opts.topRight.y)->default_value(1.f), "Restrict texturing area to UV coordinates with y-/v-component less than this.")

            ("colour-texture,colour-map,colour-image", boost_opts::value<fs::path>(&opts.colourTextureDest)->default_value("colour.png"), "Filename to use for exporting colour map.")
            ("normal-texture,normal-map,normal-image", boost_opts::value<fs::path>(&opts.normalTextureDest)->default_value("normal.png"), "Filename to use for exporting normal map.")
            ("displacement-texture,displacement-map,displacement-image", boost_opts::value<fs::path>(&opts.displacementTextureDest)->default_value(""), "Filename to use for exporting displacement map (currently deactivated).")
            ("alpha-texture,alpha-map,alpha-image", boost_opts::value<fs::path>(&opts.alphaTextureDest)->default_value(""), "Filename to use for exporting alpha map (unset to suppress export).")
            ("metadata,metadata-file,info-file", boost_opts::value<fs::path>(&opts.metadataDest)->default_value("metadata.json"), "Filename to write metadata to.")
            ("expand-area,expand,dilate", boost_opts::value<bool>(&opts.expandTextures)->default_value(true), "Whether to expand the area around the images.")
            ("bit-depth,bits", boost_opts::value<U32>(&generalBitDepth)->default_value(16), "General bit depth of exported textures (except alpha).")
            ("alpha-bit-depth,alpha-bits", boost_opts::value<U32>(&alphaBitDepth)->default_value(1), "Bit depth of exported alpha texture (if any).")
            ("colour-bits", boost_opts::value<U32>(&colourBitDepth)->default_value(0), "Bit depth of exported colour map (set to 0 to use general bit depth).")
            ("normal-bits", boost_opts::value<U32>(&normalBitDepth)->default_value(0), "Bit depth of exported normal map (set to 0 to use general bit depth).")
            ("displacement-bits", boost_opts::value<U32>(&displacementBitDepth)->default_value(16), "Bit depth of exported displacement map (set to 0 to use general bit depth).")
            ("colour-channels", boost_opts::value<U32>(&colourChannels)->default_value(3), "Number of channels in colour map.")
            ("normal-channels", boost_opts::value<U32>(&normalChannels)->default_value(3), "Number of channels in normal map.")
            ("format,f", boost_opts::value<std::string>(&generalFormat)->default_value("png"), "Export format.")
            ("colour-format", boost_opts::value<std::string>(&opts.colourExportFileType)->default_value(""), "Export format of colour map (unset to use general format).")
            ("normal-format", boost_opts::value<std::string>(&opts.normalExportFileType)->default_value(""), "Export format of normal map (unset to use general format).")
            ("displacement-format", boost_opts::value<std::string>(&opts.displacementExportFileType)->default_value(""), "Export format of displacement map (unset to use general format).")
            ("alpha-format", boost_opts::value<std::string>(&opts.alphaExportFileType)->default_value(""), "Export format of alpha map (unset to use general format).")
        ;

    boost_opts::options_description qualityEstimatorOptions("Quality estimator options");
    qualityEstimatorOptions.add_options()
            ("focus-highpass-sigma", boost_opts::value<F32>(&opts.focusHighpassSigma)->default_value(1.f), "σ to use for focus detection highpass filter (Gaußian blur).")
            ("focus-lower-cutoff", boost_opts::value<F32>(&opts.focusLowerCutoff)->default_value(0.01f), "Minimal amplitude to consider in focus detection highpass filter.")
            ("focus-cutoff-mean-iterations", boost_opts::value<U32>(&opts.focusCutoffMeanIterations)->default_value(1), "Number of times to iteratively compute the mean of in-focus amplitudes, and use that mean as new cutoff.")
            ("max-angle", boost_opts::value<F32>(&opts.maxAngle)->default_value(0.85f), "Maximal normal angle to camera for inliers (radians).")
            ("min-confidence", boost_opts::value<F32>(&opts.minConfidence)->default_value(0.f), "Minimum confidence in point depth to consider inlier.")
            ("blur-sigma", boost_opts::value<F32>(&opts.blurSigma)->default_value(10.f), "Standard deviation for blurring.")
            ("expand-radius", boost_opts::value<F32>(&opts.expandRadius)->default_value(3.f), "Radius to expand inlier areas.")
            ("expand-sigma", boost_opts::value<F32>(&opts.expandSigma)->default_value(5.f), "σ to use as parameter for Gaußian blur in quality expansion.")
            ("expand-min-confidence", boost_opts::value<F32>(&opts.expandMinConfidence)->default_value(0.75f), "Minimum confidence to consider acceptable in expanded quality map.")
        ;

    boost_opts::options_description shaderOptions("Quality mapper options");
    shaderOptions.add_options()
            ("quality-weight", boost_opts::value<F32>(&opts.qualityWeight)->default_value(1.f), "Weight of estimated quality in computation of final quality.")
            ("normal-weight", boost_opts::value<F32>(&opts.normalWeight)->default_value(1.f), "Weight of normal dot product in computation of final quality.")
            ("distance-weight", boost_opts::value<F32>(&opts.distanceWeight)->default_value(0.1f), "Weight of (Euclidean) distance from model to depth map point in computation of final quality.")
            ("squared-distance-weight", boost_opts::value<F32>(&opts.squaredDistanceWeight)->default_value(50.f), "Weight of squared (Euclidean) distance from model to depth map point in computation of final quality.")
            ("focus-weight", boost_opts::value<F32>(&opts.focusWeight)->default_value(1.f), "Weight of focus quality.")
            ("angle-good-cutoff", boost_opts::value<F32>(&opts.cosineGoodCutoff)->default_value(0.9f), "Consider any angle θ with cosθ greater than this to be perfectly acceptable.")
            ("angle-bad-cutoff", boost_opts::value<F32>(&opts.cosineBadCutoff)->default_value(0.25f), "Consider any angle θ with cosθ less than this to be completely unacceptable.")
            ("angle-discard", boost_opts::value<F32>(&opts.cosineDiscard)->default_value(0.2f), "Discard any fragment with angle θ with cosθ less than this.")
            ("focus-sd-near", boost_opts::value<F32>(&opts.focusSDNearCutoff)->default_value(0.5f), "Always accept a fragment within this many standard deviations from focus mean as in focus.")
            ("focus-sd-far", boost_opts::value<F32>(&opts.focusSDFarCutoff)->default_value(3.f), "Always consider a fragment more than this many standard deviations from focus mean as out of focus.")
            ("focus-sd-discard", boost_opts::value<F32>(&opts.focusSDFarDiscard)->default_value(5.f), "Discard fragments more than this many standard deviations from focus mean.")
            ("quality-discard", boost_opts::value<U32>(&opts.qualityDiscardParameter)->default_value(2), "Control discard of qualities < mean on a pixel level [0: disabled, 1: discard, 2: discard and rescale].")
            ("quality-transform,quality-transform-function", boost_opts::value<U32>(&opts.qualityTransformFunction)->default_value(1), "ID of function performing quality rescaling [0: identity, 1: exp(par0 * (quality / max)), 2: (quality / max)^par0].")
            ("quality-transform-par0", boost_opts::value<F32>(&opts.qualityTransformPar0)->default_value(6.5f), "Value of par0 in quality rescaling.")
        ;

    boost_opts::options_description relaxationOptions("Relaxation options");
    relaxationOptions.add_options()
            ("relax-iterations", boost_opts::value<U32>(&opts.relaxIterations)->default_value(0), "Weight of estimated quality in computation of final quality.")
            ("relax-angle-good-cutoff", boost_opts::value<F32>(&opts.cosineGoodRelaxFactor)->default_value(1.f), "Factor to relax angle acceptance criterion with.")
            ("relax-angle-bad-cutoff", boost_opts::value<F32>(&opts.cosineBadRelaxFactor)->default_value(1.f), "Factor to relax angle rejection criterion with.")
            ("relax-angle-discard", boost_opts::value<F32>(&opts.cosineDiscardRelaxFactor)->default_value(1.f), "Factor to relax angle discard criterion with.")
            ("relax-normal-weight", boost_opts::value<F32>(&opts.normalWeightRelaxFactor)->default_value(0.5f), "Factor to relax normal weight with.")
        ;

    boost_opts::options_description hiddenOptions("Hidden options");
    hiddenOptions.add_options()
            ("quality-transform-par1", boost_opts::value<F32>(&opts.qualityTransformPar1)->default_value(0.f), "Value of par1 in quality rescaling.")
            ("internal-data-directory", boost_opts::value<fs::path>(&opts.internalDataDirectory), "Location of compiled shaders and other required resources.")
            ("debug-level,debug", boost_opts::value<U32>(&opts.debugLevel)->default_value(0), "Debug output level.")
            ("dump", boost_opts::bool_switch(&opts.dumpImages)->default_value(false), "Dump (approximations of) all float images.")
        ;

    boost_opts::options_description allOptions;
    allOptions.add(generalOptions)
              .add(exportOptions)
              .add(qualityEstimatorOptions)
              .add(shaderOptions)
              .add(relaxationOptions)
              .add(hiddenOptions);

    boost_opts::options_description visibleOptions;
    visibleOptions.add(generalOptions)
                  .add(exportOptions)
                  .add(qualityEstimatorOptions)
                  .add(shaderOptions)
                  .add(relaxationOptions);

    bool dataDirectoryFound = false;

    try
    {
        boost_opts::variables_map vm;

        // Note: boost_opts::store does not change values if they already exist
        // in the variable map. Therefore it is critical that these two calls to
        // store occurs in exactly this order, since this allows command-line
        // arguments to take precedence.
        boost_opts::store(boost_opts::parse_command_line(argc, argv, allOptions), vm);

        if (vm.count("config-file"))
        {
            optionInPath = vm["config-file"].as<decltype(optionInPath)>();
        }

        if (fs::exists(optionInPath))
        {
            std::ifstream configFile(optionInPath);
            boost_opts::store(boost_opts::parse_config_file(configFile, allOptions), vm);
        }
        boost_opts::notify(vm);

        if (!optionDumpPath.empty())
        {
            std::ofstream optionDump(optionDumpPath);
            if (!optionDump.is_open())
            {
                throw std::logic_error("Failed to open config dump file.");
            }
            writeOptions(optionDump, vm, "config-dump");
        }

        if (generalBitDepth)
        {
            if (!isValidBitDepth(generalBitDepth))
            {
                throw std::runtime_error(std::to_string(generalBitDepth) + " is not a valid bit depth; only 1, 2, 4, 8 and 16 are.");
            }
            if (!colourBitDepth) colourBitDepth = generalBitDepth;
            if (!normalBitDepth) normalBitDepth = generalBitDepth;
        }
        assertValidBitDepthCombination(colourBitDepth, colourChannels, 3, 4, "Colour image");
        assertValidBitDepthCombination(normalBitDepth, normalChannels, 3, 4, "Normal image");
        assertValidBitDepthCombination(displacementBitDepth, opts.displacementFormat.channels, 1, 1, "Displacement image");
        assertValidBitDepthCombination(alphaBitDepth, opts.alphaFormat.channels, 1, 1, "Alpha image");

        opts.colourFormat.bitDepth = colourBitDepth;
        opts.colourFormat.channels = colourChannels;

        opts.normalFormat.bitDepth = normalBitDepth;
        opts.normalFormat.channels = normalChannels;

        opts.displacementFormat.bitDepth = displacementBitDepth;

        opts.alphaFormat.bitDepth = alphaBitDepth;

        if (!generalFormat.empty())
        {
            assertValidExportFormat(generalFormat);
            if (opts.colourExportFileType.empty()) opts.colourExportFileType = generalFormat;
            if (opts.normalExportFileType.empty()) opts.normalExportFileType = generalFormat;
            if (opts.displacementExportFileType.empty()) opts.displacementExportFileType = generalFormat;
            if (opts.alphaExportFileType.empty()) opts.alphaExportFileType = generalFormat;
        }

        assertValidExportFormat(opts.colourExportFileType);
        assertValidExportFormat(opts.normalExportFileType);
        assertValidExportFormat(opts.displacementExportFileType);
        assertValidExportFormat(opts.alphaExportFileType);

        if (vm.count("internal-data-directory")) dataDirectoryFound = true;

        if (vm.count("help"))
        {
            std::cout << visibleOptions << "\n";
            return 0;
        }

        if (!vm.count("mesh"))
        {
            std::cerr << "Error: No mesh file give.\n" << visibleOptions << "\n";
            return 1;
        }
    }
    catch (std::exception& except)
    {
        std::cerr << "Error in parsing options: " << except.what() << "\n";
        return 42;
    }

    if (!dataDirectoryFound)
    {
        std::vector<fs::path> searchDirectories;
        fs::path currExecutable = *argv;
        searchDirectories.push_back(currExecutable.remove_filename());
        searchDirectories.push_back(fs::current_path());
        if (fs::current_path().filename() == "build")
        {
            searchDirectories.push_back(fs::current_path() / "..");
        }
        if (argc >= 1)
        {
            searchDirectories.push_back(argv[0]);
            if (fs::canonical(searchDirectories.back().parent_path()).filename() == "build")
            {
                searchDirectories.push_back(searchDirectories.back() / "..");
            }
        }
        auto dataPath = findDirectory("data", searchDirectories);
        if (dataPath.empty())
        {
            std::cerr << "Error, couldn't find data directory.\n";
            return 13;
        }
        if (opts.debugLevel) std::cout << "Found data path " << dataPath << "\n";
        opts.internalDataDirectory = dataPath;
    }

    int err = 0;

    Texturer texturer;
    auto output = texturer.run(opts, xTiles, yTiles);

    std::vector<std::thread> imageWriters;
    struct ImageWriteInfo
    {
        const kraken::image::Image& image;
        fs::path path;
        std::string format;
        std::string type;
    };

    std::atomic<U32> writeErrCount{0};

    for (const auto& info :
        {
            ImageWriteInfo{output.colourImage, opts.colourTextureDest, opts.colourExportFileType, "colour"},
            ImageWriteInfo{output.normalImage, opts.normalTextureDest, opts.normalExportFileType, "normal"},
            ImageWriteInfo{output.displacementImage, opts.displacementTextureDest, opts.displacementExportFileType, "displacement"},
            ImageWriteInfo{output.alphaImage, opts.alphaTextureDest, opts.alphaExportFileType, "alpha"},
        })
    {
        imageWriters.emplace_back(writeImageWorkerMain,
                                  std::cref(info.image),
                                  info.path,
                                  info.format,
                                  "Error, failed to write " + info.type + " image.\n",
                                  std::ref(writeErrCount));
    }

    if (!opts.gltfDest.empty())
    {
        imageWriters.emplace_back(writeGLTFWorkerMain,
                                  std::cref(texturer.getMesh()),
                                  std::cref(output),
                                  opts.gltfDest);
    }

    if (!opts.metadataDest.empty())
    {
        std::ofstream fileOut(opts.metadataDest);
        bool err = true;
        if (fileOut.is_open())
        {
            kraken::prettyPrint(fileOut, output.metadata);
            err = !fileOut.good();
        }
        if (err)
        {
            std::cerr << "Error, failed to write metadata.\n";
            err = 5;
        }
    }

    for (auto& thread : imageWriters)
    {
        thread.join();
    }

    if (writeErrCount) err = 5;

    return err;
}
