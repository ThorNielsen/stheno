#include "texturer/dmapparser.hpp"

#include <cstring>
#include <iostream>
#include <fstream>

#include <stack>

// This is made into a private namespace to suppress warnings of unused
// functions.
namespace priv
{

inline U32 getColourType(const FloatImage& srcImg)
{
    return srcImg.channels();
}

inline kraken::image::ColourType getColourType(const kraken::image::Image& srcImg)
{
    return srcImg.colourType();
}

template <size_t componentCount>
struct VectorType{};

template<>
struct VectorType<1>
{
    using Type = float;
};

template<>
struct VectorType<4>
{
    using Type = vec4;
};

template <typename ImageType, size_t channels>
typename VectorType<channels>::Type getVectorPixel(const ImageType& img, U32 x, U32 y);

template <typename ImageType, size_t channels>
void setVectorPixel(ImageType& img, U32 x, U32 y, typename VectorType<channels>::Type val);

template<>
vec4 getVectorPixel<FloatImage, 4>(const FloatImage& img, U32 x, U32 y)
{
    return img.getPixel(x, y);
}
template<>
float getVectorPixel<FloatImage, 1>(const FloatImage& img, U32 x, U32 y)
{
    return img.getPixel(x, y)[0];
}

template<>
vec4 getVectorPixel<kraken::image::Image, 4>(const kraken::image::Image& img, U32 x, U32 y)
{
    return kraken::image::unpack(img.getPixel(x, y), img.colourType());
}
template<>
float getVectorPixel<kraken::image::Image, 1>(const kraken::image::Image& img, U32 x, U32 y)
{
    return kraken::image::unpack(img.getPixel(x, y), img.colourType())[0];
}

template<>
void setVectorPixel<FloatImage, 4>(FloatImage& img, U32 x, U32 y, vec4 val)
{
    img.setPixel(x, y, val);
}
template<>
void setVectorPixel<FloatImage, 1>(FloatImage& img, U32 x, U32 y, float val)
{
    img.setPixel(x, y, val);
}

template<>
void setVectorPixel<kraken::image::Image, 4>(kraken::image::Image& img, U32 x, U32 y, vec4 val)
{
    img.setPixel(x, y, kraken::image::pack(val, img.colourType()));
}
template<>
void setVectorPixel<kraken::image::Image, 1>(kraken::image::Image& img, U32 x, U32 y, float val)
{
    img.setPixel(x, y, kraken::image::pack(vec4{val, 0.f, 0.f, 0.f}, img.colourType()));
}

vec4 componentwiseMultiplication(vec4 a, vec4 b)
{
    return {a.x*b.x, a.y*b.y, a.z*b.z, a.w*b.w};
}

vec4 componentwiseMultiplication(vec4 a, float b)
{
    return a*b;
}

vec4 componentwiseMultiplication(float a, vec4 b)
{
    return a*b;
}

float componentwiseMultiplication(float a, float b)
{
    return a*b;
}

template <typename T>
T abs(T in) { return std::abs(in); }

/// Extracts the pixels a depth map covers reasonably confidently.
///
/// This function can be used to extract coverage information from a DMAP, along
/// with precise information for each pixel determining how well that pixel is
/// covered. This is computed using both the confidence and normal map from the
/// given DMAP, and if the values does not fall outside the thresholds specified
/// in the parameters, the returned value is the product of the confidence and
/// the inner product of the normal and viewing direction.
///
/// \param dmap DMAP from which the confidence and normal maps should be read.
/// \param maxAngle Maximal angle to view direction to consider inlier.
/// \param minConfidence Minimal confidence needed for inlier.
/// \returns An image whose pixels determine whether they are covered (the pixel
///          then has positive value), or not (the pixel then has value -1).
///
FloatImage extractCoveredPixels(const DMAPContent& dmap,
                                float maxAngle = Constant<float>::pi/5.f,
                                float minConfidence = 0.2f)
{
    U32 width = dmap.normalDepthMap.width();
    U32 height = dmap.normalDepthMap.height();
    FloatImage img(width, height, 1);

    auto cosMaxAngle = std::cos(maxAngle);

    for (U32 y = 0; y < height; ++y)
    {
        for (U32 x = 0; x < width; ++x)
        {
            float normalValue = -dmap.normalDepthMap.getPixel(x, y).z;
            float confidenceValue = dmap.confidenceMap.getPixel(x, y).x;
            float result = normalValue * confidenceValue;
            if (normalValue <= cosMaxAngle || confidenceValue <= minConfidence)
            {
                result = -1.f;
            }
            img.setPixel(x, y, result);
        }
    }
    return img;
}

struct VoronoiDiagram
{
    struct PointInfo
    {
        U32 distSqr; // |(xFrom, yFrom)-(x,y)|^2;
        Vector<2, U16> from; // origin of this point
        Vector<2, U16> loc; // Coordinate of current point.
        bool operator<(const PointInfo& other) const
        {
            // Yes, wrong way. To be used by priority queue comparison.
            return distSqr > other.distSqr;
        }
    };
    static constexpr U32 invalidCell = 0xffffffff;
    kraken::image::Image cells;
    kraken::image::Image extra; // The contents of this image can be used for
                                // auxiliary information for algorithms, e.g.
                                // storing distances, whether points have been
                                // visited or not, etc.
};

/// Todo: document this
///
/// \param diagram
/// \param img
/// \param minValue
/// \param points
///
void fillIndividualVoronoiPoints(VoronoiDiagram& diagram,
                                 const FloatImage& img, float minValue,
                                 std::priority_queue<VoronoiDiagram::PointInfo>& points)
{
    U32 width = img.width();
    U32 height = img.height();
    diagram.cells.resize(width, height, kraken::image::RG16);
    diagram.extra.resize(width, height, kraken::image::RG16);

    U32 tileID = 0;
    for (U16 y = 0; y < height; ++y)
    {
        for (U16 x = 0; x < width; ++x)
        {
            bool isFilled = img.getPixel(x, y).x > minValue;
            diagram.cells.setPixel(x, y, isFilled ? tileID++ : VoronoiDiagram::invalidCell);
            diagram.extra.setPixel(x, y, isFilled ? 0 : 0xffffffff);
            if (isFilled)
            {
                points.push({0, {x, y}, {x, y}});
            }
        }
    }
}

void fillMergedVoronoiPoints(VoronoiDiagram& diagram,
                             const FloatImage& img, float minValue,
                             std::priority_queue<VoronoiDiagram::PointInfo>& points)
{
    U32 width = img.width();
    U32 height = img.height();
    diagram.cells.resize(width, height, kraken::image::RG16);
    diagram.extra.resize(width, height, kraken::image::RG16);

    diagram.extra.fill([](U32, U32) { return 0xffffffff; });

    std::stack<Vector<2, U16>> fillNext;
    U32 tileID = 0;
    for (U16 y = 0; y < height; ++y)
    {
        for (U16 x = 0; x < width; ++x)
        {
            if (diagram.extra.getPixel(x, y) != 0xffffffff) continue;
            if (img.getPixel(x, y).x > minValue)
            {
                fillNext.push({x, y});

                while (!fillNext.empty())
                {
                    auto coords = fillNext.top();
                    fillNext.pop();
                    U16 cx = coords.x;
                    U16 cy = coords.y;
                    if (cx >= width || cy >= height) continue;
                    if (diagram.extra.getPixel(cx, cy) != 0xffffffff) continue;
                    if (img.getPixel(cx, cy).x > minValue)
                    {
                        diagram.cells.setPixel(cx, cy, tileID);
                        diagram.extra.setPixel(cx, cy, 0);
                        points.push({0, {cx, cy}, {cx, cy}});
                        if (x) fillNext.push({static_cast<U16>(cx-1), cy});
                        if (y) fillNext.push({cx, static_cast<U16>(cy-1)});
                        fillNext.push({static_cast<U16>(cx+1), cy});
                        fillNext.push({cx, static_cast<U16>(cy+1)});
                    }
                }
                ++tileID;
            }
            else
            {
                diagram.cells.setPixel(x, y, VoronoiDiagram::invalidCell);
            }
        }
    }
}

VoronoiDiagram computeVoronoiDiagram(const FloatImage& img, bool individualPoints)
{
    VoronoiDiagram diagram;
    std::priority_queue<VoronoiDiagram::PointInfo> points;

    if (individualPoints)
    {
        fillIndividualVoronoiPoints(diagram, img, 0.f, points);
    }
    else
    {
        fillMergedVoronoiPoints(diagram, img, 0.f, points);
    }

    while (!points.empty())
    {
        auto info = points.top();
        points.pop();
        for (int yOffset = -1; yOffset <= 1; ++yOffset)
        {
            for (int xOffset = -1; xOffset <= 1; ++xOffset)
            {
                if (!xOffset && !yOffset) continue;
                int currX = (int)info.loc.x + xOffset;
                int currY = (int)info.loc.y + yOffset;
                if (currX < 0 || currY < 0 || (U32)currX >= img.width() || (U32)currY >= img.height()) continue;

                auto x = (U16)currX;
                auto y = (U16)currY;

                auto xDiff = (U32)info.from.x - (U32)currX;
                auto yDiff = (U32)info.from.y - (U32)currY;
                U32 distSqr = xDiff*xDiff + yDiff*yDiff;
                if (distSqr < diagram.extra.getPixel(x, y))
                {
                    diagram.extra.setPixel(x, y, distSqr);
                    diagram.cells.setPixel(x, y, diagram.cells.getPixel(info.from.x, info.from.y));
                    points.push({distSqr, {info.from.x, info.from.y}, {x, y}});
                }
            }
        }
    }

    return diagram;
}

// This fills the region of all pixels of the same colour as in (startX, startY)
// with 'fill', but only if they are connected to (startX, startY). The returned
// value is the number of pixels filled, and INCLUDES the initial pixel.
// Note that both straight and diagonally neighbouring pixels are considered
// connected.
U32 getSizeAndFillRegion(kraken::image::Image& image, U16 startX, U16 startY, U32 fill)
{
    auto origValue = (U32)image.getPixel(startX, startY);
    std::stack<Vector<2, U16>> positions;
    positions.push({startX, startY});
    U32 count = 0;
    while (!positions.empty())
    {
        auto pos = positions.top();
        positions.pop();
        if (image.getPixel(pos.x, pos.y) != origValue) continue;
        image.setPixel(pos.x, pos.y, fill);
        ++count;

        for (int yOffset = -1; yOffset <= 1; ++yOffset)
        {
            for (int xOffset = -1; xOffset <= 1; ++xOffset)
            {
                if (!xOffset && !yOffset) continue;
                int currX = (int)pos.x + xOffset;
                int currY = (int)pos.y + yOffset;
                if (currX < 0 || currY < 0 || (U32)currX >= image.width() || (U32)currY >= image.height()) continue;
                positions.push({(U16)currX, (U16)currY});
            }
        }
    }
    return count;
}


void removeVoronoiCellsByArea(VoronoiDiagram& diagram, U32 cutoff, bool removeLarger)
{
    diagram.extra = diagram.cells;

    U32 width = diagram.cells.width();
    U32 height = diagram.cells.height();

    for (U32 y = 0; y < height; ++y)
    {
        for (U32 x = 0; x < width; ++x)
        {
            if (diagram.extra.getPixel(x, y) == VoronoiDiagram::invalidCell) continue;
            auto currValue = (U32)diagram.cells.getPixel(x, y);
            auto regionSize = getSizeAndFillRegion(diagram.cells, x, y, VoronoiDiagram::invalidCell-1);
            if ((regionSize >= cutoff) == removeLarger)
            {
                getSizeAndFillRegion(diagram.cells, x, y, VoronoiDiagram::invalidCell);
            }
            else
            {
                getSizeAndFillRegion(diagram.cells, x, y, currValue);
            }
            getSizeAndFillRegion(diagram.extra, x, y, VoronoiDiagram::invalidCell);
        }
    }
}

// 'image' determines coverage by minCoverValue and if 'qualityImg' is not
// nullptr, then that is used to compute an average quality of all the pixels
// which ARE covered
U32 getCoveredAreaAndQuality(kraken::image::Image& diagram,
                             const FloatImage& image,
                             const FloatImage* qualityImg,
                             float minCoverValue,
                             U16 startX, U16 startY, U32 newFill, float* avgQualityOut)
{
    auto origValue = (U32)diagram.getPixel(startX, startY);
    std::stack<Vector<2, U16>> positions;
    positions.push({startX, startY});
    U32 count = 0;
    float qualitySum = 0.f;
    while (!positions.empty())
    {
        auto pos = positions.top();
        positions.pop();
        if (diagram.getPixel(pos.x, pos.y) != origValue) continue;
        diagram.setPixel(pos.x, pos.y, newFill);

        bool isPixelGood = image.getPixel(pos.x, pos.y).x > minCoverValue;
        count += isPixelGood;
        if (qualityImg && isPixelGood) qualitySum += qualityImg->getPixel(pos.x, pos.y).x;

        for (int yOffset = -1; yOffset <= 1; ++yOffset)
        {
            for (int xOffset = -1; xOffset <= 1; ++xOffset)
            {
                if (!xOffset && !yOffset) continue;
                int currX = (int)pos.x + xOffset;
                int currY = (int)pos.y + yOffset;
                if (currX < 0 || currY < 0 || (U32)currX >= diagram.width() || (U32)currY >= diagram.height()) continue;
                positions.push({(U16)currX, (U16)currY});
            }
        }
    }
    if (avgQualityOut) *avgQualityOut = count ? qualitySum / static_cast<float>(count) : 1.f;
    return count;
}

void removeVoronoiCellsByCoveredArea(VoronoiDiagram& diagram, const FloatImage& img, const FloatImage* qualityImg,
                                     float minCoverValue, float minQuality, float coverFracCutoff, bool removeLarger)
{
    diagram.extra = diagram.cells;

    U32 width = diagram.cells.width();
    U32 height = diagram.cells.height();

    if (!qualityImg) minQuality = -1.f;

    for (U32 y = 0; y < height; ++y)
    {
        for (U32 x = 0; x < width; ++x)
        {
            if (diagram.extra.getPixel(x, y) == VoronoiDiagram::invalidCell) continue;
            auto currValue = (U32)diagram.cells.getPixel(x, y);
            auto regionSize = getSizeAndFillRegion(diagram.cells, x, y, VoronoiDiagram::invalidCell-1);

            float quality = 0.f;

            auto coveredArea = getCoveredAreaAndQuality(diagram.cells, img, qualityImg, minCoverValue, (U16)x, (U16)y, VoronoiDiagram::invalidCell-2, &quality);
            float coverFrac = static_cast<float>(coveredArea)/static_cast<float>(regionSize);
            if ((coverFrac >= coverFracCutoff) == removeLarger || quality < minQuality)
            {
                getSizeAndFillRegion(diagram.cells, x, y, VoronoiDiagram::invalidCell);
            }
            else
            {
                getSizeAndFillRegion(diagram.cells, x, y, currValue);
            }
            getSizeAndFillRegion(diagram.extra, x, y, VoronoiDiagram::invalidCell);
        }
    }
}

template <typename ImageInType>
FloatImage fattenIslands(const ImageInType& srcImg, float radius)
{
    struct PixelInfo
    {
        U16 x;
        U16 y;
        U16 origX;
        U16 origY;
        float value;

        bool operator<(const PixelInfo& other) const
        {
            return value < other.value;
        }
    };

    U32 width = srcImg.width();
    U32 height = srcImg.height();
    FloatImage dstImg(width, height, 1);
    std::priority_queue<PixelInfo> pixelInfo;
    for (U16 y = 0; y < height; ++y)
    {
        for (U16 x = 0; x < width; ++x)
        {
            dstImg.setPixel(x, y,
                            srcImg.getPixel(x, y).x > 0.f ? radius : -1.f);
            if (srcImg.getPixel(x, y).x > 0.f)
            {
                pixelInfo.push({x, y, x, y, radius});
            }
        }
    }

    while (!pixelInfo.empty())
    {
        auto info = pixelInfo.top();
        pixelInfo.pop();
        dstImg.setPixel(info.x, info.y, info.value);
        for (int yOffset = -1; yOffset <= 1; ++yOffset)
        {
            for (int xOffset = -1; xOffset <= 1; ++xOffset)
            {
                if (!xOffset && !yOffset) continue;
                int currX = (int)info.x + xOffset;
                int currY = (int)info.y + yOffset;
                if (currX < 0 || currY < 0 || (U32)currX >= dstImg.width() || (U32)currY >= dstImg.height()) continue;
                float dist = length(vec2{static_cast<float>(info.origX-currX), static_cast<float>(info.origY-currY)});

                float value = radius - dist;

                if (value <= 0) continue;

                auto x = (U16)currX;
                auto y = (U16)currY;
                if (value > dstImg.getPixel(x, y).x)
                {
                    dstImg.setPixel(x, y, value);
                    pixelInfo.push({x, y, info.origX, info.origY, value});
                }
            }
        }
    }
    return dstImg;
}

std::vector<float> computeGaussianKernel(U32 radius, float sigma)
{
    std::vector<float> kernel(2*radius + 1);
    float sum = 0.f;
    for (int r = -static_cast<S32>(radius); r <= static_cast<S32>(radius); ++r)
    {
        auto rf = static_cast<float>(r);
        kernel[r+radius] = std::exp(-0.5f*(rf/sigma)*(rf/sigma));
        sum += kernel[r+radius];
    }
    for (auto& x : kernel)
    {
        x /= sum;
    }
    return kernel;
}

void checkConvolutionPreconditions(const kraken::image::Image& input,
                                   kraken::image::Image& output,
                                   const std::vector<float>& kernel)
{
    auto radius = kernel.size()>>1;
    if (kernel.size() != 2*radius+1) throw std::logic_error("Convolution kernel has wrong size.");
    //if (input.colourType().channels != 1) throw std::logic_error("Convolution only works on monochrome images.");
    if (input.width() != output.width()
        || input.height() != output.height()
        || input.colourType() != output.colourType())
    {
        throw std::logic_error("Input/output images does not match (width, height, colourType).");
    }
}

void checkConvolutionPreconditions(const FloatImage& input,
                                   FloatImage& output,
                                   const std::vector<float>& kernel)
{
    auto radius = static_cast<S32>(kernel.size()>>1);
    if (static_cast<S32>(kernel.size()) != 2*radius+1) throw std::logic_error("Convolution kernel has wrong size.");
    if (input.width() != output.width()
        || input.height() != output.height()
        || input.channels() != output.channels())
    {
        throw std::logic_error("Input/output images does not match (width, height, colourType).");
    }
}

// Convolutes an image with a kernel, which must have size 2*r+1 for some r.
template <typename ImageType, size_t imageComponents, typename KernelType = float>
void convoluteRowKernel(const ImageType& input, ImageType& output,
                        const std::vector<KernelType>& kernel)
{
    checkConvolutionPreconditions(input, output, kernel);
    auto radius = static_cast<S32>(kernel.size()>>1);
    for (U32 y = 0; y < input.height(); ++y)
    {
        for (U32 x = 0; x < input.width(); ++x)
        {
            auto cx = static_cast<S32>(x);
            typename VectorType<imageComponents>::Type sum(0.f);
            for (S32 c = -radius; c <= radius; ++c)
            {
                auto at = c+cx;
                decltype(sum) pixelValue;
                if (at < 0) pixelValue = getVectorPixel<ImageType, imageComponents>(input, std::min(U32(-at), input.width()-1), y);
                else if ((U32)at >= input.width()) pixelValue = getVectorPixel<ImageType, imageComponents>(input, (U32)std::max(S32(2*input.width())-at-1, 0), y);
                else pixelValue = getVectorPixel<ImageType, imageComponents>(input, (U32)at, y);

                sum += componentwiseMultiplication(kernel[radius+c], pixelValue);
            }
            setVectorPixel<ImageType, imageComponents>(output, x, y, sum);
        }
    }
}

// Convolutes an image with a kernel, which must have size 2*r+1 for some r.
template <typename ImageType, size_t imageComponents, typename KernelType = float>
void convoluteColumnKernel(const ImageType& input, ImageType& output,
                           const std::vector<KernelType>& kernel)
{
    checkConvolutionPreconditions(input, output, kernel);
    auto radius = static_cast<S32>(kernel.size()>>1);
    for (U32 x = 0; x < input.width(); ++x)
    {
        for (U32 y = 0; y < input.height(); ++y)
        {
            auto cy = static_cast<S32>(y);
            typename VectorType<imageComponents>::Type sum(0.f);
            for (S32 c = -radius; c <= radius; ++c)
            {
                auto at = c+cy;
                decltype(sum) pixelValue;
                if (at < 0) pixelValue = getVectorPixel<ImageType, imageComponents>(input, x, std::min(U32(-at), input.height()-1));
                else if ((U32)at >= input.height()) pixelValue = getVectorPixel<ImageType, imageComponents>(input, x, (U32)std::max(S32(2*input.height())-at-1, 0));
                else pixelValue = getVectorPixel<ImageType, imageComponents>(input, x, (U32)at);

                sum += componentwiseMultiplication(kernel[radius+c], pixelValue);
            }
            setVectorPixel<ImageType, imageComponents>(output, x, y, sum);
        }
    }
}

template <typename ImageType, size_t components>
ImageType blur(ImageType source, U32 radius, float sigma)
{
    ImageType temp(source.width(), source.height(), getColourType(source));
    auto kernel = computeGaussianKernel(radius, sigma);

    convoluteRowKernel<ImageType, components>(source, temp, kernel);
    convoluteColumnKernel<ImageType, components>(temp, source, kernel);

    return source;
}

template <typename ImageType, size_t components>
ImageType highpass(ImageType source, U32 radius, float sigma)
{
    ImageType lowpass = blur<ImageType, components>(source, radius, sigma);

    for (U32 y = 0; y < source.height(); ++y)
    {
        for (U32 x = 0; x < source.width(); ++x)
        {
            auto value = getVectorPixel<ImageType, components>(source, x, y)
                       - getVectorPixel<ImageType, components>(lowpass, x, y);
            setVectorPixel<ImageType, components>(source, x, y, abs(value * 0.5f));
        }
    }

    return source;
}

struct QualityEstimatorOptions
{
    float focusHighpassSigma = 1.f; // Sigma to use for Gaußian blur in high
                                    // pass filter.
    float focusLowerCutoff = 0.01f; // Any value less than this will be
                                    // discarded from the focus estimation.
    U32 focusCutoffMeanIterations = 1; // This is the number of times to
                                       // iteratively remove points below
                                       // cutoff, compute the mean, and set the
                                       // new cutoff equal to the new mean. Note
                                       // that this may be zero.

    float maxAngle = Constant<float>::pi/5.f;
    float minConfidence = 0.85f;
    float blurSigma = 10.f; // Radius = 2*sigma.
                            // Note: Should we also blur the depth image, so
                            // shader doesn't get discontinuities (it can just
                            // check for closeness, and if there are large
                            // discontinuities, it will not be close)?

    float expandRadius = 3.f;
    float expandSigma = 5.f;
    float expandMinConfidence = 0.75f;
};

U32 computeDecimationFactor(U64 largeDim, U64 smallDim)
{
    U32 decimateFactor = 0;

    while (largeDim > smallDim)
    {
        if ((largeDim >> 1) < smallDim) break;
        largeDim >>= 1;
        ++decimateFactor;
    }
    //if (largeDim != smallDim) throw std::logic_error("Bad decimate factor.");
    return decimateFactor;
}

//template <typename ImageType, size_t components>
FocusInfo computeFocus(const DMAPContent& content, QualityEstimatorOptions opts)
{
    kraken::image::Image srccol(content.colourImage.width(),
                                content.colourImage.height(),
                                kraken::image::R16);

    for (U32 y = 0; y < srccol.height(); ++y)
    {
        for (U32 x = 0; x < srccol.width(); ++x)
        {
            vec4 c = kraken::image::unpack(content.colourImage.getPixel(x, y),
                                           content.colourImage.colourType());
            float v = std::max(std::max(c.x, c.y), c.z);
            srccol.setPixel(x, y, U16(v * 65535));
        }
    }

    auto decimateFactor = computeDecimationFactor(content.colourImage.width(), content.normalDepthMap.width());
    {
        U32 ciw = content.colourImage.width();
        U32 ref = content.normalDepthMap.width();
        if (ciw < ref)
        {
            throw std::logic_error("Depth map has larger width than colour "
                                   "image.");
        }
        while (ciw > ref)
        {
            if ((ciw >> 1) < ref) break;
            ciw >>= 1;
            ++decimateFactor;
        }
        /*
        if (ciw != ref)
        {
            throw std::runtime_error("Depth map's width is not a 2^n-multiple "
                                     "of colour image's width (colour width "
                                     + std::to_string(content.colourImage.width())
                                     + "px, depth width "
                                     + std::to_string(content.normalDepthMap.width())
                                     + "px).");
        }
        if ((content.normalDepthMap.height() << decimateFactor)
            != content.colourImage.height())
        {
            throw std::runtime_error("Depth map's height is not a 2^n-multiple "
                                     "of colour image's height (colour height "
                                     + std::to_string(content.colourImage.height())
                                     + "px, depth height "
                                     + std::to_string(content.normalDepthMap.height())
                                     + "px).");
        }*/
    }

    auto lowpassCol = blur<kraken::image::Image, 1>(srccol,
                                                    2.*(opts.focusHighpassSigma+1e-4f),
                                                    opts.focusHighpassSigma);

    FocusInfo focusInfo{0.f, 0.f};

    double firstMoment, secondMoment, totalCount;

    // If the initial cutoff is non-negative then we add one iteration.
    if (opts.focusLowerCutoff > 0)
    {
        ++opts.focusCutoffMeanIterations;
    }

    auto cutoff = opts.focusLowerCutoff;

    while (opts.focusCutoffMeanIterations)
    {
        --opts.focusCutoffMeanIterations;
        firstMoment = 0.;
        secondMoment = 0.;
        totalCount = 0;

        double highpassSum = 0.;

        for (U32 y = 0; y < srccol.height(); ++y)
        {
            for (U32 x = 0; x < srccol.width(); ++x)
            {
                auto value = getVectorPixel<decltype(lowpassCol), 4>(srccol, x, y)
                           - getVectorPixel<decltype(lowpassCol), 4>(lowpassCol, x, y);
                value = abs(value * 0.5f);

                float sumVal = (value.x + value.y + value.z) / 3.f;

                if (sumVal >= cutoff)
                {
                    highpassSum += sumVal;
                    auto depthValue = content.normalDepthMap.getPixel(x >> decimateFactor,
                                                                      y >> decimateFactor).w;

                    auto v = depthValue;
                    firstMoment += v;
                    secondMoment += v*v;

                    ++totalCount;
                }
            }
        }

        // If there are less than two points we cannot estimate the variance,
        // and if there aren't even a single point, the mean can't be estimated.
        // Thus, if that is the case, we just return the invalid focus info.
        if (totalCount <= 1.) return focusInfo;

        cutoff = highpassSum / totalCount;
    }

    firstMoment /= totalCount;
    secondMoment /= totalCount;

    double mean = firstMoment;
    // Note: We create an unbiased estimator for the variance.
    double variance = (secondMoment - firstMoment * firstMoment) * (totalCount / (totalCount - 1));

    focusInfo.depthMean = static_cast<float>(mean);
    focusInfo.depthSD = static_cast<float>(std::sqrt(variance));

    return focusInfo;
}

kraken::image::Image extractCentralConfidenceMask(const DMAPContent& content,
                                                  QualityEstimatorOptions opts)
{
    auto expanded = blur<FloatImage, 1>(content.confidenceMap,
                                        2*(opts.expandSigma+0.0001f),
                                        opts.expandSigma);
    for (U32 x = 0; x < expanded.width(); ++x)
    {
        for (U32 y = 0; y < expanded.height(); ++y)
        {
            if (expanded.getPixel(x, y).r <= opts.expandMinConfidence)
            {
                expanded.setPixel(x, y, 0.f);
            }
        }
    }

    std::stack<Vector<2, U32>> positions;

    kraken::image::Image centralMask(expanded.width(),
                                     expanded.height(),
                                     kraken::image::R1);
    centralMask.fill([](U32, U32){return 1;});

    for (U32 x = 0; x < expanded.width(); ++x)
    {
        positions.push({x, 0});
        centralMask.setPixel(x, 0, 0);

        positions.push({x, expanded.height()-1});
        centralMask.setPixel(x, expanded.height()-1, 0);
    }
    for (U32 y = 0; y < expanded.height(); ++y)
    {
        positions.push({0, y});
        centralMask.setPixel(0, y, 0);

        positions.push({expanded.width()-1, y});
        centralMask.setPixel(expanded.width()-1, y, 0);
    }


    while (!positions.empty())
    {
        auto pos = positions.top();
        positions.pop();

        for (S32 y = -1; y <= 1; ++y)
        {
            if (y < 0 && !pos.y) continue;
            if (y > 0 && pos.y + 1 == expanded.height()) continue;

            for (S32 x = -1; x <= 1; ++x)
            {
                if (x < 0 && !pos.x) continue;
                if (x > 0 && pos.x + 1 == expanded.width()) continue;
                if (!x && !y) continue;

                auto px = static_cast<U32>(pos.x + x);
                auto py = static_cast<U32>(pos.y + y);

                if (!centralMask.getPixel(px, py)) continue;
                if (expanded.getPixel(px, py).r > 0) continue;
                centralMask.setPixel(px, py, 0);
                positions.push({px, py});
            }
        }
    }

    /*
    auto hp = highpass<kraken::image::Image, 4>(content.colourImage, 2, 1);
    for (U32 y = 0; y < hp.height(); ++y)
    {
        for (U32 x = 0; x < hp.width(); ++x)
        {
            auto c = kraken::image::uunpack(hp.getPixel(x, y), hp.colourType());
            c.w = (1 << hp.colourType().bitDepth) - 1;
            hp.setPixel(x, y, kraken::image::pack(c, hp.colourType()));
        }
    }

    std::ofstream imgOut("/home/thor/Desktop/debug/highpass" + std::to_string(iterCount++) + ".png");
    if (!imgOut.is_open()) throw std::runtime_error("Can't open highpass.png.");
    if (!hp.write(imgOut, "png")) throw std::runtime_error("Failed to write image.");*/
    return centralMask;
}

FloatImage estimateQualityMap(const DMAPContent& content,
                              QualityEstimatorOptions opts)
{
    auto mask = extractCentralConfidenceMask(content, opts);

    auto decimateFactor = computeDecimationFactor(content.qualityMap.width(), content.normalDepthMap.width());
    if ((content.normalDepthMap.width() << decimateFactor) != mask.width()
                    || (content.normalDepthMap.height() << decimateFactor) != mask.height())
    {
        mask = mask.crop(content.normalDepthMap.width() << decimateFactor, content.normalDepthMap.height() << decimateFactor);
    }
    auto qualityMap = extractCoveredPixels(content, opts.maxAngle, opts.minConfidence);
    for (U32 y = 0; y < mask.height(); ++y)
    {
        for (U32 x = 0; x < mask.width(); ++x)
        {
            if (!mask.getPixel(x, y))
            {
                qualityMap.setPixel(x, y, vec4{-1.f, -1.f, -1.f, -1.f});
            }
        }
    }

    /*
    float gradAbsCutoff = 0.08f;

    const auto& ndm = content.normalDepthMap;
    FloatImage depthDeriv(ndm.width(), ndm.height(), 1);
    for (S32 y = 0; y < (S32)ndm.height(); ++y)
    {
        for (S32 x = 0; x < (S32)ndm.width(); ++x)
        {
            float depthRef = ndm.getPixel(x, y).w;
            float maxGradAbs = 0.f;
            for (S32 yOff = -1; yOff <= 1; ++yOff)
            {
                for (S32 xOff = -1; xOff <= 1; ++xOff)
                {
                    if (x+xOff < 0 || y+yOff < 0
                        || x+xOff > (S32)ndm.width()
                        || y+yOff > (S32)ndm.height())
                    {
                        continue;
                    }
                    float depth = ndm.getPixel(x+xOff, y+yOff).w;
                    if (depth <= 0.f) continue;
                    maxGradAbs = std::max(maxGradAbs, std::abs(depth-depthRef));
                }
            }
            depthDeriv.setPixel(x, y, maxGradAbs >= gradAbsCutoff);
        }
    }

    float depthFilterRadius = 3.f;

    auto expanded = fattenIslands(depthDeriv, depthFilterRadius);
    for (U32 y = 0; y < ndm.height(); ++y)
    {
        for (U32 x = 0; x < ndm.width(); ++x)
        {
            if (expanded.getPixel(x, y).r <= 0.f)
            {
                qualityMap.setPixel(x, y, vec4{-1.f, -1.f, -1.f, -1.f});
            }
        }
    }
    //*/auto

    expanded = fattenIslands(qualityMap, opts.expandRadius);

    auto& quality = expanded; // Re-use storage.
    for (U32 y = 0; y < quality.height(); ++y)
    {
        for (U32 x = 0; x < quality.width(); ++x)
        {
            quality.setPixel(x, y, expanded.getPixel(x, y).r > 0.f);
        }
    }
    return blur<FloatImage, 1>(quality, 2*(opts.blurSigma+0.0001f), opts.blurSigma);
}

} // namespace priv

namespace
{

std::string yesno(bool b)
{
    return b ? "yes" : "no";
}

} // end anonymous namespace

void dumpInfo(const DMAPContent& content)
{
    std::cout << "Depth map info:\n";
    std::cout << "File name: " << content.filename << "\n";
    std::cout << "Neighbours:";
    for (auto neighbour : content.neighbours)
    {
        std::cout << ' ' << neighbour;
    }
    std::cout << "\n";
    std::cout << "Camera matrix: " << content.camera << "\n";
    std::cout << "Rotation matrix: " << content.rotation << "\n";
    std::cout << "Position: " << content.position << "\n";
    std::cout << "\n";
}

void dumpInfo(const DMAPHeader& header)
{
    std::cout << "Depth map header:\n";
    std::cout << "Contains depth (" << yesno(header.type & DMAPHeader::depthMask) << "), "
              << "normals (" << yesno(header.type & DMAPHeader::normalMask) << "), "
              << "and confidence (" << yesno(header.type & DMAPHeader::confidenceMask) << ").\n";
    std::cout << "Raw type: " << std::hex << std::showbase << std::setw(2)
              << (U32)header.type << "\n" << std::dec;
    std::cout << "Image dimensions: " << header.imageWidth << "x"
              << header.imageHeight << "\n";
    std::cout << "Depth image dimensions: " << header.depthWidth << "x"
              << header.depthHeight << "\n";
    std::cout << "Depth range: " << header.minDepth << " - " << header.maxDepth << "\n";
    std::cout << "\n";
}

namespace
{

bool readData(void* dest, size_t bytes, const U8*& src, size_t& size)
{
    if (size < bytes) return false;
    size -= bytes;
    std::memcpy(dest, static_cast<const void*>(src), bytes);
    src += bytes;
    return true;
}

template <typename T>
bool read(T* dest, size_t bytes, const U8*& src, size_t& size)
{
    return readData(static_cast<void*>(dest), bytes, src, size);
}

bool readVec3(dvec3* valOut, const U8*& src, size_t size, bool swapEndian)
{
    dvec3 val;

    if (!read(&val, sizeof(val), src, size)) return false;
    if (swapEndian)
    {
        kraken::swapEndianIP(val.x);
        kraken::swapEndianIP(val.y);
        kraken::swapEndianIP(val.z);
    }
    *valOut = val;
    return true;
}

bool readMat3(dmat3* matOut, const U8*& src, size_t size, bool swapEndian)
{
    dvec3 reader;
    for (U32 i = 0; i < 3; ++i)
    {
        if (!readVec3(&reader, src, size, swapEndian)) return false;
        matOut->setRow(i, reader);
    }
    return true;
}

const static std::function<void(void*)> gs_dmapDeleter = [](void* dmap)
{
    if (dmap)
    {
        delete static_cast<DMAPContent*>(dmap);
    }
};

} // end anonymous namespace

const std::function<void(void*)>* DMAPParser::getDeleter()
{
    return &gs_dmapDeleter;
}

void DMAPParser::getAdditionalRequiredFiles(ResourceLoader::FileInfo* fileInfoOut,
                                            U32* fileInfoCount,
                                            ResourceLoader& loader,
                                            const ResourceLoader::RawData& filesSoFar)
{
    if (!fileInfoCount) return;

    if (filesSoFar.size() >= 3)
    {
        *fileInfoCount = 0;
        return;
    }

    *fileInfoCount = 2;
    if (!fileInfoOut) return;

    ResourceLoader::ResourceID thisID;
    const ResourceLoader::Options* thisOpts;
    const std::string* mediaType;
    const fs::path* currPath;

    getCurrentResourceInfo(loader, &thisID, &thisOpts, &mediaType, &currPath);

    if (!currPath || currPath->empty())
    {
        throw std::logic_error("No path given to DMAP file.");
    }

    auto projectDir = currPath->parent_path();
    if (!fs::exists(projectDir / "cache" / "stheno"))
    {
        fs::create_directories(projectDir / "cache" / "stheno");
    }

    auto cacheDir = projectDir / "cache";

    auto imageID = currPath->stem().string().substr(5);
    if (imageID.size() == 4) imageID = "0" + imageID;
    m_imageID = imageID;

    auto expectedPath = cacheDir / "undistorted_images" / (imageID + ".png");
    if (!fs::exists(expectedPath))
    {
        auto failedToRead = std::runtime_error("Failed to read DMAP header.");
        auto it = filesSoFar.find("main");
        if (it == filesSoFar.end()) throw failedToRead;
        const auto& currFile = it->second;
        const U8* dmapData = currFile.data.get();
        size_t dmapSize = currFile.size;
        DMAPHeader header;
        if (!read(&header, sizeof(header), dmapData, dmapSize)) throw failedToRead;

        bool swapEndian = false;
        if (header.magic == *reinterpret_cast<const U16*>("RD"))
        {
            swapEndian = true;
            kraken::swapEndianIP(header.magic);
            kraken::swapEndianIP(header.imageWidth);
            kraken::swapEndianIP(header.imageHeight);
            kraken::swapEndianIP(header.depthWidth);
            kraken::swapEndianIP(header.depthHeight);
            kraken::swapEndianIP(header.minDepth);
            kraken::swapEndianIP(header.maxDepth);
        }

        U16 twoBytes;
        if (!read(&twoBytes, 2, dmapData, dmapSize)) throw failedToRead;
        if (swapEndian) kraken::swapEndianIP(twoBytes);

        std::string filename;
        filename.resize(twoBytes);
        if (!read(filename.data(), twoBytes, dmapData, dmapSize)) throw failedToRead;

        expectedPath = filename;

        if (!fs::exists(expectedPath))
        {
            throw std::runtime_error("Couldn't find undistorted image \""
                                     + expectedPath.string() + "\".");
        }
    }
    fileInfoOut[0].name = "colour";
    fileInfoOut[0].path = expectedPath;

    fileInfoOut[1].name = "info";
    fileInfoOut[1].path = m_infoCachePath = cacheDir / "stheno" / (imageID + ".json");
    fileInfoOut[1].optional = true;

    m_dmapDirectory = projectDir;
}

bool DMAPParser::parse(const U8* data, size_t size,
                       const kraken::JSONValue& options,
                       ResourceLoader& loader,
                       ResourceLoader::HostResource* resourceOut)
{
    ResourceLoader::ResourceID thisID;
    const ResourceLoader::Options* thisOpts;
    const std::string* mediaType;
    const fs::path* currPath;

    getCurrentResourceInfo(loader, &thisID, &thisOpts, &mediaType, &currPath);

    auto contentHolder = std::make_unique<DMAPContent>();
    auto& content = *contentHolder.get();

    if (thisOpts->parseDataSource.fromType == ResourceLoader::ResourceType::HostResource)
    {
        auto* srcContent = const_cast<DMAPContent*>(reinterpret_cast<const DMAPContent*>(data));
        content = std::move(*srcContent);
        resourceOut->resource = contentHolder.release();
        resourceOut->deleter = DMAPParser::getDeleter();
        resourceOut->typeName = "DMAPContent";
        resourceOut->typeIdentifier = typeid(DMAPContent).hash_code();
    }
    else
    {
        if (size)
        {
            throw std::logic_error("Expected ResourceLoader::RawData (multiple files),"
                                   "but got raw data (from a single file).");
        }
        const auto* rawData = reinterpret_cast<const ResourceLoader::RawData*>(data);

        const auto mainIt = rawData->find("main");
        if (mainIt == rawData->end())
        {
            throw std::logic_error("Can't find main data.");
        }

        const U8* dmapData = mainIt->second.data.get();
        size_t dmapSize = mainIt->second.size;


        DMAPHeader header;
        if (!read(&header, sizeof(header), dmapData, dmapSize)) return false;

        bool swapEndian = false;
        if (header.magic == *reinterpret_cast<const U16*>("RD"))
        {
            swapEndian = true;
            kraken::swapEndianIP(header.magic);
            kraken::swapEndianIP(header.imageWidth);
            kraken::swapEndianIP(header.imageHeight);
            kraken::swapEndianIP(header.depthWidth);
            kraken::swapEndianIP(header.depthHeight);
            kraken::swapEndianIP(header.minDepth);
            kraken::swapEndianIP(header.maxDepth);
        }

        U16 twoBytes;
        if (!read(&twoBytes, 2, dmapData, dmapSize)) return false;
        if (swapEndian) kraken::swapEndianIP(twoBytes);

        content.filename.resize(twoBytes);
        if (!read(content.filename.data(), twoBytes, dmapData, dmapSize)) return false;

        U32 fourBytes;
        if (!read(&fourBytes, 4, dmapData, dmapSize)) return false;
        if (swapEndian) kraken::swapEndianIP(fourBytes);
        content.neighbours.resize(fourBytes);
        if (!read(content.neighbours.data(), 4*fourBytes, dmapData, dmapSize)) return false;
        if (swapEndian)
        {
            for (auto& neighbour : content.neighbours)
            {
                kraken::swapEndianIP(neighbour);
            }
        }

        if (!readMat3(&content.camera, dmapData, dmapSize, swapEndian)) return false;
        if (!readMat3(&content.rotation, dmapData, dmapSize, swapEndian)) return false;
        if (!readVec3(&content.position, dmapData, dmapSize, swapEndian)) return false;

        FloatImage depthMap;
        FloatImage normalMap;
        {
            depthMap.resize(header.depthWidth, header.depthHeight, 1);
            normalMap.resize(header.depthWidth, header.depthHeight, 3);
            content.normalDepthMap.resize(header.depthWidth,
                                          header.depthHeight, 4);

            if (!read(depthMap.data(),
                      depthMap.byteSize(),
                      dmapData,
                      dmapSize)) return false;
            if (!read(normalMap.data(),
                      normalMap.byteSize(),
                      dmapData,
                      dmapSize)) return false;
            if (swapEndian)
            {
                for (size_t i = 0; i < depthMap.floatCount(); ++i)
                {
                    kraken::swapEndianIP(depthMap.data()[i]);
                }
                for (size_t i = 0; i < normalMap.floatCount(); ++i)
                {
                    kraken::swapEndianIP(normalMap.data()[i]);
                }
            }

            /// TODO: Check if this is needed.
            if (options("blur"))
            {
                normalMap = blurNormals(normalMap);
            }

            for (U32 y = 0; y < content.normalDepthMap.height(); ++y)
            {
                for (U32 x = 0; x < content.normalDepthMap.width(); ++x)
                {
                    vec4 value = normalMap.getPixel(x, y);
                    value.w = depthMap.getPixel(x, y).r;
                    content.normalDepthMap.setPixel(x, y, value);
                }
            }
        }
        if (!options("debugDump"))
        {
            // If we do not dump anything, then depthMap and normalMap are no
            // longer needed. Thus, we explicitly clear them to free up some
            // memory.
            depthMap.resize(0, 0, 0);
            normalMap.resize(0, 0, 0);
        }

        content.confidenceMap.resize(header.depthWidth, header.depthHeight, 1);
        if (!read(content.confidenceMap.data(),
                  content.confidenceMap.byteSize(),
                  dmapData,
                  dmapSize)) return false;
        if (swapEndian)
        {
            for (size_t i = 0; i < content.confidenceMap.floatCount(); ++i)
            {
                kraken::swapEndianIP(content.confidenceMap.data()[i]);
            }
        }

        // Load colour image.
        {
            const auto it = rawData->find("colour");
            if (it == rawData->end())
            {
                throw std::logic_error("Can't find main data.");
            }
            MemoryIStream memstream(it->second.data.get(), it->second.size);
            auto result = content.colourImage.read(memstream);
            if (!result) return false;
            if (content.colourImage.colourType() != kraken::image::RGBA8)
            {
                content.colourImage = content.colourImage.convert(kraken::image::RGBA8);
            }

            /*auto decimateFactor = computeDecimationFactor(content.colourImage.width(), content.normalDepthMap.width());
            if ((content.normalDepthMap.width() << decimateFactor) != content.colourImage.width()
                || (content.normalDepthMap.height() << decimateFactor) != content.colourImage.height())
            {
                content.colourImage = content.colourImage.crop(content.normalDepthMap.width() << decimateFactor, content.normalDepthMap.height() << decimateFactor);
            }*/
        }

        kraken::JSONValue cachedInfo = nullptr;

        const auto infoIt = rawData->find("info");
        if (infoIt != rawData->end() && infoIt->second.size)
        {
            MemoryIStream memstream(infoIt->second.data.get(), infoIt->second.size);
            memstream >> cachedInfo;
        }

        priv::QualityEstimatorOptions opts{};

        const auto& estimatorOptions = options("qualityEstimator");

        if (estimatorOptions != nullptr)
        {
            if (estimatorOptions("focusHighpassSigma") != nullptr) opts.focusHighpassSigma = (F32)estimatorOptions("focusHighpassSigma");
            if (estimatorOptions("focusLowerCutoff") != nullptr) opts.focusLowerCutoff = (F32)estimatorOptions("focusLowerCutoff");
            if (estimatorOptions("focusCutoffMeanIterations") != nullptr) opts.focusCutoffMeanIterations = (U32)estimatorOptions("focusCutoffMeanIterations");
            if (estimatorOptions("maxAngle") != nullptr) opts.maxAngle = (F32)estimatorOptions("maxAngle");
            if (estimatorOptions("minConfidence") != nullptr) opts.minConfidence = (F32)estimatorOptions("minConfidence");
            if (estimatorOptions("blurSigma") != nullptr) opts.blurSigma = (F32)estimatorOptions("blurSigma");
            if (estimatorOptions("expandRadius") != nullptr) opts.expandRadius = (F32)estimatorOptions("expandRadius");
            if (estimatorOptions("expandSigma") != nullptr) opts.expandSigma = (F32)estimatorOptions("expandSigma");
            if (estimatorOptions("expandMinConfidence") != nullptr) opts.expandMinConfidence = (F32)estimatorOptions("expandMinConfidence");
        }

        kraken::JSONValue currInfo;
        currInfo["version"] = "0";
        currInfo["parameters"]["focus"]["highpassSigma"] = opts.focusHighpassSigma;
        currInfo["parameters"]["focus"]["lowerCutoff"] = opts.focusLowerCutoff;
        currInfo["parameters"]["focus"]["cutoffMeanIterations"] = opts.focusCutoffMeanIterations;

        if (cachedInfo("version") != currInfo("version")) cachedInfo = nullptr;

        if (cachedInfo("parameters")("focus") == currInfo("parameters")("focus"))
        {
            currInfo["data"]["focus"] = cachedInfo("data")("focus");
        }
        else
        {
            auto focusInfo = computeFocus(content, opts);
            currInfo["data"]["focus"]["depthMean"] = focusInfo.depthMean;
            currInfo["data"]["focus"]["depthSD"] = focusInfo.depthSD;
        }

        contentHolder->focusInfo.depthMean = (F32)currInfo("data")("focus")("depthMean");
        contentHolder->focusInfo.depthSD = (F32)currInfo("data")("focus")("depthSD");

        /// TODO implement: // if (opts.writeOutputCache)
        {
            std::ofstream output(m_infoCachePath);
            if (output.is_open())
            {
                kraken::prettyPrint(output, currInfo);
            }
        }

        content.qualityMap = estimateQualityMap(content, opts);

        if (options("debugDump"))
        {
            struct DumpInfo
            {
                std::string prefix;
                FloatImage& img;
            };

            for (const auto& dump :
                    {
                        DumpInfo{"depth", depthMap},
                        DumpInfo{"normal", normalMap},
                        DumpInfo{"confidence", content.confidenceMap},
                        DumpInfo{"quality", content.qualityMap},
                    })
            {
                auto dumpDir = m_dmapDirectory / "dump" / dump.prefix;
                if (!fs::exists(dumpDir)) fs::create_directories(dumpDir);
                auto imgPath = dumpDir / (m_imageID + ".png");
                if (fs::exists(imgPath)) continue;
                vec4 min, max;
                std::ofstream imgOut(imgPath);
                if (imgOut.is_open())
                {
                    toKrakenImage(dump.img, &min, &max).write(imgOut, "png");
                }
                kraken::JSONValue metadata;
                for (U32 i = 0; i < 4; ++i)
                {
                    metadata["min"][i] = min[i];
                    metadata["max"][i] = max[i];
                }
                std::ofstream metadataOut(dumpDir / (m_imageID + ".json"));
                if (metadataOut.is_open())
                {
                    kraken::prettyPrint(metadataOut, metadata);
                }
            }
        }

        resourceOut->resource = contentHolder.release();
        resourceOut->deleter = DMAPParser::getDeleter();
        resourceOut->typeName = "DMAPContent";
        resourceOut->typeIdentifier = typeid(DMAPContent).hash_code();
    }

    if ((thisOpts->type & ResourceLoader::ResourceType::VulkanResource)
        == ResourceLoader::ResourceType::VulkanResource)
    {
        auto newOptions = *thisOpts;
        newOptions.parent = thisID;
        newOptions.type = ResourceLoader::ResourceType::VulkanResource;
        newOptions.vulkanDataSource.fromType = ResourceLoader::ResourceType::None;

        auto& dmapOpts = newOptions.vulkanOptions["image"];

        dmapOpts["usageFlags"] = kraken::JSONValue::CreateArray({});
        dmapOpts["usageFlags"].push_back("VK_IMAGE_USAGE_SAMPLED_BIT");

        dmapOpts["width"] = content.normalDepthMap.width();
        dmapOpts["height"] = content.normalDepthMap.height();
        dmapOpts["format"] = "VK_FORMAT_R32G32B32A32_SFLOAT";

        /// TODO [3fbe992a] insert here.

        m_sources[0] = {0, reinterpret_cast<const void*>(&content.normalDepthMap)};
        m_sources[1] = {0, reinterpret_cast<const void*>(&content.confidenceMap)};
        m_sources[2] = {0, reinterpret_cast<const void*>(&content.qualityMap)};
        m_sources[3] = {1, reinterpret_cast<const void*>(&content.colourImage)};

        newOptions.parseDataSource.data = nullptr;
        newOptions.parseDataSource.size = 0;
        newOptions.parseDataSource.fromType = ResourceLoader::ResourceType::None;
        newOptions.vulkanDataSource.data = m_sources;
        newOptions.vulkanDataSource.size = 0;
        loader.load(*mediaType, newOptions);

        dmapOpts["format"] = "VK_FORMAT_R32_SFLOAT";
        newOptions.vulkanDataSource.data = m_sources+1;
        newOptions.vulkanDataSource.size = 0;
        loader.load(*mediaType, newOptions);

        dmapOpts["format"] = "VK_FORMAT_R32_SFLOAT";
        newOptions.vulkanDataSource.data = m_sources+2;
        newOptions.vulkanDataSource.size = 0;
        loader.load(*mediaType, newOptions);

        if (options("loadColourImage"))
        {
            dmapOpts["format"] = "VK_FORMAT_R8G8B8A8_UNORM";
            newOptions.vulkanDataSource.data = m_sources+3;
            newOptions.vulkanDataSource.size = 0;
            loader.load(*mediaType, newOptions);
        }
    }

    return true;
}

bool writeImage(fs::path out, float* floatImg, U32 width, U32 height)
{
    kraken::image::Image imgOut(width, height, kraken::image::RGBA8);
    vec4 minVals{ floatImg[0], floatImg[1], floatImg[2], floatImg[3] };
    vec4 maxVals = minVals;

    for (size_t i = 0; i < width * height; ++i)
    {
        for (U32 c = 0; c < 4; ++c)
        {
            minVals[c] = std::min(minVals[c], floatImg[4*i+c]);
            maxVals[c] = std::max(maxVals[c], floatImg[4*i+c]);
        }
    }

    vec4 scale = maxVals - minVals;
    std::cerr << "Writing image with scale " << scale << "\n";
    std::cerr << "|-> MinVals: " << minVals << "\n";
    std::cerr << "|-> MaxVals: " << maxVals << "\n";
    for (U32 c = 0; c < 4; ++c)
    {
        if (scale[c] < 1e-6f) scale[c] = 0.f;
        else scale[c] = 1.f/scale[c];
    }

    imgOut.fill([floatImg, minVals, scale, width](U32 x, U32 y)
    {
        size_t idx = y*width + x;
        size_t offset = 4*idx;
        vec4 rawVal{floatImg[offset], floatImg[offset+1], floatImg[offset+2], floatImg[offset+3]};
        vec4 rescaled;
        for (U32 c = 0; c < 4; ++c)
        {
            rescaled[c] = (rawVal[c]-minVals[c])*scale[c];
        }
        return vec4{rescaled.x, rescaled.y, rescaled.z, 1.0f};
    });


    std::ofstream outstream(out);
    return imgOut.write(outstream, "png");
}

void write1CImage(fs::path out, float* floatImg, U32 width, U32 height)
{
    kraken::image::Image imgOut(width, height, kraken::image::RGBA8);
    float minVal = floatImg[0];
    float maxVal = minVal;

    for (size_t i = 0; i < width * height; ++i)
    {
        minVal = std::min(minVal, floatImg[i]);
        maxVal = std::max(maxVal, floatImg[i]);
    }

    float scale = maxVal - minVal;
    std::cerr << "Writing image with scale " << scale << "(" << minVal << "-" << maxVal << ")\n";
    if (scale < 1e-6f) scale = 0.f;
    else scale = 1.f/scale;

    imgOut.fill([floatImg, minVal, scale, width](U32 x, U32 y)
    {
        size_t idx = y*width + x;
        float val = (floatImg[idx]-minVal) * scale;
        return vec4{val, val, val, 1.0f};
    });


    std::ofstream outstream(out);
    imgOut.write(outstream, "png");
}

FloatImage blurNormals(const FloatImage& image)
{
    FloatImage img2(image.width(), image.height(), image.channels());
    for (U32 y = 1; y+1 < image.height(); ++y)
    {
        for (U32 x = 1; x+1 < image.width(); ++x)
        {
            vec4 blurred = 1*image.getPixel(x-1, y-1)
                         + 2*image.getPixel(x  , y-1)
                         + 1*image.getPixel(x+1, y-1)
                         + 2*image.getPixel(x-1, y  )
                         + 5*image.getPixel(x  , y  )
                         + 2*image.getPixel(x+1, y  )
                         + 1*image.getPixel(x-1, y-1)
                         + 2*image.getPixel(x  , y-1)
                         + 1*image.getPixel(x+1, y-1);
            img2.setPixel(x, y, zero(blurred) ? vec4{0, 0, 0, 0} : normalise(blurred));
        }
    }
    return img2;
}



kraken::image::Image toKrakenImage(const FloatImage& floatImg,
                                   vec4* minValsOut, vec4* maxValsOut)
{
    kraken::image::Image img(floatImg.width(), floatImg.height(),
                             {(U8)floatImg.channels(), 16});
    if (floatImg.empty()) return img;
    vec4 minRange = floatImg.getPixel(0, 0);
    vec4 maxRange = minRange;
    for (U32 y = 0; y < floatImg.height(); ++y)
    {
        for (U32 x = 0; x < floatImg.width(); ++x)
        {
            vec4 val = floatImg.getPixel(x, y);
            for (U32 c = 0; c < floatImg.channels(); ++c)
            {
                minRange[c] = std::min(minRange[c], val[c]);
                maxRange[c] = std::max(maxRange[c], val[c]);
            }
        }
    }
    if (minValsOut) *minValsOut = minRange;
    if (maxValsOut) *maxValsOut = maxRange;
    vec4 scale = maxRange-minRange;
    for (U32 i = 0; i < 4; ++i)
    {
        if (scale[i] >= 1e-5f)
        {
            scale[i] = 1.f/scale[i];
        }
        else
        {
            scale[i] = 0;
        }
    }
    img.fill([&floatImg, scale, minRange](U32 x, U32 y)
    {
        vec4 res = floatImg.getPixel(x, y)-minRange;
        for (U32 i = 0; i < 4; ++i)
        {
            res[i] *= scale[i];
        }
        return res;
    });
    return img;
}

void DMAPParser::addResourceSupport(ResourceLoader& loader, std::string mediaType)
{
    loader.addResourceSupport(mediaType,
                              []{ return std::make_unique<DMAPParser>(); });
}
