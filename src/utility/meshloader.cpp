#include "utility/meshloader.hpp"

#include "vulkan/common.hpp"
#include "vulkan/mesh.hpp"
#include <kraken/io/data/mesh.hpp>
#include <kraken/io/data/types.hpp>
#include <kraken/io/read/ply.hpp>
#include <kraken/io/read/stl.hpp>

// Everything is assumed to be normalised / floating-point unless it is as large
// as a float.
VkFormat toVulkanFormat(kraken::io::PrimitiveDataType type, U32 componentCount)
{
    if (componentCount > 4) return VK_FORMAT_UNDEFINED;
    using DataType = kraken::io::PrimitiveDataType;

    constexpr static VkFormat typeToFormat[10][4] =
    {
        // Byte
        {
            VK_FORMAT_R8_SNORM, VK_FORMAT_R8G8_SNORM,
            VK_FORMAT_R8G8B8_SNORM, VK_FORMAT_R8G8B8A8_SNORM,
        },
        // UByte
        {
            VK_FORMAT_R8_UNORM, VK_FORMAT_R8G8_UNORM,
            VK_FORMAT_R8G8B8_UNORM, VK_FORMAT_R8G8B8A8_UNORM,
        },
        // Short
        {
            VK_FORMAT_R16_SNORM, VK_FORMAT_R16G16_SNORM,
            VK_FORMAT_R16G16B16_SNORM, VK_FORMAT_R16G16B16A16_SNORM,
        },
        // UShort
        {
            VK_FORMAT_R16_UNORM, VK_FORMAT_R16G16_UNORM,
            VK_FORMAT_R16G16B16_UNORM, VK_FORMAT_R16G16B16A16_UNORM,
        },
        // Int
        {
            VK_FORMAT_R32_SINT, VK_FORMAT_R32G32_SINT,
            VK_FORMAT_R32G32B32_SINT, VK_FORMAT_R32G32B32A32_SINT,
        },
        // UInt
        {
            VK_FORMAT_R32_UINT, VK_FORMAT_R32G32_UINT,
            VK_FORMAT_R32G32B32_UINT, VK_FORMAT_R32G32B32A32_UINT,
        },
        // Long
        {
            VK_FORMAT_R64_SINT, VK_FORMAT_R64G64_SINT,
            VK_FORMAT_R64G64B64_SINT, VK_FORMAT_R64G64B64A64_SINT,
        },
        // ULong
        {
            VK_FORMAT_R64_UINT, VK_FORMAT_R64G64_UINT,
            VK_FORMAT_R64G64B64_UINT, VK_FORMAT_R64G64B64A64_UINT,
        },
        // Float
        {
            VK_FORMAT_R32_SFLOAT, VK_FORMAT_R32G32_SFLOAT,
            VK_FORMAT_R32G32B32_UINT, VK_FORMAT_R32G32B32A32_SFLOAT,
        },
        // Double
        {
            VK_FORMAT_R64_SFLOAT, VK_FORMAT_R64G64_SFLOAT,
            VK_FORMAT_R64G64B64_SFLOAT, VK_FORMAT_R64G64B64A64_SFLOAT,
        },
    };

    switch (type)
    {
    case DataType::Byte:   return typeToFormat[0][componentCount];
    case DataType::UByte:  return typeToFormat[1][componentCount];
    case DataType::Short:  return typeToFormat[2][componentCount];
    case DataType::UShort: return typeToFormat[3][componentCount];
    case DataType::Int:    return typeToFormat[4][componentCount];
    case DataType::UInt:   return typeToFormat[5][componentCount];
    case DataType::Long:   return typeToFormat[6][componentCount];
    case DataType::ULong:  return typeToFormat[7][componentCount];
    case DataType::Float:  return typeToFormat[8][componentCount];
    case DataType::Double: return typeToFormat[9][componentCount];
    default:
        return VK_FORMAT_UNDEFINED;
    }
}

std::vector<NamedMeshAttributeElement>
extractAttributes(const kraken::io::VertexLayout& layout,
                  const kraken::io::ExtendedAttributeNames* extNames)
{
    std::vector<NamedMeshAttributeElement> elems;
    U32 currOffset = 0;
    for (const auto& elem : layout.attributes)
    {
        NamedMeshAttributeElement attr;
        attr.attribute.offset = currOffset;
        attr.attribute.format = toVulkanFormat(elem.dataType, elem.componentCount);
        if (auto canonicalName = getCanonicalName(elem.usage))
        {
            attr.name = canonicalName;
        }
        else if (extNames)
        {
            if (auto name = extNames->extendedName(elem.usage))
            {
                attr.name = name.value();
            }
        }

        if (attr.name == "")  attr.name = "unidentified";

        if (attr.attribute.format != VK_FORMAT_UNDEFINED)
        {
            elems.push_back(attr);
        }
        // If the format is undefined, it is either padding or unrecognised.
        // In either case, we simply skip it.
        currOffset += elem.byteSize();
    }
    return elems;
}

bool loadPLY(kraken::io::DataSource& source, Mesh& meshOut)
{
    using namespace kraken;

    namespace ply = io::formats::ply;

    meshOut.destroy();
    meshOut.resizeVertexBuffers(1);
    meshOut.resizeIndexBuffers(1);
    auto& vBuf = meshOut.vertexBuffer(0);
    auto& iBuf = meshOut.indexBuffer(0);

    ExternalMemoryAllocation vertexOutput([&vBuf](std::size_t bytes, std::size_t)
                                          {
                                              vBuf.reserveHostMemory(bytes);
                                              return vBuf.buffer;
                                          });
    ExternalMemoryAllocation triangleOutput([&iBuf](std::size_t bytes, std::size_t)
                                            {
                                                iBuf.reserveHostMemory(bytes);
                                                return iBuf.buffer;
                                            });

    io::VertexLayout vertexLayout;
    std::size_t vertexCount;
    ply::TriangleInfo triInfo;
    ply::ElementParserFunctions parsers;
    io::ExtendedAttributeNames extNames;
    parsers[ply::getVertexElementName()] = ply::createDefaultVertexParser
    (
        vertexOutput, vertexLayout, vertexCount, &extNames
    );
    parsers[ply::getFaceElementName()] = ply::createTriangulatingFaceParser
    (
        triangleOutput, triInfo
    );

    if (!ply::read(source, parsers)) return false;

    vBuf.stride = vertexLayout.vertexStride();
    vBuf.count = vBuf.hostAllocationSize / vBuf.stride;

    iBuf.count = iBuf.hostAllocationSize / triInfo.indexByteWidth;

    meshOut.setAttributes(extractAttributes(vertexLayout, &extNames));
    if (triInfo.indexByteWidth == 1)
    {
        iBuf.reserveHostMemory(2*iBuf.hostAllocationSize);
        auto* bytebuf = static_cast<U8*>(iBuf.buffer);
        auto* shortbuf = static_cast<U16*>(iBuf.buffer);
        for (U32 i = 0; i < iBuf.count; ++i)
        {
            const U32 idx = iBuf.count - 1 - i;
            shortbuf[idx] = bytebuf[idx];
        }
        triInfo.indexByteWidth = 2;
    }

    iBuf.stride = triInfo.indexByteWidth;

    meshOut.bindings()[0] =
    {
        .binding = 0,
        .stride = (uint32_t)vBuf.stride,
        .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
    };

    return true;
}

bool loadSTL(kraken::io::DataSource& source, Mesh& meshOut)
{
    using namespace kraken;

    namespace stl = io::formats::stl;

    meshOut.destroy();
    meshOut.resizeVertexBuffers(1);
    auto& vBuf = meshOut.vertexBuffer(0);

    ExternalMemoryAllocation vertexOutput([&vBuf](std::size_t bytes, std::size_t)
                                          {
                                              vBuf.reserveHostMemory(bytes);
                                              return vBuf.buffer;
                                          });

    const io::VertexLayout desiredLayout
    {{
        {io::PrimitiveDataType::Float, io::VertexAttribute::Position, 3, 0},
        //{io::PrimitiveDataType::Float, io::VertexAttribute::Normal, 3, 0},
    }};

    // Here it would be natural to add support for reserving space for tangents.
    io::VertexLayout vertexLayout;
    std::size_t vertexCount;

    if (!stl::read(source,
                   vertexOutput,
                   vertexCount,
                   vertexLayout,
                   &desiredLayout))
    {
        return false;
    }

    vBuf.stride = vertexLayout.vertexStride();
    vBuf.count = vBuf.hostAllocationSize / vBuf.stride;

    meshOut.setAttributes(extractAttributes(vertexLayout, nullptr));

    meshOut.bindings()[0] =
    {
        .binding = 0,
        .stride = (uint32_t)vBuf.stride,
        .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
    };

    return true;
}
