#include "resourceloader.hpp"
#include <cstring>
#include <fstream>

#include "vulkan/types.hpp"
#include "resourceparser.hpp"

std::atomic<bool> g_showResourceLoaderDebugOutput{false};
std::atomic<bool> g_showResourceLoaderExtraDebugOutput{false};

ResourceLoader::ResourceLoader()
{
    m_liveThreadCount = 0;
    m_activeDiskThreads = 0;
    m_activeParseThreads = 0;
    m_activeTransferThreads = 0;
}

void ResourceLoader::create(U32 maxParseThreads,
                            fs::path basePath,
                            VkDevice device,
                            QueueInformation queueInfo,
                            VkDeviceSize stagingAreaSize,
                            VkDeviceSize chunkSize,
                            VmaAllocator allocator)
{
    destroy();

    if (createTransferrer(device, queueInfo, stagingAreaSize,
                          chunkSize, allocator) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create transfer part.");
    }
    m_basePath = basePath;
    m_destroyObjects = false;
    m_nextResourceID = 0;
    m_allocator = allocator;
    m_device = device;

    addDefaultResourceSupport();

    m_diskThread = std::thread(&ResourceLoader::diskThread, this, 0);
    m_hostTransferThread = std::thread(&ResourceLoader::hostTransferThread, this, 1);
    m_parseThreads.resize(maxParseThreads);
    for (U32 i = 0; i < maxParseThreads; ++i)
    {
        m_parseThreads[i] = std::thread(&ResourceLoader::parserThread, this, i+2);
        m_currParsing[m_parseThreads[i].get_id()] = invalidResource;
    }

    m_liveThreadCount = 2 + maxParseThreads;

    m_activeDiskThreads = 0;
    m_activeParseThreads = 0;
    m_activeTransferThreads = 0;
}

VkResult ResourceLoader::createTransferrer(VkDevice device,
                                           QueueInformation queueInfo,
                                           VkDeviceSize stagingAreaSize,
                                           VkDeviceSize blockSize,
                                           VmaAllocator allocator)
{
    if (!blockSize)
    {
        return VK_ERROR_INITIALIZATION_FAILED;
    }

    if (blockSize % 1048576)
    {
        blockSize += 1048576 - (blockSize % 1048576);
    }

    if (stagingAreaSize % blockSize)
    {
        stagingAreaSize += blockSize - (stagingAreaSize % blockSize);
    }
    m_blockCount = stagingAreaSize / blockSize;

    if (m_blockCount > std::numeric_limits<decltype(m_blocks)::value_type>::max())
    {
        return VK_ERROR_INITIALIZATION_FAILED;
    }
    m_blocks.resize(m_blockCount);
    for (size_t i = 0; i < m_blocks.size(); ++i)
    {
        m_blocks[i] = i+1;
    }
    m_nextFreeBlock = 0;
    m_blockSize = blockSize;

    VkCommandPoolCreateInfo commandPoolCreate{};
    commandPoolCreate.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    commandPoolCreate.queueFamilyIndex = queueInfo.familyIndex;
    commandPoolCreate.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
    auto result = vkCreateCommandPool(device, &commandPoolCreate, nullptr, &m_commandPool);
    if (result != VK_SUCCESS) return result;

    vkGetDeviceQueue(device, queueInfo.familyIndex, queueInfo.queueIndex, &m_queue);

    VkBufferCreateInfo bufCreate{};
    bufCreate.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufCreate.size = stagingAreaSize;
    bufCreate.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    bufCreate.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;

    VmaAllocationCreateInfo vmaInfo{};
    vmaInfo.usage = VMA_MEMORY_USAGE_CPU_ONLY;

    result = vmaCreateBuffer(allocator, &bufCreate, &vmaInfo,
                             &m_stagingBuffer.buffer, &m_stagingBuffer.allocation,
                             nullptr);

    if (result == VK_SUCCESS)
    {
        vmaSetAllocationName(allocator,
                             m_stagingBuffer.allocation,
                             "Staging buffer");
    }

    return result;
}

void ResourceLoader::destroy()
{
    m_destroyObjects = true;

    destroyThreads();

    if (m_queue != VK_NULL_HANDLE)
    {
        vkQueueWaitIdle(m_queue);
    }

    m_parserCreators.clear();

    m_diskTaskQueue = {};
    m_parseTaskQueue = {};
    m_hostTransferTaskQueue = {};
    m_deviceTransferTasks.clear();

    for (auto& batch : m_submittedBatches)
    {
        vkWaitForFences(m_device, 1, &batch.fence, VK_TRUE, 0xffffffffffffffff);
        vkFreeCommandBuffers(m_device,
                             m_commandPool,
                             batch.commandBuffers.size(),
                             batch.commandBuffers.data());
    }

    m_transferFinalisations.clear();

    m_toInitTransfers.clear();
    m_submittedBatches.clear();

    m_blocks.clear();

    destroyResources();
    m_resourceInfo.clear();

    if (m_commandPool != VK_NULL_HANDLE)
    {
        vkDestroyCommandPool(m_device, m_commandPool, nullptr);
        m_commandPool = VK_NULL_HANDLE;
    }

    if (m_stagingBuffer.allocation != VK_NULL_HANDLE)
    {
        vmaDestroyBuffer(m_allocator, m_stagingBuffer.buffer, m_stagingBuffer.allocation);
        m_stagingBuffer.allocation = VK_NULL_HANDLE;
    }

    m_queue = VK_NULL_HANDLE;
    m_allocator = VK_NULL_HANDLE;
    m_device = VK_NULL_HANDLE;
}

void ResourceLoader::addResourceSupport(std::string mediaType,
                                        ResourceParserCreator creator)
{
    std::lock_guard lg(m_parserCreatorMutex);
    m_parserCreators[mediaType] = creator;
}

ResourceLoader::Status ResourceLoader::getStatus(ResourceLoader::ResourceID id)
{
    std::lock_guard lg(m_resourceInfoMutex);
    return m_resourceInfo[id].status;
}

VkResult ResourceLoader::getResult(ResourceLoader::ResourceID id)
{
    std::lock_guard lg(m_resourceInfoMutex);
    return m_resourceInfo[id].result;
}

ResourceLoader::ResourceType ResourceLoader::getAvailableResourceTypes(ResourceLoader::ResourceID id)
{
    std::lock_guard lg(m_resourceMutex);
    return m_resources[id].available;
}

#define CREATE_ACQUIRE_FUNCTION_IMPLEMENTATION(name, type, variable)\
bool ResourceLoader::acquire##name(ResourceID id, type* out)\
{\
    bool dataLeft = true;\
    {\
        std::lock_guard lg(m_resourceMutex);\
        auto* res = tryAcquire(id, ResourceType::name, dataLeft);\
        if (!res) return false;\
        if (out) *out = std::move(res->variable);\
    }\
    if (!dataLeft)\
    {\
        destroyResource(id);\
    }\
    return true;\
}

CREATE_ACQUIRE_FUNCTION_IMPLEMENTATION(RawFile, RawData, rawData)
CREATE_ACQUIRE_FUNCTION_IMPLEMENTATION(HostResource, HostResource, hostResource)
CREATE_ACQUIRE_FUNCTION_IMPLEMENTATION(VulkanResource, VulkanResource, vulkanResource)

#undef CREATE_ACQUIRE_FUNCTION_IMPLEMENTATION

const ResourceLoader::SubresourceList&
ResourceLoader::getSubresourceList(ResourceID id)
{
    std::lock_guard lg(m_resourceMutex);
    return m_resources[id].subresources;
}

void ResourceLoader::destroyResource(ResourceLoader::ResourceID id)
{
    ResourceID parent;

    {
    std::lock_guard guard0(m_resourceMutex);
    std::lock_guard guard1(m_resourceInfoMutex);

    auto info = m_resourceInfo.find(id);
    if (info != m_resourceInfo.end()
        && info->second.currentStage == Stage::ReadyInCallback)
    {
        return; // We MUST not destroy this yet as we will then kill the
                // callback function as well, which will cause trouble.
    }
    auto resource = m_resources.find(id);
    if (info == m_resourceInfo.end() || resource == m_resources.end())
    {
        throw std::runtime_error("Trying to destroy non-existent resource.");
    }

    if (info->second.transferInitialised != VK_NULL_HANDLE)
    {
        vkDestroyEvent(m_device, info->second.transferInitialised, nullptr);
    }

    if (info->second.finishedChildCount != resource->second.subresources.size())
    {
        throw std::logic_error("Trying to destroy parent before children "
                               "finished.");
    }
    if (info->second.destroyedChildCount != resource->second.subresources.size())
    {
        throw std::logic_error("Trying to destroy parent before children were "
                               "destroyed.");
    }

    parent = info->second.options.parent;

    m_resourceInfo.erase(info);
    m_resources.erase(resource);

    } // Unlock mutexes so we can notify parent of child destruction.

    notifyChildDestroyed(parent, id);
}

bool ResourceLoader::exists(ResourceID id)
{
    std::lock_guard lg(m_resourceMutex);
    return m_resources.find(id) != m_resources.end();
}


void ResourceLoader::unblockResource(ResourceID id, ResourceType types)
{
    if (!types) return;
    std::lock_guard lg0(m_resourceMutex);
    std::lock_guard lg1(m_resourceInfoMutex);
    std::lock_guard lg2(m_diskTaskMutex);
    std::lock_guard lg3(m_parseTaskMutex);
    std::lock_guard lg4(m_hostTransferTaskMutex);

    U32 disk = 0, parse = 0, transfer = 0;

    prelockedRecursiveUnblockResource(id, types, &disk, &parse, &transfer);

    if (disk) m_diskReadCond.notify_all();
    if (parse) m_parseCond.notify_all();
    if (transfer) m_hostTransferCond.notify_all();
}

void ResourceLoader::getActiveThreadCounts(U32* diskThreads,
                                           U32* parseThreads,
                                           U32* transferThreads) const
{
    if (diskThreads) *diskThreads = m_activeDiskThreads;
    if (parseThreads) *parseThreads = m_activeParseThreads;
    if (transferThreads) *transferThreads = m_activeTransferThreads;
}

bool ResourceLoader::isIdle() const
{
    U32 a, b, c;
    getActiveThreadCounts(&a, &b, &c);
    return !a && !b && !c;
}

void ResourceLoader::panicIfIdle(bool writeDebugOutput)
{
    writeDebugOutput = writeDebugOutput || g_showResourceLoaderDebugOutput;
    if (writeDebugOutput) std::cerr << "RESOURCELOADER PANIC REQUESTED!!!\n";
    if (!isIdle())
    {
        if (writeDebugOutput) std::cerr << "Well, no longer idle.\n";
        return;
    }

    // Yes, danger, not protected by a mutex. But this is for debugging, where
    // there may already be a mistake (e.g. two threads waiting for the same
    // mutex), and we do NOT want this to have any risk of deadlocking.
    // It also *should* be safe since all threads were just idle, and while they
    // may be scheduled to run now, we are only reading the size of some queues.

    if (writeDebugOutput)
    {
        std::cerr << "Waiting disk tasks: " << m_diskTaskQueue.size() << "\n";
        std::cerr << "Waiting parse tasks: " << m_parseTaskQueue.size() << "\n";
        std::cerr << "Waiting host transfer tasks: " << m_hostTransferTaskQueue.size() << "\n";
        std::cerr << "Waiting device transfer tasks: " << m_deviceTransferTasks.size() << "\n";

        std::cerr << "Is idle now? " << (isIdle() ? "Yes" : "No") << "\n";

        if (m_diskTaskQueue.empty() && m_parseTaskQueue.empty() && m_hostTransferTaskQueue.empty() && m_deviceTransferTasks.empty() && isIdle())
        {
            std::cerr << "This is very bad. Nothing on queues, all threads idle, yet this was called. Way more information follows now:\n";
            std::cerr << "\033[31mBlocked disk tasks\033[0m:\n";
            for (auto& task : m_blockedDiskTasks)
            {
                std::cerr << "Key (ID): " << task.first << "\n";
                auto& val = task.second;
                std::cerr << "Value (Task):\n";
                std::cerr << "| path " << val.path << "\n"
                             "| priority " << val.priority << "\n"
                             "| id " << val.id << "\n"
                             "| relativeToLoaderPath " << val.relativeToLoaderPath << "\n";
            }
            std::cerr << "EOS\n"; // End of struct :)

            std::cerr << "\033[31mBlocked parse tasks\033[0m:\n";
            for (auto& task : m_blockedParseTasks)
            {
                std::cerr << "Key (ID): " << task.first << "\n";
                auto& val = task.second;
                std::cerr << "Value (Task):\n";
                std::cerr << "| priority " << val.priority << "\n"
                             "| id " << val.id << "\n";
            }
            std::cerr << "EOS\n";

            std::cerr << "\033[31mBlocked host transfer tasks\033[0m:\n";
            for (auto& task : m_blockedHostTransferTasks)
            {
                std::cerr << "Key (ID): " << task.first << "\n";
                auto& val = task.second;
                std::cerr << "Value (Task):\n";
                std::cerr << "| priority " << val.priority << "\n"
                             "| id " << val.id << "\n";
            }
            std::cerr << "EOS\n";

            std::cerr << "\033[1;32mResources\033[0m:\n";
            for (auto& entry : m_resources)
            {
                std::cerr << "Id: " << entry.first << "\n";
                auto& res = entry.second;
                std::cerr << "| acquirable " << (U32)res.acquirable << "\n"
                             "| available " << (U32)res.available << "\n";
            }
            std::cerr << "EOS\n";

            std::cerr << "\033[1;32mResource info\033[0m:\n";
            for (auto& entry : m_resourceInfo)
            {
                std::cerr << "Id: " << entry.first << "\n";
                auto& res = entry.second;
                std::cerr << "| parent " << res.options.parent << "\n"
                             "| mediaType " << res.mediaType << "\n"
                             "| path " << res.path << "\n"
                             "| childCount " << res.childCount << "\n"
                             "| finishedChildCount " << res.finishedChildCount << "\n"
                             "| destroyedChildCount " << res.destroyedChildCount << "\n"
                             "| status " << (U32)res.status << "\n"
                             "| currentStage " << (U32)res.currentStage << "\n"
                             "| \033[1;34mid " << res.id << "\033[0m\n"
                             "| result " << res.result << "\n"
                             "| blockTypes " << (U32)res.blockTypes << "\n"
                             "\n";
            }
            std::cerr << "EOS\n";
        }
    }

    if (writeDebugOutput) std::cerr << "Trying to force-start threads ...\n";

    m_diskReadCond.notify_all();
    m_parseCond.notify_all();
    m_hostTransferCond.notify_all();
    if (writeDebugOutput) std::cerr << "Waiting a second ...";
    {
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(1s);
    }
    if (writeDebugOutput) std::cerr << "Done!\n";
    if (writeDebugOutput) std::cerr << "Is idle now? " << (isIdle() ? "Yes" : "No") << "\n";
}

void ResourceLoader::prelockedRecursiveUnblockResource(ResourceID id,
                                                       ResourceType types,
                                                       U32* diskUnblock,
                                                       U32* parseUnblock,
                                                       U32* transferUnblock)
{
    auto& subresources = m_resources[id].subresources;
    m_resourceInfo[id].blockTypes &= ~types;
    auto disk = m_blockedDiskTasks.find(id);
    if (disk != m_blockedDiskTasks.end())
    {
        m_diskTaskQueue.push(disk->second);
        m_blockedDiskTasks.erase(disk);
        if (diskUnblock) ++*diskUnblock;
        if (g_showResourceLoaderDebugOutput) { std::stringstream ss; ss << "\033[31m" << id << "\033[0m_"; std::cerr << ss.str(); }
    }
    auto parse = m_blockedParseTasks.find(id);
    if (parse != m_blockedParseTasks.end())
    {
        m_parseTaskQueue.push(parse->second);
        m_blockedParseTasks.erase(parse);
        if (parseUnblock) ++*parseUnblock;
        if (g_showResourceLoaderDebugOutput) { std::stringstream ss; ss << "\033[32m" << id << "\033[0m_"; std::cerr << ss.str(); }
    }
    auto transfer = m_blockedHostTransferTasks.find(id);
    if (transfer != m_blockedHostTransferTasks.end())
    {
        m_hostTransferTaskQueue.push(transfer->second);
        m_blockedHostTransferTasks.erase(transfer);
        if (transferUnblock) ++*transferUnblock;
        if (g_showResourceLoaderDebugOutput) { std::stringstream ss; ss << "\033[34m" << id << "\033[0m_"; std::cerr << ss.str(); }
    }

    for (auto subId : subresources)
    {
        prelockedRecursiveUnblockResource(subId,
                                          types,
                                          diskUnblock,
                                          parseUnblock,
                                          transferUnblock);
    }
}


bool ResourceLoader::atomicMoveBlockedTask(ResourceID id, ResourceType type,
                                           DiskTask* disk,
                                           ParseTask* parse,
                                           HostTransferTask* host)
{
    std::lock_guard lg(m_resourceInfoMutex);
    if (g_showResourceLoaderExtraDebugOutput) { std::stringstream ss; ss << "\033[96m" << id << ":" << U32(m_resourceInfo[id].blockTypes & type) << "\033[0m_"; std::cerr << ss.str(); }
    if (!!(m_resourceInfo[id].blockTypes & type))
    {
        if (g_showResourceLoaderExtraDebugOutput) { std::stringstream ss; ss << "\033[1;96m" << id << ":" << U32(m_resourceInfo[id].blockTypes & type) << "\033[0m_"; std::cerr << ss.str(); }

        if (type == ResourceType::RawFile && disk)
        {
            std::lock_guard lg2(m_diskTaskMutex);
            m_blockedDiskTasks[id] = *disk;
        }
        else if (type == ResourceType::HostResource && parse)
        {
            std::lock_guard lg2(m_parseTaskMutex);
            m_blockedParseTasks[id] = *parse;
        }
        else if (type == ResourceType::VulkanResource && host)
        {
            std::lock_guard lg2(m_hostTransferTaskMutex);
            m_blockedHostTransferTasks[id] = *host;
        }
        else
        {
            throw std::logic_error("Invalid argument(s) to "
                                   "atomicMoveBlockedTask().");
        }
        return true;
    }
    if (g_showResourceLoaderExtraDebugOutput) { std::stringstream ss; ss << "\033[1;93m" << id << ":" << U32(m_resourceInfo[id].blockTypes & type) << "\033[0m_"; std::cerr << ss.str(); }

    return false;
}

ResourceLoader::ResourceID
ResourceLoader::createResource(std::string mediaType,
                               fs::path path,
                               Options opts, Stage stage)
{
    bool usesRawFile = ((opts.parseDataSource.fromType
                         | opts.vulkanDataSource.fromType)
                        & ResourceType::RawFile)
                       == ResourceType::RawFile;
    if (usesRawFile && stage != Stage::DiskReadQueued)
    {
        throw std::runtime_error("A resource source is specified as file, but "
                                 "no file will be read.");
    }

    bool needsParser = (opts.type &
                        (ResourceType::RawFile
                         | ResourceType::HostResource
                         | ResourceType::VulkanResource))
                       != ResourceType::None;

    std::unique_ptr<ResourceParser> parser;

    if (needsParser)
    {
        std::lock_guard lg(m_parserCreatorMutex);
        auto it = m_parserCreators.find(mediaType);
        if (it == m_parserCreators.end())
        {
            throw std::runtime_error("An appropriate parser for the given "
                                     "resource type was not found.");
        }
        parser = it->second();
    }

    auto id = newResourceID();
    {
        std::lock_guard lg(m_resourceInfoMutex);
        m_resourceInfo[id] = {opts, mediaType, path, 0, 0, 0,
                              Status::Good, stage, id, VK_SUCCESS,
                              VK_NULL_HANDLE, 0, 0, opts.type,
                              requiredResourcesToLoad(opts),
                              ResourceType::None,
                              opts.blockTypes};
    }
    {
        std::lock_guard lg(m_resourceMutex);
        (m_resources[id] = {}).parser = std::move(parser);
    }
    if (opts.parent != invalidResource)
    {
        appendChild(opts.parent, id);
    }

    return id;
}

ResourceLoader::ResourceID ResourceLoader::load(std::string mediaType,
                                                fs::path path, Options opts)
{
    auto id = createResource(mediaType, path, opts, Stage::DiskReadQueued);
    std::lock_guard lg(m_diskTaskMutex);

    m_diskTaskQueue.push({path, opts.priority, id, opts.pathRelativeToLoaderPath});
    m_diskReadCond.notify_one();

    return id;
}

ResourceLoader::ResourceID ResourceLoader::load(std::string mediaType, Options opts)
{
    auto id = createResource(mediaType, "", opts, Stage::ParsingQueued);
    if (id == invalidResource) return invalidResource; // Failed for some reason.
    if (opts.vulkanDataSource.fromType != ResourceType::HostResource
        && (opts.type & ResourceType::HostResource) != ResourceType::HostResource)
    {
        // Parsing is neither needed as Vulkan data source nor will it be
        // acquired; thus we should immediately transfer.
        if ((opts.type & ResourceType::VulkanResource) != ResourceType::VulkanResource)
        {
            throw std::logic_error("ResourceLoader asked to load resource "
                                   "without any part of it being acquirable.");
        }

        transferResource(id);
    }
    else
    {
        std::lock_guard lg(m_parseTaskMutex);
        m_parseTaskQueue.push({opts.priority, id});
        m_parseCond.notify_one();
    }
    return id;
}

void ResourceLoader::diskThread(U32 threadID)
{
    ++m_activeDiskThreads;
    for (;;)
    {
        std::unique_lock<std::mutex> lock(m_diskTaskMutex);
        while (m_diskTaskQueue.empty())
        {
            if (shouldDestroyThread(threadID))
            {
                notifyThreadFinished(threadID);
                --m_activeDiskThreads;
                return;
            }
            --m_activeDiskThreads;
            m_diskReadCond.wait(lock);
            ++m_activeDiskThreads;
        }
        if (shouldDestroyThread(threadID))
        {
            notifyThreadFinished(threadID);
            --m_activeDiskThreads;
            return;
        }
        auto info = m_diskTaskQueue.top();
        m_diskTaskQueue.pop();
        lock.unlock();

        if (atomicMoveBlockedTask(info.id, ResourceType::RawFile, &info))
        {
            if (g_showResourceLoaderDebugOutput) { std::stringstream ss; ss << "\033[1;41m" << info.id << "\033[0m_"; std::cerr << ss.str(); }
            continue;
        }

        setStage(info.id, Stage::DiskReading);
        if (g_showResourceLoaderDebugOutput) { std::stringstream ss; ss << "\033[1;31m" << info.id << "\033[0m_"; std::cerr << ss.str(); }
        RawData rawData;
        std::vector<FileInfo> fileInfo;
        if (!info.path.empty())
        {
            FileInfo fi;
            fi.name = "main";
            fi.path = info.path;
            fi.relativeToResourceLoaderBasePath = info.relativeToLoaderPath;
            fileInfo.push_back(fi);
        }
        ResourceParser* currParser = nullptr;
        {
            std::lock_guard lg(m_resourceMutex);
            currParser = m_resources[info.id].parser.get();
        }
        m_currParsing[std::this_thread::get_id()] = info.id;
        for (;;)
        {
            if (fileInfo.empty())
            {
                U32 extraFileCount = 0;
                currParser->getAdditionalRequiredFiles(nullptr,
                                                       &extraFileCount,
                                                       *this,
                                                       rawData);
                if (extraFileCount)
                {
                    fileInfo.resize(extraFileCount);

                    for (auto& fi : fileInfo)
                    {
                        fi.relativeToResourceLoaderBasePath = info.relativeToLoaderPath;
                    }

                    currParser->getAdditionalRequiredFiles(fileInfo.data(),
                                                           &extraFileCount,
                                                           *this,
                                                           rawData);
                    if (fileInfo.size() < extraFileCount)
                    {
                        throw std::logic_error("More files written to fileInfo"
                                               " than fileInfo can hold "
                                               "(overflow).");
                    }
                    else if (fileInfo.size() > extraFileCount)
                    {
                        fileInfo.resize(extraFileCount);
                    }
                }
                else
                {
                    break;
                }
            }

            auto currFileInfo = fileInfo.back();
            Bytedata bytedata;
            if (readFile(currFileInfo.relativeToResourceLoaderBasePath
                         ? m_basePath / currFileInfo.path
                         : currFileInfo.path,
                         info.id,
                         bytedata,
                         currFileInfo.optional))
            {
                rawData[currFileInfo.name] = std::move(bytedata);
            }
            else if (currFileInfo.optional)
            {
                // If the file couldn't be found, but was optional, then we
                // insert a dummy element to indicate that we tried to look for
                // it, but couldn't find it / open it / read it / whatever.
                rawData[currFileInfo.name].data = nullptr;
                rawData[currFileInfo.name].size = 0;
            }
            else
            {
                // If the file which couldn't be loaded wasn't optional (i.e.
                // it was required), then we should abort.
                break;
            }
            fileInfo.pop_back();
        }
        m_currParsing[std::this_thread::get_id()] = invalidResource;

        {
            std::lock_guard lg(m_resourceMutex);
            auto& res = m_resources[info.id];
            res.rawData = std::move(rawData);
            res.available |= ResourceType::RawFile;
        }

        if (fileInfo.empty())
        {
            scheduleNextStage(info.id, Stage::DiskReading);
        } // Otherwise, disk transfer failed.
    }
}

void ResourceLoader::parserThread(U32 threadID)
{
    ++m_activeParseThreads;
    for (;;)
    {
        std::unique_lock<std::mutex> lock(m_parseTaskMutex);
        while (m_parseTaskQueue.empty())
        {
            if (shouldDestroyThread(threadID))
            {
                notifyThreadFinished(threadID);
                --m_activeParseThreads;
                return;
            }
            --m_activeParseThreads;
            m_parseCond.wait(lock);
            ++m_activeParseThreads;
        }
        if (shouldDestroyThread(threadID))
        {
            notifyThreadFinished(threadID);
            --m_activeParseThreads;
            return;
        }
        auto info = m_parseTaskQueue.top();
        m_parseTaskQueue.pop();
        lock.unlock();

        if (atomicMoveBlockedTask(info.id, ResourceType::HostResource,
                                  nullptr, &info))
        {
            if (g_showResourceLoaderDebugOutput) { std::stringstream ss; ss << "\033[1;42m" << info.id << "\033[0m_"; std::cerr << ss.str(); }
            continue;
        }

        if (g_showResourceLoaderDebugOutput) { std::stringstream ss; ss << "\033[1;32m" << info.id << "\033[0m_"; std::cerr << ss.str(); }
        setStage(info.id, Stage::Parsing);
        if (parseResource(info.id))
        {
            scheduleNextStage(info.id, Stage::Parsing);
        }
    }
}

void ResourceLoader::hostTransferThread(U32 threadID)
{
    ++m_activeTransferThreads;
    for (;;)
    {
        std::unique_lock<std::mutex> lock(m_hostTransferTaskMutex);
        while (m_hostTransferTaskQueue.empty())
        {
            if (shouldDestroyThread(threadID))
            {
                notifyThreadFinished(threadID);
                --m_activeTransferThreads;
                return;
            }
            --m_activeTransferThreads;
            m_hostTransferCond.wait(lock);
            ++m_activeTransferThreads;
        }
        if (shouldDestroyThread(threadID))
        {
            notifyThreadFinished(threadID);
            --m_activeTransferThreads;
            return;
        }
        auto hostTransfer = m_hostTransferTaskQueue.top();
        m_hostTransferTaskQueue.pop();
        lock.unlock();

        if (atomicMoveBlockedTask(hostTransfer.id, ResourceType::VulkanResource,
                                  nullptr, nullptr, &hostTransfer))
        {
            if (g_showResourceLoaderDebugOutput) { std::stringstream ss; ss << "\033[1;44m" << hostTransfer.id << "\033[0m_"; std::cerr << ss.str(); }
            continue;
        }
        if (g_showResourceLoaderDebugOutput) { std::stringstream ss; ss << "\033[2;34m" << hostTransfer.id << "\033[0m_"; std::cerr << ss.str(); }

        size_t block = m_blockCount;

        bool moreData = true;

        while (moreData && (block = allocateTransferBlock()) != m_blockCount)
        {
            const U8* dataOut = nullptr;
            size_t sizeOut = 0;
            const void* tagOut = nullptr;
            U32 extraTagOut = 0;

            moreData =
                hostTransfer.parser->getNextTransferData(hostTransfer.source,
                                                         m_blockSize,
                                                         &dataOut,
                                                         &sizeOut,
                                                         &tagOut,
                                                         &extraTagOut);

            if (!sizeOut || !dataOut)
            {
                hostTransfer.parser->getNextTransferData(hostTransfer.source,
                                                         m_blockSize,
                                                         &dataOut,
                                                         &sizeOut,
                                                         &tagOut,
                                                         &extraTagOut);
                throw std::logic_error("Contradiction: No data to transfer in "
                                       "a transfer task.");
            }

            if (!hostTransfer.blockCount)
            {
                std::lock_guard lg(m_deviceTransferTaskMutex);
                m_toInitTransfers.push_back(hostTransfer.id);
            }
            ++hostTransfer.blockCount;
            void* memory;
            vmaMapMemory(m_allocator, m_stagingBuffer.allocation, &memory);
            auto stagingOffset = block * m_blockSize;
            void* stagingLoc = static_cast<void*>(static_cast<U8*>(memory) + stagingOffset);
            std::memcpy(stagingLoc, dataOut, sizeOut);
            vmaUnmapMemory(m_allocator, m_stagingBuffer.allocation);

            scheduleDeviceTransfer(hostTransfer.id,
                                   stagingOffset,
                                   sizeOut,
                                   tagOut,
                                   extraTagOut);

        }

        if (moreData)
        {
            if (g_showResourceLoaderDebugOutput) { std::stringstream ss; ss << "\033[1;95m" << hostTransfer.id << "\033[0m_"; std::cerr << ss.str(); }
            lock.lock();
            m_hostTransferTaskQueue.push(hostTransfer);
            lock.unlock();
        }
        else
        {
            std::lock_guard lg0(m_resourceMutex);
            std::lock_guard lg1(m_resourceInfoMutex);
            std::lock_guard lg2(m_totalBlockUpdateMutex);
            m_resourceInfo[hostTransfer.id].totalBlockCount = hostTransfer.blockCount;

            if (m_resourceInfo[hostTransfer.id].blocksTransferred == m_resourceInfo[hostTransfer.id].totalBlockCount)
            {
                m_transferFinalisations.push_back({hostTransfer.id,
                                                   hostTransfer.parser,
                                                   &m_resources[hostTransfer.id].vulkanResource});
            }

            if (g_showResourceLoaderExtraDebugOutput) { std::stringstream ss; ss << "\033[1;95mTotalBlockCount(" << hostTransfer.id << ") is " << hostTransfer.blockCount << "\033[0m_"; std::cerr << ss.str(); }
        }
    }
}

bool ResourceLoader::readFile(fs::path path, ResourceID id, Bytedata& dataOut,
                              bool optional)
{
    if (!fs::exists(path))
    {
        if (!optional) setStatus(id, Status::FileNotFound);
        return false;
    }
    if (fs::is_directory(path))
    {
        if (!optional) setStatus(id, Status::FileNotAFile);
        return false;
    }
    std::ifstream inFile(path, std::ios::binary);
    if (!inFile.is_open())
    {
        if (!optional) setStatus(id, Status::FileAccessFailed);
        return false;
    }
    dataOut.size = fs::file_size(path);
    dataOut.data = std::make_unique<U8[]>(dataOut.size);
    inFile.read(reinterpret_cast<char*>(dataOut.data.get()), dataOut.size);
    if ((size_t)inFile.gcount() != dataOut.size)
    {
        if (!optional) setStatus(id, Status::FileReadFailed);
        return false;
    }
    return true;
}


void ResourceLoader::transferResource(ResourceID id)
{
    HostTransferTask task;

    {
        VkEventCreateInfo evtCreate{};
        evtCreate.sType = VK_STRUCTURE_TYPE_EVENT_CREATE_INFO;
        VkEvent evt;
        if (vkCreateEvent(m_device, &evtCreate, nullptr, &evt) != VK_SUCCESS)
        {
            throw std::runtime_error("Failed to create event.");
        }
        std::lock_guard lg0(m_resourceMutex);
        std::lock_guard lg1(m_resourceInfoMutex);

        auto& resource = m_resources[id];
        auto& info = m_resourceInfo[id];

        info.transferInitialised = evt;

        task.parser = resource.parser.get();
        task.source = info.options.vulkanDataSource;
        task.blockCount = 0;
        task.priority = info.options.priority;
        task.id = id;

        if (info.options.vulkanDataSource.fromType == ResourceType::HostResource)
        {
            task.source.data = resource.hostResource.resource;
            task.source.size = 0;
            task.source.fromType = ResourceType::HostResource;
        }
        else if (info.options.vulkanDataSource.fromType == ResourceType::RawFile)
        {
            task.source = toDataSource(resource.rawData);
        }

        info.blocksTransferred = 0;
        info.totalBlockCount = 0;
    }

    std::lock_guard lg(m_hostTransferTaskMutex);
    m_hostTransferTaskQueue.push(task);
    m_hostTransferCond.notify_one();
}

void ResourceLoader::scheduleNextStage(ResourceID id, Stage currentStage)
{
    Stage nextStage = Stage::Ready;
    decltype(Options::priority) priority;

    {
        std::lock_guard resGuard(m_resourceMutex);
        std::lock_guard resInfoGuard(m_resourceInfoMutex);

        auto& resource = m_resources[id];
        auto& resourceInfo = m_resourceInfo[id];

        priority = resourceInfo.options.priority;

        bool shouldParse = (resourceInfo.resourcesToLoad & ResourceType::HostResource) != ResourceType::None;
        bool shouldTransfer = (resourceInfo.resourcesToLoad & ResourceType::VulkanResource) != ResourceType::None;

        bool hasParsed = false;
        bool hasTransferred = false;
        ResourceType finishedResourceType = ResourceType::None;

        switch (currentStage)
        {
        case Stage::DiskReading:
            finishedResourceType = ResourceType::RawFile;
            if (shouldParse) nextStage = Stage::ParsingQueued;
            else if (shouldTransfer) nextStage = Stage::TransferQueued;
            break;
        case Stage::Parsing:
            finishedResourceType = ResourceType::HostResource;
            hasParsed = true;
            if (shouldTransfer) nextStage = Stage::TransferQueued;
            break;
        case Stage::Transferring:
            finishedResourceType = ResourceType::VulkanResource;
            hasParsed = true;
            hasTransferred = true;
            if (resourceInfo.transferInitialised != VK_NULL_HANDLE)
            {
                vkDestroyEvent(m_device, resourceInfo.transferInitialised, nullptr);
                resourceInfo.transferInitialised = VK_NULL_HANDLE;
            }
            break;
        default:
        case Stage::DiskReadQueued: case Stage::ParsingQueued:
        case Stage::TransferQueued: case Stage::Ready:
            throw std::logic_error("Invalid stages in scheduleNextStage().");
        }

        ResourceType reserved = ResourceType::None;
        if (shouldParse && !hasParsed) reserved |= resourceInfo.options.parseDataSource.fromType;
        if (shouldTransfer && !hasTransferred) reserved |= resourceInfo.options.vulkanDataSource.fromType;

        resource.available |= finishedResourceType;

        ResourceType toPrune = resource.available & ~reserved & ~resourceInfo.resourcesToLoad;
        trimResource(resource, toPrune);

        resource.acquirable = resource.available & ~reserved;

        resourceInfo.currentStage = nextStage;

        if (nextStage == Stage::Ready)
        {
            trimResource(resource, ~resourceInfo.resourcesToAcquire);
        }
    }

    if (nextStage == Stage::Ready)
    {
        endResource(id);
        return;
    }

    if (nextStage == Stage::TransferQueued)
    {
        transferResource(id);
        return;
    }
    if (nextStage == Stage::ParsingQueued)
    {
        std::lock_guard lg(m_parseTaskMutex);
        m_parseTaskQueue.push({priority, id});
        m_parseCond.notify_one();
        return;
    }
    throw std::logic_error("Reached end of scheduleNextStage() without scheduling anything.");
}

size_t ResourceLoader::allocateTransferBlock()
{
    std::lock_guard lg(m_blockMutex);
    size_t result = m_nextFreeBlock;
    if (result < m_blockCount)
    {
        m_nextFreeBlock = m_blocks[result];
        m_blocks[result] = m_blockCount;
    }
    return result;
}

void ResourceLoader::deallocateTransferBlock(size_t block)
{
    std::lock_guard lg(m_blockMutex);
    m_blocks[block] = m_nextFreeBlock;
    m_nextFreeBlock = block;
}

void ResourceLoader::scheduleDeviceTransfer(ResourceID id,
                                            VkDeviceSize bufferOffset,
                                            VkDeviceSize bufferSize,
                                            const void* tag,
                                            U32 extraTag)
{
    std::lock_guard lg(m_resourceMutex);
    std::lock_guard lg1(m_resourceInfoMutex);
    std::lock_guard lg2(m_deviceTransferTaskMutex);

    auto& resource = m_resources[id];
    auto& info = m_resourceInfo[id];

    info.currentStage = Stage::TransferQueued;

    m_deviceTransferTasks.push_back({id,
                                     bufferOffset,
                                     bufferSize,
                                     resource.parser.get(),
                                     &resource.vulkanResource,
                                     info.transferInitialised,
                                     &info.blocksTransferred,
                                     &info.totalBlockCount,
                                     tag,
                                     extraTag});
}

VkResult ResourceLoader::executeVulkanCommands(U64 timeout)
{
    return submitTransfers(timeout);
}

void ResourceLoader::appendChild(ResourceID parent,
                                 ResourceID child)
{
    auto unblockTypes = ResourceType::None;
    {
        std::lock_guard lg0(m_resourceMutex);
        std::lock_guard lg1(m_resourceInfoMutex);

        m_resources[parent].subresources.push_back(child);

        auto& parentInfo = m_resourceInfo[parent];
        auto& childInfo = m_resourceInfo[child];

        unblockTypes = childInfo.blockTypes & ~parentInfo.blockTypes;

        ++parentInfo.childCount;

        if (parentInfo.status == Status::Ready)
        {
            // Since the parent is ready, we have already notified that it has
            // finished... This is NOT ideal, and we cannot unnotify, so we
            // throw an exception so that the creator of this resource/resource
            // parser can ensure that children are enqueued before finishing.
            throw std::logic_error("Tried to append a subresource to an "
                                   "already finished resource.");
        }

        auto depends = childInfo.options.parentDependencies;

        parentInfo.resourcesToLoad |= depends;
        parentInfo.resourcesUsedByChildren |= depends;

        if (g_showResourceLoaderDebugOutput) { std::stringstream ss; ss << "\033[1;36m[" << parent << "+=" << child << "]\033[0m"; std::cerr << ss.str(); }
    }
    if (!!unblockTypes)
    {
        unblockResource(child, unblockTypes);
    }
}

void ResourceLoader::getResourceInformation(ResourceID id,
                                            const Options** options,
                                            const std::string** mediaType,
                                            const fs::path** path)
{
    std::lock_guard lg(m_resourceInfoMutex);
    const auto& info = m_resourceInfo[id];
    if (options) *options = &info.options;
    if (mediaType) *mediaType = &info.mediaType;
    if (path) *path = &info.path;
}

void ResourceLoader::getCurrentResourceInfo(ResourceID* idOut,
                                            const Options** options,
                                            const std::string** mediaType,
                                            const fs::path** path)
{
    auto id = m_currParsing[std::this_thread::get_id()];
    if (idOut) *idOut = id;
    getResourceInformation(id, options, mediaType, path);
}

/// TODO: Handle errors where some resources are not successfully parsed and/or
/// command buffer allocation fails. Currently this will not remove any
/// offending resource (if applicable), and it will potentially re-call already
/// called functions, which it shouldn't.
/// So instead of returning the error code, something else should be done.
VkResult ResourceLoader::submitTransfers(U64 timeout)
{
    VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandBufferCount = 1;
    allocInfo.commandPool = m_commandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    std::vector<VkCommandBuffer> commandBuffers;
    VkResult result;
    TransferSubmitBatch nextBatch;

    for (size_t i = 0; i < m_submittedBatches.size();)
    {
        if (vkWaitForFences(m_device, 1, &m_submittedBatches[i].fence, VK_FALSE, timeout) == VK_SUCCESS)
        {
            vkDestroyFence(m_device, m_submittedBatches[i].fence, nullptr);
            vkFreeCommandBuffers(m_device,
                                 m_commandPool,
                                 m_submittedBatches[i].commandBuffers.size(),
                                 m_submittedBatches[i].commandBuffers.data());
            for (auto& task : m_submittedBatches[i].transfers)
            {
                deallocateTransferBlock(task.bufferOffset / m_blockSize);
                ++*task.blocksTransferred;

                if (g_showResourceLoaderExtraDebugOutput) { std::stringstream ss; ss << "\033[2;3;4;37mGather(" << task.id << ";" << *task.blocksTransferred << "/" << *task.totalBlockCount << ")\033[0m_"; std::cerr << ss.str(); }

                // Observe: task.blocksTransferred is always nonzero, so the
                // following can ONLY be true if task.totalBlockCount is set,
                // so all blocks have been scheduled, and if they are equal,
                // then all those scheduled have been transferred.
                std::lock_guard lg(m_totalBlockUpdateMutex);
                if (*task.blocksTransferred == *task.totalBlockCount)
                {
                    if (g_showResourceLoaderExtraDebugOutput) { std::stringstream ss; ss << "\033[2;3;4;37mFinalise(" << task.id << ")\033[0m_"; std::cerr << ss.str(); }

                    m_transferFinalisations.push_back({task.id, task.parser, task.vulkanResource});
                }
            }
            for (auto id : m_submittedBatches[i].finishes)
            {
                scheduleNextStage(id, Stage::Transferring);
            }

            std::swap(m_submittedBatches[i], m_submittedBatches.back());
            m_submittedBatches.pop_back();
        }
        else
        {
            ++i;
        }
    }

    { // Begin mutex scope
        std::lock_guard lg(m_totalBlockUpdateMutex);
        if (!m_transferFinalisations.empty())
        {
            VkCommandBuffer commandBuf;
            result = vkAllocateCommandBuffers(m_device, &allocInfo, &commandBuf);
            if (result != VK_SUCCESS) return result;
            vkBeginCommandBuffer(commandBuf, &beginInfo);

            for (auto finalisation : m_transferFinalisations)
            {
                result = finalisation.parser->finaliseTransfer(commandBuf,
                                                               m_device,
                                                               m_allocator,
                                                               finalisation.vulkanResource);
                if (result != VK_SUCCESS) return result;

                nextBatch.finishes.push_back(finalisation.id);
            }

            vkEndCommandBuffer(commandBuf);
            commandBuffers.push_back(commandBuf);

            m_transferFinalisations.clear();
        }
    } // End mutex scope


    { // Begin mutex scope

    std::lock_guard lg0(m_resourceMutex);
    std::lock_guard lg1(m_resourceInfoMutex);
    std::lock_guard lg2(m_deviceTransferTaskMutex);

    for (auto id : m_toInitTransfers)
    {
        VkCommandBuffer commandBuf;
        result = vkAllocateCommandBuffers(m_device, &allocInfo, &commandBuf);
        if (result != VK_SUCCESS) return result;
        vkBeginCommandBuffer(commandBuf, &beginInfo);

        auto& resource = m_resources[id];
        auto& resourceInfo = m_resourceInfo[id];

        auto source = resourceInfo.options.vulkanDataSource;

        if (source.fromType == ResourceType::HostResource)
        {
            source.data = resource.hostResource.resource;
            source.size = 0;
        }

        result = resource.parser
                 ->initialiseTransfer(commandBuf,
                                      m_device,
                                      m_allocator,
                                      resourceInfo.options.vulkanOptions,
                                      source,
                                      &resource.vulkanResource);

        if (result != VK_SUCCESS) return result;

        vkCmdSetEvent(commandBuf, resourceInfo.transferInitialised, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT);

        vkEndCommandBuffer(commandBuf);
        commandBuffers.push_back(commandBuf);
    }
    m_toInitTransfers.clear();

    for (size_t i = 0; i < m_deviceTransferTasks.size();)
    {
        auto& task = m_deviceTransferTasks[i];

        if (vkGetEventStatus(m_device, task.waitFor) != VK_EVENT_SET)
        {
            ++i;
            continue;
        }
        VkCommandBuffer commandBuf;
        auto result = vkAllocateCommandBuffers(m_device, &allocInfo, &commandBuf);
        if (result != VK_SUCCESS) return result;
        vkBeginCommandBuffer(commandBuf, &beginInfo);

        task.parser->transfer(commandBuf,
                              m_stagingBuffer.buffer,
                              task.bufferOffset,
                              task.bufferSize,
                              task.tag,
                              task.extraTag,
                              task.vulkanResource);
        vkEndCommandBuffer(commandBuf);
        commandBuffers.push_back(commandBuf);
        nextBatch.transfers.push_back(task);
        std::swap(m_deviceTransferTasks[i], m_deviceTransferTasks.back());
        m_deviceTransferTasks.pop_back();

        if (g_showResourceLoaderExtraDebugOutput) { std::stringstream ss; ss << "\033[2;3;4;37mSubmit(" << task.id << ")\033[0m_"; std::cerr << ss.str(); }
    }

    } // End mutex scope

    if (commandBuffers.empty())
    {
        if (!nextBatch.finishes.empty() || !nextBatch.transfers.empty())
        {
            throw std::logic_error("No command buffers recorded, but finishes "
                                   "or transfers are non-empty.");
        }
        return VK_SUCCESS;
    }

    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = commandBuffers.size();
    submitInfo.pCommandBuffers = commandBuffers.data();

    VkFenceCreateInfo fenceCreate{};
    fenceCreate.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    result = vkCreateFence(m_device, &fenceCreate, nullptr, &nextBatch.fence);
    if (result != VK_SUCCESS)
    {
        return result;
    }
    result = vkQueueSubmit(m_queue, 1, &submitInfo, nextBatch.fence);
    if (result < 0)
    {
        vkDestroyFence(m_device, nextBatch.fence, nullptr);
        return result;
    }
    m_submittedBatches.push_back({});
    m_submittedBatches.back().fence = nextBatch.fence;
    m_submittedBatches.back().transfers = std::move(nextBatch.transfers);
    m_submittedBatches.back().finishes = std::move(nextBatch.finishes);
    m_submittedBatches.back().commandBuffers = std::move(commandBuffers);
    return VK_SUCCESS;
}

bool ResourceLoader::parseResource(ResourceLoader::ResourceID id)
{
    const ResourceInfo* info;
    {
        std::lock_guard lg(m_resourceInfoMutex);
        info = &m_resourceInfo[id];
    }
    const U8* data = static_cast<const U8*>(info->options.parseDataSource.data);
    size_t size = info->options.parseDataSource.size;
    if (info->options.parseDataSource.fromType == ResourceType::RawFile)
    {
        std::lock_guard lg(m_resourceMutex);
        auto& resource = m_resources[id];
        auto dataSource = toDataSource(resource.rawData);
        data = static_cast<const U8*>(dataSource.data);
        size = dataSource.size;
    }

    Resource* resource = nullptr;

    {
        std::lock_guard lg(m_resourceMutex);
        resource = &m_resources[id];
    }

    HostResource hostRes;
    m_currParsing[std::this_thread::get_id()] = id;
    auto result = resource->parser->parse(data, size, info->options.parseOptions, *this, &hostRes);
    m_currParsing[std::this_thread::get_id()] = invalidResource;
    if (result)
    {
        resource->hostResource = std::move(hostRes);
        resource->available |= ResourceType::HostResource;
    }
    else
    {
        setStatus(id, Status::ParsingFailed);
    }

    if (!resource->subresources.empty())
    {
        std::lock_guard lg(m_resourceInfoMutex);
        auto& info = m_resourceInfo[id];
        info.resourcesToAcquire &= ~(ResourceType::VulkanResource);
        info.resourcesToLoad &= ~(ResourceType::VulkanResource);
    }

    return result;
}

void ResourceLoader::setStatus(ResourceID id, Status status, VkResult result)
{
    const decltype(Options::finishNotify)* finishNotify = nullptr;
    {
        std::lock_guard lg(m_resourceInfoMutex);
        auto& info = m_resourceInfo[id];
        info.status = status;
        info.result = result;
        // All statuses with numerical values before Ready indicates that the
        // resource is still being parsed, while a status with numerical value
        // greater than Ready indicates some kind of failure. In the latter
        // case, the resource has finished parsing, and so we retreive the
        // finishNotify-function.
        if (static_cast<U32>(status) > static_cast<U32>(Status::Ready))
        {
            finishNotify = &info.options.finishNotify;
        }
    }
    if (finishNotify) (*finishNotify)(status, id, *this);
}

void ResourceLoader::setStage(ResourceID id, Stage stage)
{
    std::lock_guard lg(m_resourceInfoMutex);
    m_resourceInfo[id].currentStage = stage;
}

void ResourceLoader::notifyChildFinished(ResourceID parent, ResourceID)
{
    bool finished = false;
    {
        std::lock_guard lg(m_resourceInfoMutex);
        auto& info = m_resourceInfo[parent];
        finished = ++info.finishedChildCount == info.childCount;
    }
    if (finished)
    {
        endResource(parent);
    }
}

void ResourceLoader::notifyChildDestroyed(ResourceID parent, ResourceID)
{
    if (parent == invalidResource) return;
    bool allDestroyed = false;
    {
        std::lock_guard lg(m_resourceInfoMutex);
        auto& info = m_resourceInfo[parent];
        allDestroyed = ++info.destroyedChildCount == info.childCount;
    }

    if (allDestroyed)
    {
        std::lock_guard lg(m_resourceMutex);
        auto& resource = m_resources[parent];
        /// GREPME: Trims Vulkan resources as they are not supported with
        /// subresource lists.
        trimResource(resource, ResourceType::Subresource | ResourceType::VulkanResource);
        auto dataLeft = resource.acquirable != ResourceType::None;
        if (!dataLeft)
        {
            destroyResource(parent);
        }
    }
}

void ResourceLoader::endResource(ResourceID id)
{
    ResourceID parentToNotify = invalidResource;

    auto acquirable = ResourceType::None;
    const decltype(Options::finishNotify)* finishNotify = nullptr;

    {
        std::lock_guard lg(m_resourceInfoMutex);

        auto& info = m_resourceInfo[id];

        if (info.finishedChildCount != info.childCount)
        {
            info.status = Status::WaitingForSubresources;
            return;
        }

        info.status = Status::Ready;
        parentToNotify = info.options.parent;
        finishNotify = &info.options.finishNotify;
        acquirable = info.resourcesToAcquire;
        info.currentStage = Stage::ReadyInCallback;
    }

    (*finishNotify)(Status::Ready, id, *this);

    bool shouldDestroy = false;

    {
        std::lock_guard guard0(m_resourceMutex);
        std::lock_guard guard1(m_resourceInfoMutex);
        m_resourceInfo[id].currentStage = Stage::Ready;
        auto& currResource = m_resources[id];
        trimResource(currResource, ~acquirable);

        shouldDestroy = !currResource.available;
    }

    if (parentToNotify != invalidResource)
    {
        notifyChildFinished(parentToNotify, id);
    }
    if (shouldDestroy)
    {
        // If there is a parent and its child should already be destroyed, then
        // there might be a problem. But OTOH, the parent's acquirer might have
        // looted its children, so it can be valid.
        destroyResource(id);
    }
}

bool ResourceLoader::shouldDestroyThread(U32) const
{
    return m_destroyObjects;
}

void ResourceLoader::notifyThreadFinished(U32)
{
    --m_liveThreadCount;
}

void ResourceLoader::destroyThreads()
{
    while (m_liveThreadCount)
    {
        m_diskReadCond.notify_all();
        m_parseCond.notify_all();
        m_hostTransferCond.notify_all();

        using namespace std::chrono_literals;
        std::this_thread::sleep_for(1ms);
    }
    if (m_diskThread.joinable()) m_diskThread.join();
    if (m_hostTransferThread.joinable()) m_hostTransferThread.join();
    for (auto& thread : m_parseThreads)
    {
        thread.join();
    }
    m_parseThreads.clear();
}

void ResourceLoader::destroyResources()
{
    for (auto& resource : m_resources)
    {
        trimResource(resource.second, ~ResourceType::None);
    }
    m_resources.clear();
    for (auto& info : m_resourceInfo)
    {
        if (info.second.transferInitialised != VK_NULL_HANDLE)
        {
            vkDestroyEvent(m_device, info.second.transferInitialised, nullptr);
        }
    }
    m_resourceInfo.clear();
}


void ResourceLoader::addDefaultResourceSupport()
{
    addResourceSupport<ImageParser>("image/bmp");
    addResourceSupport<ImageParser>("image/png");
    addResourceSupport<ImageParser>("image/x-portable-anymap");
    addResourceSupport<ImageParser>("image/x-portable-bitmap");
    addResourceSupport<ImageParser>("image/x-portable-graymap");
    addResourceSupport<ImageParser>("image/x-portable-pixmap");
    addResourceSupport("model/ply", []{ return std::make_unique<PLYParser>();});
    addResourceSupport("model/stl", []{ return std::make_unique<STLParser>();});
}

ResourceLoader::DataSource
ResourceLoader::toDataSource(const RawData& data, ResourceType fromType) const
{
    ResourceLoader::DataSource src;
    src.fromType = fromType;
    if (data.size() == 1)
    {
        src.data = data.begin()->second.data.get();
        src.size = data.begin()->second.size;
    }
    else
    {
        src.data = &data;
        src.size = 0;
    }
    return src;
}

ResourceLoader::Resource* ResourceLoader::getResource(ResourceID id)
{
    auto res = m_resources.find(id);
    if (res == m_resources.end()) return nullptr;
    return &res->second;
}

bool ResourceLoader::isAcquirable(const Resource& resource, ResourceType type) const
{
    return (resource.acquirable & type) == type && type != ResourceType::None;
}

bool ResourceLoader::isAvailable(const Resource& resource, ResourceType type) const
{
    return (resource.available & type) == type && type != ResourceType::None;
}

ResourceLoader::Resource*
ResourceLoader::tryAcquire(ResourceID id, ResourceType type, bool& dataLeft)
{
    auto* resource = getResource(id);
    if (resource && isAcquirable(*resource, type))
    {
        resource->available &= ~type;
        resource->acquirable &= ~type;
        dataLeft = resource->acquirable != ResourceType::None;
        return resource;
    }
    dataLeft = resource != nullptr;
    return nullptr;
}

void ResourceLoader::trimResource(Resource& resource, ResourceType toRemove) const
{
    if (isAvailable(resource, ResourceType::RawFile & toRemove))
    {
        resource.rawData.clear();
    }
    if (isAvailable(resource, ResourceType::HostResource & toRemove))
    {
        resource.hostResource.destroy();
    }
    if (isAvailable(resource, ResourceType::VulkanResource & toRemove))
    {
        resource.vulkanResource.image.destroy(m_allocator, m_device);
    }
    resource.available &= ~toRemove;
    resource.acquirable &= ~toRemove;
}

ResourceLoader::ResourceType ResourceLoader::requiredResourcesToLoad(const Options& opts) const
{
    bool shouldParse = (opts.type & ResourceType::HostResource) != ResourceType::None;
    bool shouldTransfer = (opts.type & ResourceType::VulkanResource) != ResourceType::None;

    ResourceType required = opts.type;
    if (shouldParse) required |= opts.parseDataSource.fromType;
    if (shouldTransfer) required |= opts.vulkanDataSource.fromType;

    return required;
}
