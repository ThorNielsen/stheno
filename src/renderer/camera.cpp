#include "renderer/camera.hpp"

#include "math/transform.hpp"

#include <kraken/math/comparison.hpp>
#include <kraken/math/matrix.hpp>
#include <kraken/math/transform.hpp>

#include <algorithm>
#include <cmath>
#include <optional>
#include <stdexcept>

using namespace kraken::math;

TurntableCamera::TurntableCamera(dvec3 initialLookat,
                                 std::optional<double> initialDistance,
                                 std::optional<dvec3> initialUp,
                                 std::optional<dvec3> initialRight,
                                 std::optional<dvec3> initialForward)
    : m_direction{0, 1, 0}
    , m_rotationCenter{initialLookat}
    , m_up{0, 0, 1}
    , m_right{1, 0, 0}
    , m_pendingOffset{0, 0}
    , m_distance{std::max(1e-16, initialDistance.value_or(1.))}
{
    if (initialUp.has_value() && squaredLength(*initialUp) > 1e-16)
    {
        m_up = normalise(*initialUp);
    }
    if (initialRight.has_value() && squaredLength(*initialRight) > 1e-16)
    {
        m_right = normalise(*initialRight);
    }

    m_direction = cross(m_up, m_right);
    if (squaredLength(m_direction) < 1e-16)
    {
        if (initialForward.has_value()
            && length(*initialForward) > 1e-16)
        {
            m_direction = normalise(*initialForward);
        }
        else
        {
            throw std::logic_error("Failed to correct initial forward direc"
                                   "tion; no fallback implemented yet.");
        }
    }
    else m_direction = normalise(m_direction);

    if (squaredLength(cross(m_right, m_direction)) < 1e-16)
    {
        throw std::logic_error("Got parallel right and forwards directions"
                               " and no fallback has yet been added.");
    }

    reorthonormalise();
}

dmat4 TurntableCamera::worldToCamera() const noexcept
{
    auto pending = getPendingTransform();
    auto currFwd = pending * m_direction;
    auto currUp = pending * cross(m_right, m_direction);
    auto currPos = m_rotationCenter - m_distance * currFwd;
    return vulkanLookat(currPos,
                        currPos + currFwd,
                        currUp);
}

dvec3 TurntableCamera::position() const noexcept
{
    return m_rotationCenter - m_distance * m_direction;
}

dvec3 TurntableCamera::forward() const noexcept
{
    auto pending = getPendingTransform();
    return normalise(pending * m_direction);
}

dvec3 TurntableCamera::up() const noexcept
{
    // NOT m_up, since that just gives the natural "up" direction, but not OUR
    // current up direction.
    auto pending = getPendingTransform();
    return normalise(pending * cross(m_right, m_direction));
}

void TurntableCamera::translate(dvec2 mouseMove) noexcept
{
    mouseMove *= m_distance;
    auto localUp = cross(m_right, m_direction);
    m_rotationCenter += mouseMove.x * m_right
                      + mouseMove.y * localUp;
}

void TurntableCamera::rotate(dvec2 mouseMove) noexcept
{
    m_pendingOffset += mouseMove;
}

void TurntableCamera::scale(double movement) noexcept
{
    m_distance = clamp(m_distance * std::exp(movement),
                       1e-100,
                       1e+100);
}

void TurntableCamera::endTransform() noexcept
{
    auto pending = getPendingTransform();
    m_pendingOffset = {0., 0.};
    m_direction = pending * m_direction;
    m_right = pending * m_right;
    // And NOT for m_up as we are not a trackball camera.
    reorthonormalise();
}

dmat3 TurntableCamera::getPendingTransform() const noexcept
{
    auto expectedUp = cross(m_right, m_direction);
    return rotation(m_up,
                    dot(expectedUp, m_up) >= 0.
                    ? m_pendingOffset.x
                    : -m_pendingOffset.x)
         * rotation(m_right, m_pendingOffset.y);
}

void TurntableCamera::reorthonormalise()
{
    m_direction = normalise(m_direction);
    m_right -= project(m_direction, m_right);
}
