#include "renderer/perspective.hpp"

#include "math/transform.hpp"

#include <cmath>
#include <stdexcept>

#include <kraken/math/comparison.hpp>
#include <kraken/math/constants.hpp>
#include <kraken/math/matrix.hpp>

namespace km = kraken::math;

PerspectiveTransform::PerspectiveTransform(double aspectRatio,
                                           double verticalFOV,
                                           double nearPlane,
                                           double farPlane)
{
    if (!setAspect(aspectRatio)
        || !setFov(verticalFOV)
        || !setNear(nearPlane)
        || !setFar(farPlane))
    {
        throw std::domain_error("Invalid argument to perspective transform.");
    }
    if (near() >= far())
    {
        throw std::domain_error("Near plane cannot be closer than far plane.");
    }
}

PerspectiveTransform::~PerspectiveTransform()
{
    ;
}

double PerspectiveTransform::aspect() const noexcept
{
    return m_aspect;
}

double PerspectiveTransform::fov() const noexcept
{
    return m_vertFOV;
}

double PerspectiveTransform::near() const noexcept
{
    return m_nearDist;
}

double PerspectiveTransform::far() const noexcept
{
    return m_farDist;
}

bool PerspectiveTransform::setAspect(double aspectRatio) noexcept
{
    m_aspect = km::clamp(aspectRatio, 1e-7, 1e+7);
    return m_aspect == aspectRatio;
}

bool PerspectiveTransform::setAspect(double width, double height) noexcept
{
    return setAspect(width / height);
}

bool PerspectiveTransform::setFov(double verticalFOV) noexcept
{
    m_vertFOV = km::clamp(verticalFOV,
                          1e-5,
                          (1-1e-5)*km::Constant<double>::pi);
    return m_vertFOV == verticalFOV;
}

bool PerspectiveTransform::setNear(double nearPlane) noexcept
{
    // Technically the near plane could be anything, but we limit it to a
    // “reasonable” range...
    m_nearDist = km::clamp(nearPlane, 1e-100, 1e+100);
    return m_nearDist == nearPlane;
}

bool PerspectiveTransform::setFar(double farPlane) noexcept
{
    m_farDist = km::clamp(farPlane, 1e-100, 1e+100);
    return m_farDist == farPlane;
}

km::dmat4 PerspectiveTransform::transform() const
{
    auto cachedNear = near();
    auto cachedFar = far();
    if (cachedNear >= cachedFar)
    {
        throw std::domain_error("Got a near plane closer than (or equal to) far"
                                " plane.");
    }
    return vulkanPerspective(fov(), aspect(), cachedNear, cachedFar);
}

km::dmat3
PerspectiveTransform::viewportToCameraSpace(double depth,
                                            double viewportHeight) const noexcept
{
    auto tanThetaHalf = std::tan(fov() * .5);
    auto depthTan = depth * tanThetaHalf;
    km::dmat3 result(2. * depthTan / viewportHeight);
    result.setCol(2, {- aspect() * depthTan, - depthTan, depth});
    return result;
}

double PerspectiveTransform::ndcToCameraDepth(double ndcDepth) const noexcept
{
    auto n = near();
    auto f = far();
    // And if ndcDepth = f/(f-n) (up to precision issues), KABOOM!
    return n / (1. - ndcDepth * (f - n) / f);
}
