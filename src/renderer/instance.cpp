#include "renderer/instance.hpp"

#include "vulkan/device.hpp"
#include "vulkan/types.hpp"

#include <kraken/window/window.hpp>

#include <iostream>
#include <string>
#include <regex>

static VKAPI_ATTR VkBool32 VKAPI_CALL
debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT severity,
              VkDebugUtilsMessageTypeFlagsEXT,
              const VkDebugUtilsMessengerCallbackDataEXT* callbackData,
              void*)
{
    if (severity > VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
    {
        std::regex pattern(R"(vkQueuePresentKHR: pSwapchains\[[0-9]+\] image at index [0-9]+ was not acquired from the swapchain.)",
                           std::regex_constants::extended);

        if (std::regex_search(callbackData->pMessage, pattern))
        {
            std::cerr << "WARNING: Swapchain image not acquired; ignoring.\n";
        }
        else
        {
            std::cerr << callbackData->pMessage << "\n";
            throw std::runtime_error("Got an error.");
        }
        return VK_FALSE;
    }
    return VK_FALSE;
}


VulkanInstance::VulkanInstance(const VulkanInstanceParameters& params)
    : m_instance{VK_NULL_HANDLE}
{
    VkApplicationInfo appinfo{};
    appinfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appinfo.apiVersion = VK_MAKE_VERSION(1, 1, 0);
    appinfo.pApplicationName = params.name.c_str();
    appinfo.applicationVersion = VK_MAKE_VERSION(0, 0, 0);
    appinfo.engineVersion = VK_MAKE_VERSION(0, 0, 0);

    std::vector<VkValidationFeatureEnableEXT> enabledValidationFeatures =
    {
        VK_VALIDATION_FEATURE_ENABLE_BEST_PRACTICES_EXT,
    };

    VkValidationFeaturesEXT validationFeatures{};
    validationFeatures.sType = VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT;
    validationFeatures.enabledValidationFeatureCount = enabledValidationFeatures.size();
    validationFeatures.pEnabledValidationFeatures = enabledValidationFeatures.data();

    VkInstanceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appinfo;

    std::vector<std::string> wantedLayers;
    if (params.enableValidation)
    {
        createInfo.pNext = &validationFeatures;

        wantedLayers.push_back("VK_LAYER_LUNARG_standard_validation");
        wantedLayers.push_back("VK_LAYER_KHRONOS_validation");
    }

    for (auto elem : getSupportedVulkanLayers(wantedLayers))
    {
        auto [iter, wasInserted] = m_enabledLayers.emplace(elem);
        if (wasInserted)
        {
            m_layerPointers.emplace_back(iter->data());
        }
    }
    if (!m_enabledLayers.empty())
    {
        std::cerr << "Enabling " << m_enabledLayers.size() << " layer"
                  << (m_enabledLayers.size() > 1 ? "s:\n" : ":\n");
        for (const auto& layer : m_enabledLayers)
        {
            std::cerr << layer << "\n";
        }
    }
    createInfo.ppEnabledLayerNames = m_layerPointers.data();
    createInfo.enabledLayerCount = m_layerPointers.size();

    auto requiredExtensions = kraken::window::VulkanSurfaceCreationInfo::requiredInstanceExtensions();

    if (!params.swapchainSupport) requiredExtensions.clear();

    std::vector<std::string> optionalInstanceExtensions;
    if (params.enableValidation) optionalInstanceExtensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);

    auto instanceExtensions = getSupportedVulkanInstanceExtensions(requiredExtensions, optionalInstanceExtensions);
    createInfo.ppEnabledExtensionNames = instanceExtensions.data();
    createInfo.enabledExtensionCount = instanceExtensions.size();

    VkResult result = vkCreateInstance(&createInfo, nullptr, &m_instance);
    if (result != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create Vulkan instance "
                                 "(" + std::string(toString(result)) + ")");
    }

    if (params.enableValidation)
    {
        for (size_t i = 0; i < createInfo.enabledExtensionCount; ++i)
        {
            if (!strcmp(createInfo.ppEnabledExtensionNames[i], VK_EXT_DEBUG_UTILS_EXTENSION_NAME))
            {
                VkDebugUtilsMessengerCreateInfoEXT debugCreate{};
                debugCreate.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
                debugCreate.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT
                                            | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT
                                            /*| VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT
                                            | VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT*/;
                debugCreate.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT
                                        | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT
                                        | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT;
                debugCreate.pfnUserCallback = debugCallback;
                debugCreate.pUserData = nullptr;

                auto CreateDebugUtilsMessengerEXT = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(m_instance, "vkCreateDebugUtilsMessengerEXT");

                if (!CreateDebugUtilsMessengerEXT)
                {
                    throw std::runtime_error("Failed to load required function pointer for debugging.");
                }

                CreateDebugUtilsMessengerEXT(m_instance, &debugCreate, nullptr, &m_debugUtils);
            }
        }
    }
}

VulkanInstance::~VulkanInstance()
{
    if (m_instance != VK_NULL_HANDLE)
    {
        if (m_debugUtils)
        {
            auto DestroyDebugUtilsMessengerEXT = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(m_instance, "vkDestroyDebugUtilsMessengerEXT");
            if (DestroyDebugUtilsMessengerEXT)
            {
                DestroyDebugUtilsMessengerEXT(m_instance, m_debugUtils, nullptr);
            }
            else
            {
                std::cerr << "CRITICAL problem: Couldn't get instance address of "
                             "debug utils messenger, so this WILL memory.\n";
            }
        }
        vkDestroyInstance(m_instance, nullptr);
    }
}
