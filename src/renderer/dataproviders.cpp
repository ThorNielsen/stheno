#include "renderer/dataproviders.hpp"

#include "plotter/gpuopcodes.h"

#include <kraken/utility/memory.hpp>

ReadbackArrayProvider::ReadbackArrayProvider(VkDevice device,
                                             VmaAllocator allocator,
                                             size_t maxElementCount)
    : m_device{device}
    , m_allocator{allocator}
    , m_maxElementCount{maxElementCount}
{
    VkBufferCreateInfo storageBufferCreate{};
    storageBufferCreate.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    storageBufferCreate.size = totalBufferByteSize();
    storageBufferCreate.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
    storageBufferCreate.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    VmaAllocationCreateInfo storageBufferVma{};
    storageBufferVma.usage = VMA_MEMORY_USAGE_GPU_TO_CPU;

    for (auto& alloc : m_allocations)
    {
        // WARNING: This can leak memory.
        if (vmaCreateBuffer(m_allocator,
                            &storageBufferCreate,
                            &storageBufferVma,
                            &alloc.buffer,
                            &alloc.allocation,
                            nullptr) != VK_SUCCESS)
        {
            throw std::runtime_error("Failed to allocate storage buffer.");
        }
        vmaSetAllocationName(m_allocator,
                             alloc.allocation,
                             "Readback array buffer");

        MemoryMapGuard mmg(m_allocator, alloc.allocation);
        *reinterpret_cast<uint32_t*>(mmg.data()) = (uint32_t)m_maxElementCount;
        std::memset(mmg.data(4), 0, storageBufferCreate.size - 4);
    }
}

ReadbackArrayProvider::~ReadbackArrayProvider()
{
    for (auto& alloc : m_allocations)
    {
        alloc.destroy(m_allocator);
    }
}

void ReadbackArrayProvider::write(VkDescriptorSet descSet,
                                  size_t descriptorIndex)
{
    if (descriptorIndex >= cachedFrameCount)
    {
        throw std::logic_error("Bad descriptor index.");
    }
    VkDescriptorBufferInfo bufInfo
    {
        .buffer = m_allocations[descriptorIndex].buffer,
        .offset = 0,
        .range = totalBufferByteSize(),
    };
    VkWriteDescriptorSet descSetWrite
    {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .pNext = nullptr,
        .dstSet = descSet,
        .dstBinding = 1, /// TODO: This should be made relocatable!
        .dstArrayElement = 0,
        .descriptorCount = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
        .pImageInfo = nullptr,
        .pBufferInfo = &bufInfo,
        .pTexelBufferView = nullptr,
    };

    vkUpdateDescriptorSets(m_device,
                           1,
                           &descSetWrite,
                           0, nullptr);
}

std::vector<ReadbackArrayElement>
ReadbackArrayProvider::readback(size_t frameIndex, bool reset)
{
    MemoryMapGuard mmg(m_allocator, m_allocations.at(frameIndex).allocation);

    auto numAttemptedWritten = *reinterpret_cast<uint32_t*>(mmg.data(4));

    auto numWritten = std::min<uint32_t>(numAttemptedWritten,
                                         m_maxElementCount);

    std::vector<ReadbackArrayElement> results(numWritten);

    std::memcpy(results.data(),
                mmg.data(16),
                numWritten*sizeof(ReadbackArrayElement));

    // We clear just the count instead of the entire buffer...
    if (reset) std::memset(mmg.data(4), 0, 4);

    return results;
}

size_t ReadbackArrayProvider::totalBufferByteSize() const
{
    return m_maxElementCount * sizeof(ReadbackArrayElement)
           + headerSize;
}

BufferProvider::BufferProvider(MultiAllocatedBuffer& allocFrom,
                               VkDeviceSize requiredSize,
                               VkDevice device)
{
    m_bufAllocator = &allocFrom;
    m_device = device;
    for (auto& bufInfo : m_bufferInfos)
    {
        // Note: Leaks memory if any but the first allocation throws.
        // Also note that alignment is just set by default to 16, maybe some
        // types require more. TBD.
        if (m_bufAllocator->allocateBuffer(requiredSize, 16, bufInfo) != VK_SUCCESS)
        {
            throw std::logic_error("Failed to allocate buffer space.");
        }
    }
}

BufferProvider::~BufferProvider()
{
    for (const auto& bufInfo : m_bufferInfos)
    {
        if (bufInfo.buffer != VK_NULL_HANDLE)
        {
            m_bufAllocator->freeBuffer(bufInfo);
        }
    }
}

void BufferProvider::write(VkDescriptorSet descSet,
                           size_t descriptorIndex)
{
    if (descriptorIndex >= cachedFrameCount)
    {
        throw std::logic_error("Bad descriptor index.");
    }

    VkWriteDescriptorSet descSetWrite
    {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .pNext = nullptr,
        .dstSet = descSet,
        .dstBinding = 0xffffffff,
        .dstArrayElement = 0,
        .descriptorCount = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_MAX_ENUM,
        .pImageInfo = nullptr,
        .pBufferInfo = &m_bufferInfos[descriptorIndex],
        .pTexelBufferView = nullptr,
    };
    updateDescriptorSets(&descSetWrite);

    vkUpdateDescriptorSets(m_device,
                           1,
                           &descSetWrite,
                           0, nullptr);
}

void BufferProvider::update(size_t frameIndex)
{
    auto mapping = m_bufAllocator->mapBuffer();
    performUpdate(mapping.data(m_bufferInfos[frameIndex].offset));
}

GlobalTransformProvider::GlobalTransformProvider(MultiAllocatedBuffer& allocFrom,
                                                 VkDevice device)
    : BufferProvider(allocFrom,
                     2 * sizeof(mat4),
                     device)
{}

void GlobalTransformProvider::updateDescriptorSets(VkWriteDescriptorSet* pDescriptors)
{
    /// NOTE: This might be made relocatable so perhaps support that.
    pDescriptors->dstBinding = 0;
    pDescriptors->descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
}

void GlobalTransformProvider::performUpdate(void* allocBegin)
{
    std::memcpy(allocBegin, worldToCamera.data(), sizeof(worldToCamera));
    allocBegin = kraken::pointeradd(allocBegin, sizeof(worldToCamera));
    std::memcpy(allocBegin, cameraToClip.data(), sizeof(cameraToClip));
}

PickDataProvider::PickDataProvider(MultiAllocatedBuffer& allocFrom,
                                   VkDevice device)
    : BufferProvider(allocFrom,
                     4 * sizeof(U32),
                     device)
{}

void PickDataProvider::updateDescriptorSets(VkWriteDescriptorSet* pDescriptors)
{
    /// NOTE: This might be made relocatable so perhaps support that.
    pDescriptors->dstBinding = 2;
    pDescriptors->descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
}

void PickDataProvider::performUpdate(void* allocBegin)
{
    uvec4 data{center.x, center.y, radius, 0};
    std::memcpy(allocBegin, data.data(), sizeof(data));
}

LightDataProvider::LightDataProvider(MultiAllocatedBuffer& allocFrom,
                                     VkDevice device)
    : BufferProvider(allocFrom,
                     sizeof(lights),
                     device)
{}

void LightDataProvider::updateDescriptorSets(VkWriteDescriptorSet* pDescriptors)
{
    /// TODO: Make relocatable.
    pDescriptors->dstBinding = 0;
    pDescriptors->descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
}

void LightDataProvider::performUpdate(void* allocBegin)
{
    std::memcpy(allocBegin, &lights, sizeof(lights));
}

DefaultMaterialParameterProvider::DefaultMaterialParameterProvider(MultiAllocatedBuffer& allocFrom,
                                                                   VkDevice device,
                                                                   const Image& placeholderTexture)
    : BufferProvider(allocFrom, sizeof(vec4), device)
    , tint{0., 0., 0., 0.}
    , m_placeholderTexture{&placeholderTexture}
{}

DefaultMaterialParameterProvider::~DefaultMaterialParameterProvider()
{
    ;
}

VkDescriptorImageInfo getDescriptorInfo(const Image* image)
{
    return
    {
        .sampler = image->sampler,
        .imageView = image->view,
        .imageLayout = image->layout,
    };
}

void DefaultMaterialParameterProvider::write(VkDescriptorSet descSet,
                                             size_t frameIndex)
{
    if (bufferCount() != 1)
    {
        throw std::logic_error("Bad buffer count.");
    }

    std::array<VkWriteDescriptorSet, 3> write;
    for (U32 k = 0; k < write.size(); ++k)
    {
        write[k] =
        {
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .pNext = nullptr,
            .dstSet = descSet,
            .dstBinding = k,
            .dstArrayElement = 0,
            .descriptorCount = 1,
            .pImageInfo = nullptr,
            .pBufferInfo = nullptr,
            .pTexelBufferView = nullptr,
        };
    }

    auto colInfo = getDescriptorInfo(colourTexture
                                     ? colourTexture.get()
                                     : m_placeholderTexture);
    auto nmlInfo = getDescriptorInfo(normalTexture
                                     ? normalTexture.get()
                                     : m_placeholderTexture);

    write[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    write[0].pImageInfo = &colInfo;
    write[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    write[1].pImageInfo = &nmlInfo;
    write[2].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    write[2].pBufferInfo = pBufferInfos(frameIndex);

    vkUpdateDescriptorSets(getDevice(), write.size(), write.data(), 0, nullptr);
}

void DefaultMaterialParameterProvider::updateDescriptorSets(VkWriteDescriptorSet*)
{
    throw std::logic_error("This function is not supposed to be called.");
}

void DefaultMaterialParameterProvider::performUpdate(void* allocBegin)
{
    std::memcpy(allocBegin, &tint, sizeof(tint));
}

void DefaultMaterialParameterProvider::moveFromOther(DefaultMaterialParameterProvider& other) noexcept
{
    colourTexture = other.colourTexture;
    normalTexture = other.normalTexture;
    tint = other.tint;
    m_placeholderTexture = other.m_placeholderTexture;
}

PlotFunctionBufferProvider::PlotFunctionBufferProvider(VkDevice device,
                                                       VmaAllocator allocator)
    : m_device{device}
    , m_allocator{allocator}
{
    VkBufferCreateInfo storageBufferCreate{};
    storageBufferCreate.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    storageBufferCreate.size = m_opcodeBufSize + m_constantsBufSize;
    storageBufferCreate.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
    storageBufferCreate.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    VmaAllocationCreateInfo storageBufferVma{};
    storageBufferVma.usage = VMA_MEMORY_USAGE_GPU_TO_CPU;

    for (std::size_t cachedFrameIndex = 0; auto& alloc : m_allocations)
    {
        // WARNING: This can leak memory if subsequent allocations throws.
        if (vmaCreateBuffer(m_allocator,
                            &storageBufferCreate,
                            &storageBufferVma,
                            &alloc.buffer,
                            &alloc.allocation,
                            nullptr) != VK_SUCCESS)
        {
            throw std::runtime_error("Failed to allocate storage buffer.");
        }
        vmaSetAllocationName(m_allocator,
                             alloc.allocation,
                             "Plot function opcode buffer");

        // sin(x) + 3*cos(abs(x) + abs(y))
        const U32 defaultCode[]
        {
            PLOT_OP_PUSH_Y,
            PLOT_OP_UNARY_FUNCTION, PLOT_FUNCTION_ABS,
            PLOT_OP_PUSH_X,
            PLOT_OP_UNARY_FUNCTION, PLOT_FUNCTION_ABS,
            PLOT_OP_ADD,
            PLOT_OP_UNARY_FUNCTION, PLOT_FUNCTION_COS,
            PLOT_OP_PUSH_CONSTANT, 0u,
            PLOT_OP_MUL,
            PLOT_OP_PUSH_X,
            PLOT_OP_UNARY_FUNCTION, PLOT_FUNCTION_SIN,
            PLOT_OP_ADD,
            PLOT_OP_RETURN,
        };

        const F32 defaultConstants[] =
        {
            3.,
        };

        // We fill it in with a small default function -- technically we could
        // just write push, return, but this gives a more interesting default.
        MemoryMapGuard mmg(m_allocator, alloc.allocation);
        std::memcpy(mmg.data(), defaultCode, sizeof(defaultCode));
        std::memcpy(mmg.data(m_opcodeBufSize), defaultConstants, sizeof(defaultConstants));

        m_bufferInfos[cachedFrameIndex] =
        {{
            {
                .buffer = alloc.buffer,
                .offset = 0,
                .range = m_opcodeBufSize,
            },
            {
                .buffer= alloc.buffer,
                .offset = m_opcodeBufSize,
                .range = m_constantsBufSize,
            },
        }};

        m_dirty[cachedFrameIndex] = false;

        ++cachedFrameIndex;
    }
}

PlotFunctionBufferProvider::~PlotFunctionBufferProvider()
{
    for (auto& alloc : m_allocations)
    {
        alloc.destroy(m_allocator);
    }
}

void PlotFunctionBufferProvider::setFunction(std::vector<U32>&& code,
                                             std::vector<F32>&& constants)
{
    if (code.size() * 4 >= m_opcodeBufSize
        || constants.size() * 4 >= m_constantsBufSize)
    {
        throw std::domain_error("Cannot set function: Too large for buffer, and"
                                " reallocation unimplemented.");
    }
    m_code = std::move(code);
    m_constants = std::move(constants);
    for (auto& isDirty : m_dirty) isDirty = true;
}

void PlotFunctionBufferProvider::write(VkDescriptorSet descSet,
                                       size_t descriptorIndex)
{
    if (descriptorIndex >= cachedFrameCount)
    {
        throw std::logic_error("Bad descriptor index.");
    }

    VkWriteDescriptorSet writes[2];
    writes[0] =
    {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .pNext = nullptr,
        .dstSet = descSet,
        .dstBinding = 0,
        .dstArrayElement = 0,
        .descriptorCount = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
        .pImageInfo = nullptr,
        .pBufferInfo = &m_bufferInfos[descriptorIndex][0],
        .pTexelBufferView = nullptr,
    };
    writes[1] = writes[0];
    writes[1].dstBinding = 1;
    writes[1].pBufferInfo = &m_bufferInfos[descriptorIndex][1];

    vkUpdateDescriptorSets(m_device,
                           2,
                           writes,
                           0, nullptr);
}

void PlotFunctionBufferProvider::update(size_t frameIndex)
{
    if (!m_dirty[frameIndex]) return;
    MemoryMapGuard mmg(m_allocator, m_allocations[frameIndex].allocation);
    std::memcpy(mmg.data(), m_code.data(), m_code.size() * sizeof(m_code[0]));
    std::memcpy(mmg.data(m_opcodeBufSize),
                m_constants.data(),
                m_constants.size() * sizeof(m_constants[0]));
    m_dirty[frameIndex] = false;
}
