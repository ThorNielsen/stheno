#include "renderer/windowopener.hpp"

#include "vulkan/types.hpp"

#include <kraken/window/window.hpp>

#include <functional>
#include <stdexcept>

WindowOpener::WindowOpener(kraken::window::Window& window,
                           VkInstance instance,
                           const kraken::window::WindowCreationSettings& settings,
                           bool closeOnDestruction)
    : m_window{&window}
    , m_surface{VK_NULL_HANDLE}
    , m_instance{instance}
{
    VkResult surfaceCreationResult = VK_ERROR_UNKNOWN;
    m_window->open(settings,
                   std::make_unique<kraken::window::VulkanSurfaceCreationInfo>
                   (
                       m_instance,
                       nullptr,
                       std::ref(surfaceCreationResult),
                       std::ref(m_surface)
                   ));


    if (surfaceCreationResult != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to Vulkanise surface: "
                                 + (std::string)toString(surfaceCreationResult));
    }

    if (!closeOnDestruction) m_window = nullptr;
}

WindowOpener::~WindowOpener()
{
    if (m_instance != VK_NULL_HANDLE
        && m_surface != VK_NULL_HANDLE)
    {
        vkDestroySurfaceKHR(m_instance, m_surface, nullptr);
    }
    if (m_window) m_window->close();
}

VkSurfaceKHR WindowOpener::acquireSurface() noexcept
{
    auto surface = m_surface;
    m_surface = VK_NULL_HANDLE;
    return surface;
}
