#include "renderer/device.hpp"

#include "vulkan/common.hpp"
#include "vulkan/device.hpp"
#include "vulkan/types.hpp"

#include <stdexcept>
#include <string>
#include <vector>

#include <vulkan/vulkan_core.h>

VulkanDevice::VulkanDevice(const VulkanDeviceParameters& params)
    : m_device{VK_NULL_HANDLE}
    , m_physicalDevice{VK_NULL_HANDLE}
    , m_queue{VK_NULL_HANDLE}
{
    if (params.physicalDevice != VK_NULL_HANDLE)
    {
        m_physicalDevice = params.physicalDevice;
    }
    else
    {
        if (params.instance == VK_NULL_HANDLE)
        {
            throw std::logic_error("Cannot construct Vulkan device when "
                                   "neither physical device nor Vulkan "
                                   "instance is provided.");
        }
        auto foundDevice = selectBestPhysicalDevice(params.instance);
        if (!foundDevice.has_value())
        {
            // Todo: Check for queue/present support as weææ?
            throw std::runtime_error("Failed to find an appropriate physical "
                                     "device.");
        }
        m_physicalDevice = foundDevice.value();
    }

    auto queueCreateInfo = selectDeviceQueues(m_physicalDevice,
                                              params.surface,
                                              1,
                                              &params.queue,
                                              &m_queueInfo);

    std::vector<std::string> requiredExtensions;
    requiredExtensions.reserve(params.requiredExtensions.size());
    for (const auto& ext : params.requiredExtensions)
    {
        requiredExtensions.emplace_back(ext);
    }
    if (params.surface != VK_NULL_HANDLE
        && !params.requiredExtensions.contains(VK_KHR_SWAPCHAIN_EXTENSION_NAME))
    {
        requiredExtensions.emplace_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
    }

    std::vector<std::string> optionalExtensions;
    optionalExtensions.reserve(params.optionalExtensions.size());
    for (const auto& ext : params.optionalExtensions)
    {
        optionalExtensions.emplace_back(ext);
    }

    auto devExt = getSupportedVulkanDeviceExtensions(m_physicalDevice,
                                                     requiredExtensions,
                                                     optionalExtensions);

    VkDeviceCreateInfo deviceCreate{};
    deviceCreate.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    deviceCreate.pQueueCreateInfos = queueCreateInfo.first.data();
    deviceCreate.queueCreateInfoCount = queueCreateInfo.first.size();
    deviceCreate.pEnabledFeatures = &params.features;
    // Device layers are deprecated in Vulkan 1.1.
    deviceCreate.ppEnabledLayerNames = nullptr;
    deviceCreate.enabledLayerCount = 0;

    deviceCreate.ppEnabledExtensionNames = devExt.data();
    deviceCreate.enabledExtensionCount = devExt.size();



    const VkResult result = vkCreateDevice(m_physicalDevice,
                                           &deviceCreate,
                                           nullptr,
                                           &m_device);
    if (result != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create Vulkan device "
                                 "(" + std::string(toString(result)) + ")");
    }

    vkGetDeviceQueue(m_device,
                     m_queueInfo.familyIndex,
                     m_queueInfo.queueIndex,
                     &m_queue);
}

VulkanDevice::~VulkanDevice()
{
    if (m_device != VK_NULL_HANDLE)
    {
        vkDestroyDevice(m_device, nullptr);
    }
}
