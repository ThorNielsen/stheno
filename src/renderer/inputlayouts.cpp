#include "renderer/inputlayouts.hpp"
#include "vulkan/types.hpp"

// #define DEBUG_PRINT_COMPOSITION_OPERATIONS

#ifdef DEBUG_PRINT_COMPOSITION_OPERATIONS
#include <iostream>
#endif // DEBUG_PRINT_COMPOSITION_OPERATIONS
#include <ostream>
#include <stdexcept>


VkResult compileLayout(ShaderInputSetLayout& layout, VkDevice device)
{
    const VkDescriptorSetLayoutCreateInfo createInfo
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .bindingCount = (U32)layout.bindings.size(),
        .pBindings = layout.bindings.data(),
    };
    auto result = vkCreateDescriptorSetLayout(device,
                                              &createInfo,
                                              nullptr,
                                              &layout.layout);
    if (result != VK_SUCCESS) layout.layout = VK_NULL_HANDLE;
    return result;
}

std::string to_string(InputLayoutElementTag tag)
{
    using enum InputLayoutElementTag;
    switch (tag)
    {
    case Location: return "Location";
    case Binding: return "Binding";
    case Format: return "Format";
    case Offset: return "Offset";
    case Stride: return "Stride";
    case InputRate: return "InputRate";
    case DescriptorType: return "DescriptorType";
    case DescriptorCount: return "DescriptorCount";
    case StageFlags: return "StageFlags";
    case ImmutableSamplers: return "ImmutableSamplers";
    case Size: return "Size";
    case TagCount: default: return "Invalid";
    }
}

template <typename Tuple, size_t CurrIndex = std::tuple_size_v<Tuple> - 1>
inline void copyTupleElementsByBitfield(U32 bits, Tuple& dst, const Tuple& src) noexcept(std::is_nothrow_assignable_v<Tuple, Tuple>)
{
    if constexpr (CurrIndex)
    {
        copyTupleElementsByBitfield<Tuple, CurrIndex - 1>(bits, dst, src);
    }
    if ((bits >> CurrIndex) & 1)
    {
        std::get<CurrIndex>(dst) = std::get<CurrIndex>(src);
    }
}
/*
InputLayoutSpecificationElement2&
InputLayoutSpecificationElement2::operator+=(const InputLayoutSpecificationElement2& other)
{
    return *this = *this + other;
}

InputLayoutSpecificationElement2&
InputLayoutSpecificationElement2::operator*=(const InputLayoutSpecificationElement2& other)
{
    siblings.push_back(other);

    return *this;
}

InputLayoutSpecificationElement2
operator+(const InputLayoutSpecificationElement2& first,
          const InputLayoutSpecificationElement2& second)
{
    InputLayoutSpecificationElement2 newElem;
    for (const auto* toAdd : {&first, &second})
    {
        if (toAdd->element.has_value())
        {
            newElem.children.push_back(*toAdd);
        }
    }
    newElem.children.push_back(first);
    newElem.children.push_back(second);
    return newElem;
}*/

template <InputLayoutElementTag tag>
auto getValue(const InputLayoutElements& source)
{
    return std::get<size_t(tag)>(source);
}

ElementLayoutCreateInfo
InputLayoutSpecificationElements::getCreateInfo() const
{
    using enum InputLayoutElementTag;
    constexpr auto VertexAttrMask
        = asBitMask(Location)
        | asBitMask(Binding)
        | asBitMask(Format)
        | asBitMask(Offset)
        ;
    constexpr auto VertexBindingMask
        = asBitMask(Binding)
        | asBitMask(Stride)
        | asBitMask(InputRate)
        ;
    constexpr auto DescriptorSetMask
        = asBitMask(Binding)
        | asBitMask(DescriptorType)
        | asBitMask(DescriptorCount)
        | asBitMask(StageFlags)
        | asBitMask(ImmutableSamplers)
        ;
    constexpr auto DescriptorSetMaskNoSamplers
        = asBitMask(Binding)
        | asBitMask(DescriptorType)
        | asBitMask(DescriptorCount)
        | asBitMask(StageFlags)
        ;
    constexpr auto PushConstantRangeMask
        = asBitMask(StageFlags)
        | asBitMask(Offset)
        | asBitMask(Size)
        ;

    switch (validElements)
    {
    case VertexAttrMask:
        return VkVertexInputAttributeDescription
        {
            .location = getValue<Location>(value),
            .binding = getValue<Binding>(value),
            .format = getValue<Format>(value),
            .offset = getValue<Offset>(value),
        };
    case VertexBindingMask:
        return VkVertexInputBindingDescription
        {
            .binding = getValue<Binding>(value),
            .stride = getValue<Stride>(value),
            .inputRate = getValue<InputRate>(value),
        };
    case DescriptorSetMask:
        return VkDescriptorSetLayoutBinding
        {
            .binding = getValue<Binding>(value),
            .descriptorType = getValue<DescriptorType>(value),
            .descriptorCount = getValue<DescriptorCount>(value),
            .stageFlags = getValue<StageFlags>(value),
            .pImmutableSamplers = getValue<ImmutableSamplers>(value),
        };
    case DescriptorSetMaskNoSamplers:
        return VkDescriptorSetLayoutBinding
        {
            .binding = getValue<Binding>(value),
            .descriptorType = getValue<DescriptorType>(value),
            .descriptorCount = getValue<DescriptorCount>(value),
            .stageFlags = getValue<StageFlags>(value),
            .pImmutableSamplers = nullptr,
        };
    case PushConstantRangeMask:
        return VkPushConstantRange
        {
            .stageFlags = getValue<StageFlags>(value),
            .offset = getValue<Offset>(value),
            .size = getValue<Size>(value),
        };
    default:
        throw std::domain_error("Invalid element layout specification.");
    }
}
std::vector<ElementLayoutCreateInfo>
getCreateInfo(const InputLayoutSpecificationElementSet& elemSet)
{
    std::vector<ElementLayoutCreateInfo> createInfos;
    createInfos.reserve(elemSet.elements.size());
    for (const auto& elem : elemSet.elements)
    {
        createInfos.push_back(elem.getCreateInfo());
    }
    return createInfos;
}

InputLayoutSpecificationElements&
InputLayoutSpecificationElements::operator*=(const InputLayoutSpecificationElements& other)
{
    auto toCopy = other.validElements;

#ifdef DEBUG_PRINT_COMPOSITION_OPERATIONS
    std::cerr << "" << *this << " *= " << other << "\n";
#endif

    using InputLayoutElementTag::StageFlags;
    // StageFlags may be repeated, so we handle that specifically.
    if (validElements & toCopy & asBitMask(StageFlags))
    {
        std::get<size_t(StageFlags)>(value)
            |= std::get<size_t(StageFlags)>(other.value);
        toCopy &=~ asBitMask(StageFlags);
    }
    if (validElements & toCopy)
    {
        throw std::domain_error("Element specified multiple times.");
    }
    validElements |= toCopy;

    copyTupleElementsByBitfield(toCopy, value, other.value);

    return *this;
}

template <typename T>
void printSingleInputLayoutElement(std::ostream& ost, const T& elem)
{
    ost << elem;
}

void printSingleInputLayoutElement(std::ostream& ost, const VkFormat& elem)
{
    ost << toString(elem);
}

void printSingleInputLayoutElement(std::ostream& ost, const VkSampler* ptr)
{
    ost << static_cast<const void*>(ptr);
}

template <typename Tuple, size_t CurrIndex = std::tuple_size_v<Tuple> - 1>
inline void printTupleElementsByBitfield(U32 bits,
                                         const Tuple& src,
                                         std::ostream& ost,
                                         bool& hasPrinted)
{
    if constexpr (CurrIndex)
    {
        printTupleElementsByBitfield<Tuple, CurrIndex - 1>(bits, src, ost, hasPrinted);
    }
    else hasPrinted = false;
    if ((bits >> CurrIndex) & 1)
    {
        if (hasPrinted) ost << '*';
        ost << to_string(InputLayoutElementTag(CurrIndex))
            << '(';
        printSingleInputLayoutElement(ost, std::get<CurrIndex>(src));
        ost << ')';
        hasPrinted = true;
    }
}

std::ostream& operator<<(std::ostream& ost,
                         const InputLayoutSpecificationElements& element)
{
    bool hasPrinted;

    printTupleElementsByBitfield(element.validElements,
                                 element.value,
                                 ost,
                                 hasPrinted);

    return ost;
}

InputLayoutSpecificationElementSet&
InputLayoutSpecificationElementSet::operator|=(const InputLayoutSpecificationElements& other)
{
#ifdef DEBUG_PRINT_COMPOSITION_OPERATIONS
    for (const auto& elem : elements)
    {
        std::cerr << elem << " ";
    }
    std::cerr << "|= " << other << "\n";
#endif
    elements.push_back(other);
    return *this;
}

InputLayoutSpecificationElementSet&
InputLayoutSpecificationElementSet::operator|=(const InputLayoutSpecificationElementSet& other)
{
    for (const auto& elem : other.elements)
    {
        elements.push_back(elem);
    }
    return *this;
}

InputLayoutSpecificationElementSet&
InputLayoutSpecificationElementSet::operator*=(const InputLayoutSpecificationElements& other)
{
#ifdef DEBUG_PRINT_COMPOSITION_OPERATIONS
    std::cerr << "Operator *= start (overload #1)\n";
#endif
    for (auto& elem : elements)
    {
        elem *= other;
    }
#ifdef DEBUG_PRINT_COMPOSITION_OPERATIONS
    std::cerr << "Operator *= end (overload #1)\n";
#endif
    return *this;
}

InputLayoutSpecificationElementSet&
InputLayoutSpecificationElementSet::operator*=(const InputLayoutSpecificationElementSet& other)
{
#ifdef DEBUG_PRINT_COMPOSITION_OPERATIONS
    std::cerr << "Operator *= start (overload #2)\n";
#endif
    const auto currOrigSize = elements.size();
    for (size_t otherIndex = 1; otherIndex < other.elements.size(); ++otherIndex)
    {
        for (size_t i = 0; i < currOrigSize; ++i)
        {
            // Yes, push_back instead of reserve to avoid worst-case behaviour
            // with successive reserves if there are succesive multiplications.
            elements.push_back(elements[i]);
        }
    }
    for (size_t otherIndex = 0; otherIndex < other.elements.size(); ++otherIndex)
    {
        auto currOffset = currOrigSize * otherIndex;
        for (size_t i = 0; i < currOrigSize; ++i)
        {
            elements[currOffset+i] *= other.elements[otherIndex];
        }
    }
#ifdef DEBUG_PRINT_COMPOSITION_OPERATIONS
    std::cerr << "Operator *= end (overload #2)\n";
#endif
    return *this;
}

std::optional<VkPipelineLayout>
ShaderInputSetLayoutSpecification::createPipelineLayout(VkDevice device,
                                                        U32 maxSet)
{
    maxSet = std::max<decltype(maxSet)>(maxSet, layouts.size());

    std::vector<VkDescriptorSetLayout> setLayouts;
    for (U32 set = 0; set < maxSet && layouts[set]; ++set)
    {
        setLayouts.push_back(layouts[set]->layout);
    }

    const VkPipelineLayoutCreateInfo pipelineCreate
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .setLayoutCount = (U32)setLayouts.size(),
        .pSetLayouts = setLayouts.data(),
        .pushConstantRangeCount = 0,
        .pPushConstantRanges = nullptr,
    };

    VkPipelineLayout pipelineLayout;

    auto result = vkCreatePipelineLayout(device,
                                         &pipelineCreate,
                                         nullptr,
                                         &pipelineLayout);

    if (result == VK_SUCCESS) return pipelineLayout;

    return std::nullopt;
}
