#include "renderer/scene.hpp"

#include <cstring>
#include <stack>
#include <type_traits>

template <typename T>
struct is_memcmpable_array
{
    constexpr static bool value = std::is_trivially_copyable<T>::value
                                  && sizeof(T[2]) == 2*sizeof(T);
};

bool operator==(const MaterialIndex& a, const MaterialIndex& b)
{
    // Note: This compares *pointers* for the extra properties; which is a
    // *really* ugly solution, so that should be fixed asap.
    if (a.attrCount != b.attrCount || a.bindingCount != b.bindingCount)
    {
        return false;
    }
    static_assert(is_memcmpable_array<VkVertexInputAttributeDescription>::value);
    static_assert(is_memcmpable_array<VkVertexInputBindingDescription>::value);
    if (std::memcmp(a.pAttrs, b.pAttrs,
                    sizeof(VkVertexInputAttributeDescription)*a.attrCount))
    {
        return false;
    }
    if (std::memcmp(a.pBindings, b.pBindings,
                    sizeof(VkVertexInputBindingDescription)*a.bindingCount))
    {
        return false;
    }
    return true;
}
SceneMaterial& SceneMaterial::operator=(SceneMaterial&& other)
{
    destroy();

    m_shader = other.m_shader;
    m_template = other.m_template;
    m_pipeline = other.m_pipeline;
    m_device = other.m_device;

    other.m_shader = nullptr;
    other.m_template = nullptr;
    other.m_pipeline = VK_NULL_HANDLE;
    other.m_device = VK_NULL_HANDLE;

    return *this;
}

SceneMaterial::~SceneMaterial()
{
    destroy();
}

void SceneMaterial::destroy()
{
    if (m_device != VK_NULL_HANDLE && m_pipeline != VK_NULL_HANDLE)
    {
        vkDestroyPipeline(m_device, m_pipeline, nullptr);
    }
    m_shader = nullptr;
    m_template = nullptr;
    m_pipeline = VK_NULL_HANDLE;
    m_device = VK_NULL_HANDLE;
}

SceneMaterialInstance SceneMaterial::allocateInstance()
{
    SceneMaterialInstance instance;
    instance.material = this;
/*
    auto alloc = m_mab.allocateBuffer(m_allocTypeIndex,
                                      instance.descriptorBufferInfo);

    instance.setCount = m_setsPerInstance;
    instance.pDescriptorSets = allocateDescriptorSets();

    size_t setsToAllocate = m_shader->info().usedSets.size();*/
    return instance;
}

SceneObjectLoader::SceneObjectLoader(ResourceLoader& loader, VkDevice device)
    : m_resourceLoader{&loader}
    , m_nextObjectID{0}
    , m_device{device}
{}

SceneObjectLoader::~SceneObjectLoader()
{
    for (auto& batch : m_currTransitions)
    {
        if (batch.event != VK_NULL_HANDLE)
        {
            vkDestroyEvent(m_device, batch.event, nullptr);
        }
    }
    for (auto event : m_eventCache)
    {
        vkDestroyEvent(m_device, event, nullptr);
    }
}

void SceneObjectLoader::clear()
{
    std::lock_guard lg(m_mutex);
    m_objects.clear();
    m_images.clear();
    m_meshes.clear();
    m_imgPendingLoads.clear();
    m_meshPendingLoads.clear();
    m_modified.clear();

    m_nextObjectID = 0;
}

std::optional<kraken::JSONValue> readJSON(fs::path path);

std::optional<mat4> parseTransform(const kraken::JSONValue& transform) noexcept
{
    if (transform.type() == kraken::JSONType::String)
    {
        if (transform == "identity") return mat4(1.);
        return std::nullopt;
    }
    if (transform.type() != kraken::JSONType::Array) return std::nullopt;
    const auto& asArray = transform.asArray();
    if (asArray.size() != 3 && asArray.size() != 4)
    {
        return std::nullopt;
    }
    mat4 result(1.);
    for (size_t rowIndex = 0; rowIndex < asArray.size(); ++rowIndex)
    {
        if (asArray[rowIndex].type() != kraken::JSONType::Array
            || asArray[rowIndex].asArray().size() != 4)
        {
            return std::nullopt;
        }
        const auto& row = asArray[rowIndex].asArray();
        for (size_t col = 0; col < row.size(); ++col)
        {
            if (row[col].type() != kraken::JSONType::Number)
            {
                return std::nullopt;
            }
            result(rowIndex, col) = row[col].asNumber();
        }
    }
    return result;
}

void SceneObjectLoader::loadScene(const kraken::JSONValue& description,
                                  fs::path searchDir)
{
    struct SceneNodeIterator
    {
        const kraken::JSONValue* data;
        mat4 parentTransform;
    };

    std::stack<SceneNodeIterator> nodes;
    nodes.push({&description, mat4{1.}});
    while (!nodes.empty())
    {
        auto currNode = nodes.top();
        nodes.pop();

        const auto& currDesc = *currNode.data;
        if (currDesc.type() != kraken::JSONType::Object)
        {
            throw std::runtime_error("Got bad non-object json "
                                     + currDesc.serialise(true));
        }

        auto currTransform = currNode.parentTransform
                           * parseTransform(currDesc("transform")).value_or(mat4(1.));

        const auto& type = currDesc("type");
        if (type == "model")
        {
            if (currDesc("source") != "uri")
            {
                std::cerr << "Error: Only \"uri\" source is supported.\n";
                continue;
            }
            if (currDesc("uri").type() != kraken::JSONType::String)
            {
                std::cerr << "Error: URI must be a string.";
                continue;
            }

            auto path = searchDir / currDesc("uri").asString();

            if (!fs::exists(path))
            {
                std::cerr << "Failed to find model description " << path << "\n";
                continue;
            }
            auto modelDesc = readJSON(path);
            if (!modelDesc.has_value())
            {
                std::cerr << "Model " << currDesc << " had no valid description.\n";
                continue;
            }

            enqueueModelLoad(*modelDesc, currTransform);
        }
        else if (type == "group")
        {
            // We explicitly don't do anything about groups.
        }
        else
        {
            std::cerr << "Warning: Got unknown scene node type " << type << "\n";
        }

        const auto& children = currDesc("children");
        if (children.type() == kraken::JSONType::Array)
        {
            for (const auto& child : children.asArray())
            {
                nodes.push({&child, currTransform});
            }
        }
        else if (children.type() == kraken::JSONType::Object)
        {
            nodes.push({&children, currTransform});
        }
        else if (children.type() != kraken::JSONType::Null)
        {
            std::cerr << "Error: Got invalid children " << children << "\n";
        }
    }
}

void SceneObjectLoader::recordVulkanCommands(VkCommandBuffer commandBuf,
                                             SamplerCache& samplerCache)
{
    std::lock_guard guard(m_mutex);

    for (size_t batchIndex = 0; batchIndex < m_currTransitions.size();)
    {
        auto& batch = m_currTransitions[batchIndex];
        if (!batch.event)
        {
            batch.event = newEvent(true);
            std::vector<VkImageMemoryBarrier> barriers(batch.imgs.size());
            for (size_t k = 0; k < batch.imgs.size(); ++k)
            {
                auto currImg = batch.imgs[k].second.get();

                if (samplerCache.getSampler(&currImg->sampler,
                                            VK_FILTER_LINEAR,
                                            VK_FILTER_LINEAR,
                                            VK_SAMPLER_MIPMAP_MODE_NEAREST,
                                            VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT,
                                            0.f) != VK_SUCCESS)
                {
                    throw std::runtime_error("Failed to acquire sampler.");
                }

                barriers[k] =
                {
                    .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                    .srcAccessMask = currImg->accessMask,
                    .dstAccessMask = VK_ACCESS_SHADER_READ_BIT,
                    .oldLayout = currImg->layout,
                    .newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,

                    .image = currImg->image,
                    .subresourceRange = currImg->defaultSubresourceRange(),
                };
                currImg->accessMask = barriers[k].dstAccessMask;
                currImg->layout = barriers[k].newLayout;
            }
            vkCmdPipelineBarrier(commandBuf,
                                 VK_PIPELINE_STAGE_TRANSFER_BIT,
                                 VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
                                 0,
                                 0, nullptr,
                                 0, nullptr,
                                 barriers.size(), barriers.data());
            // Transition above takes place before fragment shader, so that is
            // what we will wait for.
            vkCmdSetEvent(commandBuf,
                          batch.event,
                          VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT);
        }
        else if (vkGetEventStatus(m_device, batch.event) == VK_EVENT_SET)
        {
            for (const auto& [uri, image] : batch.imgs)
            {
                m_images[uri] = image;
                auto [pendBegin, pendEnd] = m_imgPendingLoads.equal_range(uri);

                for (auto pIt = pendBegin; pIt != pendEnd; ++pIt)
                {
                    auto [objectID, imgDst] = pIt->second;
                    getDst(m_objects.at(objectID), imgDst) = image;
                    markModified(objectID, true);
                }

                m_imgPendingLoads.erase(pendBegin, pendEnd);
            }
            deleteEvent(batch.event, true);
            std::swap(batch, m_currTransitions.back());
            m_currTransitions.pop_back();
            continue; // Do not increase batchIndex.
        }

        ++batchIndex;
    }
}

std::optional<SceneObjectID> SceneObjectLoader::nextModifiedObject()
{
    std::lock_guard guard(m_mutex);
    if (m_modified.empty()) return std::nullopt;

    auto it = m_modified.begin();
    auto result = *it;
    m_modified.erase(it);
    return result;
}

SceneObject SceneObjectLoader::get(SceneObjectID id) const
{
    std::lock_guard guard(m_mutex);
    return m_objects.at(id);
}


void SceneObjectLoader::loadMesh(std::string uri)
{
    auto& currObject = m_objects[nextID()];
    currObject.name = uri;
    currObject.transform = mat4(1.);
    auto id = nextID();
    m_objects[id] = {};
    loadMesh(uri, id);
}

void SceneObjectLoader::enqueueModelLoad(const kraken::JSONValue& description,
                                         mat4 transform)
{
    if (description("shader") != "shaders/default.json")
    {
        throw std::logic_error("Dynamic loading of different shaders not yet "
                               "implemented.");
    }

    if (description("mesh").type() != kraken::JSONType::String)
    {
        throw std::runtime_error("Cannot load mesh from non-path description.");
    }
    if (description("textures")("colour").type() != kraken::JSONType::String
        || description("textures")("normal").type() != kraken::JSONType::String)
    {
        throw std::logic_error("Missing either a valid colour or normal "
                               "texture path from description.");
    }

    auto currObjectID = nextID();

    auto& currObject = m_objects[currObjectID];
    currObject.name = description("mesh").asString();
    currObject.transform = transform;
    loadMesh((std::string)description("mesh"), currObjectID);
    loadImage((std::string)description("textures")("colour"), ImageDestinationah::Colour, currObjectID);
    loadImage((std::string)description("textures")("normal"), ImageDestinationah::Normal, currObjectID);
}

std::shared_ptr<Image>&
SceneObjectLoader::getDst(SceneObject& sceneObj, ImageDestinationah dst)
{
    switch (dst)
    {
    case ImageDestinationah::Colour: return sceneObj.colour;
    case ImageDestinationah::Normal: return sceneObj.normal;
    default:
        throw std::logic_error("Bad enum.");
    }
}

void SceneObjectLoader::onImageLoadFinish(std::string uri,
                                          std::shared_ptr<Image> img)
{
    std::lock_guard guard(m_mutex);

    ImageTransitionBatch* pendingBatch = nullptr;
    for (auto& batch : m_currTransitions)
    {
        if (batch.event == VK_NULL_HANDLE)
        {
            pendingBatch = &batch;
            break;
        }
    }
    if (!pendingBatch)
    {
        m_currTransitions.emplace_back();
        pendingBatch = &m_currTransitions.back();
        pendingBatch->event = VK_NULL_HANDLE;
    }
    pendingBatch->imgs.emplace_back(std::pair{uri, img});
}

void SceneObjectLoader::onMeshLoadFinish(std::string uri,
                                         std::shared_ptr<Mesh> mesh)
{
    std::lock_guard guard(m_mutex);

    m_meshes[uri] = mesh;

    auto [pendingBegin, pendingEnd] = m_meshPendingLoads.equal_range(uri);

    for (auto it = pendingBegin; it != pendingEnd; ++it)
    {
        m_objects.at(it->second).mesh = mesh;
        markModified(it->second, true);
    }

    m_meshPendingLoads.erase(pendingBegin, pendingEnd);
}

void SceneObjectLoader::loadImage(std::string uri,
                                  ImageDestinationah imgDst,
                                  SceneObjectID objDst)
{
    std::unique_lock guard(m_mutex);
    auto it = m_images.find(uri);
    if (it != m_images.end() && it->second)
    {
        getDst(m_objects.at(objDst), imgDst) = it->second;
        markModified(objDst, true);
    }
    else m_imgPendingLoads.emplace(uri, std::pair{objDst, imgDst});

    // In this case we need also enqueue it.
    if (it == m_images.end())
    {
        m_images.emplace(uri, nullptr);
        ResourceLoader::Options opts;
        opts.priority = 1.f; // There's no particular reason for any prioritisation
                             // in this current messy renderer, except for wanting
                             // images to load later than meshes, because meshes can
                             // be shown with placeholder images, but the reverse
                             // isn't really feasible.
        opts.type = ResourceLoader::ResourceType::VulkanResource;
        opts.vulkanOptions["type"] = "image";
        opts.vulkanOptions["image"]["usageFlags"][0] = "VK_IMAGE_USAGE_SAMPLED_BIT";

        opts.finishNotify = [this, uri]
        (ResourceLoader::Status, ResourceLoader::ResourceID id, ResourceLoader&)
        {
            ResourceLoader::VulkanResource vulkanResource;
            if (!m_resourceLoader->acquireVulkanResource(id, &vulkanResource))
            {
                throw std::runtime_error("Failed to acquire Vulkan resource.");
            }
            auto image = std::make_shared<Image>();
            *image = std::move(vulkanResource.image);

            onImageLoadFinish(uri, image);
        };

        m_resourceLoader->load("image/png", uri, opts);
    }
}

void SceneObjectLoader::loadMesh(std::string uri, SceneObjectID objDst)
{
    std::unique_lock guard(m_mutex);
    auto it = m_meshes.find(uri);
    if (it != m_meshes.end() && it->second)
    {
        m_objects.at(objDst).mesh = it->second;
        markModified(objDst, true);
    }
    else m_meshPendingLoads.emplace(uri, objDst);

    // In this case we need also enqueue it.
    if (it == m_meshes.end())
    {
        m_meshes.emplace(uri, nullptr);
        ResourceLoader::Options opts;
        opts.priority = 0.f;
        opts.type = ResourceLoader::ResourceType::HostResource
                  | ResourceLoader::ResourceType::VulkanResource;
        opts.finishNotify = [this, uri]
        (ResourceLoader::Status, ResourceLoader::ResourceID id, ResourceLoader&)
        {
            ResourceLoader::VulkanResource vulkanResource;
            if (!m_resourceLoader->acquireVulkanResource(id, &vulkanResource))
            {
                throw std::runtime_error("Failed to acquire Vulkan resource.");
            }
            ResourceLoader::HostResource hostResource;
            if (!m_resourceLoader->acquireHostResource(id, &hostResource))
            {
                throw std::runtime_error("Failed to acquire host resource.");
            }
            onMeshLoadFinish(uri,
                             std::make_shared<Mesh>(hostResource.move<Mesh>()));
        };

        //opts.parseOptions["generateTangents"] = true;

        std::string toLoad = "model/ply"; // By default.

        // Should uri be converted to a path or some other type for ease here?
        auto dotpos = uri.rfind('.');
        if (dotpos != std::string::npos)
        {
            auto ext = uri.substr(dotpos+1);
            if (ext == "ply") toLoad = "model/ply";
            else if (ext == "stl") toLoad = "model/stl";
            else
            {
                std::cerr << "ERROR: Unknown extension "
                          << ext << ", defaulting to " << toLoad << "\n";
            }
        }
        m_resourceLoader->load(toLoad, uri, opts);
    }
}

void SceneObjectLoader::markModified(SceneObjectID id,
                                     bool externalSynced)
{
    if (!externalSynced)
    {
        std::lock_guard guard(m_mutex);
        markModified(id, true);
    }
    else
    {
        m_modified.emplace(id);
    }
}

SceneObjectID SceneObjectLoader::nextID() noexcept
{
    std::lock_guard guard(m_mutex);
    while (!m_nextObjectID || m_objects.contains(m_nextObjectID))
    {
        ++m_nextObjectID;
    }
    return m_nextObjectID;
}

VkEvent SceneObjectLoader::newEvent(bool externalSynced)
{
    if (!externalSynced)
    {
        std::lock_guard guard(m_mutex);
        return newEvent(true);
    }

    if (!m_eventCache.empty())
    {
        auto event = m_eventCache.back();
        m_eventCache.pop_back();
        if (vkResetEvent(m_device, event) != VK_SUCCESS)
        {
            throw std::logic_error("Failed to reset event.");
        }
        return event;
    }
    VkEvent event;
    VkEventCreateInfo evtCreate{};
    evtCreate.sType = VK_STRUCTURE_TYPE_EVENT_CREATE_INFO;
    auto result = vkCreateEvent(m_device, &evtCreate, nullptr, &event);
    if (result != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create event: Got status "
                                 + std::string(toString(result)) + ".");
    }
    return event;
}

void SceneObjectLoader::deleteEvent(VkEvent event, bool externalSynced)
{
    if (!externalSynced)
    {
        std::lock_guard guard(m_mutex);
        return deleteEvent(event, true);
    }
    m_eventCache.push_back(event);
}
