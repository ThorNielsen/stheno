#include "renderer/controls.hpp"

#include "renderer/perspective.hpp"

#include <kraken/math/matrix.hpp>
#include <kraken/math/transform.hpp>

#include <cmath>
#include <cstdlib>
#include <stdexcept>

using namespace kraken::math;

dmat4 DragMovementInfo::transformAtPosition(dmat4 worldToCamera,
                                            const PerspectiveTransform& pt,
                                            dvec2 currMousePos,
                                            uvec2 viewportDimensions)
{
    state = MovementState::Translating;
    auto origPosAsVec4 = dvec4(movementOrigPos, 1.);
    auto camSpaceDepth = std::abs((worldToCamera * origPosAsVec4).z);

    auto viewportToCS = pt.viewportToCameraSpace(camSpaceDepth,
                                                 viewportDimensions.y);

    // The movement is a direction, therefore .z = 0.
    auto movement = dvec3(currMousePos - startPos, 0.);
    movement = viewportToCS * movement;

    dmat4 translator(1.);
    translator.setCol(3, {movement.x, movement.y, 0., 1.});
    auto transformInWorldSpace = rigidInverse(worldToCamera)*translator*worldToCamera;
    return transformInWorldSpace*origTransform;
}

dmat4 DragMovementInfo::rotateAtPosition(dmat4 worldToCamera,
                                         dvec2 currMousePos)
{
    state = MovementState::Rotating;

    dmat4 clickPosToOrigin(1.);
    clickPosToOrigin.setCol(3, dvec4(-movementOrigPos, 1.));

    auto cameraToWorld = rigidInverse(worldToCamera);

    auto movement = currMousePos - startPos;

    dvec4 camRight{1., 0., 0., 0.};
    dvec4 camUp{0., 1., 0., 0.};
    camRight = cameraToWorld * camRight;
    camUp = cameraToWorld * camUp;

    return rigidInverse(clickPosToOrigin)
         * dmat4(rotation(dvec3(camRight), movement.y * 0.005), 1.)
         * dmat4(rotation(dvec3(camUp), movement.x * -0.005), 1.)
         * clickPosToOrigin
         * origTransform;
}

dmat4 DragMovementInfo::scaleAtPosition(dvec2 currMousePos)
{
    state = MovementState::Scaling;

    dmat4 clickPosToOrigin(1.);
    clickPosToOrigin.setCol(3, dvec4(-movementOrigPos, 1.));

    auto movement = currMousePos - startPos;
    const dvec2 scaleDir{1./std::sqrt(2.), -1./std::sqrt(2)};

    auto scaleFac = std::pow(10, dot(scaleDir, movement) * 0.0025);

    dmat4 scaleMat(scaleFac);
    scaleMat(3, 3) = 1.;

    return rigidInverse(clickPosToOrigin)
         * scaleMat
         * clickPosToOrigin
         * origTransform;
}


dmat4 DragMovementInfo::continueDrag(dmat4 worldToCamera,
                                     const PerspectiveTransform& pt,
                                     dvec2 currMousePos,
                                     uvec2 viewportDimensions)
{
    switch (state)
    {
    case MovementState::None:
        throw std::logic_error("No movement state set (it is nonsense to "
                               "continue).");
    case MovementState::Translating:
        return transformAtPosition(worldToCamera,
                                   pt,
                                   currMousePos,
                                   viewportDimensions);
    case MovementState::Rotating:
        return rotateAtPosition(worldToCamera, currMousePos);
    case MovementState::Scaling:
        return scaleAtPosition(currMousePos);
    default:
        throw std::domain_error("Unknown movement state (corrupt/"
                                "uninitialised memory?).");
    }
}
