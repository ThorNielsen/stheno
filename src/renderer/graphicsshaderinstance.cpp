#include "renderer/graphicsshaderinstance.hpp"

#include <iostream>

GraphicsShaderInstance::~GraphicsShaderInstance() {}

FlatDrawShader::~FlatDrawShader()
{
    destroy();
}

void FlatDrawShader::create(VkDevice device,
                            GraphicsShader&& shader,
                            const VertexInputSpecification& spec,
                            const FrameGraph::RenderPass* pass)
{
    if (m_material) throw std::logic_error("Already created.");
    m_material = std::make_unique<Material>();
    m_spec = spec;
    m_material->shader = std::make_shared<GraphicsShader>(std::move(shader));
    m_device = device;

    createLayouts(*m_material->shader);

    if (pass) refresh(*pass);
}

void FlatDrawShader::destroy()
{
    destroyPipeline();
    if (m_device == VK_NULL_HANDLE) return;
    if (m_material && m_material->layout != VK_NULL_HANDLE)
    {
        vkDestroyPipelineLayout(m_device, m_material->layout, nullptr);
    }
    m_material = nullptr;
}

void FlatDrawShader::createPipeline(GraphicsShader& shader, const FrameGraph::RenderPass& pass)
{
    if (m_device == VK_NULL_HANDLE
        || !m_material
        || m_material->layout == VK_NULL_HANDLE)
    {
        throw std::logic_error("Cannot create pipeline when members aren't created.");
    }
    GraphicsPipelineInfo info;

    info.fillInfo(pass.layout().width,
                  pass.layout().height,
                  VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
                  VK_POLYGON_MODE_FILL,
                  VK_CULL_MODE_BACK_BIT,
                  VK_FRONT_FACE_COUNTER_CLOCKWISE,
                  VK_SAMPLE_COUNT_1_BIT);

    info.vertexInputCreate.vertexAttributeDescriptionCount = m_spec.attributes.size();
    info.vertexInputCreate.pVertexAttributeDescriptions = m_spec.attributes.data();
    info.vertexInputCreate.vertexBindingDescriptionCount = m_spec.bindings.size();
    info.vertexInputCreate.pVertexBindingDescriptions = m_spec.bindings.data();

    auto graphicsCreate = info.getGraphicsPipelineCreateInfo();
    graphicsCreate.pTessellationState = nullptr;
    graphicsCreate.pDynamicState = nullptr;
    graphicsCreate.stageCount = shader.stageCount();
    graphicsCreate.pStages = shader.pStages();
    graphicsCreate.layout = m_material->layout;
    graphicsCreate.renderPass = pass.vulkanPass().pass;
    graphicsCreate.subpass = pass.vulkanPass().subpass;

    if (vkCreateGraphicsPipelines(m_device, VK_NULL_HANDLE, 1, &graphicsCreate, nullptr, &m_material->pipeline) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create graphics pipeline.");
    }
}

void FlatDrawShader::destroyPipeline()
{
    if (!m_material|| m_material->pipeline == VK_NULL_HANDLE) return;
    vkDestroyPipeline(m_device, m_material->pipeline, nullptr);
    m_material->pipeline = VK_NULL_HANDLE;
}

const VertexInputSpecification& FlatDrawShader::vertexInputSpec()
{
    return m_spec;
}

void FlatDrawShader::refresh(const FrameGraph::RenderPass& pass)
{
    destroyPipeline();
    createPipeline(*m_material->shader, pass);
}

std::optional<MaterialInstance>
FlatDrawShader::createInstance(size_t descCopyCount,
                               DescriptorAllocator& descAllocator) noexcept
{
    if (!m_material) return std::nullopt;
    MaterialInstance matInstance;
    matInstance.material = m_material.get();
    matInstance.descriptors.allocateCopies(descCopyCount);

    if (!createDescriptorSets(matInstance.descriptors.sets(), descCopyCount,
                              &m_layout, descAllocator))
    {
        std::cerr << "Error: Failed to create descriptor sets.\n";
        return std::nullopt;
    }

    return matInstance;
}

void FlatDrawShader::createLayouts(GraphicsShader& shader)
{
    m_layout.construct(m_device, nullptr);

    shader.info().appendLayoutInfo(m_layout.layoutInfo(),
                                   shader.info().usedSets.at(0));

    std::sort(m_layout.layoutInfo().bindings.begin(),
              m_layout.layoutInfo().bindings.end(),
              [](const VkDescriptorSetLayoutBinding& bindA,
                 const VkDescriptorSetLayoutBinding& bindB)
              {
                  return bindA.binding < bindB.binding;
              });

    if (m_layout.compileLayout() != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create flat draw descriptor set layout.");
    }

    auto setLayout = m_layout.layout();

    VkPipelineLayoutCreateInfo pipelineCreate{};
    pipelineCreate.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineCreate.setLayoutCount = 1;
    pipelineCreate.pSetLayouts = &setLayout;

    VkPushConstantRange pushConstant;
    pushConstant.size = sizeof(mat4) + sizeof(U32);
    pushConstant.offset = 0;
    pushConstant.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

    pipelineCreate.pPushConstantRanges = &pushConstant;
    pipelineCreate.pushConstantRangeCount = 1;

    pushConstant.stageFlags = VK_SHADER_STAGE_GEOMETRY_BIT;

    if (vkCreatePipelineLayout(m_device, &pipelineCreate, nullptr, &m_material->layout) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create pipeline layout.");
    }
}
