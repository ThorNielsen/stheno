#include "vulkan/shader.hpp"
#include <array>
#include <fstream>
#include <stdexcept>

#include <set>

#define KRAKEN_ENABLE_UNICODE_SUPPORT
#include <kraken/utility/base64.hpp>
#include <kraken/utility/json.hpp>
#include <kraken/utility/memory.hpp>
#include <kraken/utility/string.hpp>
#include <kraken/utility/zlib.hpp>

#include "vulkan/types.hpp"
#include "vulkan/utility.hpp"

namespace
{

std::array<bool, 5> shaderStageFlagsToArray(int flags) noexcept
{
    std::array<bool, 5> arr;
    arr[0] = !!(flags & VK_SHADER_STAGE_VERTEX_BIT);
    arr[1] = !!(flags & VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT);
    arr[2] = !!(flags & VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT);
    arr[3] = !!(flags & VK_SHADER_STAGE_GEOMETRY_BIT);
    arr[4] = !!(flags & VK_SHADER_STAGE_FRAGMENT_BIT);
    return arr;
}

U32 graphicsShaderStageIndex(VkShaderStageFlagBits stage) noexcept
{
    if (stage & VK_SHADER_STAGE_VERTEX_BIT) return 0;
    if (stage & VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT) return 1;
    if (stage & VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT) return 2;
    if (stage & VK_SHADER_STAGE_GEOMETRY_BIT) return 3;
    if (stage & VK_SHADER_STAGE_FRAGMENT_BIT) return 4;
    return 0xffffffff;
}

} // End anonymous namespace

void ShaderInfo::appendLayoutInfo(DescriptorSetLayoutInfo& layoutOut, U32 set) const
{
    for (const auto& desc : descriptors)
    {
        if (desc.set == set)
        {
            layoutOut.addBinding(desc.binding, desc.type, desc.stages);
        }
    }
}

GraphicsShader::GraphicsShader()
    : m_device{VK_NULL_HANDLE}
{
    reset(0);
}

GraphicsShader::GraphicsShader(GraphicsShader&& other) noexcept
{
    *this = std::move(other);
}

GraphicsShader& GraphicsShader::operator=(GraphicsShader&& other) noexcept
{
    m_shaderInfo = std::move(other.m_shaderInfo);
    m_device = other.m_device;
    m_enabledStages = other.m_enabledStages;

    for (size_t k = 0; k < 5; ++k)
    {
        m_modules[k] = other.m_modules[k];
        m_infos[k] = std::move(other.m_infos[k]);
        m_entrypoints[k] = std::move(other.m_entrypoints[k]);
        m_mapping[k] = other.m_mapping[k];
    }

    // Fixup pointers which are not necessarily preserved when moving strings.
    for (U32 currStageIndex = 0; currStageIndex < 5; ++currStageIndex)
    {
        m_infos[m_mapping[currStageIndex]].pName = m_entrypoints[currStageIndex].c_str();
    }

    other.reset(0);
    other.m_device = VK_NULL_HANDLE;

    return *this;
}

VkResult createShaderModule(const kraken::JSONValue& module,
                            VkDevice device,
                            std::string* entrypointOut,
                            VkShaderStageFlagBits* stageOut,
                            VkShaderModule* moduleOut)
{
    VkShaderModuleCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    if (!toVkShaderStageFlagBits((std::string)module("stage"), stageOut))
    {
        return VK_ERROR_FORMAT_NOT_SUPPORTED;
    }
    const auto& entrypoint = module("entrypoint");
    if (entrypoint == nullptr) return VK_ERROR_UNKNOWN;
    *entrypointOut = (std::string)entrypoint;
    const auto& location = module("location");
    const auto& data = module("data");
    std::vector<U8> code;
    if (location == "base64" && data.type() == kraken::JSONType::String)
    {
        kraken::base64Decode(data.asString().begin(),
                             data.asString().end(),
                             std::back_inserter(code));
    }
    else if (location == "deflated_base64" && data.type() == kraken::JSONType::String)
    {
        auto raw = kraken::base64Decode(data.asString());
        if (!kraken::inflate(reinterpret_cast<const U8*>(raw.data()),
                             raw.size(),
                             std::back_inserter(code)))
        {
            return VK_ERROR_UNKNOWN;
        }
    }
    else
    {
        return VK_ERROR_FORMAT_NOT_SUPPORTED; // location == "file" explicitly unsupported for now.
    }
    if (code.size() % 4 || code.empty()) return VK_ERROR_UNKNOWN;
    if (reinterpret_cast<U32*>(code.data())[0] == 0x03022307)
    {
        size_t count = code.size() / 4;
        U32* codeData = reinterpret_cast<U32*>(code.data());
        for (size_t i = 0; i < count; ++i)
        {
            kraken::swapEndianIP(codeData[i]);
        }
    }
    createInfo.codeSize = code.size();
    createInfo.pCode = reinterpret_cast<U32*>(code.data());

    return vkCreateShaderModule(device, &createInfo, nullptr, moduleOut);
}

bool GraphicsShader::create(const kraken::JSONValue& description,
                            VkDevice device)
{
    destroy();
    m_device = device;
    const auto& modules = description("modules");
    if (modules.type() != kraken::JSONType::Array) return false;
    int usedStages = 0;
    for (const auto& moduleDescription : modules.asArray())
    {
        VkShaderStageFlagBits stage;
        VkShaderModule module;
        std::string entrypoint;
        if (createShaderModule(moduleDescription,
                               m_device,
                               &entrypoint,
                               &stage,
                               &module) != VK_SUCCESS)
        {
            return false;
        }

        auto currStageIndex = graphicsShaderStageIndex(stage);
        if (currStageIndex == 0xffffffff ||
            (usedStages | stage) == usedStages)
        {
            return false;
        }
        usedStages |= stage;
        enableStages(stage);
        if (m_modules[currStageIndex] != VK_NULL_HANDLE)
        {
            vkDestroyShaderModule(m_device, module, nullptr);
            throw std::logic_error("About to overwrite already existing "
                                   "shader stage -- incomplete destruction?");
        }
        m_modules[currStageIndex] = module;
        m_entrypoints[currStageIndex] = entrypoint;
        auto& info = m_infos[m_mapping[currStageIndex]];
        info.stage = stage;
        info.pName = m_entrypoints[currStageIndex].c_str();
        info.module = m_modules[currStageIndex];
    }

    const auto& attributes = description("attributes");
    if (attributes.type() != kraken::JSONType::Array) return false;
    std::vector<ShaderInfo::Attribute> inputAttrs;
    for (const auto& desc : attributes.asArray())
    {
        ShaderInfo::Attribute attr;
        if (!toVkFormat((std::string)desc("format"), &attr.format))
        {
            return false;
        }
        attr.location = (U32)desc("location");
        attr.name = (std::string)desc("name");
        inputAttrs.push_back(attr);
    }

    std::set<U32> usedSets;

    const auto& descriptors = description("descriptors");
    if (descriptors.type() != kraken::JSONType::Array) return false;
    std::vector<ShaderInfo::Descriptor> descInfo;
    for (const auto& jDesc : descriptors.asArray())
    {
        ShaderInfo::Descriptor desc;
        desc.name = (std::string)jDesc("name");
        desc.set = (U32)jDesc("set");
        usedSets.insert(desc.set);
        desc.binding = (U32)jDesc("binding");
        desc.size = (U32)jDesc("size");
        if (!toVkDescriptorType((std::string)jDesc("type"), &desc.type))
        {
            return false;
        }
        desc.stages = VkShaderStageFlagBits(0);
        const auto& stages = jDesc("stages");
        if (stages.type() != kraken::JSONType::Array) return false;
        for (const auto& stage : stages.asArray())
        {
            VkShaderStageFlagBits stageFlag;
            if (!toVkShaderStageFlagBits((std::string)stage, &stageFlag))
            {
                return false;
            }
            desc.stages = VkShaderStageFlagBits(desc.stages | stageFlag);
        }
        // Todo: Layout.
        descInfo.push_back(desc);
    }
    m_shaderInfo.attributes = std::move(inputAttrs);
    m_shaderInfo.descriptors = std::move(descInfo);
    m_shaderInfo.usedSets.clear();
    m_shaderInfo.usedSets.reserve(usedSets.size());

    for (auto used : usedSets) m_shaderInfo.usedSets.push_back(used);

    return true;
}

bool GraphicsShader::createFromFile(fs::path filePath, VkDevice device)
{
    std::ifstream shaderInfoFile(filePath);
    if (!shaderInfoFile.is_open())
    {
        return false;
    }
    kraken::JSONValue shaderInfo;
    shaderInfoFile >> shaderInfo;
    return create(shaderInfo, device);
}

void GraphicsShader::reset(int stagesToEnable) noexcept
{
    auto enabled = shaderStageFlagsToArray(stagesToEnable);

    U32 nextSlot = 0;
    U32 slotEnd = 5;
    for (size_t i = 0; i < 5; ++i)
    {
        m_modules[i] = VK_NULL_HANDLE;
        m_infos[i] = VkPipelineShaderStageCreateInfo{};
        m_infos[i].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        m_infos[i].pNext = nullptr;
        if (enabled[i])
        {
            m_mapping[i] = nextSlot++;
        }
        else
        {
            m_mapping[i] = --slotEnd;
        }
    }
    m_enabledStages = nextSlot;
    vertexInfo().stage = VK_SHADER_STAGE_VERTEX_BIT;
    tessellationContolInfo().stage = VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
    tessellationEvaluationInfo().stage = VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
    geometryInfo().stage = VK_SHADER_STAGE_GEOMETRY_BIT;
    fragmentInfo().stage = VK_SHADER_STAGE_FRAGMENT_BIT;
}

void GraphicsShader::enableStages(int which)
{
    auto toEnable = shaderStageFlagsToArray(which);

    for (size_t i = 0; i < 5; ++i)
    {
        if (toEnable[i] && m_mapping[i] >= m_enabledStages)
        {
            U32 desiredSlot = m_enabledStages;
            U32 ownerOfDesiredSlot = getSlotOwner(desiredSlot);
            if (ownerOfDesiredSlot != i)
            {
                std::swap(m_infos[m_mapping[i]], m_infos[desiredSlot]);
                m_mapping[ownerOfDesiredSlot] = m_mapping[i];
                m_mapping[i] = desiredSlot;
            }
            ++m_enabledStages;
        }
    }
}

void GraphicsShader::disableStages(int which)
{
    auto toDisable = shaderStageFlagsToArray(which);

    for (size_t i = 0; i < 5; ++i)
    {
        if (toDisable[i] && m_mapping[i] < m_enabledStages)
        {
            U32 desiredSlot = m_enabledStages-1;
            U32 ownerOfDesiredSlot = getSlotOwner(desiredSlot);
            if (ownerOfDesiredSlot != i)
            {
                std::swap(m_infos[m_mapping[i]], m_infos[desiredSlot]);
                m_mapping[ownerOfDesiredSlot] = m_mapping[i];
                m_mapping[i] = desiredSlot;
            }
            --m_enabledStages;
        }
    }
}

void GraphicsShader::destroyStages(int which)
{
    auto toDestroy = shaderStageFlagsToArray(which);
    for (size_t i = 0; i < 5; ++i)
    {
        if (toDestroy[i] && m_modules[i] != VK_NULL_HANDLE)
        {
            vkDestroyShaderModule(m_device, m_modules[i], nullptr);
            m_modules[i] = VK_NULL_HANDLE;
        }
    }
}

GraphicsShader::~GraphicsShader()
{
    destroy();
}

void GraphicsShader::destroy()
{
    for (size_t i = 0; i < 5; ++i)
    {
        if (m_modules[i] != VK_NULL_HANDLE)
        {
            vkDestroyShaderModule(m_device, m_modules[i], nullptr);
            m_modules[i] = VK_NULL_HANDLE;
        }
        m_mapping[i] = i;
    }
}

U32 GraphicsShader::getSlotOwner(U32 index) const
{
    for (size_t i = 0; i < 5; ++i)
    {
        if (m_mapping[i] == index) return i;
    }
    throw std::logic_error("No slot owner found.");
}
