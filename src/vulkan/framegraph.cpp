#include "vulkan/framegraph.hpp"

#include "vulkan/image.hpp"
#include "vulkan/swapchain.hpp"
#include "vulkan/types.hpp"
#include "vulkan/vma.hpp"

#include <kraken/globaltypes.hpp>

#include <vulkan/vulkan_core.h>

#include <algorithm>
#include <iostream>
#include <iterator>
#include <optional>
#include <stack>
#include <stdexcept>
#include <string>
#include <vector>

void FrameGraph::RenderPass::AttachmentImageInfo::dump(std::string_view prefix) const
{
    std::cerr << prefix << "Previous {pass id: ";
    if (previous.pass) std::cerr << previous.pass->passID();
    else std::cerr << "none";
    std::cerr           << ", "
                        << "attachment id: " << previous.attachmentID << ", "
                        << "isInput: " << (previous.isInput ? "true" : "false")
                        << "}\n";
    std::cerr << prefix << "Next {pass id: ";
    if (next.pass) std::cerr << next.pass->passID();
    else std::cerr << "none";
    std::cerr           << ", "
                        << "attachment id: " << next.attachmentID << ", "
                        << "isInput: " << (next.isInput ? "true" : "false")
                        << "}\n";
    std::cerr << prefix << "Image: " << image << "\n";
    std::cerr << prefix << "View: " << view << "\n";
    std::cerr << prefix << "Usage flags: " << usage << "\n";
    std::cerr << prefix << "Initial layout: " << toString(initialLayout) << "\n";
    std::cerr << prefix << "Used layout: " << toString(usedLayout) << "\n";
    std::cerr << prefix << "Final layout: " << toString(finalLayout) << "\n";
    std::cerr << prefix << "Last usage (in scheduled order): " << lastUsage << "\n";
}

void FrameGraph::RenderPass::dump()
{
    std::cerr << "\033[1;31mRender pass #" << m_passID << "\033[0m (" << this
              << ") with " << m_inputs.size() << " inputs and "
              << m_outputs.size() << " outputs.\n";

    std::string indent = "| ";

    if (!m_inputs.empty())
    {
        std::cerr << indent << "\033[1;32mInputs\033[0m\n";
        for (U32 i = 0; i < m_inputs.size(); ++i)
        {
            indent = "| | ";
            const auto& input = m_inputs[i];
            std::cerr << indent << "Input #" << i << " '" << input.name << "' has:\n";
            indent = "| | | ";
            std::cerr << indent << "Format " << toString(input.format) << "\n";
            std::cerr << indent << "Read as " << (input.attachment ? "input attachment" : "sampled image") << "\n";
            std::cerr << indent << "Connected pass #" << (input.connectedPass ? input.connectedPass->m_passID : 0xffffffff)
                      << ", output attachment #" << input.connectedAttachment << "\n";
            std::cerr << indent << "Image info:\n";
            input.imgInfo.dump(indent + "| ");
        }
    }
    if (!m_outputs.empty())
    {
        indent = "| ";
        std::cerr << indent << "\033[1;34mOutputs\033[0m\n";
        for (U32 i = 0; i < m_outputs.size(); ++i)
        {
            indent = "| | ";
            const auto& output = m_outputs[i];
            std::cerr << indent << "Output #" << i << " '" << output.name << "' has:\n";
            indent = "| | | ";
            std::cerr << indent << "Format " << toString(output.format) << "\n";
            std::cerr << indent << "Load/clear on begin: " << (output.loadOrClear ? "Yes" : "No") << "\n";
            std::cerr << indent;
            if (output.sourcePass)
            {
                std::cerr << "Source pass #" << output.sourcePass->m_passID
                          << ", source output #" << output.sourceAttachment << "\n";
            }
            else
            {
                std::cerr << "Source pass: None\n";
            }
            std::cerr << indent << "Image info:\n";
            output.imgInfo.dump(indent + "| ");
            std::cerr << indent << "Outgoing connections:\n";
            indent = "| | | | ";
            for (const auto& conn : output.connections)
            {
                std::cerr << indent << "To pass #" << conn.pass->m_passID
                                    << ", attachment #" << conn.attachment
                                    << ", connection type: {";
                std::cerr << "usage: " << (conn.type.attachment ? "Input attachment" : "Sampled image")
                          << ", simple (noconv): " << (conn.type.simple ? "true" : "false")
                          << ", type: " << (conn.type.toInput ? "To input" : "To output") << "}\n";
            }
        }
    }
}

void FrameGraph::RenderPass::createIncomingConnection(RenderPass* otherPass,
                                                      U32 otherAttachment,
                                                      U32 thisAttachment,
                                                      bool isInput)
{
    if (isInput)
    {
        if (thisAttachment >= m_inputs.size())
        {
            throw std::domain_error("Attachment ID too large.");
        }
        m_inputs[thisAttachment].connectedPass = otherPass;
        m_inputs[thisAttachment].connectedAttachment = otherAttachment;
    }
    else
    {
        if (thisAttachment >= m_outputs.size())
        {
            throw std::domain_error("Output attachment ID too large.");
        }
        m_outputs[thisAttachment].sourcePass = otherPass;
        m_outputs[thisAttachment].sourceAttachment = otherAttachment;
    }
    if (!subpassCompatible(this, thisAttachment))
    {
        m_subpassCompatibleInputs = false;
    }
}

void FrameGraph::RenderPass::createOutgoingConnection(RenderPass* otherPass,
                                                      U32 otherAttachment,
                                                      U32 thisAttachment,
                                                      bool isToInput)
{
    if (thisAttachment >= m_outputs.size())
    {
        throw std::domain_error("Attachment ID too large.");
    }
    ConnectionType ct;
    ct.attachment = isToInput ? otherPass->m_inputs[otherAttachment].attachment
                              : otherPass->m_outputs[otherAttachment].sourceAttachment;
    ct.simple = m_layout == otherPass->m_layout; /// TODO: Also make note of whether a format conversion needs to take place.
    ct.toInput = isToInput;

    if (!subpassCompatible(otherPass, otherAttachment))
    {
        m_subpassCompatibleInputs = false;
    }

    m_outputs[thisAttachment].connections.push_back({otherPass, otherAttachment, ct});
}

FrameGraph::PassAttachmentReference
FrameGraph::RenderPass::operator[](std::string_view name)
{
    PassAttachmentReference ref{m_passID, 0xffffffff, 0xffffffff};
    for (U32 i = 0; i < m_inputs.size(); ++i)
    {
        if (m_inputs[i].name == name) ref.inputID = i;
    }
    for (U32 i = 0; i < m_outputs.size(); ++i)
    {
        if (m_outputs[i].name == name) ref.outputID = i;
    }
    return ref;
}

FrameGraph::RenderPass& FrameGraph::addPass(std::string_view name,
                                            PassLayout layout,
                                            VkPipelineStageFlags stages)
{
    auto passID = m_currPassID++;
    if (!m_currPassID)
    {
        throw std::logic_error("Overflow in # of passes.");
    }

    auto result = m_renderPasses.emplace(name, RenderPass(passID, layout, stages));

    m_passReferences.emplace_back(result.first);
    return (*m_passReferences.back())->second;
}

FrameGraph::RenderPass* FrameGraph::pass(const std::string& name)
{
    auto it = m_renderPasses.find(name);
    if (it != m_renderPasses.end())
    {
        return &(it->second);
    }
    return nullptr;
}

VkImageLayout FrameGraph::RenderPass::getFinalLayout(const OutputAttachmentInfo& attach)
{
    bool isUsedAsImage = false;
    bool isUsedAsAttachment = false;
    for (const auto& connection : attach.connections)
    {
        bool attachment = connection.pass->m_inputs[connection.attachment].attachment;
        if (attachment) isUsedAsAttachment = true;
        else isUsedAsImage = true;
    }

    if (isUsedAsAttachment && isUsedAsImage)
    {
        throw std::logic_error("UNIMPLEMENTED: Output attachment used both as "
                               "input attachment and sampled image in same "
                               "render pass.");
    }
    if (isUsedAsImage)
    {
        return VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    }
    else
    {
        if (hasDepthComponent(attach.format) || hasStencilComponent(attach.format))
        {
            return VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
        }
        else
        {
            return VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        }
    }
}

VkAttachmentDescription* FrameGraph::RenderPass::updateAttachmentInfo(U32& attachmentCount)
{
    // Attachment descriptions determine what happens with the attachments
    // before and after this render pass.

    m_attachDescCache.resize(m_inputs.size()+m_outputs.size());
    for (U32 i = 0; i < m_inputs.size(); ++i)
    {
        bool hasIncomingConnection = m_inputs[i].connectedPass != nullptr;
        bool isExternal = m_inputs[i].externalInfo.view != VK_NULL_HANDLE;

        bool isLast = m_inputs[i].imgInfo.next.pass == nullptr;

        bool stencil = hasStencilComponent(m_inputs[i].format);

        auto& attachDesc = m_attachDescCache[i];
        attachDesc.flags = 0;
        attachDesc.format = m_inputs[i].format;
        attachDesc.samples = hasIncomingConnection ? m_inputs[i].connectedPass->m_layout.samples : m_layout.samples;
        attachDesc.loadOp = hasIncomingConnection || isExternal ? VK_ATTACHMENT_LOAD_OP_LOAD : VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachDesc.storeOp = !isLast ? VK_ATTACHMENT_STORE_OP_STORE : VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachDesc.stencilLoadOp = stencil ? VK_ATTACHMENT_LOAD_OP_LOAD : VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachDesc.stencilStoreOp = stencil && !isLast ? VK_ATTACHMENT_STORE_OP_STORE : VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachDesc.initialLayout = m_inputs[i].imgInfo.initialLayout;
        attachDesc.finalLayout = m_inputs[i].imgInfo.finalLayout;
    }
    for (U32 i = 0; i < m_outputs.size(); ++i)
    {
        VkAttachmentLoadOp loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        if (m_outputs[i].loadOrClear)
        {
            loadOp = m_outputs[i].sourcePass || m_outputs[i].isClearedExternally
                     ? VK_ATTACHMENT_LOAD_OP_LOAD
                     : VK_ATTACHMENT_LOAD_OP_CLEAR;
        }

        bool stencil = hasStencilComponent(m_outputs[i].format);
        auto& attachDesc = m_attachDescCache[i+m_inputs.size()];
        attachDesc.flags = 0;
        attachDesc.format = m_outputs[i].format;
        attachDesc.samples = m_layout.samples;
        attachDesc.loadOp = loadOp;
        attachDesc.storeOp = m_outputs[i].hasExternalUsage || !m_outputs[i].connections.empty()
                             ? VK_ATTACHMENT_STORE_OP_STORE
                             : VK_ATTACHMENT_STORE_OP_DONT_CARE; // Only store if there are outputs or it is externally used.
        attachDesc.stencilLoadOp = stencil ? loadOp : VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachDesc.stencilStoreOp = attachDesc.storeOp;
        attachDesc.initialLayout = m_outputs[i].imgInfo.initialLayout;
        attachDesc.finalLayout = m_outputs[i].imgInfo.finalLayout;
    }
    attachmentCount = static_cast<U32>(m_attachDescCache.size());
    return m_attachDescCache.data();
}

VkResult FrameGraph::create(VkDevice device, VmaAllocator allocator)
{
    if (m_device != VK_NULL_HANDLE) destroy();
    m_device = device;
    m_allocator = allocator;

    for (auto& size : m_descAllocator.poolSizes)
    {
        if (size.type == VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT)
        {
            size.descriptorCount = 256;
        }
        else if (size.type == VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE)
        {
            size.descriptorCount = 128;
        }
        else
        {
            size.descriptorCount = 1;
        }
    }
    m_descAllocator.construct(device);
    return VK_SUCCESS;
}

void FrameGraph::destroy()
{
    if (m_device == VK_NULL_HANDLE) return;
    for (auto framebuf : m_swapchainFramebuffers)
    {
        vkDestroyFramebuffer(m_device, framebuf, nullptr);
    }
    for (auto& image : m_images)
    {
        image.destroy(m_allocator, m_device, true);
    }
    for (auto& pass : m_renderPasses)
    {
        auto passInfo = pass.second.vulkanPass();
        if (passInfo.pass != VK_NULL_HANDLE)
        {
            vkDestroyRenderPass(m_device, passInfo.pass, nullptr);
        }
        if (passInfo.framebuffer != VK_NULL_HANDLE)
        {
            vkDestroyFramebuffer(m_device, passInfo.framebuffer, nullptr);
        }
    }
    m_descAllocator.destroy();
    resetVariables();
}

void FrameGraph::ensurePassReferenceValid(PassAttachmentReference ref) const
{
    if (ref.passID == 0xffffffff)
    {
        throw std::logic_error("Invalid pass id.");
    }
    if (ref.passID >= m_passReferences.size()
        || !m_passReferences[ref.passID].has_value())
    {
        throw std::logic_error("Pass ID referring to non-existent pass; "
                               "possible corruption/use-after-free.");
    }
}

void FrameGraph::ensureInputReferenceValid(PassAttachmentReference ref) const
{
    if (ref.inputID == 0xffffffff)
    {
        throw std::logic_error("Invalid input attachment reference.");
    }
    ensurePassReferenceValid(ref);
    const auto& pass = (*m_passReferences[ref.passID])->second;

    if (ref.inputID >= pass.m_inputs.size())
    {
        throw std::runtime_error("Unexpected invalid input attachment "
                                 "reference.");
    }
}

void FrameGraph::ensureOutputReferenceValid(PassAttachmentReference ref) const
{
    if (ref.outputID == 0xffffffff)
    {
        throw std::logic_error("Invalid output attachment reference.");
    }
    ensurePassReferenceValid(ref);
    const auto& pass = (*m_passReferences[ref.passID])->second;

    if (ref.outputID >= pass.m_outputs.size())
    {
        throw std::runtime_error("Unexpected invalid output attachment "
                                 "reference.");
    }
}

void FrameGraph::ensureExactlyOnePassReferenceValid(PassAttachmentReference ref) const
{
    if (ref.inputID == 0xffffffff)
    {
        ensureOutputReferenceValid(ref);
        return;
    }
    if (ref.outputID == 0xffffffff)
    {
        ensureInputReferenceValid(ref);
        return;
    }
    throw std::runtime_error("It does not hold that exactly one pass reference "
                             "is valid.");
}

void FrameGraph::connect(PassAttachmentReference src,
                         PassAttachmentReference dst)
{
    if (dst.inputID != 0xffffffff && dst.outputID != 0xffffffff)
    {
        throw std::logic_error("Ambiguous connection reference (exists both as "
                               "input and output).");
    }
    if (dst.inputID != 0xffffffff)
    {
        connectToInput(src, dst);
    }
    else
    {
        connectToOutput(src, dst);
    }
}


void FrameGraph::connectToInput(PassAttachmentReference src,
                                PassAttachmentReference dst)
{
    ensureOutputReferenceValid(src);
    ensureInputReferenceValid(dst);

    auto& srcPass = (*m_passReferences[src.passID])->second;
    auto& dstPass = (*m_passReferences[dst.passID])->second;

    dstPass.createIncomingConnection(&srcPass, src.outputID, dst.inputID, true);
    srcPass.createOutgoingConnection(&dstPass, dst.inputID, src.outputID, true);
}

void FrameGraph::connectToOutput(PassAttachmentReference src,
                                 PassAttachmentReference dst)
{
    ensureOutputReferenceValid(src);
    ensureOutputReferenceValid(dst);

    auto& srcPass = (*m_passReferences[src.passID])->second;
    auto& dstPass = (*m_passReferences[dst.passID])->second;

    dstPass.createIncomingConnection(&srcPass, src.outputID, dst.outputID, false);
    srcPass.createOutgoingConnection(&dstPass, dst.outputID, src.outputID, false);
}

FrameGraph::AttachmentInfo
FrameGraph::getAttachmentInfo(const RenderPass::AttachmentImageInfo* attachRef) const
{
    AttachmentInfo info;

    info.image = attachRef->image;
    info.view = attachRef->view;
    info.usageFlags = attachRef->usage;

    while (attachRef->next.pass)
    {
        if (attachRef->next.isInput)
        {
            attachRef = &attachRef->next.pass->m_inputs[attachRef->next.attachmentID].imgInfo;
        }
        else
        {
            attachRef = &attachRef->next.pass->m_outputs[attachRef->next.attachmentID].imgInfo;
        }
    }
    info.finalLayout = attachRef->finalLayout;
    return info;
}

FrameGraph::AttachmentInfo
FrameGraph::inputAttachmentInfo(PassAttachmentReference inputRef) const
{
    ensureInputReferenceValid(inputRef);
    auto& pass = (*m_passReferences[inputRef.passID])->second;
    return getAttachmentInfo(&pass.m_inputs[inputRef.inputID].imgInfo);
}

FrameGraph::AttachmentInfo
FrameGraph::outputAttachmentInfo(PassAttachmentReference outputRef) const
{
    ensureOutputReferenceValid(outputRef);
    auto& pass = (*m_passReferences[outputRef.passID])->second;
    return getAttachmentInfo(&pass.m_outputs[outputRef.outputID].imgInfo);
}

void FrameGraph::markOutputUsed(PassAttachmentReference output,
                                VkImageLayout desiredFinalLayout,
                                VkImageUsageFlags extraUsageFlags)
{
    ensureOutputReferenceValid(output);
    auto& pass = (*m_passReferences[output.passID])->second;
    pass.m_outputs[output.outputID].extraCreationFlags |= extraUsageFlags;
    pass.m_outputs[output.outputID].hasExternalUsage = true;
    pass.m_outputs[output.outputID].imgInfo.finalLayout = desiredFinalLayout;
}

void FrameGraph::markExternal(PassAttachmentReference ref,
                              ExternalImageInfo info)
{
    ensureExactlyOnePassReferenceValid(ref);
    auto& pass = (*m_passReferences[ref.passID])->second;
    if (ref.outputID == 0xffffffff)
    {
        pass.m_inputs[ref.inputID].externalInfo = info;
    }
    else
    {
        pass.m_outputs[ref.inputID].externalInfo = info;
    }
    return;
}

void FrameGraph::resetVisited()
{
    m_visited.resize(maxPassID());
    for (U32 i = 0; i < m_visited.size(); ++i)
    {
        m_visited[i] = false;
    }
}

bool FrameGraph::RenderPass::subpassCompatible(const RenderPass* dstPass, U32 inputID)
{
    return false;
    // Todo: Refactor.
    if (!dstPass) return false;
    const auto& dstInfo = dstPass->m_inputs.at(inputID);
    const RenderPass* srcPass = dstInfo.connectedPass;
    if (!srcPass) return false;
    const auto& srcInfo = srcPass->m_outputs[dstInfo.connectedAttachment];

    if (!dstInfo.attachment) return false;
    if (dstInfo.format != srcInfo.format) return false;
    if (dstPass->m_layout != srcPass->m_layout) return false;
    return true;
}

/// TODO: Sort output connections for every output attachment, and ensure that
/// there is an implicit dependency between all output-to-inputs and output-to-
/// output.
void FrameGraph::scheduleConnectedPasses(U32 startAt)
{
    if (m_passNumbers.size() < maxPassID())
    {
        m_passNumbers.resize(maxPassID(), invalidID);
    }

    struct VisitInfo
    {
        U32 id;
        bool sourcesVisited;
    };

    std::stack<VisitInfo> unprocessed;
    unprocessed.push({startAt, false});


    while (!unprocessed.empty())
    {
        auto top = unprocessed.top();

        if (m_visited[top.id])
        {
            unprocessed.pop();
            continue;
        }

        const RenderPass* currPass = &(*m_passReferences[top.id])->second;
        if (!top.sourcesVisited)
        {
            unprocessed.top().sourcesVisited = true;

            for (const auto& input : currPass->m_inputs)
            {
                const auto* ancestralPass = input.connectedPass;
                if (!ancestralPass)
                {
                    // It is valid not to be connected when inputs are external.
                    if (input.externalInfo.view != VK_NULL_HANDLE) continue;
                    throw std::logic_error("Unconnected input in pass.");
                }
                auto ancestorID = ancestralPass->m_passID;
                if (!m_visited[ancestorID])
                {
                    unprocessed.push({ancestorID, false});
                }
            }
            // We can have output->output connections; handle those.
            for (const auto& output : currPass->m_outputs)
            {
                const auto* ancestralPass = output.sourcePass;
                if (ancestralPass && !m_visited[ancestralPass->m_passID])
                {
                    unprocessed.push({ancestralPass->m_passID, false});
                }
            }
        }
        else
        {
            unprocessed.pop();
            m_visited[top.id] = true;
            m_passNumbers[top.id] = static_cast<U32>(m_order.size());
            m_order.push_back(top.id);
            for (const auto& output : currPass->m_outputs)
            {
                for (const auto& connOut : output.connections)
                {
                    const auto* childPass = connOut.pass;
                    auto childID = childPass->m_passID;
                    if (!m_visited[childID])
                    {
                        unprocessed.push({childID, false});
                    }
                }
            }
        }
    }
}

FrameGraph::RenderPass&
FrameGraph::RenderPass::addOutput(std::string name, VkFormat format,
                                  std::optional<VkClearValue> cv,
                                  VkImageUsageFlags extraUsageFlags)
{
    bool shouldClear = cv.has_value();
    if (!shouldClear)
    {
        cv = VkClearValue{};
        cv.value().color.uint32[0] = 0;
        cv.value().color.uint32[1] = 0;
        cv.value().color.uint32[2] = 0;
        cv.value().color.uint32[3] = 0;
    }
    m_outputs.push_back({name, format, {}, {}, nullptr, 0xffffffff, cv.value(), shouldClear});
    auto layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    if (hasDepthComponent(format) || hasStencilComponent(format))
    {
        layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    }
    m_outputs.back().imgInfo.usedLayout = layout;
    m_outputs.back().imgInfo.usage = extraUsageFlags;
    return *this;
}

FrameGraph::RenderPass&
FrameGraph::RenderPass::addOutput(std::string name, const Swapchain& swapchain,
                                  std::optional<VkClearValue> cv)
{
    if (m_swapchainImageID != 0xffffffff)
    {
        throw std::logic_error("RenderPass already contains a swapchain image.");
    }
    if (m_layout.width != swapchain.extent().width
        || m_layout.height != swapchain.extent().height)
    {
        throw std::runtime_error("Swapchain layout does not match pass layout.");
    }

    bool shouldClear = cv.has_value();
    if (!shouldClear)
    {
        cv = VkClearValue{};
        cv.value().color.uint32[0] = 0;
        cv.value().color.uint32[1] = 0;
        cv.value().color.uint32[2] = 0;
        cv.value().color.uint32[3] = 0;
    }

    m_swapchainImageID = m_outputs.size();
    m_outputs.push_back({name, swapchain.format().format, {}, {}, nullptr, 0, cv.value(), shouldClear});
    m_outputs.back().imgInfo.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    m_outputs.back().imgInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    m_outputs.back().imgInfo.usedLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    return *this;
}

VkImageUsageFlags
FrameGraph::RenderPass::getRequiredImageUsageFlags(const OutputAttachmentInfo& output)
{
    if (output.imgInfo.view != VK_NULL_HANDLE)
    {
        throw std::logic_error("Bug: Getting usage flags for already allocated "
                               "framebuffer attachment.");
    }

    VkImageUsageFlags usageFlags{0};

    usageFlags |= output.extraCreationFlags;

    // If we have a scaled output-to-output situation, then initialLayout is
    // set to VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL in order to signal this.
    if (output.imgInfo.initialLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
    {
        usageFlags |= VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    }
    if (output.imgInfo.usedLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL)
    {
        usageFlags |= VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    }
    else if (output.imgInfo.usedLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
    {
        usageFlags |= VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
    }
    else
    {
        throw std::logic_error("BUG: Invalid .usedLayout value in output attachment.");
    }

    for (const auto& outgoing : output.connections)
    {
        if (!outgoing.type.simple)
        {
            usageFlags |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
        }

        if (!outgoing.type.toInput)
        {
            usageFlags |= getRequiredImageUsageFlags(outgoing.pass->m_outputs[outgoing.attachment]);
        }
        else
        {
            if (outgoing.type.attachment)
            {
                usageFlags |= VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT;
            }
            else
            {
                usageFlags |= VK_IMAGE_USAGE_SAMPLED_BIT;
            }
        }
    }
    return usageFlags;
}

U32 FrameGraph::getLastUsage(const RenderPass::OutputAttachmentInfo& output, U32 outputPassID) const
{
    if (output.hasExternalUsage) return 0xfffffffe;
    U32 lastPass = m_inverseOrder[outputPassID];
    for (const auto& outgoing : output.connections)
    {
        U32 last = lastPass;
        if (!outgoing.type.toInput)
        {
            const auto& nextOutput = outgoing.pass->m_outputs[outgoing.attachment];
            last = getLastUsage(nextOutput, outgoing.pass->m_passID);
        }
        if (outgoing.type.simple)
        {
            last = m_inverseOrder[outgoing.pass->m_passID];
        }
        lastPass = std::max(lastPass, last);
    }
    return lastPass;
}

FrameGraph::RenderPass::AttachmentImageInfo*
FrameGraph::RenderPass::getImageInfo(FrameGraph::RenderPass::AttachmentReference ref)
{
    if (!ref.pass) return nullptr;
    if (ref.isInput)
    {
        return &ref.pass->m_inputs[ref.attachmentID].imgInfo;
    }
    else
    {
        return &ref.pass->m_outputs[ref.attachmentID].imgInfo;
    }
}

void FrameGraph::assignImagesToPasses(bool printDebugInfo)
{
    U32 swapchainCount = 0;
    for (auto passID : m_order)
    {
        RenderPass* pass = &(*m_passReferences[passID])->second;
        if (pass->m_swapchainImageID != 0xffffffff)
        {
            ++swapchainCount;
            m_swapchainPassID = passID;
        }

        if (swapchainCount > 1)
        {
            throw std::logic_error("FrameGraph contains several different "
                                   "swap chain outputs.");
        }

        for (auto& output : pass->m_outputs)
        {
            using OConn = RenderPass::OutputAttachmentInfo::OutgoingConnection;
            // Sort in order to ensure that we can place the appropriate layout
            // transitions.
            std::sort(output.connections.begin(),
                      output.connections.end(),
                      [this](const OConn& a, const OConn& b)
                      {
                          return m_inverseOrder[a.pass->m_passID] < m_inverseOrder[b.pass->m_passID];
                      });

            const OConn* outputToOutput = nullptr;
            for (const auto& outgoing : output.connections)
            {
                if (outputToOutput)
                {
                    throw std::runtime_error("Multiple connections from output "
                                             "to outputs detected, or output-"
                                             "to-input after output-to-output, "
                                             "which is invalid.");
                }
                if (!outgoing.type.toInput)
                {
                    outputToOutput = &outgoing;
                }
            }

            bool needsImage = false;
            if (!output.sourcePass ||
                output.imgInfo.initialLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
            {
                needsImage = true;
            }

            if (output.externalInfo.view != VK_NULL_HANDLE
                || output.imgInfo.finalLayout == VK_IMAGE_LAYOUT_PRESENT_SRC_KHR)
            {
                needsImage = false;
            }

            if (output.sourcePass)
            {
                const auto& srcRef = output.sourcePass->m_outputs[output.sourceAttachment];
                output.imgInfo.view = srcRef.imgInfo.view;
                output.imgInfo.image = srcRef.imgInfo.image;
                output.imgInfo.usage = srcRef.imgInfo.usage;
                output.imgInfo.lastUsage = srcRef.imgInfo.lastUsage;
                output.imgInfo.initialLayout = srcRef.imgInfo.finalLayout;
            }

            if (needsImage)
            {
                if (output.imgInfo.view)
                {
                    throw std::logic_error("BUG: Output attachment needs image,"
                                           " but already has one!");
                }
                auto usageFlags = RenderPass::getRequiredImageUsageFlags(output);
                usageFlags |= output.imgInfo.usage;
                auto lastPass = getLastUsage(output, passID);
                const auto& image = getFramebufferAttachmentImage(pass->m_layout,
                                                                  output.format,
                                                                  usageFlags,
                                                                  m_inverseOrder[passID],
                                                                  lastPass);
                output.imgInfo.view = image.view;
                output.imgInfo.image = image.image;
                output.imgInfo.usage = usageFlags;
                output.imgInfo.lastUsage = lastPass;
            }

            // We now need only to determine the final layout of the image,
            // and set the layout of any descendent to the appropriate one.

            bool needsTransfer = false;

            RenderPass::AttachmentReference lastRef;
            RenderPass::AttachmentReference* lastNextRef = &output.imgInfo.next;
            lastRef.isInput = false;
            lastRef.pass = pass;
            lastRef.attachmentID = std::distance(pass->m_outputs.data(), &output);

            for (auto& outgoing : output.connections)
            {
                auto* outPass = outgoing.pass;
                RenderPass::AttachmentImageInfo* currRef = nullptr;
                if (outgoing.type.toInput)
                {
                    currRef = &outPass->m_inputs[outgoing.attachment].imgInfo;
                }
                else
                {
                    currRef = &outPass->m_outputs[outgoing.attachment].imgInfo;
                }

                // If the type is non-simple then we handle everything when we
                // create the input attachment. Thus, this doesn't affect the
                // chain of changes for this output image.
                if (!outgoing.type.simple)
                {
                    needsTransfer = true;
                    currRef->initialLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
                }
                else
                {
                    currRef->initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

                    RenderPass::AttachmentReference ref;
                    ref.isInput = outgoing.type.toInput;
                    ref.attachmentID = outgoing.attachment;
                    ref.pass = outgoing.pass;
                    *lastNextRef = ref;

                    currRef->previous = lastRef;
                    lastNextRef = &currRef->next;
                    lastRef = ref;
                }
            }

            if (needsTransfer && output.imgInfo.finalLayout == VK_IMAGE_LAYOUT_PRESENT_SRC_KHR)
            {
                throw std::logic_error("FrameGraph bug: Swapchain image appears to need transfer.");
            }

            if (needsTransfer)
            {
                output.imgInfo.finalLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
                if (output.imgInfo.next.pass)
                {
                    RenderPass::AttachmentImageInfo* nextRef = nullptr;
                    if (output.imgInfo.next.isInput)
                    {
                        nextRef = &output.imgInfo.next.pass->m_inputs[output.imgInfo.next.attachmentID].imgInfo;
                    }
                    else
                    {
                        nextRef = &output.imgInfo.next.pass->m_outputs[output.imgInfo.next.attachmentID].imgInfo;
                    }
                    if (nextRef->initialLayout != VK_IMAGE_LAYOUT_UNDEFINED)
                    {
                        throw std::logic_error("Expected undefined image layout"
                                               ", but actual layout differs.");
                    }
                    nextRef->initialLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
                }
            }
            else
            {
                auto nextInfo = output.imgInfo.next;
                if (nextInfo.pass)
                {
                    output.imgInfo.finalLayout = RenderPass::getImageInfo(nextInfo)->usedLayout;
                }
                else if (output.hasExternalUsage)
                {
                    if (printDebugInfo) std::cerr << "Debug: Has external usage -- should something be done about that?\n";
                }
                else if (output.imgInfo.finalLayout != VK_IMAGE_LAYOUT_PRESENT_SRC_KHR)
                {
                    std::cerr << "IMPLEMENTATION (DE)BUG: Unused pass output.\n";
                    if (output.imgInfo.finalLayout != VK_IMAGE_LAYOUT_UNDEFINED)
                    {
                        throw std::logic_error("FrameGraph bug: Semi-"
                                               "initialised unattached output.");
                    }
                    output.imgInfo.finalLayout = output.imgInfo.usedLayout;
                }
            }
        }
        for (auto& input : pass->m_inputs)
        {
            if (!input.connectedPass)
            {
                if (input.externalInfo.view == VK_NULL_HANDLE)
                {
                    throw std::logic_error("Unconnected input attachment with "
                                           "no external image information.");
                }
                input.imgInfo.initialLayout = input.externalInfo.initialLayout;
                input.imgInfo.finalLayout = input.externalInfo.finalLayout;
                input.imgInfo.image = input.externalInfo.image;
                input.imgInfo.view = input.externalInfo.view;
            }
            else
            {
                auto& output = input.connectedPass->m_outputs[input.connectedAttachment];

                RenderPass::OutputAttachmentInfo::OutgoingConnection* outgoing = nullptr;

                for (auto& info : output.connections)
                {
                    if (info.pass->m_passID == passID &&
                        info.attachment == std::distance(pass->m_inputs.data(), &input))
                    {
                        outgoing = &info;
                        break;
                    }
                }
                if (!outgoing)
                {
                    throw std::logic_error("Corruption: Input attachment does not exist as outgoing.");
                }

                bool requiresNewImage = !outgoing->type.simple;
                if (requiresNewImage)
                {
                    const auto& sourceAttachment = input.connectedPass->m_outputs[input.connectedAttachment];

                    if (!(sourceAttachment.imgInfo.usage & VK_IMAGE_USAGE_TRANSFER_SRC_BIT))
                    {
                        throw std::logic_error("Bug: Image needing duplication was "
                                               "not created with appropriate TRANSFER_SRC_BIT.");
                    }
                    VkImageUsageFlags usageFlags = input.imgInfo.usage;
                    if (input.attachment) usageFlags |= VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT;
                    else usageFlags |= VK_IMAGE_USAGE_SAMPLED_BIT;
                    usageFlags |= VK_IMAGE_USAGE_TRANSFER_DST_BIT;

                    const auto& image = getFramebufferAttachmentImage(pass->m_layout,
                                                                      input.format,
                                                                      usageFlags,
                                                                      m_inverseOrder[input.connectedPass->m_passID],
                                                                      m_inverseOrder[passID]);
                    input.imgInfo.view = image.view;
                    input.imgInfo.image = image.image;
                    input.imgInfo.usage = usageFlags;
                }
                else
                {
                    input.imgInfo.view = input.connectedPass->m_outputs[input.connectedAttachment].imgInfo.view;
                }
            }
            VkImageLayout expectedLayout = VK_IMAGE_LAYOUT_UNDEFINED;

            bool depthStencil = hasDepthComponent(input.format) || hasStencilComponent(input.format);

            if (depthStencil)
            {
                expectedLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
            }
            else
            {
                expectedLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            }

            if (input.imgInfo.initialLayout != VK_IMAGE_LAYOUT_UNDEFINED
                && input.externalInfo.view == VK_NULL_HANDLE)
            {
                if (input.imgInfo.initialLayout != VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
                    && input.imgInfo.initialLayout != VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
                {
                    throw std::logic_error("Input layout already set.");
                }
                // We get here if we have transfer src/dst layouts or if the
                // input is external.
            }
            else
            {
                input.imgInfo.initialLayout = expectedLayout;
            }
            if (input.imgInfo.usedLayout != expectedLayout)
            {
                throw std::logic_error("Actual used layout is not what was expected...");
            }
            if (input.imgInfo.next.pass)
            {
                RenderPass::AttachmentImageInfo* nextRef = nullptr;
                if (input.imgInfo.next.isInput)
                {
                    nextRef = &input.imgInfo.next.pass->m_inputs[input.imgInfo.next.attachmentID].imgInfo;
                }
                else
                {
                    nextRef = &input.imgInfo.next.pass->m_outputs[input.imgInfo.next.attachmentID].imgInfo;
                }
                input.imgInfo.finalLayout = nextRef->usedLayout;
            }
            // We may already have set another layout as the desired output
            // layout.
            else if (input.imgInfo.finalLayout == VK_IMAGE_LAYOUT_UNDEFINED)
            {
                input.imgInfo.finalLayout = input.imgInfo.usedLayout;
            }
        }
    }
}

/// What this function needs to do:
/// * Order passes (and merge to subpasses if possible).
/// * Assign framebuffers for each input/output attachment.
///   | Create these if necessary.
/// * Construct the actual Vulkan objects (VkRenderpass, etc.)
/// * Determine which commands (synchronisation, vkCmdNextSubpass, etc.) needs
///   to be run at which time.
void FrameGraph::compile(const VkImageView* swapchainViews,
                         U32 swapchainViewCount,
                         bool printDebugInfo)
{
    if (m_device == VK_NULL_HANDLE || m_allocator == VK_NULL_HANDLE)
    {
        throw std::logic_error("No device or allocator in FrameGraph.");
    }
    resetVisited();
    m_order.clear();

    for (U32 i = 0; i < maxPassID(); ++i)
    {
        if (!m_passReferences[i].has_value() || m_visited[i]) continue;
        scheduleConnectedPasses(i);
    }
    m_inverseOrder.clear();
    m_inverseOrder.resize(maxPassID(), 0xffffffff);
    if (printDebugInfo) std::cout << "Passes:";
    for (U32 i = 0; i < m_order.size(); ++i)
    {
        auto id = m_order[i];
        m_inverseOrder[id] = i;
        if (printDebugInfo) std::cout << " " << (*m_passReferences[id])->first << "(" << id << ")";
    }
    if (printDebugInfo) std::cout << "\n";


    if (printDebugInfo) std::cout << "Assigning images to passes ..." << "\n";
    assignImagesToPasses(printDebugInfo);

    if (printDebugInfo)
    {
        for (auto passID : m_order)
        {
            auto name = (*m_passReferences[passID])->first;
            RenderPass* pass = &(*m_passReferences[passID])->second;
            std::cout << "Render pass '" << name << "' info: ";
            pass->dump();
        }
    }

    /// We now need to get a list of all attachment layouts used in every render
    /// pass. Then, we need to split that into a list as follows:
    /// [inputs, colourOutputs, depthStencilOutput]

    if (printDebugInfo) std::cout << "Creating actual render passes ..." << "\n";
    // For now, we have only a single subpass per pass. This greatly simplifies
    // everything.

    for (auto passID : m_order)
    {
        RenderPass* pass = &(*m_passReferences[passID])->second;
        auto rpdesc = getScheduledPassDescription(passID);
        createVulkanRenderPass(passID, rpdesc);
        if (passID == m_swapchainPassID)
        {
            // It already has this value, but this is to emphasise that fact.
            pass->m_renderPassInfo.framebuffer = VK_NULL_HANDLE;
            if (!swapchainViews || !swapchainViewCount)
            {
                throw std::runtime_error("No swap chain views given, however a "
                                         "pass contains a swap chain output.");
            }
            for (U32 i = 0; i < swapchainViewCount; ++i)
            {
                auto framebuf = createFramebuffer(passID, rpdesc, swapchainViews[i]);
                m_swapchainFramebuffers.push_back(framebuf);
            }
        }
        else
        {
            pass->m_renderPassInfo.framebuffer = createFramebuffer(passID, rpdesc);
        }
    }

    // Everything has been set up; now we can find the descriptors. This can
    // probably be merged into the above, but I was too lazy to check.
    for (auto passID : m_order)
    {
        RenderPass* pass = &(*m_passReferences[passID])->second;
        auto& descInfo = pass->m_descInfo;
        descInfo.attachmentInfo.clear();
        descInfo.sampledInfo.clear();
        descInfo.descWrites.clear();

        U32 attachCount = 0;
        U32 sampledCount = 0;

        for (const auto& input : pass->m_inputs)
        {
            VkDescriptorImageInfo imageInfo;
            imageInfo.imageLayout = input.imgInfo.usedLayout;
            imageInfo.imageView = input.imgInfo.view;
            imageInfo.sampler = input.sampler;
            if (input.attachment)
            {
                ++attachCount;
                descInfo.attachmentInfo.push_back(imageInfo);
            }
            else
            {
                ++sampledCount;
                descInfo.sampledInfo.push_back(imageInfo);
            }
        }

        if (attachCount + sampledCount == 0) continue; // No descriptors to write.

        U32 attachIndex = 0;
        U32 sampledIndex = 0;

        VkWriteDescriptorSet descWrite{};
        descWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descWrite.descriptorCount = 1;

        for (const auto& input : pass->m_inputs)
        {
            U32 index = attachIndex + sampledIndex;
            descWrite.dstBinding = index;

            if (input.attachment)
            {
                descWrite.pImageInfo = &descInfo.attachmentInfo[attachIndex++];
                descWrite.descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
            }
            else
            {
                descWrite.pImageInfo = &descInfo.sampledInfo[sampledIndex++];
                descWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
            }
            descInfo.descWrites.push_back(descWrite);
        }
    }
}

FrameGraph::ScheduledRenderPassDescription
FrameGraph::getScheduledPassDescription(U32 passID) const
{
    RenderPass* pass = &(*m_passReferences[passID])->second;

    ScheduledRenderPassDescription passDesc;
    passDesc.subpassStorage.resize(1);

    passDesc.dependencies.resize(2);

    auto& dependencyIn = passDesc.dependencies[0];
    auto& dependencyOut = passDesc.dependencies[1];

    dependencyIn = {};
    dependencyIn.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;
    dependencyIn.srcSubpass = VK_SUBPASS_EXTERNAL;
    dependencyIn.dstSubpass = 0;

    dependencyOut = {};
    dependencyOut.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;
    dependencyOut.srcSubpass = 0;
    dependencyOut.dstSubpass = VK_SUBPASS_EXTERNAL;


    U32 attachmentCount;
    auto* attachments = pass->updateAttachmentInfo(attachmentCount);
    passDesc.attachmentDescriptions.reserve(attachmentCount);
    for (U32 i = 0; i < attachmentCount; ++i)
    {
        passDesc.attachmentDescriptions.push_back(attachments[i]);
    }

    auto& subpassInfo = passDesc.subpassStorage.back();

    auto& depthStencil = subpassInfo.depthStencil;
    depthStencil.attachment = VK_ATTACHMENT_UNUSED;
    depthStencil.layout = VK_IMAGE_LAYOUT_UNDEFINED;

    auto& inputRefs = subpassInfo.inputRefs;
    auto& colourRefs = subpassInfo.colourRefs;
    auto& imageViews = passDesc.imageViews;

    for (U32 i = 0; i < attachmentCount; ++i)
    {
        const auto currAttach = passDesc.attachmentDescriptions[i];

        bool isInput = i < pass->m_inputs.size();
        bool isDepthStencil = hasDepthComponent(currAttach.format) || hasStencilComponent(currAttach.format);
        auto outputI = i - pass->m_inputs.size();

        RenderPass::AttachmentImageInfo* imgInfo = nullptr;

        if (isInput)
        {
            VkAttachmentReference ref;
            ref.attachment = i;
            ref.layout = pass->m_inputs[i].imgInfo.usedLayout;
            if (pass->m_inputs[i].attachment) inputRefs.push_back(ref);
            imgInfo = &pass->m_inputs[i].imgInfo;

            if (!pass->m_inputs[i].attachment)
            {
                dependencyIn.dependencyFlags = dependencyIn.dependencyFlags & ~VK_DEPENDENCY_BY_REGION_BIT;
            }

            if (imgInfo->initialLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
            {
                dependencyIn.srcStageMask |= VK_PIPELINE_STAGE_TRANSFER_BIT;
                dependencyIn.srcAccessMask |= VK_ACCESS_TRANSFER_WRITE_BIT;
            }
            if (imgInfo->initialLayout == VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
            {
                dependencyIn.srcStageMask |= VK_PIPELINE_STAGE_TRANSFER_BIT;
                dependencyIn.srcAccessMask |= VK_ACCESS_TRANSFER_READ_BIT;
            }

            if (isDepthStencil)
            {
                dependencyIn.srcStageMask |= VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT; // Wait for depth-stencil stuff.
                dependencyIn.srcAccessMask |= VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

                //dependencyIn.dstAccessMask |= VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT;
                dependencyIn.dstAccessMask |= VK_ACCESS_SHADER_READ_BIT;
            }
            else
            {
                dependencyIn.srcStageMask |= VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
                dependencyIn.srcAccessMask |= VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
                if (pass->m_inputs[i].attachment)
                {
                    dependencyIn.dstAccessMask |= VK_ACCESS_INPUT_ATTACHMENT_READ_BIT;
                }
                else
                {
                    dependencyIn.dstAccessMask |= VK_ACCESS_SHADER_READ_BIT;
                }
            }
            dependencyIn.dstStageMask |= VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;

            if (imgInfo->view == VK_NULL_HANDLE)
            {
                throw std::logic_error("Bug: Input image not assigned image view.");
            }
            imageViews.push_back(imgInfo->view);
        }
        else
        {
            VkAttachmentReference ref;
            ref.attachment = i;
            ref.layout = pass->m_outputs[outputI].imgInfo.usedLayout;
            imgInfo = &pass->m_outputs[outputI].imgInfo;

            if (imgInfo->finalLayout == VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
            {
                dependencyOut.srcStageMask |= isDepthStencil
                                              ? VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT
                                              : VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
                dependencyOut.srcAccessMask |= isDepthStencil
                                               ? VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT
                                               : VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
                dependencyOut.dstStageMask |= VK_PIPELINE_STAGE_TRANSFER_BIT;
                dependencyOut.dstAccessMask |= VK_ACCESS_TRANSFER_READ_BIT;
                dependencyOut.dependencyFlags = dependencyOut.dependencyFlags & ~VK_DEPENDENCY_BY_REGION_BIT;
            }
            /*
            else if (imgInfo->usedLayout != imgInfo->finalLayout)
            {
                if (imgInfo->usedLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
                    || imgInfo->usedLayout == VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL
                    || imgInfo->usedLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
                    || imgInfo->usedLayout == VK_IMAGE_LAYOUT_STENCIL_ATTACHMENT_OPTIMAL)
                {
                    dependencyOut.srcStageMask |= isDepthStencil
                                                  ? VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT
                                                  : VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
                    dependencyOut.srcAccessMask |= isDepthStencil
                                                   ? VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT
                                                   : VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
                    dependencyOut.dstStageMask |= dependencyOut.srcStageMask;
                    dependencyOut.dstAccessMask |= VK_ACCESS_COLOR_ATTACHMENT_READ_BIT;
                }
                else
                {
                    throw std::logic_error("Unexpected used layout.");
                }
            }*/


            if (imgInfo->previous.pass)
            {
                bool srcDepthStencil = hasDepthComponent(pass->m_outputs[outputI].format)
                                       || hasStencilComponent(pass->m_outputs[outputI].format);
                if (srcDepthStencil)
                {
                    dependencyIn.srcStageMask |= VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
                    dependencyIn.srcAccessMask |= VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

                    dependencyIn.dstStageMask |= VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
                    dependencyIn.dstAccessMask |= VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
                }
            }

            if (isDepthStencil)
            {
                depthStencil = ref;
            }
            else
            {
                colourRefs.push_back(ref);
            }
            if (imgInfo->view == VK_NULL_HANDLE
                && outputI != pass->m_swapchainImageID)
            {
                throw std::logic_error("Bug: Output image not assigned image view.");
            }
            imageViews.push_back(imgInfo->view);
        }
    }

    if (!dependencyIn.srcStageMask)
    {
        dependencyIn.srcStageMask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
    }
    if (!dependencyIn.dstStageMask)
    {
        dependencyIn.dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
    }

    for (U32 i = 0; i < passDesc.dependencies.size(); ++i)
    {
        if (!(passDesc.dependencies[passDesc.dependencies.size() - 1 - i].srcAccessMask))
        {
            passDesc.dependencies.pop_back();
        }
    }

    passDesc.passes.push_back(pass);

    passDesc.subpassDescriptions.resize(1);
    auto& subpassDesc = passDesc.subpassDescriptions.back();
    subpassDesc.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpassDesc.inputAttachmentCount = inputRefs.size();
    subpassDesc.pInputAttachments = inputRefs.data();
    subpassDesc.colorAttachmentCount = colourRefs.size();
    subpassDesc.pColorAttachments = colourRefs.data();
    subpassDesc.pDepthStencilAttachment = &depthStencil;

    return passDesc;
}

void FrameGraph::createVulkanRenderPass(U32 passID,
                                        const ScheduledRenderPassDescription& rpdesc)
{
    RenderPass* pass = &(*m_passReferences[passID])->second;

    VkRenderPassCreateInfo renderPassCreate{};
    renderPassCreate.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassCreate.subpassCount = rpdesc.subpassDescriptions.size();
    renderPassCreate.pSubpasses = rpdesc.subpassDescriptions.data();
    renderPassCreate.attachmentCount = rpdesc.attachmentDescriptions.size();
    renderPassCreate.pAttachments = rpdesc.attachmentDescriptions.data();
    renderPassCreate.dependencyCount = rpdesc.dependencies.size();
    renderPassCreate.pDependencies = rpdesc.dependencies.data();

    // Only create a render pass if it has not already been done.
    if (pass->m_renderPassInfo.pass == VK_NULL_HANDLE)
    {
        VkRenderPass renderPass;
        if (vkCreateRenderPass(m_device, &renderPassCreate, nullptr, &renderPass) != VK_SUCCESS)
        {
            throw std::runtime_error("Failed to create render pass.");
        }
        pass->m_renderPassInfo.pass = renderPass;
        pass->m_renderPassInfo.subpass = 0; /// TODO: Replace when subpass merging.
    }
    else
    {
        throw std::logic_error("FrameGraph bug: Render pass already created.");
    }
}

VkFramebuffer FrameGraph::createFramebuffer(U32 passID,
                                            ScheduledRenderPassDescription& rpdesc,
                                            VkImageView swapchainReplacement)
{
    RenderPass* pass = &(*m_passReferences[passID])->second;

    if (pass->m_swapchainImageID != ~0U)
    {
        /* This function will be called several times if we have a swap chain
         * with more than one image, and then in the second call, it will not be
         * VK_NULL_HANDLE.
        if (rpdesc.imageViews.at(pass->m_swapchainImageID) != VK_NULL_HANDLE)
        {
            throw std::logic_error("FrameGraph bug: Pass should contain an "
                                   "empty view for swap chain image, however "
                                   "the relevant view is not VK_NULL_HANDLE.");
        }*/
        if (swapchainReplacement == VK_NULL_HANDLE)
        {
            throw std::logic_error("FrameGraph bug: No swap chain replacement "
                                   "image, though a swap chain image location "
                                   "was present.");
        }
        // Inputs are the first elements, so we offset the index.
        rpdesc.imageViews.at(pass->m_inputs.size() + pass->m_swapchainImageID) = swapchainReplacement;
    }

    VkFramebufferCreateInfo framebufferCreate{};
    framebufferCreate.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    framebufferCreate.renderPass = pass->m_renderPassInfo.pass;
    framebufferCreate.attachmentCount = rpdesc.imageViews.size();
    framebufferCreate.pAttachments = rpdesc.imageViews.data();
    framebufferCreate.width = pass->m_layout.width;
    framebufferCreate.height = pass->m_layout.height;
    framebufferCreate.layers = pass->m_layout.layers;

    VkFramebuffer framebuffer;
    if (vkCreateFramebuffer(m_device, &framebufferCreate, nullptr, &framebuffer) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create framebuffer.");
    }
    return framebuffer;
}

const Image& FrameGraph::getFramebufferAttachmentImage(PassLayout layout,
                                                       VkFormat fmt,
                                                       VkImageUsageFlags requiredUsages,
                                                       U32 firstPass, U32 lastPassUsed)
{
    // Reusing not yet implemented.
    (void)firstPass;
    (void)lastPassUsed;
    m_images.push_back({});
    auto& img = m_images.back();

    VkImageCreateInfo imageCreate{};
    imageCreate.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageCreate.imageType = VK_IMAGE_TYPE_2D;
    imageCreate.format = fmt;
    imageCreate.extent = VkExtent3D{layout.width, layout.height, 1};
    imageCreate.mipLevels = 1;
    imageCreate.arrayLayers = layout.layers;
    imageCreate.samples = layout.samples;
    imageCreate.tiling = VK_IMAGE_TILING_OPTIMAL;
    imageCreate.usage = requiredUsages;

    imageCreate.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    imageCreate.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

    if (img.allocate(imageCreate, 2, m_allocator, m_device, true) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to allocate framebuffer attachment image.");
    }
    return img;
}

void FrameGraph::execute(VkCommandBuffer commandBuf,
                         U32 swapchainIndex,
                         bool printDebugInfo)
{
    for (auto passID : m_order)
    {
        if (printDebugInfo) std::cout << "Execute " << (*m_passReferences[passID])->first << "\n";

        RenderPass* pass = &(*m_passReferences[passID])->second;

        std::vector<VkClearValue> clearValues;
        VkClearValue dummy;
        dummy.color.float32[0] = 1000.;
        dummy.color.float32[1] = 0;
        dummy.color.float32[2] = 1000.;
        dummy.color.float32[3] = 0;
        for (U32 i = 0; i < pass->m_inputs.size() + pass->m_outputs.size(); ++i)
        {
            bool isInput = i < pass->m_inputs.size();
            auto outputI = i - pass->m_inputs.size();
            if (isInput) clearValues.push_back(dummy);
            else clearValues.push_back(pass->m_outputs[outputI].clearValue);
        }

        VkRenderPassBeginInfo beginInfo{};
        beginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        beginInfo.clearValueCount = clearValues.size();
        beginInfo.pClearValues = clearValues.data();
        beginInfo.framebuffer = pass->m_renderPassInfo.framebuffer;

        if (beginInfo.framebuffer == VK_NULL_HANDLE)
        {
            beginInfo.framebuffer = m_swapchainFramebuffers[swapchainIndex];
        }

        VkRect2D area;
        area.offset = {0, 0};
        area.extent.width = pass->m_layout.width;
        area.extent.height = pass->m_layout.height;
        beginInfo.renderArea = area;
        beginInfo.renderPass = pass->m_renderPassInfo.pass;

        vkCmdBeginRenderPass(commandBuf, &beginInfo, VK_SUBPASS_CONTENTS_INLINE);

        pass->m_callback(commandBuf);

        vkCmdEndRenderPass(commandBuf);

        // Perform end-of-pass transfers if applicable.

        for (const auto& output : pass->m_outputs)
        {
            if (output.imgInfo.finalLayout == VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
            {
                for (auto outgoing : output.connections)
                {
                    if (outgoing.type.simple) continue;

                    RenderPass::AttachmentImageInfo* dstRef = nullptr;
                    if (outgoing.type.toInput)
                    {
                        dstRef = &outgoing.pass->m_inputs[outgoing.attachment].imgInfo;
                    }
                    else
                    {
                        dstRef = &outgoing.pass->m_outputs[outgoing.attachment].imgInfo;
                    }

                    VkImageAspectFlags srcAspect{0}, dstAspect{0};

                    if (hasDepthComponent(output.format)) srcAspect |= VK_IMAGE_ASPECT_DEPTH_BIT;
                    if (hasStencilComponent(output.format)) srcAspect |= VK_IMAGE_ASPECT_STENCIL_BIT;
                    if (!srcAspect) srcAspect = VK_IMAGE_ASPECT_COLOR_BIT;

                    dstAspect = srcAspect; /// TODO: Determine if aspects needs changning.

                    VkOffset3D srcDim{0, 0, 1};
                    VkOffset3D dstDim{0, 0, 1};

                    srcDim.x = pass->m_layout.width;
                    srcDim.y = pass->m_layout.height;

                    dstDim.x = outgoing.pass->m_layout.width;
                    dstDim.y = outgoing.pass->m_layout.height;

                    VkImageBlit region{};
                    region.srcOffsets[0] = {0, 0, 0};
                    region.srcOffsets[1] = srcDim;
                    region.srcSubresource.aspectMask = srcAspect;
                    region.srcSubresource.baseArrayLayer = 0;
                    region.srcSubresource.layerCount = 1; /// TODO: Support multiple layers.
                    region.srcSubresource.mipLevel = 0;

                    region.dstOffsets[0] = {0, 0, 0};
                    region.dstOffsets[1] = dstDim;
                    region.dstSubresource = region.srcSubresource;
                    region.dstSubresource.aspectMask = dstAspect;


                    VkImageMemoryBarrier membar{};
                    membar.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
                    membar.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED; /// GREPO!!! dstRef->finalLayout
                    membar.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
                    membar.srcAccessMask = 0;
                    membar.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
                    membar.image = dstRef->image;
                    membar.subresourceRange.aspectMask = region.dstSubresource.aspectMask;
                    membar.subresourceRange.baseArrayLayer = region.dstSubresource.baseArrayLayer;
                    membar.subresourceRange.baseMipLevel = region.dstSubresource.mipLevel;
                    membar.subresourceRange.layerCount = region.dstSubresource.layerCount;
                    membar.subresourceRange.levelCount = 1;

                    vkCmdPipelineBarrier(commandBuf,
                                         VK_PIPELINE_STAGE_TRANSFER_BIT,
                                         VK_PIPELINE_STAGE_TRANSFER_BIT,
                                         0, 0, nullptr, 0, nullptr,
                                         1, &membar);

                    vkCmdBlitImage(commandBuf,
                                   output.imgInfo.image,
                                   VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                                   dstRef->image,
                                   VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                                   1,
                                   &region,
                                   VK_FILTER_LINEAR); /// TODO: Allow for different filters.
                }
            }
        }
    }
}

void FrameGraph::RenderPass::writeDescriptorSet(VkDescriptorSet dstSet,
                                                U32* count,
                                                VkWriteDescriptorSet* writeOut) const
{
    *count = m_descInfo.descWrites.size();
    if (!writeOut) return;
    for (U32 i = 0; i < m_descInfo.descWrites.size(); ++i)
    {
        writeOut[i] = m_descInfo.descWrites[i];
        writeOut[i].dstSet = dstSet;
    }
}
