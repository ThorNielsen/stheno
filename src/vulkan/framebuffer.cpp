#include "vulkan/framebuffer.hpp"

#include "vulkan/vma.hpp"

#include <kraken/globaltypes.hpp>

#include <vulkan/vulkan_core.h>

#include <cstddef>
#include <utility>
#include <vector>

bool FramebufferAttachment::hasDepthComponent() const
{
    switch (format)
    {
    case VK_FORMAT_X8_D24_UNORM_PACK32:
    case VK_FORMAT_D16_UNORM:
    case VK_FORMAT_D16_UNORM_S8_UINT:
    case VK_FORMAT_D24_UNORM_S8_UINT:
    case VK_FORMAT_D32_SFLOAT:
    case VK_FORMAT_D32_SFLOAT_S8_UINT:
        return true;
    default:
        return false;
    }
}

bool FramebufferAttachment::hasStencilComponent() const
{
    switch (format)
    {
    case VK_FORMAT_S8_UINT:
    case VK_FORMAT_D16_UNORM_S8_UINT:
    case VK_FORMAT_D24_UNORM_S8_UINT:
    case VK_FORMAT_D32_SFLOAT_S8_UINT:
        return true;
    default:
        return false;
    }
}

Framebuffer& Framebuffer::operator=(Framebuffer&& other) noexcept
{
    destroy();
    attachments = std::move(other.attachments);
    m_device = other.m_device;
    m_allocator = other.m_allocator;
    m_width = other.m_width;
    m_height = other.m_height;
    other.resetVariables();
    return *this;
}

VkResult Framebuffer::create(VkDevice device, VmaAllocator allocator, U32 width, U32 height)
{
    destroy();
    m_device = device;
    m_allocator = allocator;
    m_width = width;
    m_height = height;

    return VK_SUCCESS;
}

void Framebuffer::destroy() noexcept
{
    for (auto& attachment : attachments)
    {
        if (attachment.alloc.alloc == VK_NULL_HANDLE) continue;
        destroyView(attachment.view);
        destroyImage(attachment.alloc);
    }
    resetVariables();
}

void Framebuffer::destroyImage(ImageAllocation& alloc) noexcept
{
    if (alloc.alloc == VK_NULL_HANDLE || m_allocator == VK_NULL_HANDLE) return;
    vmaDestroyImage(m_allocator, alloc.image, alloc.alloc);
    alloc.alloc = VK_NULL_HANDLE;
    alloc.image = VK_NULL_HANDLE;
}

void Framebuffer::destroyView(VkImageView& view) noexcept
{
    if (view == VK_NULL_HANDLE || m_device == VK_NULL_HANDLE) return;
    vkDestroyImageView(m_device, view, nullptr);
    view = VK_NULL_HANDLE;
}

VkResult Framebuffer::createAttachment(AttachmentInfo info)
{
    attachments.push_back({});
    auto& attachment = attachments.back();
    attachment.format = info.format;
    auto result = createImage(info, &attachment.alloc);
    if (result != VK_SUCCESS)
    {
        attachments.pop_back();
        return result;
    }
    result = createView(info, attachment.alloc.image, &attachment.view);
    if (result != VK_SUCCESS)
    {
        destroyImage(attachment.alloc);
        attachments.pop_back();
        return result;
    }
    auto& desc = attachment.description;
    desc = {};
    desc.format = info.format;
    desc.samples = info.samples;
    desc.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    desc.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    desc.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    desc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    desc.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    desc.finalLayout = (attachment.hasDepthComponent() || attachment.hasStencilComponent())
                       ? VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
                       : VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    return VK_SUCCESS;
}

VkResult Framebuffer::createImage(AttachmentInfo info, ImageAllocation* alloc)
{
    if (m_allocator == VK_NULL_HANDLE) return VK_ERROR_UNKNOWN;
    VkImageCreateInfo imgCreate{};
    imgCreate.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imgCreate.imageType = VK_IMAGE_TYPE_2D;
    imgCreate.format = info.format;
    imgCreate.extent = {m_width, m_height, 1};
    imgCreate.mipLevels = 1;
    imgCreate.arrayLayers = 1;
    imgCreate.samples = info.samples;
    imgCreate.tiling = VK_IMAGE_TILING_OPTIMAL;
    imgCreate.usage = info.usage;
    imgCreate.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    VmaAllocationCreateInfo allocInfo{};
    allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
    allocInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
    auto result = vmaCreateImage(m_allocator, &imgCreate, &allocInfo,
                                 &alloc->image, &alloc->alloc, nullptr);
    if (result == VK_SUCCESS)
    {
        vmaSetAllocationName(m_allocator, alloc->alloc, "Framebuffer image");
    }
    return result;
}

VkResult Framebuffer::createView(AttachmentInfo info, VkImage image, VkImageView* view)
{
    if (m_allocator == VK_NULL_HANDLE) return VK_ERROR_UNKNOWN;
    VkImageViewCreateInfo viewCreate{};
    viewCreate.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    viewCreate.image = image;
    viewCreate.viewType = VK_IMAGE_VIEW_TYPE_2D;
    viewCreate.format = info.format;
    viewCreate.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    viewCreate.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    viewCreate.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    viewCreate.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    viewCreate.subresourceRange.aspectMask = info.aspectMask;
    viewCreate.subresourceRange.baseArrayLayer = 0;
    viewCreate.subresourceRange.baseMipLevel = 0;
    viewCreate.subresourceRange.layerCount = 1;
    viewCreate.subresourceRange.levelCount = 1;
    return vkCreateImageView(m_device, &viewCreate, nullptr, view);
}

VkResult Framebuffer::createFramebuffer(VkRenderPass renderpass, VkFramebuffer* framebuffer)
{
    std::vector<VkImageView> attachedViews(attachments.size());
    for (size_t i = 0; i < attachments.size(); ++i)
    {
        attachedViews[i] = attachments[i].view;
    }
    VkFramebufferCreateInfo framebufferCreate{};
    framebufferCreate.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    framebufferCreate.renderPass = renderpass;
    framebufferCreate.attachmentCount = attachedViews.size();
    framebufferCreate.pAttachments = attachedViews.data();
    framebufferCreate.width = m_width;
    framebufferCreate.height = m_height;
    framebufferCreate.layers = 1;
    return vkCreateFramebuffer(m_device, &framebufferCreate, nullptr, framebuffer);
}

VkResult Framebuffer::createRenderpass(VkRenderPass* renderpass)
{
    std::vector<VkAttachmentDescription> attachmentDescriptions(attachments.size());
    std::vector<VkAttachmentReference> references(attachments.size());

    bool hasDSAttachment = false;

    size_t dsAttachmentLocation = 0;

    for (size_t i = 0; i < attachments.size(); ++i)
    {
        attachmentDescriptions[i] = attachments[i].description;
        references[i].attachment = i;
        if (attachments[i].hasDepthComponent() || attachments[i].hasStencilComponent())
        {
            if (hasDSAttachment)
            {
                // There cannot be more than a single non-colour attachment,
                // i.e. more than one depth/stencil attachment.
                return VK_ERROR_FORMAT_NOT_SUPPORTED;
            }
            dsAttachmentLocation = i;
            hasDSAttachment = true;
            references[i].layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
        }
        else
        {
            references[i].layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        }
    }
    if (hasDSAttachment)
    {
        std::swap(references[dsAttachmentLocation], references.back());
    }

    VkSubpassDescription subpassDesc{};
    subpassDesc.colorAttachmentCount = references.size() - hasDSAttachment;
    subpassDesc.pColorAttachments = references.data();
    subpassDesc.pDepthStencilAttachment = hasDSAttachment
                                          ? &references.back()
                                          : nullptr;
    subpassDesc.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

    VkSubpassDependency dependency{};
    dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
    dependency.dstSubpass = 0;
    dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.srcAccessMask = 0;
    dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

    VkRenderPassCreateInfo passCreate{};
    passCreate.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    passCreate.attachmentCount = attachmentDescriptions.size();
    passCreate.pAttachments = attachmentDescriptions.data();
    passCreate.subpassCount = 1;
    passCreate.pSubpasses = &subpassDesc;
    passCreate.dependencyCount = 1;
    passCreate.pDependencies = &dependency;

    return vkCreateRenderPass(m_device, &passCreate, nullptr, renderpass);
}
