#include "vulkan/common.hpp"

#include <kraken/globaltypes.hpp>

#include <vulkan/vulkan_core.h>

#include <algorithm>
#include <cstddef>
#include <filesystem>
#include <memory>
#include <optional>
#include <stdexcept>
#include <string_view>
#include <utility>
#include <vector>

std::optional<VkPhysicalDevice>
selectBestPhysicalDevice(VkInstance instance, bool requireGPU)
{
    auto devices = getArrayOfStructs<VkPhysicalDevice>(vkEnumeratePhysicalDevices, instance);

    std::optional<VkPhysicalDevice> fallbackDevice;
    std::optional<VkPhysicalDevice> integratedGPU;
    std::optional<VkPhysicalDevice> virtualGPU;

    for (auto device : devices)
    {
        VkPhysicalDeviceProperties devprops;
        vkGetPhysicalDeviceProperties(device, &devprops);
        switch (devprops.deviceType)
        {
        case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:
            return device;
        case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU:
            virtualGPU = device;
            break;
        case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:
            integratedGPU = device;
            break;
        case VK_PHYSICAL_DEVICE_TYPE_CPU:
        default:
            break;
        }
        if (!requireGPU) fallbackDevice = device;
    }

    // .value_or is nice and all, and would work here except for the fact that
    // all may be empty, so the argument to the terminating value_or might be
    // impossible to evaluate.
    if (virtualGPU.has_value()) return virtualGPU;
    if (integratedGPU.has_value()) return integratedGPU;
    return fallbackDevice;
}

std::pair<std::vector<VkDeviceQueueCreateInfo>, std::unique_ptr<std::vector<float>[]>>
selectDeviceQueues(VkPhysicalDevice device,
                   VkSurfaceKHR surface,
                   U32 infoCount,
                   const QueueSelectionInfo* selectionInfo,
                   QueueInformation* queueInformation)
{
    auto queueProps = getArrayOfStructs<VkQueueFamilyProperties>(vkGetPhysicalDeviceQueueFamilyProperties, device);

    U32 currCreate = 0;

    std::pair<std::vector<VkDeviceQueueCreateInfo>,
              std::unique_ptr<std::vector<float>[]>> queues;

    auto& dcInfo = queues.first;
    dcInfo.resize(queueProps.size());
    auto& priorities = queues.second;
    priorities = std::make_unique<std::vector<float>[]>(dcInfo.size());
    for (U32 i = 0; i < dcInfo.size(); ++i)
    {
        auto& info = dcInfo[i];
        info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        info.pNext = nullptr;
        info.flags = 0;
        info.queueCount = 0;
        info.queueFamilyIndex = i;
        info.pQueuePriorities = nullptr;
    }

    for (U32 createIdx = 0; createIdx < infoCount; ++createIdx)
    {
        auto reqFlags = selectionInfo[createIdx].requiredFlags;
        auto optFlags = selectionInfo[createIdx].optionalFlags;
        auto requirePresent = selectionInfo[createIdx].requirePresentSupport;
        if (requirePresent && surface == VK_NULL_HANDLE)
        {
            throw std::logic_error("Inconsistency in selectDeviceQueues(): "
                                   "requirePresent is true for at least one "
                                   "queue, yet surface is VK_NULL_HANDLE.");
        }
        U32 selectedFamily = 0xffffffff; // Invalid queue.
        for (size_t family = 0; family < queueProps.size(); ++family)
        {
            if (!queueProps[family].queueCount) continue;
            if ((queueProps[family].queueFlags & reqFlags) == reqFlags)
            {
                if (requirePresent)
                {
                    VkBool32 presentSupported{};
                    vkGetPhysicalDeviceSurfaceSupportKHR(device, family, surface, &presentSupported);
                    if (!presentSupported) continue;
                }
                selectedFamily = family;
                if ((queueProps[family].queueFlags & optFlags) == optFlags)
                {
                    break; // If the family matches everything, then we use that.
                    // TODO: Match using hamming weight of queueFlags & optFlags
                    // such that if no queue matching exactly exists, then the
                    // queue with the most optional features is selected.
                }
            }
        }
        if (queueInformation)
        {
            queueInformation[createIdx].familyIndex = selectedFamily;
            queueInformation[createIdx].queueIndex = dcInfo[selectedFamily].queueCount;
        }
        if (selectedFamily == 0xffffffff) continue;

        priorities[selectedFamily].push_back(selectionInfo[createIdx].priority);
        ++dcInfo[selectedFamily].queueCount;
        // This must be set at every loop to ensure that if a reallocation
        // happens in one of priorities's subvectors, then pQueuePriorities
        // still point to valid data. However, reallocations there can occur
        // only if we modify the container, and above is the only place where we
        // do so.
        dcInfo[selectedFamily].pQueuePriorities = priorities[selectedFamily].data();

        --queueProps[currCreate].queueCount;
        ++currCreate;
    }

    // Ensure that only create infos with nonzero queue counts are passed.
    for (U32 i = 0; i < dcInfo.size();)
    {
        if (!dcInfo[i].queueCount)
        {
            std::swap(dcInfo[i], dcInfo.back());
            dcInfo.pop_back();
        }
        else
        {
            ++i;
        }
    }

    return queues;
}
U32 getSuitableMemoryType(VkPhysicalDevice device,
                          VkMemoryPropertyFlags requiredProperties,
                          U32 allowedTypes)
{
    VkPhysicalDeviceMemoryProperties memprops;
    vkGetPhysicalDeviceMemoryProperties(device, &memprops);
    return getSuitableMemoryType(memprops, requiredProperties, allowedTypes);
}

U32 getSuitableMemoryType(VkPhysicalDeviceMemoryProperties memprops,
                          VkMemoryPropertyFlags requiredProperties,
                          U32 allowedTypes)
{
    for (U32 i = 0; i < memprops.memoryTypeCount; ++i)
    {
        bool allowed = allowedTypes & (U32(1) << i);
        bool hasRequiredProperties = (requiredProperties & memprops.memoryTypes[i].propertyFlags)
                                     == requiredProperties;
        if (allowed && hasRequiredProperties)
        {
            return i;
        }
    }
    throw std::runtime_error("Could not find a suitable memory type.");
}

fs::path findDirectory(std::string_view name,
                       std::vector<fs::path> searchDirectories)
{
    for (const auto& path : searchDirectories)
    {
        if (fs::is_directory(path / name))
        {
            return fs::canonical(path/name);
        }
    }
    return {};
}

VkImageCreateInfo simpleImageCreateInfo(VkFormat format,
                                        VkExtent3D extents,
                                        VkImageUsageFlags usageFlags)
{
    VkImageCreateInfo info{};
    info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;

    info.imageType = VK_IMAGE_TYPE_1D;
    if (extents.height > 0) info.imageType = VK_IMAGE_TYPE_2D;
    if (extents.depth > 0) info.imageType = VK_IMAGE_TYPE_3D;

    extents.height = std::max<decltype(extents.height)>(extents.height, 1);
    extents.depth = std::max<decltype(extents.height)>(extents.depth, 1);

    info.format = format;
    info.extent = extents;
    info.mipLevels = 1;
    info.arrayLayers = 1;
    info.samples = VK_SAMPLE_COUNT_1_BIT;
    info.tiling = VK_IMAGE_TILING_OPTIMAL;
    info.usage = usageFlags;
    info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    return info;
}

bool operator<(const VkVertexInputAttributeDescription& a,
               const VkVertexInputAttributeDescription& b)
{
    if (a.location != b.location) return a.location < b.location;
    if (a.offset != b.offset) return a.offset < b.offset;
    if (a.format != b.format) return (U32)a.format < (U32)b.format;
    if (a.binding != b.binding) return a.binding < b.binding;
    return false;
}
bool operator<(const ShaderInputAttributeDescription& a,
               const ShaderInputAttributeDescription& b)
{
    if (a.size() != b.size()) return a.size() < b.size();
    for (size_t i = 0; i < a.size(); ++i)
    {
        if (a[i] < b[i]) return true;
    }
    return false;
}


void toCanonicalForm(ShaderInputAttributeDescription& desc)
{
    std::sort(desc.begin(), desc.end());
}

ShaderInputAttributeDescription canonicalForm(ShaderInputAttributeDescription desc)
{
    toCanonicalForm(desc);
    return desc;
}
