#include "vulkan/swapchain.hpp"

#include "vulkan/common.hpp"
#include "vulkan/vma.hpp"

#include <kraken/globaltypes.hpp>

#include <vulkan/vulkan_core.h>

#include <algorithm>
#include <cstddef>
#include <stdexcept>

VkResult Swapchain::create(const SwapchainCreateInfo& createInfo, VmaAllocator allocator,
                           bool createSyncObjects)
{
    if (m_device != VK_NULL_HANDLE) destroy();
    m_allocator = allocator;
    m_device = createInfo.device;
    m_surface = createInfo.surface;
    m_maxCachedFrames = createInfo.maxCachedFrames;
    m_currFrame = 0;

    auto result = createSwapchain(createInfo);
    if (result != VK_SUCCESS) return result;

    if (createSyncObjects)
    {
        result = createSynchronisationObjects();
        if (result != VK_SUCCESS) return result;
    }

    return VK_SUCCESS;
}

VkResult Swapchain::acquireNextImage(U32& imageIndex, VkSemaphore imageAcquireSemaphore)
{
    if (hasSyncObjects())
    {
        vkWaitForFences(m_device, 1, &m_frameFences[m_currFrame], VK_TRUE, 0xffffffffffffffffull);
        imageAcquireSemaphore = m_imageAcquireSemaphores[m_currFrame];
    }

    auto result = vkAcquireNextImageKHR(m_device,
                                        m_swapchain,
                                        0xffffffffffffffffull,
                                        imageAcquireSemaphore,
                                        VK_NULL_HANDLE,
                                        &imageIndex);
    if (result != VK_SUCCESS) return result;

    if (hasSyncObjects())
    {
        if (m_cachedFrameFences[imageIndex] != VK_NULL_HANDLE)
        {
            result = vkWaitForFences(m_device,
                                     1,
                                     &m_cachedFrameFences[imageIndex],
                                     VK_TRUE,
                                     0xffffffffffffffffull);
            if (result < 0) return result;
        }
        m_cachedFrameFences[imageIndex] = m_frameFences[m_currFrame];

        result = vkResetFences(m_device, 1, &m_frameFences[m_currFrame]);
        if (result < 0) return result;
    }

    m_currImageIndex = imageIndex;

    return VK_SUCCESS;
}

void Swapchain::getCurrentSemaphores(VkSemaphore& imageAcquire,
                                     VkSemaphore& renderFinished)
{
    if (!hasSyncObjects())
    {
        throw std::logic_error("Sync objects were explicitly not created.");
    }
    imageAcquire = m_imageAcquireSemaphores[m_currFrame];
    renderFinished = m_renderFinishedSemaphores[m_currFrame];
}

VkResult Swapchain::present(VkQueue queue, U32 waitSemaphoreCount,
                            const VkSemaphore* pWaitSemaphores)
{
    bool externalSemaphores = waitSemaphoreCount;

    if (externalSemaphores == hasSyncObjects())
    {
        throw std::logic_error("Present should either use external semaphores "
                               "OR internal ones, but neither neither nor both.");
    }

    VkPresentInfoKHR presentInfo{};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    if (externalSemaphores)
    {
        presentInfo.waitSemaphoreCount = waitSemaphoreCount;
        presentInfo.pWaitSemaphores = pWaitSemaphores;
    }
    else
    {
        presentInfo.waitSemaphoreCount = 1;
        presentInfo.pWaitSemaphores = &m_renderFinishedSemaphores[m_currFrame];
    }
    presentInfo.pSwapchains = &m_swapchain;
    presentInfo.swapchainCount = 1;
    presentInfo.pImageIndices = &m_currImageIndex;

    m_currFrame = (m_currFrame + 1) % m_maxCachedFrames;

    return vkQueuePresentKHR(queue, &presentInfo);
}

VkResult Swapchain::createSwapchain(const SwapchainCreateInfo& createInfo)
{
    auto surfaceFormats = getArrayOfStructs<VkSurfaceFormatKHR>(vkGetPhysicalDeviceSurfaceFormatsKHR,
                                                                createInfo.physicalDevice,
                                                                createInfo.surface);
    m_format = surfaceFormats.at(0);
    for (const auto& format : surfaceFormats)
    {
        if (format.format == createInfo.swapchain.desiredFormat.format
            && format.colorSpace == createInfo.swapchain.desiredFormat.colorSpace)
        {
            m_format = format;
            break;
        }
    }

    auto presentModes = getArrayOfStructs<VkPresentModeKHR>(vkGetPhysicalDeviceSurfacePresentModesKHR,
                                                            createInfo.physicalDevice,
                                                            createInfo.surface);

    m_presentMode = VK_PRESENT_MODE_FIFO_KHR;
    for (auto& mode : presentModes)
    {
        if (mode == createInfo.swapchain.desiredPresentMode)
        {
            m_presentMode = mode;
            break;
        }
    }
    VkSurfaceCapabilitiesKHR surfaceCaps;
    auto result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(createInfo.physicalDevice,
                                                            createInfo.surface,
                                                            &surfaceCaps);
    if (result != VK_SUCCESS) return result;
    m_extent = createInfo.swapchain.extent;
    m_extent.width = std::max(surfaceCaps.minImageExtent.width,
                              std::min(m_extent.width, surfaceCaps.maxImageExtent.width));
    m_extent.height = std::max(surfaceCaps.minImageExtent.height,
                               std::min(m_extent.height, surfaceCaps.maxImageExtent.height));

    VkSwapchainCreateInfoKHR swapchainCreate{};
    swapchainCreate.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    swapchainCreate.surface = m_surface;
    swapchainCreate.minImageCount = std::max(surfaceCaps.minImageCount,
                                             createInfo.swapchain.minImageCount);
    if (surfaceCaps.maxImageCount)
    {
        swapchainCreate.minImageCount = std::min(swapchainCreate.minImageCount,
                                                 surfaceCaps.maxImageCount);
    }
    swapchainCreate.imageFormat = m_format.format;
    swapchainCreate.imageColorSpace = m_format.colorSpace;
    swapchainCreate.imageExtent = m_extent;
    swapchainCreate.imageArrayLayers = 1;
    swapchainCreate.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    swapchainCreate.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    swapchainCreate.queueFamilyIndexCount = 0;
    swapchainCreate.pQueueFamilyIndices = nullptr;
    swapchainCreate.preTransform = surfaceCaps.currentTransform;
    swapchainCreate.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    swapchainCreate.presentMode = m_presentMode;
    swapchainCreate.clipped = createInfo.swapchain.clipped;
    swapchainCreate.oldSwapchain = VK_NULL_HANDLE; // TODO: Reuse old resources if possible.

    result = vkCreateSwapchainKHR(m_device, &swapchainCreate, nullptr, &m_swapchain);
    if (result != VK_SUCCESS) return result;

    m_swapImages = getArrayOfStructs<VkImage>(vkGetSwapchainImagesKHR,
                                              m_device,
                                              m_swapchain);
    m_swapViews.resize(imageCount(), VK_NULL_HANDLE);

    for (U32 i = 0; i < imageCount(); ++i)
    {
        VkImageViewCreateInfo viewCreate{};
        viewCreate.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        viewCreate.image = m_swapImages[i];
        viewCreate.viewType = VK_IMAGE_VIEW_TYPE_2D;
        viewCreate.format = m_format.format;
        viewCreate.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        viewCreate.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        viewCreate.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        viewCreate.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        viewCreate.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        viewCreate.subresourceRange.baseArrayLayer = 0;
        viewCreate.subresourceRange.baseMipLevel = 0;
        viewCreate.subresourceRange.layerCount = 1;
        viewCreate.subresourceRange.levelCount = 1;
        result = vkCreateImageView(m_device, &viewCreate,
                                   nullptr, &m_swapViews[i]);
        if (result != VK_SUCCESS) return result;
    }
    return VK_SUCCESS;
}

VkResult Swapchain::createSynchronisationObjects()
{
    m_imageAcquireSemaphores.resize(m_maxCachedFrames, VK_NULL_HANDLE);
    m_renderFinishedSemaphores.resize(m_maxCachedFrames, VK_NULL_HANDLE);
    m_frameFences.resize(m_maxCachedFrames, VK_NULL_HANDLE);
    m_cachedFrameFences.resize(imageCount(), VK_NULL_HANDLE);
    for (size_t i = 0; i < m_maxCachedFrames; ++i)
    {
        VkSemaphoreCreateInfo semaphoreCreate{};
        semaphoreCreate.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        auto result = vkCreateSemaphore(m_device, &semaphoreCreate, nullptr, &m_imageAcquireSemaphores[i]);
        if (result != VK_SUCCESS) return result;
        result = vkCreateSemaphore(m_device, &semaphoreCreate, nullptr, &m_renderFinishedSemaphores[i]);
        if (result != VK_SUCCESS) return result;
        VkFenceCreateInfo fenceCreate{};
        fenceCreate.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        fenceCreate.flags = VK_FENCE_CREATE_SIGNALED_BIT;
        result = vkCreateFence(m_device, &fenceCreate, nullptr, &m_frameFences[i]);
        if (result != VK_SUCCESS) return result;
    }
    return VK_SUCCESS;
}

bool Swapchain::hasSyncObjects() const
{
    return !m_frameFences.empty();
}

void Swapchain::destroy()
{
    if (m_device == VK_NULL_HANDLE) return;
    for (size_t i = 0; i < imageCount(); ++i)
    {
        if (i < m_imageAcquireSemaphores.size()
            && m_imageAcquireSemaphores[i] != VK_NULL_HANDLE)
        {
            vkDestroySemaphore(m_device, m_imageAcquireSemaphores[i], nullptr);
        }
        if (i < m_renderFinishedSemaphores.size()
            && m_renderFinishedSemaphores[i] != VK_NULL_HANDLE)
        {
            vkDestroySemaphore(m_device, m_renderFinishedSemaphores[i], nullptr);
        }
        if (i < m_frameFences.size() && m_frameFences[i] != VK_NULL_HANDLE)
        {
            vkDestroyFence(m_device, m_frameFences[i], nullptr);
        }

        if (i < m_swapViews.size() && m_swapViews[i] != VK_NULL_HANDLE)
        {
            vkDestroyImageView(m_device, m_swapViews[i], nullptr);
        }
    }
    m_imageAcquireSemaphores.clear();
    m_renderFinishedSemaphores.clear();
    m_frameFences.clear();
    m_cachedFrameFences.clear();
    m_swapViews.clear();
    m_swapImages.clear(); // Managed by swap chain.
    if (m_swapchain != VK_NULL_HANDLE)
    {
        vkDestroySwapchainKHR(m_device, m_swapchain, nullptr);
        m_swapchain = VK_NULL_HANDLE;
    }
    m_allocator = VK_NULL_HANDLE;
    m_device = VK_NULL_HANDLE;
    m_surface = VK_NULL_HANDLE;
}
