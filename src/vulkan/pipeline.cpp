#include "vulkan/pipeline.hpp"

#include <kraken/globaltypes.hpp>
#include <vulkan/vulkan_core.h>

VkGraphicsPipelineCreateInfo
GraphicsPipelineInfo::getGraphicsPipelineCreateInfo() const
{
    viewportCreate.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportCreate.pScissors = scissors.data();
    viewportCreate.pViewports = viewports.data();
    viewportCreate.scissorCount = scissors.size();
    viewportCreate.viewportCount = viewports.size();

    colourBlendCreate.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colourBlendCreate.attachmentCount = colourBlendStates.size();
    colourBlendCreate.pAttachments = colourBlendStates.data();

    VkGraphicsPipelineCreateInfo graphicsCreate{};
    graphicsCreate.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    graphicsCreate.pVertexInputState = &vertexInputCreate;
    graphicsCreate.pInputAssemblyState = &inputAssemblyCreate;
    graphicsCreate.pTessellationState = &tesselationCreate;
    graphicsCreate.pViewportState = &viewportCreate;
    graphicsCreate.pRasterizationState = &rasterizerCreate;
    graphicsCreate.pMultisampleState = &multisampleCreate;
    graphicsCreate.pDepthStencilState = &depthStencilCreate;
    graphicsCreate.pColorBlendState = &colourBlendCreate;
    graphicsCreate.pDynamicState = &dynamicState;

    return graphicsCreate;
}

void GraphicsPipelineInfo::clear()
{
    vertexInputCreate.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    inputAssemblyCreate.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    tesselationCreate.sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
    viewportCreate.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    rasterizerCreate.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    multisampleCreate.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    depthStencilCreate.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    colourBlendCreate.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    pipelineCreate.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;

    scissors.clear();
    viewports.clear();
    colourBlendStates.clear();
}

void GraphicsPipelineInfo::fillInfo(U32 width,
                                    U32 height,
                                    VkPrimitiveTopology topology,
                                    VkPolygonMode polygonMode,
                                    VkCullModeFlags cullMode,
                                    VkFrontFace frontFace,
                                    VkSampleCountFlagBits samples)
{
    clear();
    inputAssemblyCreate.topology = topology;
    viewports.push_back({0.f, 0.f, (float)width, (float)height, 0.f, 1.f});
    scissors.push_back({VkOffset2D{0, 0}, VkExtent2D{width, height}});

    rasterizerCreate.cullMode = cullMode;
    rasterizerCreate.depthBiasEnable = VK_FALSE;
    rasterizerCreate.depthClampEnable = VK_FALSE;
    rasterizerCreate.frontFace = frontFace;
    rasterizerCreate.lineWidth = 1.0f;
    rasterizerCreate.polygonMode = polygonMode;
    rasterizerCreate.rasterizerDiscardEnable = VK_FALSE;

    multisampleCreate.minSampleShading = 1.f;
    multisampleCreate.rasterizationSamples = samples;

    colourBlendStates.push_back({});

    auto& colourBlendState = colourBlendStates.back();
    colourBlendState.alphaBlendOp = VK_BLEND_OP_ADD;
    colourBlendState.blendEnable = VK_FALSE;
    colourBlendState.colorBlendOp = VK_BLEND_OP_ADD;
    colourBlendState.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    colourBlendState.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    colourBlendState.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    colourBlendState.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    colourBlendState.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;

    depthStencilCreate.depthTestEnable = VK_TRUE;
    depthStencilCreate.depthWriteEnable = VK_TRUE;
    depthStencilCreate.depthCompareOp = VK_COMPARE_OP_LESS;
    depthStencilCreate.depthBoundsTestEnable = VK_FALSE;
    depthStencilCreate.stencilTestEnable = VK_FALSE;
    depthStencilCreate.minDepthBounds = 0.f;
    depthStencilCreate.maxDepthBounds = 1.f;
}
