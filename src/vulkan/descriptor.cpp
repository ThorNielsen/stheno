#include "vulkan/descriptor.hpp"

#include <kraken/globaltypes.hpp>

#include <vulkan/vulkan_core.h>

#include <algorithm>
#include <cstddef>
#include <stdexcept>
#include <utility>
#include <vector>

DescriptorAllocator::DescriptorAllocator()
{
    m_device = VK_NULL_HANDLE;
}

DescriptorAllocator::DescriptorAllocator(VkDevice device)
{
    construct(device);
}

DescriptorAllocator::~DescriptorAllocator()
{
    destroy();
}

void DescriptorAllocator::construct(VkDevice device)
{
    destroy();
    if (device == VK_NULL_HANDLE) return;
    m_device = device;
    createNewDescriptorPool();
}

void DescriptorAllocator::destroy()
{
    if (m_device == VK_NULL_HANDLE) return;
    for (auto pool : m_pools)
    {
        if (pool == VK_NULL_HANDLE) continue;
        vkDestroyDescriptorPool(m_device, pool, nullptr);
    }
    m_pools.clear();
    m_poolFull.clear();
    m_nextFreePool.clear();
    m_device = VK_NULL_HANDLE;
}


bool DescriptorAllocator::allocate(VkDescriptorSet* setsOut,
                                   const VkDescriptorSetLayout* layouts,
                                   U32 layoutCount)
{
    VkDescriptorSetAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorSetCount = layoutCount;
    allocInfo.pSetLayouts = layouts;

    // We try once to create a new descriptor pool if all the others are full.
    // But if it happens that this then still does not have enough memory, then
    // it does not make sense to continue allocation.
    bool newPoolAllocated = false;
    while (!m_nextFreePool.empty() || !newPoolAllocated)
    {
        if (m_nextFreePool.empty())
        {
            createNewDescriptorPool();
            newPoolAllocated = true;
        }
        auto currPoolIndex = m_nextFreePool.back();
        allocInfo.descriptorPool = m_pools[currPoolIndex];
        auto result = vkAllocateDescriptorSets(m_device, &allocInfo, setsOut);
        if (result == VK_SUCCESS)
        {
            return true;
        }
        else if (result == VK_ERROR_FRAGMENTED_POOL
                 || result == VK_ERROR_OUT_OF_POOL_MEMORY)
        {
            m_poolFull[currPoolIndex] = true;
            m_nextFreePool.pop_back();
        }
        else
        {
            // If the error is not a fragmented or otherwise memory-lacking pool
            // then it is likely a lack of either host or device memory, none of
            // which we can handle here.
            return false;
        }
    }
    return false;
}

void DescriptorAllocator::resetPools()
{
    m_nextFreePool.clear();
    for (size_t i = 0; i < m_pools.size(); ++i)
    {
        vkResetDescriptorPool(m_device, m_pools[i], 0);
        m_poolFull[i] = false;
        m_nextFreePool.push_back(i);
    }
}


void DescriptorAllocator::createNewDescriptorPool()
{
    VkDescriptorPoolCreateInfo poolCreate{};
    poolCreate.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolCreate.maxSets = 1;
    for (auto sz : poolSizes)
    {
        poolCreate.maxSets = std::max(poolCreate.maxSets, sz.descriptorCount);
    }
    poolCreate.pPoolSizes = poolSizes.data();
    poolCreate.poolSizeCount = poolSizes.size();
    m_pools.push_back(VK_NULL_HANDLE);
    if (vkCreateDescriptorPool(m_device, &poolCreate, nullptr, &m_pools.back())
        != VK_SUCCESS)
    {
        // Clean up this one pool which is unallocated, so the destructor
        // does not attempt to destroy it.
        m_pools.pop_back();
        throw std::runtime_error("Failed to create descriptor pool.");
    }
    const U32 poolIndex = m_pools.size() - 1;
    m_poolFull.push_back(false);
    m_nextFreePool.push_back(poolIndex);
}

void DescriptorSetLayoutInfo::addBinding(U32 binding, VkDescriptorType type,
                                         VkShaderStageFlags stages, U32 count)
{
    VkDescriptorSetLayoutBinding dslBind;
    dslBind.binding = binding;
    dslBind.descriptorCount = count;
    dslBind.descriptorType = type;
    dslBind.stageFlags = stages;
    dslBind.pImmutableSamplers = nullptr;
    bindings.push_back(dslBind);
}

VkDescriptorSetLayoutCreateInfo DescriptorSetLayoutInfo::descriptorSetLayoutCreateInfo() const
{
    VkDescriptorSetLayoutCreateInfo layoutCreate{};
    layoutCreate.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutCreate.bindingCount = bindings.size();
    layoutCreate.pBindings = bindings.data();
    return layoutCreate;
}

VkWriteDescriptorSet DescriptorSetLayoutInfo::writeDescriptorSetInfo(size_t binding, VkDescriptorSet set) const
{
    VkWriteDescriptorSet descSetWrite{};
    descSetWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descSetWrite.descriptorCount = bindings[binding].descriptorCount;
    descSetWrite.descriptorType = bindings[binding].descriptorType;
    descSetWrite.dstBinding = bindings[binding].binding;
    descSetWrite.dstSet = set;
    return descSetWrite;
}

DescriptorSetLayout::DescriptorSetLayout()
{
    m_device = VK_NULL_HANDLE;
    m_alloc = nullptr;
    m_layout = VK_NULL_HANDLE;
}

DescriptorSetLayout::DescriptorSetLayout(VkDevice device, VkAllocationCallbacks* alloc)
{
    m_device = VK_NULL_HANDLE;
    m_alloc = nullptr;
    m_layout = VK_NULL_HANDLE;
    construct(device, alloc);
}

DescriptorSetLayout::~DescriptorSetLayout()
{
    destroy();
}

DescriptorSetLayout::DescriptorSetLayout(DescriptorSetLayout&& other) noexcept
{
    *this = std::move(other);
}

DescriptorSetLayout& DescriptorSetLayout::operator=(DescriptorSetLayout&& other) noexcept
{
    destroy();
    m_layoutInfo = std::move(other.m_layoutInfo);
    m_device = other.m_device;
    m_alloc = other.m_alloc;
    m_layout = other.m_layout;

    other.m_device = VK_NULL_HANDLE;
    other.m_layout = VK_NULL_HANDLE;
    other.m_alloc = nullptr;
    return *this;
}

void DescriptorSetLayout::construct(VkDevice device, VkAllocationCallbacks* alloc)
{
    destroy();
    m_device = device;
    m_alloc = alloc;
}

void DescriptorSetLayout::destroy()
{
    if (m_device == VK_NULL_HANDLE) return;
    destroyLayout();
    m_device = VK_NULL_HANDLE;
    m_alloc = nullptr;
    m_layoutInfo.bindings.clear();
}

void DescriptorSetLayout::destroyLayout()
{
    if (m_layout == VK_NULL_HANDLE) return;
    vkDestroyDescriptorSetLayout(m_device, m_layout, m_alloc);
    m_layout = VK_NULL_HANDLE;
}

VkResult DescriptorSetLayout::compileLayout()
{
    if (m_device == VK_NULL_HANDLE) return VK_ERROR_DEVICE_LOST;
    auto layoutCreate = m_layoutInfo.descriptorSetLayoutCreateInfo();
    return vkCreateDescriptorSetLayout(m_device, &layoutCreate, m_alloc, &m_layout);
}

bool createDescriptorSets(VkDescriptorSet* setsOut, VkDeviceSize count,
                          const DescriptorSetLayout* pLayout,
                          DescriptorAllocator& descAllocator)
{
    std::vector<VkDescriptorSetLayout> layouts(count, pLayout->layout());
    return descAllocator.allocate(setsOut, layouts.data(), layouts.size());
}

DescriptorSetLayoutCache::DescriptorSetLayoutCache()
{
    construct();
}

DescriptorSetLayoutCache::~DescriptorSetLayoutCache()
{
    destroy();
}

void DescriptorSetLayoutCache::construct()
{
    ;
}

void DescriptorSetLayoutCache::destroy()
{
    m_layouts.clear();
    m_userCount.clear();
}

DescriptorSetLayoutCache::SetLayoutID
DescriptorSetLayoutCache::addUser(DescriptorSetLayout&& setLayout)
{
    DescriptorSetLayout layout(std::move(setLayout));
    auto id = findID(layout);
    if (id == m_layouts.size())
    {
        m_layouts.emplace_back(std::move(layout));
        m_userCount.push_back(1);
    }
    else
    {
        ++m_userCount[id];
    }
    return id;
}

void DescriptorSetLayoutCache::addUser(DescriptorSetLayoutCache::SetLayoutID id)
{
    ++m_userCount.at(id);
}

void DescriptorSetLayoutCache::removeUser(DescriptorSetLayoutCache::SetLayoutID id)
{
    --m_userCount.at(id);
    if (!m_userCount[id])
    {
        // Cleanup and remove id.
    }
}

const DescriptorSetLayout*
DescriptorSetLayoutCache::layout(DescriptorSetLayoutCache::SetLayoutID id) const
{
    if (id >= m_layouts.size()) return nullptr;
    if (m_userCount[id] == 0) return nullptr;
    return &m_layouts[id];
}

DescriptorSetLayoutCache::SetLayoutID
DescriptorSetLayoutCache::findID(const DescriptorSetLayout& dset) const
{
    size_t id = 0;
    for (; id < m_layouts.size(); ++id)
    {
        if (m_layouts[id].layoutInfo() == dset.layoutInfo()) return id;
    }
    return id;
}
