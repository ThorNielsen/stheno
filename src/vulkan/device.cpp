#include "vulkan/device.hpp"

#include "vulkan/common.hpp"

#include <vulkan/vulkan_core.h>

#include <stdexcept>
#include <string>
#include <vector>

std::vector<const char*>
getSupportedVulkanLayers(const std::vector<std::string>& layers)
{
    std::vector<const char*> enabledLayers;
    auto layerProps = getArrayOfStructs<VkLayerProperties>(vkEnumerateInstanceLayerProperties);
    for (const auto& layer : layers)
    {
        for (const auto& layerProp : layerProps)
        {
            if (layerProp.layerName == layer)
            {
                enabledLayers.push_back(layer.data());
                break;
            }
        }
    }
    return enabledLayers;
}

std::vector<const char*>
getSupportedVulkanInstanceExtensions(const std::vector<std::string>& required,
                                     const std::vector<std::string>& optional)
{
    auto extensionProps = getArrayOfStructs<VkExtensionProperties>(vkEnumerateInstanceExtensionProperties, nullptr);
    return getSupportedVulkanExtensions(extensionProps, required, optional, "instance");
}

std::vector<const char*>
getSupportedVulkanDeviceExtensions(VkPhysicalDevice device,
                                   const std::vector<std::string>& required,
                                   const std::vector<std::string>& optional)
{
    auto extensionProps = getArrayOfStructs<VkExtensionProperties>(vkEnumerateDeviceExtensionProperties, device, nullptr);
    return getSupportedVulkanExtensions(extensionProps, required, optional, "device");
}

std::vector<const char*>
getSupportedVulkanExtensions(const std::vector<VkExtensionProperties>& extensionProps,
                             const std::vector<std::string>& required,
                             const std::vector<std::string>& optional,
                             std::string_view extensionType)
{
    std::vector<const char*> extensions;
    for (const auto& reqName : required)
    {
        bool found = false;
        for (const auto& prop : extensionProps)
        {
            if (prop.extensionName == reqName)
            {
                extensions.push_back(reqName.data());
                found = true;
                break;
            }
        }
        if (!found)
        {
            throw std::runtime_error("Couldn't find required "
                                     + std::string(extensionType)
                                     + " extension '"
                                     + reqName
                                     + "'.");
        }
    }
    for (const auto& optName : optional)
    {
        for (const auto& prop : extensionProps)
        {
            if (prop.extensionName == optName)
            {
                extensions.push_back(optName.data());
            }
        }
    }
    return extensions;
}
