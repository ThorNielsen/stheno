#include "vulkan/allocation_utils.hpp"

#include "vulkan/shader.hpp"
#include "vulkan/vma.hpp"

#include <kraken/globaltypes.hpp>

#include <vulkan/vulkan_core.h>

#include <cstddef>
#include <stdexcept>
#include <utility>
#include <vector>

VkDeviceSize
getRequiredBufferSize(VkDescriptorType type,
                      const ShaderInfo& info,
                      std::vector<VkDescriptorBufferInfo>* bufInfoOut,
                      U64 setBits)
{
    // Non-(uniform buffers) unsupported; at least for now.
    if (type != VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER) return 0;

    VkDeviceSize requiredBufSize = 0;
    for (const auto& desc : info.descriptors)
    {
        auto currSetBit = (size_t)1 << (size_t)desc.set;
        if (!(currSetBit & setBits)) continue; // Wrong set.
        if (desc.type != type) continue;

        // Assumption: desc.type == type == VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER
        auto currRequired = desc.size;
        if (currRequired % 16) currRequired += 16 - (currRequired % 16);
        if (bufInfoOut)
        {
            VkDescriptorBufferInfo info;
            info.buffer = VK_NULL_HANDLE;
            info.offset = requiredBufSize;
            info.range = currRequired;
            bufInfoOut->push_back(info);
        }
        requiredBufSize += currRequired;
    }

    return requiredBufSize;
}

MultiAllocatedBuffer&
MultiAllocatedBuffer::operator=(MultiAllocatedBuffer&& other) noexcept
{
    m_allocTypes = std::move(other.m_allocTypes);
    m_allocation = other.m_allocation;
    other.m_allocation.allocation = VK_NULL_HANDLE;
    m_allocatedBytes = other.m_allocatedBytes;
    m_descriptorType = other.m_descriptorType;
    m_allocator = other.m_allocator;
    return *this;
}

VkResult MultiAllocatedBuffer::construct(VmaAllocator allocator,
                                         VkDescriptorType type,
                                         size_t byteSize)
{
    destroyAllocation(m_allocation);
    m_allocator = allocator;
    m_usedBytes = 0;
    m_descriptorType = type;
    if (byteSize & 0xf) byteSize += 0x10 - (byteSize & 0xf);
    auto result = allocate(byteSize, m_allocation);
    if (result != VK_SUCCESS)
    {
        destroyAllocation(m_allocation);
        m_allocator = VK_NULL_HANDLE;
        return result;
    }
    m_allocatedBytes = byteSize;
    return VK_SUCCESS;
}

VkResult
MultiAllocatedBuffer::allocateBuffer(VkDeviceSize size,
                                     VkDeviceSize alignment,
                                     VkDescriptorBufferInfo& bufInfoOut)
{
    auto begin = suballocate(size, alignment);
    if (begin >= m_allocatedBytes) return VK_ERROR_OUT_OF_POOL_MEMORY;
    bufInfoOut.buffer = m_allocation.buffer;
    bufInfoOut.offset = begin;
    bufInfoOut.range = size;
    return VK_SUCCESS;
}

void MultiAllocatedBuffer::freeBuffer(VkDescriptorBufferInfo bufInfo)
{
    if (bufInfo.buffer != m_allocation.buffer)
    {
        // At least when debugging, it would be a good idea to throw if
        // someone tries to free a buffer that isn't allocated from here, or
        // whose information is otherwise corrupted.
        throw std::logic_error("Unrecognised buffer (memory corruption?).");
    }
    auto it = m_allocatedRanges.find({bufInfo.offset,
                                      bufInfo.offset+bufInfo.range});
    if (it == m_allocatedRanges.end())
    {
        throw std::logic_error("Double free or corruption (slowtop).");
    }

    // In case this is the last buffer, we can move m_allocatedBytes back to
    // the beginning of this allocation so new allocations can happen from
    // there instead of having to search for free space.
    auto nextIt = it;
    ++nextIt;
    if (nextIt == m_allocatedRanges.end())
    {
        m_usedBytes = it->from;
    }

    // Oh... and actually delete the allocated range to mark the memory as
    // freed.
    m_allocatedRanges.erase(it);
}

VkDeviceSize MultiAllocatedBuffer::suballocate(VkDeviceSize size,
                                               VkDeviceSize alignment)
{
    auto alignMask = alignment-1;
    VkDeviceSize begin = m_allocatedBytes;
    if ((begin = align(m_usedBytes, alignment, alignMask)) + size
        < m_allocatedBytes)
    {
        m_allocatedRanges.insert({begin, begin+size});
        m_usedBytes = begin+size;
        return begin;
    }
    // Slow, linear allocator... Maybe replace with a multimap mapping a
    // free size to the locations in which they are ...
    for (auto it = m_allocatedRanges.begin();
         it != m_allocatedRanges.end();
         ++it)
    {
        auto nextIt = it;
        ++nextIt;
        if (nextIt == m_allocatedRanges.end()) continue;
        begin = align(it->to, alignment, alignMask);
        auto to = nextIt->from;
        if (to - begin >= size)
        {
            m_allocatedRanges.insert({begin, to});
            return begin;
        }
    }
    return m_allocatedBytes;
}

VkResult MultiAllocatedBuffer::allocate(VkDeviceSize size,
                                        BufferAllocation& allocOut)
{
    if (m_descriptorType == VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER
        || m_descriptorType == VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC)
    {
        return allocate(size,
                        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                        VMA_MEMORY_USAGE_CPU_TO_GPU,
                        allocOut);
    }
    else if (m_descriptorType == VK_DESCRIPTOR_TYPE_STORAGE_BUFFER
             || m_descriptorType == VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC)
    {
        throw std::logic_error("Storage buffers not yet implemented!");
    }
    else return VK_ERROR_FORMAT_NOT_SUPPORTED;
}

VkResult MultiAllocatedBuffer::allocate(VkDeviceSize size,
                                        VkBufferUsageFlags flags,
                                        VmaMemoryUsage usage,
                                        BufferAllocation& allocOut)
{
   VkBufferCreateInfo bufCreate{};
   bufCreate.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
   bufCreate.size = size;
   bufCreate.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
   bufCreate.usage = flags;

   VmaAllocationCreateInfo vmaInfo{};
   vmaInfo.usage = usage;

   auto result = vmaCreateBuffer(m_allocator,
                                 &bufCreate,
                                 &vmaInfo,
                                 &allocOut.buffer,
                                 &allocOut.allocation,
                                 nullptr);
   if (result == VK_SUCCESS)
   {
       vmaSetAllocationName(m_allocator,
                            allocOut.allocation,
                            "MultiAllocatedBuffer");
   }
   return result;
}

void MultiAllocatedBuffer::destroyAllocation(BufferAllocation& alloc)
{
    if (alloc.allocation != VK_NULL_HANDLE)
    {
        vmaDestroyBuffer(m_allocator,
                         alloc.buffer,
                         alloc.allocation);
        alloc.allocation = VK_NULL_HANDLE;
    }
}
