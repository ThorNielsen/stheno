// VMA_IMPLEMENTATION must be defined in exactly one source file, before
// inclusion of the header itself. Thus, it is placed at the very top of this
// file.

#if defined(__clang__)
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Weverything"
#elif defined(__GNUC__) || defined(__GNUG__)
    // Weirdly it appears one cannot disable all warnings in GCC, which is
    // really nice when dealing with external code...
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wmissing-field-initializers"
    #pragma GCC diagnostic ignored "-Wunused-parameter"
    #pragma GCC diagnostic ignored "-Wtype-limits"
    #pragma GCC diagnostic ignored "-Wunused-variable"
    #pragma GCC diagnostic ignored "-Wunused-function"
#endif

#define VMA_IMPLEMENTATION

#include "vulkan/vma.hpp"

#if defined(__clang__)
    #pragma clang diagnostic pop
#elif defined(__GNUC__) || defined(__GNUG__)
    #pragma GCC diagnostic pop
#endif
