#include "vulkan/utility.hpp"

#include <kraken/globaltypes.hpp>

#include <vulkan/vulkan_core.h>

#include <stdexcept>
#include <string>

U32 getRequiredPadding(U32 currAlignment, ShaderDataFormat fmt)
{
    auto align = getAlignment(fmt);
    return currAlignment % align ? (align - currAlignment % align) : 0;
}

bool parseFormat(const std::string& fmt, VkFormat& formatOut)
{
    if (fmt == "vec3") formatOut = VK_FORMAT_R32G32B32_SFLOAT;
    else if (fmt == "uvec3") formatOut = VK_FORMAT_R32G32B32_UINT;
    else if (fmt == "ivec3") formatOut = VK_FORMAT_R32G32B32_SINT;
    else if (fmt == "vec4") formatOut = VK_FORMAT_R32G32B32A32_SFLOAT;
    else if (fmt == "uvec4") formatOut = VK_FORMAT_R32G32B32A32_UINT;
    else if (fmt == "ivec4") formatOut = VK_FORMAT_R32G32B32A32_SINT;
    else if (fmt == "vec2") formatOut = VK_FORMAT_R32G32_SFLOAT;
    else if (fmt == "uvec2") formatOut = VK_FORMAT_R32G32_UINT;
    else if (fmt == "ivec2") formatOut = VK_FORMAT_R32G32_SINT;

    else throw std::runtime_error("Probably unimplemented format?");

    return true;
}

bool parseFormat(const std::string& fmt, ShaderDataFormat& formatOut)
{
    if (fmt == "float") formatOut = ShaderDataFormat::Float;
    else if (fmt == "double") formatOut = ShaderDataFormat::Double;
    else if (fmt == "int") formatOut = ShaderDataFormat::Int;
    else if (fmt == "uint") formatOut = ShaderDataFormat::UInt;

    else if (fmt == "vec2") formatOut = ShaderDataFormat::Vec2;
    else if (fmt == "dvec2") formatOut = ShaderDataFormat::DVec2;
    else if (fmt == "ivec2") formatOut = ShaderDataFormat::IVec2;
    else if (fmt == "uvec2") formatOut = ShaderDataFormat::UVec2;

    else if (fmt == "vec3") formatOut = ShaderDataFormat::Vec3;
    else if (fmt == "dvec3") formatOut = ShaderDataFormat::DVec3;
    else if (fmt == "ivec3") formatOut = ShaderDataFormat::IVec3;
    else if (fmt == "uvec3") formatOut = ShaderDataFormat::UVec3;

    else if (fmt == "vec4") formatOut = ShaderDataFormat::Vec4;
    else if (fmt == "dvec4") formatOut = ShaderDataFormat::DVec4;
    else if (fmt == "ivec4") formatOut = ShaderDataFormat::IVec4;
    else if (fmt == "uvec4") formatOut = ShaderDataFormat::UVec4;

    else if (fmt == "mat2") formatOut = ShaderDataFormat::Mat2;
    else if (fmt == "dmat2") formatOut = ShaderDataFormat::DMat2;
    else if (fmt == "imat2") formatOut = ShaderDataFormat::IMat2;
    else if (fmt == "umat2") formatOut = ShaderDataFormat::UMat2;

    else if (fmt == "mat3") formatOut = ShaderDataFormat::Mat3;
    else if (fmt == "dmat3") formatOut = ShaderDataFormat::DMat3;
    else if (fmt == "imat3") formatOut = ShaderDataFormat::IMat3;
    else if (fmt == "umat3") formatOut = ShaderDataFormat::UMat3;

    else if (fmt == "mat4") formatOut = ShaderDataFormat::Mat4;
    else if (fmt == "dmat4") formatOut = ShaderDataFormat::DMat4;
    else if (fmt == "imat4") formatOut = ShaderDataFormat::IMat4;
    else if (fmt == "umat4") formatOut = ShaderDataFormat::UMat4;

    else return false;

    return true;
}
