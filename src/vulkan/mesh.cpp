#include "vulkan/mesh.hpp"
#include "vulkan/shader.hpp"
#include "vulkan/types.hpp"

#include <cstring>

MeshBuffer::MeshBuffer(MeshBuffer&& other) noexcept
{
    *this = std::move(other);
}

MeshBuffer& MeshBuffer::operator=(MeshBuffer&& other) noexcept
{
    if (&other == this) return *this;
    destroy();
    deviceBuffer = std::move(other.deviceBuffer);
    allocator = other.allocator;
    buffer = other.buffer;
    deviceAllocationSize = other.deviceAllocationSize;
    hostAllocationSize = other.hostAllocationSize;
    stride = other.stride;
    count = other.count;


    other.deviceBuffer.allocation = VK_NULL_HANDLE;
    other.deviceBuffer.buffer = VK_NULL_HANDLE;
    other.allocator = VK_NULL_HANDLE;
    other.buffer = nullptr;

    other.destroy();

    return *this;
}

MeshBuffer::~MeshBuffer()
{
    destroy();
}

void MeshBuffer::destroy() noexcept
{
    if (deviceBuffer.buffer != VK_NULL_HANDLE && allocator != VK_NULL_HANDLE)
    {
        deviceBuffer.destroy(allocator);
    }
    deviceBuffer.allocation = VK_NULL_HANDLE;
    deviceBuffer.buffer = VK_NULL_HANDLE;
    allocator = VK_NULL_HANDLE;
    if (buffer) delete[] static_cast<U8*>(buffer);
    buffer = nullptr;
    deviceAllocationSize = 0;
    hostAllocationSize = 0;
    stride = 0;
    count = 0;
}

void MeshBuffer::reserveHostMemory(U64 bytes)
{
    U8* newBuf = nullptr;
    if (bytes) newBuf = new U8[bytes];
    if (hostAllocationSize && bytes && buffer)
    {
        std::memcpy(newBuf, buffer, std::min(bytes, hostAllocationSize));
    }
    if (buffer) delete[] static_cast<U8*>(buffer);
    buffer = newBuf;
    hostAllocationSize = bytes;
}

Mesh::Mesh()
{
    m_vertexBuffers = m_vbufStorage;
    m_indexBuffers = m_ibufStorage;
    m_bindings = m_bindingStorage;

    m_vertexBufferCount = 0;
    m_indexBufferCount = 0;
}

Mesh& Mesh::operator=(Mesh&& other) noexcept
{
    if (&other == this) return *this;
    destroy();
    moveIntoThis(std::move(other));
    return *this;
}

Mesh::~Mesh()
{
    destroy();
}


namespace
{

template <typename T>
void destroyBuffer(T& obj)
{
    obj.destroy();
}

template <>
void destroyBuffer<VkVertexInputBindingDescription>(VkVertexInputBindingDescription&)
{}

template <typename T>
void resizeBuffers(U32 oldCount, U32 newCount,
                   T*& buffer, U32 preallocatedBufs,
                   T* bufStorage)
{
    if (newCount == oldCount) return;

    // Destroy any extra buffers.
    if (newCount < oldCount)
    {
        for (U32 i = newCount; i < oldCount; ++i)
        {
            destroyBuffer(buffer[i]);
        }
    }

    // Reserve storage -- fit inside the preallocated ones if possible.
    T* newBuffers = bufStorage;
    if (newCount > preallocatedBufs)
    {
        newBuffers = new T[newCount];
    }

    // Move old buffers.
    for (U32 i = 0; i < oldCount; ++i)
    {
        // This does not throw -- guaranteed from move assignment.
        newBuffers[i] = std::move(buffer[i]);
    }

    // Delete any old buffers and assign them to the newly constructed buffers.
    if (oldCount > preallocatedBufs) delete[] buffer;
    buffer = newBuffers;
}

template <typename T>
void destroy(T*& buf, T* preallocated, U32 count) noexcept
{
    if (buf != preallocated)
    {
        delete[] buf;
        buf = preallocated;
    }
    else
    {
        for (U32 i = 0; i < count; ++i)
        {
            destroyBuffer(buf[i]);
        }
    }
}

} // end anonymous namespace

void Mesh::destroy() noexcept
{
    ::destroy(m_vertexBuffers, m_vbufStorage, m_vertexBufferCount);
    ::destroy(m_indexBuffers, m_ibufStorage, m_indexBufferCount);
    ::destroy(m_bindings, m_bindingStorage, m_vertexBufferCount);
    m_vertexBufferCount = 0;
    m_indexBufferCount = 0;
}

void Mesh::resizeVertexBuffers(U32 count)
{
    resizeBuffers(m_vertexBufferCount, count,
                  m_vertexBuffers, preallocatedVertexBufs,
                  m_vbufStorage);
    // If an exception is thrown, then the object is in inconsistent state...
    // but then we have severe problems as there isn't even enough memory for
    // 'count' more VkVertexInputBindingDescription structures. TODO: Fix this.
    // However, it is extremely unlikely, and probably not worth spending time
    // on.
    resizeBuffers(m_vertexBufferCount, count,
                  m_bindings, preallocatedVertexBufs,
                  m_bindingStorage);
    m_vertexBufferCount = count;
}

void Mesh::resizeIndexBuffers(U32 count)
{
    resizeBuffers(m_indexBufferCount, count,
                  m_indexBuffers, preallocatedIndexBufs,
                  m_ibufStorage);
    m_indexBufferCount = count;
}

void Mesh::moveIntoThis(Mesh&& other) noexcept
{
    for (U32 i = 0; i < preallocatedVertexBufs; ++i)
    {
        m_vbufStorage[i] = std::move(other.m_vbufStorage[i]);
        m_bindingStorage[i] = std::move(other.m_bindingStorage[i]);
    }
    for (U32 i = 0; i < preallocatedIndexBufs; ++i)
    {
        m_ibufStorage[i] = std::move(other.m_ibufStorage[i]);
    }
    m_vertexBuffers = other.m_vertexBuffers;
    m_indexBuffers = other.m_indexBuffers;
    m_bindings = other.m_bindings;

    other.m_vertexBuffers = other.m_vbufStorage;
    other.m_indexBuffers = other.m_ibufStorage;
    other.m_bindings = other.m_bindingStorage;

    m_attributes = std::move(other.m_attributes);

    m_vertexBufferCount = other.m_vertexBufferCount;
    m_indexBufferCount = other.m_indexBufferCount;

    other.m_vertexBufferCount = 0;
    other.m_indexBufferCount = 0;

    if (m_vertexBufferCount <= preallocatedVertexBufs)
    {
        m_vertexBuffers = m_vbufStorage;
        m_bindings = m_bindingStorage;
    }
    if (m_indexBufferCount <= preallocatedIndexBufs)
    {
        m_indexBuffers = m_ibufStorage;
    }
}

struct InputAttribute
{
    U32 location;
    VkFormat format;
};

auto getShaderInputSpec(const ShaderInfo& shaderInfo)
{
    std::map<std::string, InputAttribute> attrs;
    for (const auto& elem : shaderInfo.attributes)
    {
        // We ignore built-ins which we cannot set anyway.
        if (!elem.name.compare(0, 3, "gl_")) continue;

        if (!attrs.emplace(elem.name, InputAttribute{elem.location, elem.format}).second)
        {
            std::cerr << "Warning: Duplicate attribute " << elem.name
                      << " in shader.\n";
        }
    }
    return attrs;
}

std::vector<VkVertexInputAttributeDescription>
getAttributeDescriptions(const NamedMeshAttributeElement* pAttributes,
                         VkDeviceSize attrCounts,
                         const ShaderInfo& shaderInfo)
{
    auto spec = getShaderInputSpec(shaderInfo);

    std::vector<VkVertexInputAttributeDescription> attrDescs;
    for (size_t k = 0; k < attrCounts; ++k)
    {
        VkVertexInputAttributeDescription desc;
        const auto& [attr, name] = pAttributes[k];
        desc.format = attr.format;
        desc.offset = attr.offset;
        desc.binding = 0;
        auto it = spec.find(name);
        if (it == spec.end())
        {
            std::cerr << "Debug: Skipping superfluous element "
                      << name << " in mesh as shader doesn't use it.\n";
            continue;
        }
        desc.location = it->second.location;
        if (it->second.format != attr.format)
        {
            std::cerr << "Warning: Format mismatchm, got "
                      << toString(attr.format)
                      << " but shader expects "
                      << toString(it->second.format)
                      << "\n";
        }
        attrDescs.push_back(desc);
    }

    // Nullopt instead? Or warning or something?
    if (attrDescs.size() != spec.size())
    {
        throw std::runtime_error("Got wrong number of attribute descriptions.");
    }

    return attrDescs;
}
