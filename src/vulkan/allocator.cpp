#include "vulkan/allocator.hpp"

#include "vulkan/vma.hpp"

#include <vulkan/vulkan_core.h>

void BufferAllocation::destroy(VmaAllocator allocator)
{
    if (allocator && buffer)
    {
        vmaDestroyBuffer(allocator, buffer, allocation);
    }
    allocation = VK_NULL_HANDLE;
    buffer = VK_NULL_HANDLE;
}
