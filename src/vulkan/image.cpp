#include "vulkan/image.hpp"

#include "vulkan/vma.hpp"

#include <cstring>
#include <kraken/globaltypes.hpp>
#include <stdexcept>
#include <utility>
#include <vulkan/vulkan_core.h>

bool hasDepthComponent(VkFormat format)
{
    switch (format)
    {
    case VK_FORMAT_X8_D24_UNORM_PACK32:
    case VK_FORMAT_D16_UNORM:
    case VK_FORMAT_D16_UNORM_S8_UINT:
    case VK_FORMAT_D24_UNORM_S8_UINT:
    case VK_FORMAT_D32_SFLOAT:
    case VK_FORMAT_D32_SFLOAT_S8_UINT:
        return true;
    default:
        return false;
    }
}

bool hasStencilComponent(VkFormat format)
{
    switch (format)
    {
    case VK_FORMAT_S8_UINT:
    case VK_FORMAT_D16_UNORM_S8_UINT:
    case VK_FORMAT_D24_UNORM_S8_UINT:
    case VK_FORMAT_D32_SFLOAT_S8_UINT:
        return true;
    default:
        return false;
    }
}

VkImageViewCreateInfo Image::viewCreateInfo(VkFormat format) const
{
    VkImageViewCreateInfo textureViewCreate{};
    textureViewCreate.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    textureViewCreate.image = image;
    textureViewCreate.format = format;
    textureViewCreate.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    textureViewCreate.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    textureViewCreate.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    textureViewCreate.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    textureViewCreate.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    textureViewCreate.subresourceRange.baseArrayLayer = 0;
    textureViewCreate.subresourceRange.baseMipLevel = 0;
    textureViewCreate.subresourceRange.layerCount = layerCount;
    textureViewCreate.subresourceRange.levelCount = mipmapCount;

    auto& aspect = textureViewCreate.subresourceRange.aspectMask;
    if (hasDepthComponent(format) || hasStencilComponent(format))
    {
        aspect = 0;
        if (hasDepthComponent(format))
        {
            aspect |= VK_IMAGE_ASPECT_DEPTH_BIT;
        }
        if (hasStencilComponent(format))
        {
            aspect |= VK_IMAGE_ASPECT_STENCIL_BIT;
        }
    }

    if (dimensions == 1)
    {
        textureViewCreate.viewType = layerCount > 1 ? VK_IMAGE_VIEW_TYPE_1D_ARRAY
                                                    : VK_IMAGE_VIEW_TYPE_1D;
    }
    else if (dimensions == 2)
    {
        textureViewCreate.viewType = layerCount > 1 ? VK_IMAGE_VIEW_TYPE_2D_ARRAY
                                                    : VK_IMAGE_VIEW_TYPE_2D;
    }
    else if (dimensions == 3)
    {
        textureViewCreate.viewType = VK_IMAGE_VIEW_TYPE_3D;
    }
    return textureViewCreate;
}

VkResult Image::allocate(const VkImageCreateInfo& createInfo,
                         U32 dimensionCount,
                         VmaAllocator allocator_,
                         VkDevice device_,
                         bool createView)
{
    allocator = allocator_;
    device = device_;
    extent = createInfo.extent;
    mipmapCount = createInfo.mipLevels;
    layerCount = createInfo.arrayLayers;
    dimensions = dimensionCount;

    VmaAllocationCreateInfo imgAllocInfo{};
    imgAllocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

    auto result = vmaCreateImage(allocator, &createInfo, &imgAllocInfo,
                                 &image, &alloc, nullptr);
    if (result != VK_SUCCESS) return result;

    vmaSetAllocationName(allocator, alloc, "Image creation");

    if (!createView) return VK_SUCCESS;

    auto viewCreate = viewCreateInfo(createInfo.format);
    result = vkCreateImageView(device, &viewCreate, nullptr, &view);
    if (result != VK_SUCCESS)
    {
        vmaDestroyImage(allocator, image, alloc);
        return result;
    }
    return result;
}

void Image::destroy(VmaAllocator allocator_, VkDevice device_, bool destroyView)
{
    if (destroyView && view != VK_NULL_HANDLE)
    {
        vkDestroyImageView(device_, view, nullptr);
        view = VK_NULL_HANDLE;
    }
    if (image != VK_NULL_HANDLE)
    {
        vmaDestroyImage(allocator_, image, alloc);
        image = VK_NULL_HANDLE;
        alloc = VK_NULL_HANDLE;
    }
}

void Image::destroy()
{
    if (allocator != VK_NULL_HANDLE && device != VK_NULL_HANDLE)
    {
        destroy(allocator, device);
    }
}

VkImageSubresourceRange Image::defaultSubresourceRange() const
{
    VkImageSubresourceRange srange;
    srange.baseMipLevel = 0;
    srange.levelCount = mipmapCount;
    srange.baseArrayLayer = 0;
    srange.layerCount = layerCount;
    srange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    return srange;
}


bool operator==(const VkSamplerCreateInfo& a, const VkSamplerCreateInfo& b)
{
    return !std::memcmp(&a, &b, sizeof(a));
}

SamplerCache::SamplerCache()
{
    resetVariables();
}

SamplerCache::SamplerCache(VkDevice device)
{
    resetVariables();
    if (create(device) != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create sampler cache.");
    }
}

SamplerCache::SamplerCache(SamplerCache&& other)
{
    resetVariables();
    (*this) = std::move(other);
}

SamplerCache::~SamplerCache()
{
    destroy();
}

SamplerCache& SamplerCache::operator=(SamplerCache&& other)
{
    destroy();
    m_samplers = std::move(other.m_samplers);
    m_device = other.m_device;
    other.resetVariables();
    return *this;
}

VkResult SamplerCache::create(VkDevice device)
{
    resetVariables();
    m_device = device;
    return VK_SUCCESS;
}

void SamplerCache::destroy()
{
    for (auto& sampler : m_samplers)
    {
        vkDestroySampler(m_device, sampler.second, nullptr);
    }
    resetVariables();
}

VkResult SamplerCache::getSampler(VkSampler* sampler,
                                  VkFilter magFilter,
                                  VkFilter minFilter,
                                  VkSamplerMipmapMode mipmapMode,
                                  VkSamplerAddressMode addressMode,
                                  float maxAnisotropy)
{
    VkSamplerCreateInfo samplerCreate{};
    samplerCreate.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    samplerCreate.magFilter = magFilter;
    samplerCreate.minFilter = minFilter;
    samplerCreate.mipmapMode = mipmapMode;
    samplerCreate.addressModeU = addressMode;
    samplerCreate.addressModeV = addressMode;
    samplerCreate.addressModeW = addressMode;
    samplerCreate.mipLodBias = 0.f;
    samplerCreate.anisotropyEnable = maxAnisotropy > 0;
    samplerCreate.compareEnable = VK_FALSE;
    samplerCreate.minLod = 0.f;
    samplerCreate.maxLod = VK_LOD_CLAMP_NONE;
    samplerCreate.unnormalizedCoordinates = VK_FALSE;
    return getSampler(sampler, samplerCreate);
}

VkResult SamplerCache::getSampler(VkSampler* sampler,
                                  const VkSamplerCreateInfo& samplerCreate)
{
    auto at = m_samplers.find(samplerCreate);
    if (at != m_samplers.end())
    {
        *sampler = at->second;
        return VK_SUCCESS;
    }
    auto result = vkCreateSampler(m_device, &samplerCreate, nullptr, sampler);
    if (result == VK_SUCCESS)
    {
        m_samplers[samplerCreate] = *sampler;
    }
    return result;
}

void SamplerCache::resetVariables()
{
    m_samplers.clear();
    m_device = VK_NULL_HANDLE;
}
