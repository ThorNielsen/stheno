#include "resourceparser.hpp"
#include <kraken/image/image.hpp>
#include <kraken/graphics/import.hpp>
#include <kraken/math/matrix.hpp>
#include <kraken/utility/memory.hpp>
#include <kraken/utility/range.hpp>

#include "utility/meshloader.hpp"

#include "mikktspace.h"

#include "vulkan/types.hpp"

#include <cstring>

void ResourceParser::getAdditionalRequiredFiles(ResourceLoader::FileInfo*,
                                                U32* pathCount,
                                                ResourceLoader&,
                                                const ResourceLoader::RawData&)
{
    if (pathCount) *pathCount = 0;
}

bool ResourceParser::parse(const U8*, size_t, const kraken::JSONValue&,
                           ResourceLoader&,
                           ResourceLoader::HostResource*)
{
    throw std::logic_error("Parse function called on a resource parser without "
                           "an implementation of that function.");
}

bool ResourceParser::getNextTransferData(ResourceLoader::DataSource,
                                         VkDeviceSize, const U8**,
                                         size_t*, const void**, U32*)

{
    throw std::logic_error("getNextTransferData() called on a resource parser "
                           "without an implementation of that function.");
}

VkResult ResourceParser::initialiseTransfer(VkCommandBuffer,
                                            VkDevice,
                                            VmaAllocator,
                                            const kraken::JSONValue&,
                                            ResourceLoader::DataSource,
                                            ResourceLoader::VulkanResource*)
{
    return VK_SUCCESS;
}

VkResult ResourceParser::transfer(VkCommandBuffer, VkBuffer, VkDeviceSize,
                                  VkDeviceSize, const void*, U32,
                                  ResourceLoader::VulkanResource*)
{
    throw std::logic_error("Transfer function called on a resource parser "
                           "without an implementation of that function.");
}

VkResult ResourceParser::finaliseTransfer(VkCommandBuffer,
                                          VkDevice,
                                          VmaAllocator,
                                          ResourceLoader::VulkanResource*)
{
    return VK_SUCCESS;
}

void ResourceParser::getCurrentResourceInfo(ResourceLoader& loader,
                                            ResourceLoader::ResourceID* id,
                                            const ResourceLoader::Options** options,
                                            const std::string** mediaType,
                                            const fs::path** path) const
{
    loader.getCurrentResourceInfo(id, options, mediaType, path);
}



VkResult ImageTransferrer::initialiseTransfer(VkCommandBuffer commandBuf,
                                              VkDevice device,
                                              VmaAllocator allocator,
                                              const kraken::JSONValue& options,
                                              ResourceLoader::DataSource source,
                                              ResourceLoader::VulkanResource* resourceOut)
{
    if (!mp_width) readImageData(source);

    VkImageCreateInfo imgCreate{};
    imgCreate.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imgCreate.imageType = VK_IMAGE_TYPE_2D;
    imgCreate.extent.width = mp_width;
    imgCreate.extent.height = mp_height;
    imgCreate.extent.depth = 1;
    imgCreate.mipLevels = 1;
    imgCreate.arrayLayers = 1;
    imgCreate.samples = VK_SAMPLE_COUNT_1_BIT;
    imgCreate.tiling = VK_IMAGE_TILING_OPTIMAL;
    if (options("image")("usageFlags").type() == kraken::JSONType::Array)
    {
        VkImageUsageFlags flag;
        for (const auto& jval : options("image")("usageFlags").asArray())
        {
            if (toVkImageUsageFlags((std::string)jval, &flag))
            {
                imgCreate.usage |= flag;
            }
        }
    }
    imgCreate.usage |= VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    imgCreate.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    imgCreate.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imgCreate.format = VK_FORMAT_R8G8B8A8_SRGB;
    toVkFormat((std::string)options("image")("format"), &imgCreate.format);
    auto result = resourceOut->image.allocate(imgCreate, 2, allocator, device, true);
    if (result != VK_SUCCESS)
    {
        return result;
    }
    resourceOut->image.accessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    resourceOut->image.layout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;

    resourceOut->available |= ResourceLoader::VulkanResource::Type::Image;

    auto subresourceRange = resourceOut->image.defaultSubresourceRange();

    VkImageMemoryBarrier barrier{};
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.srcAccessMask = 0;
    barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    barrier.image = resourceOut->image.image;
    barrier.subresourceRange = subresourceRange;

    vkCmdPipelineBarrier(commandBuf, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                         VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0,
                         nullptr, 0, nullptr, 1, &barrier);

    return VK_SUCCESS;
}

bool ImageTransferrer::getNextTransferData(ResourceLoader::DataSource source,
                                           VkDeviceSize maxSize,
                                           const U8** dataOut,
                                           size_t* sizeOut,
                                           const void** tagOut,
                                           U32*)
{
    if (!mp_width) readImageData(source);
    if (maxSize % mp_rowLength)
    {
        maxSize -= (maxSize % mp_rowLength);
    }

    auto totalBytes = mp_rowLength * mp_height;
    auto currOffset = mp_rowLength * m_currLine;
    if (maxSize > totalBytes - currOffset)
    {
        maxSize = totalBytes - currOffset;
    }
    m_currLine += maxSize / mp_rowLength;
    *dataOut = mp_data + currOffset;
    *sizeOut = maxSize;
    *tagOut = *dataOut;

    bool moreData = m_currLine != mp_height;
    return moreData;
}

VkResult ImageTransferrer::transfer(VkCommandBuffer commandBuf,
                                    VkBuffer buffer,
                                    VkDeviceSize bufferOffset,
                                    VkDeviceSize bufferSize,
                                    const void* tag,
                                    U32,
                                    ResourceLoader::VulkanResource* resourceOut)
{
    auto dataOffset = std::distance<const U8*>(mp_data,
                                               static_cast<const U8*>(tag));
    U32 startLine = 0;
    if (dataOffset > 0)
    {
        if (dataOffset % mp_rowLength)
        {
            throw std::logic_error("Data offset is guaranteed to be "
                                   "divisible by row length, but that is "
                                   "not the case.");
        }
        startLine = dataOffset / mp_rowLength;
    }
    if (bufferSize % mp_rowLength)
    {
        throw std::logic_error("Data offset is guaranteed to be divisible "
                               "by row length, but that is not the case.");
    }
    U32 lineCount = bufferSize / mp_rowLength;
    VkExtent3D imgExtent = {mp_width, lineCount, 1};
    VkBufferImageCopy imgCopy{};
    imgCopy.bufferOffset = bufferOffset;
    imgCopy.bufferImageHeight = 0;
    imgCopy.bufferRowLength = 0;
    imgCopy.imageOffset = {0, (S32)startLine, 0};
    imgCopy.imageExtent = imgExtent;
    auto& imgSR = imgCopy.imageSubresource;
    imgSR.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    imgSR.baseArrayLayer = 0;
    imgSR.layerCount = 1;
    imgSR.mipLevel = 0;
    vkCmdCopyBufferToImage(commandBuf, buffer, resourceOut->image.image,
                           VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                           1, &imgCopy);
    return VK_SUCCESS;
}

namespace
{

const static std::function<void(void*)> gs_imgDeleter = [](void* img)
{
    if (img)
    {
        delete static_cast<kraken::image::Image*>(img);
    }
};

const static std::function<void(void*)> gs_meshDeleter = [](void* mesh)
{
    if (mesh)
    {
        delete static_cast<Mesh*>(mesh);
    }
};

} // end anonymous namespace


bool ImageParser::parse(const U8* data, size_t size,
                        const kraken::JSONValue&,
                        ResourceLoader&,
                        ResourceLoader::HostResource* resourceOut)
{
    auto image = std::make_unique<kraken::image::Image>();
    MemoryIStream memstream(data, size);
    auto result = image->read(memstream);
    if (!result) return false;

    /// TODO: Add option to change this behaviour -- it is justified here as
    /// usually one wants to convert to a four-channel RGBA image in any case
    /// to use on GPU.

    *image = image->convert(kraken::image::RGBA8);

    resourceOut->deleter = &gs_imgDeleter;
    resourceOut->resource = m_image = image.release();
    resourceOut->typeName = "kraken::image::Image";
    resourceOut->typeIdentifier = typeid(kraken::image::Image).hash_code();

    return true;
}

void ImageParser::readImageData(ResourceLoader::DataSource source)
{
    if (source.size) throw std::logic_error("Byte data given (expected kraken::image::Image*).");
    if (!source.data) throw std::logic_error("Data source pointer invalid.");
    auto image = static_cast<const kraken::image::Image*>(source.data);
    mp_width = image->width();
    mp_height = image->height();
    mp_data = image->data();
    mp_rowLength = image->rowBytes();
}

VkResult MeshTransferrer::initialiseTransfer(VkCommandBuffer,
                                             VkDevice,
                                             VmaAllocator allocator,
                                             const kraken::JSONValue&,
                                             ResourceLoader::DataSource,
                                             ResourceLoader::VulkanResource* resourceOut)
{
    if (!mp_mesh)
    {
        throw std::logic_error("Mesh is nullptr.");
    }
    VkBufferCreateInfo bufCreate{};
    bufCreate.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufCreate.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    VmaAllocationCreateInfo allocInfo{};
    allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
    for (U32 i = 0; i < mp_mesh->vertexBufferCount() + mp_mesh->indexBufferCount(); ++i)
    {
        MeshBuffer* meshBuffer = nullptr;
        if (i < mp_mesh->vertexBufferCount())
        {
            bufCreate.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
            meshBuffer = &mp_mesh->vertexBuffer(i);
        }
        else
        {
            bufCreate.usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
            meshBuffer = &mp_mesh->indexBuffer(i-mp_mesh->vertexBufferCount());
        }
        auto size = meshBuffer->count * meshBuffer->stride;
        bufCreate.size = size;
        auto result = vmaCreateBuffer(allocator, &bufCreate, &allocInfo,
                                      &meshBuffer->deviceBuffer.buffer,
                                      &meshBuffer->deviceBuffer.allocation,
                                      nullptr);
        if (result != VK_SUCCESS)
        {
            return result;
        }
        vmaSetAllocationName(allocator,
                             meshBuffer->deviceBuffer.allocation,
                             "Mesh buffer created in initialiseTransfer");

        meshBuffer->allocator = allocator;
        meshBuffer->deviceAllocationSize = size;
    }
    resourceOut->mesh = mp_mesh;

    return VK_SUCCESS;
}

bool MeshTransferrer::getNextTransferData(ResourceLoader::DataSource source,
                                          VkDeviceSize maxSize,
                                          const U8** dataOut,
                                          size_t* sizeOut,
                                          const void** tagOut,
                                          U32* extraTagOut)
{
    if (source.size) throw std::logic_error("Unexpected byte data.");
    if (!source.data) throw std::logic_error("Expected const Mesh*.");
    const Mesh* mesh = static_cast<const Mesh*>(source.data);
    const MeshBuffer* currBuffer = nullptr;
    if (m_currId < mesh->vertexBufferCount())
    {
        currBuffer = &mesh->vertexBuffer(m_currId);
    }
    else
    {
        currBuffer = &mesh->indexBuffer(m_currId - mesh->vertexBufferCount());
    }
    if (!m_currPtr)
    {
        m_currPtr = static_cast<const U8*>(currBuffer->buffer);
    }
    size_t dataLeft = currBuffer->hostAllocationSize
                      - std::distance(static_cast<const U8*>(currBuffer->buffer),
                                      m_currPtr);

    maxSize = std::min(maxSize, dataLeft);

    if (!currBuffer->stride)
    {
        throw std::logic_error("Invalid buffer stride (0 bytes).");
    }

    if (maxSize % currBuffer->stride)
    {
        maxSize -= (maxSize % currBuffer->stride);
    }

    *dataOut = m_currPtr;
    *sizeOut = maxSize;
    *tagOut = m_currPtr;
    *extraTagOut = m_currId;

    if (maxSize == dataLeft)
    {
        ++m_currId;
        m_currPtr = nullptr;
    }
    else
    {
        m_currPtr += maxSize;
    }

    return m_currId < mesh->vertexBufferCount() + mesh->indexBufferCount();
}

VkResult MeshTransferrer::transfer(VkCommandBuffer commandBuf,
                                   VkBuffer buffer,
                                   VkDeviceSize bufferOffset,
                                   VkDeviceSize bufferSize,
                                   const void* tag,
                                   U32 extraTag,
                                   ResourceLoader::VulkanResource*)
{
    const MeshBuffer* meshbuf = nullptr;
    if (extraTag < mp_mesh->vertexBufferCount())
    {
        meshbuf = &mp_mesh->vertexBuffer(extraTag);
    }
    else
    {
        meshbuf = &mp_mesh->indexBuffer(extraTag - mp_mesh->vertexBufferCount());
    }
    auto dataOffset = std::distance(static_cast<const U8*>(meshbuf->buffer),
                                    static_cast<const U8*>(tag));
    VkBufferCopy bufCopy{};
    bufCopy.srcOffset = bufferOffset;
    bufCopy.size = bufferSize;
    bufCopy.dstOffset = dataOffset;

    vkCmdCopyBuffer(commandBuf, buffer, meshbuf->deviceBuffer.buffer,
                    1, &bufCopy);
    return VK_SUCCESS;
}

namespace
{

struct MeshInfo
{
    const void* vertices = nullptr;
    const void* indices = nullptr;

    // Write tangents to here. Note that while this is in mikktspace, it will
    // not write out the bitangent, but that can be computed as
    // bitangent = sign * cross(vertexNormal, tangent),
    // where the sign is not explicitly written. Instead, this stores the sign
    // implicitly, by setting the length to 2 for negative sign, i.e.:
    // length(tangent) == 2  =>  bitangent = -cross(vertexNormal, normalise(tangent))
    // length(tangent) == 1  =>  bitangent = cross(vertexNormal, tangent)
    // Observe that even though the sign is stored as the length, it does NOT
    // need to be applied to the tangent.
    // The tangents are written as vec3's.
    void* tangentOutStart = nullptr;
    U64 tangentOutStride = 0;
    U64 tangentOutOffset = 0;
    U32 vertexCount = 0;
    U64 vertexStride = 0; // In bytes.

    // All offsets are in bytes (!)
    U64 positionOffset = 0;
    U64 normalOffset = 0;
    U64 texCoordOffset = 0;
    U64 indexBW = 0;

    const float* getVertexPropertyStart(U64 face, U64 vertex,
                                        U64 propOffset) const
    {
        U64 vertexNumber = 3*face+vertex;
        const U8* ptr = nullptr;
        if (indexBW == 1)
        {
            ptr = reinterpret_cast<const U8*>(vertices)
                  + vertexStride*reinterpret_cast<const U8*>(indices)[vertexNumber];
        }
        else if (indexBW == 2)
        {
            ptr = reinterpret_cast<const U8*>(vertices)
                  + vertexStride*reinterpret_cast<const U16*>(indices)[vertexNumber];
        }
        else if (indexBW == 4)
        {
            ptr = reinterpret_cast<const U8*>(vertices)
                  + vertexStride*reinterpret_cast<const U32*>(indices)[vertexNumber];
        }
        else
        {
            ptr = reinterpret_cast<const U8*>(vertices) + vertexStride*vertexNumber;
        }
        return reinterpret_cast<const float*>(ptr+propOffset);
    }

    kraken::math::vec3 getPosition(U64 face, U64 vertex) const
    {
        const float* data = getVertexPropertyStart(face, vertex, positionOffset);
        return {data[0], data[1], data[2]};
    }

    kraken::math::vec3 getNormal(U64 face, U64 vertex) const
    {
        const float* data = getVertexPropertyStart(face, vertex, normalOffset);
        return {data[0], data[1], data[2]};
    }

    kraken::math::vec2 getTexCoord(U64 face, U64 vertex) const
    {
        const float* data = getVertexPropertyStart(face, vertex, texCoordOffset);
        return {data[0], data[1]};
    }

    void setTangent(U64 face, U64 vertex, kraken::math::vec3 tangent) const
    {
        U8* byteDataAt = static_cast<U8*>(tangentOutStart)
                         + (3 * face + vertex) * tangentOutStride
                         + tangentOutOffset;
        /*std::cerr << "Offset: " << (3 * face + vertex) * tangentOutStride
                     + tangentOutOffset << "\n";
        std::cin.get();*/
        *reinterpret_cast<kraken::math::vec3*>(byteDataAt) = tangent;
    }
};

extern "C"
{

int getNumFaces(const SMikkTSpaceContext* context)
{
    return static_cast<const MeshInfo*>(context->m_pUserData)->vertexCount / 3;
}

int getNumVerticesOfFace(const SMikkTSpaceContext*, const int)
{
    return 3;
}

void getPosition(const SMikkTSpaceContext* context, float fvPosOut[],
                 const int iFace, const int iVert)
{
    auto v = static_cast<const MeshInfo*>(context->m_pUserData)->getPosition(iFace, iVert);
    fvPosOut[0] = v[0];
    fvPosOut[1] = v[1];
    fvPosOut[2] = v[2];
}

void getNormal(const SMikkTSpaceContext* context, float fvPosOut[],
               const int iFace, const int iVert)
{
    auto v = static_cast<const MeshInfo*>(context->m_pUserData)->getNormal(iFace, iVert);
    fvPosOut[0] = v[0];
    fvPosOut[1] = v[1];
    fvPosOut[2] = v[2];
}

void getTexCoord(const SMikkTSpaceContext* context, float fvPosOut[],
                 const int iFace, const int iVert)
{
    auto v = static_cast<const MeshInfo*>(context->m_pUserData)->getTexCoord(iFace, iVert);
    fvPosOut[0] = v[0];
    fvPosOut[1] = v[1];
}

void setTSpaceBasic(const SMikkTSpaceContext* context, const float fvTangent[],
                    const float fSign, const int iFace, const int iVert)
{
    kraken::math::vec3 tangent{fvTangent[0], fvTangent[1], fvTangent[2]};
    if (fSign < 0) tangent *= 2.f;
    static_cast<const MeshInfo*>(context->m_pUserData)->setTangent(iFace, iVert, tangent);
}


} // end extern "C"

/*
// This will write out 3*vertexCount tangents (of type vec3) to the data pointed
// to by tangentOutStart (where each element has size tangentOutStride), and
// place them with an offset of tangentOutOffset within each of the elements.
// Note: It is legal (and required) to set indices to nullptr if the mesh is not
// indexed, but simply a triangle soup. Note that this will generate tangents
// for every vertex in every triangle, and thus NOT necessarily respect any
// indexing. Therefore, any index list must be regenerated.
// Futhermore note that any index list is assumed to be an unsigned integer
// consisting of either 1, 2 or 4 bytes (i.e. either U8, U16 or U32).
bool generateTangentsFromTriangularMesh(const void* vertices,
                                        const void* indices,
                                        U32 vertexCount,
                                        U64 vertexStride,
                                        U64 positionOffset,
                                        U64 normalOffset,
                                        U64 texCoordOffset,
                                        U64 indexByteWidth,
                                        void* tangentOutStart,
                                        U64 tangentOutStride,
                                        U64 tangentOutOffset)
{
    MeshInfo info;
    info.vertices = vertices;
    info.indices = indices;
    info.vertexCount = vertexCount;
    info.vertexStride = vertexStride;
    info.positionOffset = positionOffset;
    info.normalOffset = normalOffset;
    info.texCoordOffset = texCoordOffset;
    info.indexBW = indexByteWidth;
    info.tangentOutStart = tangentOutStart;
    info.tangentOutStride = tangentOutStride;
    info.tangentOutOffset = tangentOutOffset;

    SMikkTSpaceInterface interface;
    interface.m_getNumFaces = getNumFaces;
    interface.m_getNumVerticesOfFace = getNumVerticesOfFace;

    interface.m_getPosition = getPosition;
    interface.m_getNormal = getNormal;
    interface.m_getTexCoord = getTexCoord;

    interface.m_setTSpaceBasic = setTSpaceBasic;

    interface.m_setTSpace = nullptr;

    SMikkTSpaceContext context;
    context.m_pInterface = &interface;
    context.m_pUserData = &info;

    return genTangSpaceDefault(&context);
}*/

// This constructs a new mesh with tangents inserted at the appropriate location.
// IF there are already tangents in the old mesh, then tangentLocation MUST point
// to those tangents, which are then all updated to be MikkTSpace tangents.
// Note that the returned mesh will NOT have an index buffer, so it is
// recommended to re-weld the mesh (i.e. merge duplicate vertices and generate
// an index list).
// Of course this supports both indexed and non-indexed meshes as input, so in
// particular the input mesh should not be split before.
Mesh generateTangentsFromTriangularMesh(const Mesh& oldMesh,
                                        U32 tangentLocation)
{
    if (!oldMesh.vertexBufferCount())
    {
        throw std::logic_error("Cannot generate tangents for mesh with no "
                               "vertex buffer.");
    }
    std::optional<U64> positionOffset;
    std::optional<U64> normalOffset;
    std::optional<U64> texCoordOffset;
    std::optional<U64> tangentOffset;

    const auto* pAttributes = oldMesh.attributes();

    for (U32 i = 0; i < oldMesh.attributeCount(); ++i)
    {
        if (pAttributes[i].name == "position")
        {
            positionOffset = pAttributes[i].attribute.offset;
        }
        else if (pAttributes[i].name == "normal")
        {
            normalOffset = pAttributes[i].attribute.offset;
        }
        else if (pAttributes[i].name == "texcoord")
        {
            texCoordOffset = pAttributes[i].attribute.offset;
        }
        else if (pAttributes[i].name == "tangent")
        {
            tangentOffset = pAttributes[i].attribute.offset;
            if (i != tangentLocation)
            {
                throw std::runtime_error("Mesh already have tangent at different location.");
            }
        }
    }

    if (!positionOffset || !normalOffset || !texCoordOffset)
    {
        throw std::runtime_error("Cannot generate tangent without having both "
                                 "positions, normals AND texture coordinates.");
    }

    auto oldVertexSize = oldMesh.vertexBuffer(0).stride;
    auto newVertexSize = oldVertexSize + !tangentOffset.has_value() * sizeof(kraken::math::vec3);

    std::vector<NamedMeshAttributeElement> newAttributes(pAttributes,
                                                         pAttributes + oldMesh.attributeCount());

    if (!tangentOffset.has_value())
    {
        if (tangentLocation < newAttributes.size())
        {
            tangentOffset = pAttributes[tangentLocation].attribute.offset;
            for (auto i = tangentLocation; i + 1 < newAttributes.size(); ++i)
            {
                newAttributes[i].attribute.offset = newAttributes[i+1].attribute.offset;
            }
            newAttributes.back().attribute.offset = oldVertexSize;
            newAttributes.emplace(newAttributes.begin() + tangentLocation);
        }
        else
        {
            tangentLocation = newAttributes.size();
            newAttributes.emplace_back();
        }
        newAttributes[tangentLocation].attribute.format = VK_FORMAT_R32G32B32_SFLOAT;
        newAttributes[tangentLocation].attribute.offset = tangentOffset.value();
        newAttributes[tangentLocation].name = "tangent";
    }

    MeshInfo info;
    info.vertices = oldMesh.vertexBuffer(0).buffer;
    info.indices = oldMesh.indexBufferCount() ? oldMesh.indexBuffer(0).buffer : nullptr;

    U32 vertexCount = info.indices ? oldMesh.indexBuffer(0).count : oldMesh.vertexBuffer(0).count;

    Mesh meshOut;
    meshOut.resizeVertexBuffers(1);
    meshOut.vertexBuffer(0).reserveHostMemory(newVertexSize * vertexCount);
    meshOut.vertexBuffer(0).stride = newVertexSize;
    meshOut.vertexBuffer(0).count = vertexCount;

    info.tangentOutStart = meshOut.vertexBuffer(0).buffer;
    info.tangentOutStride = newVertexSize;

    info.tangentOutOffset = *tangentOffset;
    info.vertexCount = vertexCount;
    info.vertexStride = oldVertexSize;
    info.positionOffset = positionOffset.value();
    info.normalOffset = normalOffset.value();
    info.texCoordOffset = texCoordOffset.value();

    info.indexBW = info.indices ? oldMesh.indexBuffer(0).stride : 0;

    SMikkTSpaceInterface interface;
    interface.m_getNumFaces = getNumFaces;
    interface.m_getNumVerticesOfFace = getNumVerticesOfFace;

    interface.m_getPosition = getPosition;
    interface.m_getNormal = getNormal;
    interface.m_getTexCoord = getTexCoord;

    interface.m_setTSpaceBasic = setTSpaceBasic;

    interface.m_setTSpace = nullptr;

    SMikkTSpaceContext context;
    context.m_pInterface = &interface;
    context.m_pUserData = &info;

    if (!genTangSpaceDefault(&context))
    {
        throw std::logic_error("MikkTSpace failed to generate tangent space.");
    }

    // Construct new vertex type

    const auto* vertexSrc = info.vertices;
    auto* vertexDst = meshOut.vertexBuffer(0).buffer;

    const auto bytesAfterTangent = oldVertexSize - *tangentOffset;
    const auto oldTangentEnd = bytesAfterTangent
                             ? pAttributes[tangentLocation].attribute.offset
                             : oldVertexSize
                             ;
    const auto newTangentEnd = bytesAfterTangent
                             ? newAttributes[tangentLocation+1].attribute.offset
                             : oldVertexSize
                             ;

    for (auto newVert : kraken::range(vertexCount))
    {
        U32 oldVert = 0;
        if (info.indexBW == 4) oldVert = reinterpret_cast<const U32*>(info.indices)[newVert];
        else if (info.indexBW == 2) oldVert = reinterpret_cast<const U16*>(info.indices)[newVert];
        else if (info.indexBW == 1) oldVert = reinterpret_cast<const U8*>(info.indices)[newVert];
        else oldVert = newVert;

        // Tangents are already computed. First we try to copy all data before the
        // tangents.
        std::memcpy(kraken::pointeradd(vertexDst, newVertexSize * newVert),
                    kraken::pointeradd(vertexSrc, oldVertexSize * oldVert),
                    *tangentOffset);

        // Then we copy what comes after the tangent (if anything).
        std::memcpy(kraken::pointeradd(vertexDst, newVertexSize * newVert + newTangentEnd),
                    kraken::pointeradd(vertexSrc, oldVertexSize * oldVert + oldTangentEnd),
                    newVertexSize - *tangentOffset);
    }

    U32 indexBWOut = vertexCount > 0xffff ? 4 : 2;

    meshOut.resizeIndexBuffers(1);
    meshOut.indexBuffer(0).reserveHostMemory(indexBWOut * vertexCount);
    meshOut.indexBuffer(0).stride = indexBWOut;
    meshOut.indexBuffer(0).count = vertexCount;

    if (indexBWOut == 4)
    {
        U32* indices = reinterpret_cast<U32*>(meshOut.indexBuffer(0).buffer);
        for (U32 i = 0; i < vertexCount; ++i)
        {
            indices[i] = i;
        }
    }
    else
    {
        U16* indices = reinterpret_cast<U16*>(meshOut.indexBuffer(0).buffer);
        for (U32 i = 0; i < vertexCount; ++i)
        {
            indices[i] = i;
        }
    }

    return meshOut;
}

} // end anonymous namespace

bool PLYParser::parse(const U8* data, size_t size,
                      const kraken::JSONValue& opts,
                      ResourceLoader&,
                      ResourceLoader::HostResource* resourceOut)
{
    auto mesh = std::make_unique<Mesh>();
    mp_mesh = mesh.get();

    kraken::io::DataSource dataSource(data, size);
    if (!loadPLY(dataSource, *mesh)) return false;

    if (opts("generateTangents"))
    {
        auto newLocation = static_cast<U32>(mesh->attributeCount());
        if (opts("tangentLocation"))
        {
            newLocation = (U32)opts("tangentLocation");
        }
        bool generateTangents = true;
        for (U32 i = 0; i < mesh->attributeCount(); ++i)
        {
            if (mesh->attributes()[i].name == "tangent")
            {
                newLocation = i;
                generateTangents = (bool)opts("force");
                break;
            }
        }
        if (generateTangents)
        {
            *mesh = generateTangentsFromTriangularMesh(*mesh, newLocation);
        }
    }

    resourceOut->deleter = &gs_meshDeleter;
    resourceOut->resource = mesh.release();
    resourceOut->typeName = "Mesh";
    resourceOut->typeIdentifier = typeid(Mesh).hash_code();
    return true;
}

bool STLParser::parse(const U8* data, size_t size,
                      const kraken::JSONValue& opts,
                      ResourceLoader&,
                      ResourceLoader::HostResource* resourceOut)
{
    auto mesh = std::make_unique<Mesh>();
    mp_mesh = mesh.get();

    kraken::io::DataSource dataSource(data, size);
    if (!loadSTL(dataSource, *mesh)) return false;

    if (opts("generateTangents"))
    {
        auto newLocation = static_cast<U32>(mesh->attributeCount());
        if (opts("tangentLocation"))
        {
            newLocation = (U32)opts("tangentLocation");
        }
        bool generateTangents = true;
        for (U32 i = 0; i < mesh->attributeCount(); ++i)
        {
            if (mesh->attributes()[i].name == "tangent")
            {
                newLocation = i;
                generateTangents = (bool)opts("force");
                break;
            }
        }
        if (generateTangents)
        {
            *mesh = generateTangentsFromTriangularMesh(*mesh, newLocation);
        }
    }

    resourceOut->deleter = &gs_meshDeleter;
    resourceOut->resource = mesh.release();
    resourceOut->typeName = "Mesh";
    resourceOut->typeIdentifier = typeid(Mesh).hash_code();
    return true;
}


void ImageParser::addResourceSupport(ResourceLoader& loader, std::string mediaType)
{
    auto imgloadercreator = []()
    {
        return std::make_unique<ImageParser>();
    };
    loader.addResourceSupport(mediaType, imgloadercreator);
}
