#!/bin/bash
vkheader="${1}"
cd $(dirname $(readlink -f "$0"))
if [[ ! -f "${vkheader}" ]]; then
    echo "Usage: $(basename "${0}") [path to vulkan_core.h]"
    exit 1
fi

impl/generate-vulkan-types.sh "${vkheader}" header > ../include/vulkan/types.hpp
impl/generate-vulkan-types.sh "${vkheader}" source > ../src/vulkan/types.cpp
