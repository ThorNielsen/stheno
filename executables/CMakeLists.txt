add_executable(sthenoint sthenoint.cpp)
set_target_properties(sthenoint PROPERTIES LINKER_LANGUAGE CXX)
target_link_libraries(sthenoint stheno-interpreter)
