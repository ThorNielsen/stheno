#include "interpreter/lexer.hpp"
#include "interpreter/parser.hpp"

#include "interpreter/astmanip/simplification.hpp"
#include "interpreter/astmanip/to_string.hpp"
#include "interpreter/evaluator.hpp"

#include <iostream>
#include <numbers>

auto printAST(interpreter::ProgramSyntaxTree&& syntaxTree)
{
    std::cout << to_string(syntaxTree) << "\n";
    return syntaxTree;
}

void runInterpreter()
{
    std::string buf;
    while (std::cin)
    {
        std::getline(std::cin, buf);
        if (buf.empty()) continue;
        if (buf == "q" || buf == "quit") return;
        auto program =
        interpreter::tokenise(buf)
        .and_then(interpreter::parse)
        .transform(printAST)
        .transform(interpreter::simplifyProgram)
        .transform(printAST)
        .transform_error
        ([](auto&& error)
        {
            if (error)
            {
                if (auto sourceLoc = error->location())
                {
                    std::cerr << std::format("Error in {}, line {}, column {}: {}\n",
                                             error->compilerPass(),
                                             sourceLoc->rangeBegin.line + 1,
                                             sourceLoc->rangeBegin.character,
                                             error->description());
                }
                else
                {
                    std::cerr << std::format("Error in {}: {}\n",
                                             error->compilerPass(),
                                             error->description());
                }
            }
            else std::cerr << "Parsing failed, no error information available.\n";

            return error;
        });
        if (program)
        {
            interpreter::EvaluationContext context;
            context.astContext = &program->context;
            context.bindings["x"] = interpreter::PrimitiveVariable::create(5.2);
            context.bindings["y"] = interpreter::PrimitiveVariable::create(3.);
            context.bindings["z"] = interpreter::PrimitiveVariable::create(21.);
            context.bindings["pi"] = interpreter::PrimitiveVariable::create(std::numbers::pi);

            evaluateExpression(*program->expression, context)
            .transform([](auto&& expr)
            {
                std::cout << std::format("Evaluation result: {}\n", expr.element.asDouble);
                return expr;
            })
            .transform_error([](auto&& error)
            {
                std::cout << std::format("Evaluation error: {}\n", error.message);
                return error;
            });
        }
    }
}

int main()
{
    try
    {
        runInterpreter();
    }
    catch (std::exception& except)
    {
        std::cerr << "Got exception '" << except.what() << "'.\n";
        return 1;
    }
    catch (...)
    {
        std::cerr << "Got unknown exception, exiting.\n";
        return 2;
    }
}
