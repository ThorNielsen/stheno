#ifndef RESOURCELOADER_HPP_INCLUDED
#define RESOURCELOADER_HPP_INCLUDED

#include "vulkan/allocator.hpp"
#include "vulkan/common.hpp"
#include "vulkan/image.hpp"
#include "vulkan/mesh.hpp"

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <functional>
#include <map>
#include <mutex>
#include <queue>
#include <thread>
#include <vector>

#include <kraken/globaltypes.hpp>
#include <kraken/utility/json.hpp>

extern std::atomic<bool> g_showResourceLoaderDebugOutput;
extern std::atomic<bool> g_showResourceLoaderExtraDebugOutput;

class ResourceParser;

/// Asynchronously parses various resources.
///
/// This class can be used for loading many kinds of resources, like images,
/// meshes etc. in an asynchronous manner, which can be kept both in main memory
/// and/or be uploaded to a Vulkan device. The number of threads used for
/// parsing is configurable with no pre-defined limit, and resource support can
/// be extended to arbitrary formats by deriving from the ResourceParser class.
///
/// \remark If a resource is expanded during parsing to multiple subresources,
///         then it will not support transfer/Vulkan resources. Instead, the
///         host resource (if any) will be a control structure giving info about
///         the subresources (while it will also have a subresource list, the
///         host resource will have information about which subresource is
///         which).
class ResourceLoader
{
public:
    using ResourceID = U32;
    using ResourceParserCreator = std::function<std::unique_ptr<ResourceParser>()>;
    using SubresourceList = std::vector<ResourceID>;

    /// Object owning some memory.
    struct Bytedata
    {
        std::unique_ptr<U8[]> data; ///< Smart pointer owning the memory.
        size_t size; ///< Size of owned memory in bytes.
    };

    struct FileInfo
    {
        std::string name;
        fs::path path;
        bool relativeToResourceLoaderBasePath = true;
        bool optional = false;
    };

    using RawData = std::map<std::string, Bytedata>;

    struct HostResource
    {
        HostResource() = default;
        HostResource(HostResource&& other)
        {
            *this = std::move(other);
        }
        HostResource& operator=(HostResource&& other)
        {
            if (&other == this) return *this;
            destroy();
            resource = other.resource;
            deleter = other.deleter;
            other.resource = nullptr;
            other.deleter = nullptr;
            return *this;
        }
        HostResource(const HostResource&) = delete;
        HostResource& operator=(const HostResource&) = delete;
        ~HostResource()
        {
            destroy();
        }

        void destroy()
        {
            if (deleter && resource)
            {
                (*deleter)(resource);
                resource = nullptr;
                deleter = nullptr;
            }
        }

        template <typename T>
        T* extract()
        {
            void* ptr = resource;
            resource = nullptr;
            return static_cast<T*>(ptr);
        }

        template <typename T>
        T&& move()
        {
            return std::move(*extract<T>());
        }

        void* resource = nullptr;
        const std::function<void(void*)>* deleter = nullptr;
        const char* typeName = ""; ///< Human-readable type name (is neither
                                   ///< guaranteed to be unique nor to
                                   ///< correspond to the actual type name).
        decltype(typeid(void).hash_code()) typeIdentifier = typeid(void).hash_code();
    };

    struct VulkanResource
    {
        enum class Type : U32
        {
            None = 0,
            Image = 1,
            Mesh = 2,
        };
        Image image;
        const Mesh* mesh = nullptr;
        Type available = Type::None;
    };

    static constexpr ResourceID invalidResource = 0xffffffff;
    enum class Status : U32
    {
        Good = 0,
        WaitingForSubresources,
        Ready,

        FileNotFound,
        FileNotAFile,
        FileAccessFailed,
        FileReadFailed,
        ParserNotFound,
        ParsingFailed,
        TransferFailed,
        TransferrerNotFound,

        SubresourceFailed,
    };

    enum class ResourceType : U32
    {
        None = 0,

        RawFile = 1,
        HostResource = 2,
        VulkanResource = 4,
        Subresource = 8,
    };

    /// Class used to describe where data is sourced from.
    ///
    /// This class does not own any of the memory but rather just points to
    /// it, along with some information about where it is located, namely
    /// fromType. If this is None, the data is external to ResourceLoader,
    /// and should be kept around until the resource has finished the
    /// relevant stage.
    /// When used within Options, it can also be set to any resource stage to
    /// indicate that the data should be taken from there; in that case the
    /// pointer and size is ignored.
    ///
    /// If data is a valid pointer, then size determines what it points to. In
    /// case of size > 0, data is a const U8* to an array of size bytes. If
    /// size == 0, then data may point to any type of data structure, and the
    /// resource parser is expected to know what it is/define the valid data
    /// type.
    struct DataSource
    {
        const void* data = nullptr; ///< Non-owning pointer to memory.
        size_t size = 0; ///< Size of memory in bytes.

        ResourceType fromType = ResourceType::None; ///< Location of data.
    };
    /// Options to use for loading resources.
    struct Options
    {
        kraken::JSONValue parseOptions; ///< Options for parsing stage.
        kraken::JSONValue vulkanOptions; ///< Options for transfer stage.

        DataSource parseDataSource = {nullptr, 0, ResourceType::RawFile};
        DataSource vulkanDataSource = {nullptr, 0, ResourceType::HostResource};

        ResourceID parent = invalidResource; ///< ID of this resource's parent;
                                             ///< if invalid, this is not a
                                             ///< child of any other resource.
        ResourceType parentDependencies = ResourceType::None;
        ResourceType type; ///< Desired types to load (which are promised to be
                           ///< acquired externally).
        std::function<void(Status, ResourceID, ResourceLoader&)> finishNotify
            = [](Status, ResourceID, ResourceLoader&){};
        float priority = 1.f; ///< Priority of this resource (lower number means
                              ///< higher priority).
        ResourceType blockTypes = ResourceType::None; ///< Types to wait for
                                                      ///< explicit
                                                      ///< synchronisation;
                                                      ///< achieved by calling
                                                      ///< unblockResource().
        bool pathRelativeToLoaderPath = true;
    };

    ResourceLoader();
    ~ResourceLoader() { destroy(); }

    /// Constructs an instance of this class.
    ///
    /// This constructs a fresh instance of this class; it may be called at any
    /// time, but if the class is already constructed it will be destroyed
    /// before re-construction. Note that this MUST be called before any other
    /// function, except for destroy() and the C++ constructor and destructor.
    ///
    /// \param maxParseThreads Maximum number of simultaneous parser threads.
    /// \param basePath Working directory; all other paths will be considered
    ///                 relative to this one.
    /// \param device Vulkan device to use. May be VK_NULL_HANDLE; then Vulkan
    ///               resource support is disabled.
    /// \param queueInfo Information about the queue transfer operations should
    ///                  be scheduled on. Unused if device is VK_NULL_HANDLE.
    /// \param stagingAreaSize Size (in bytes) of staging area for transfer
    ///                        operations. About 128-256MiB is a good default.
    ///                        Unused if device is VK_NULL_HANDLE.
    /// \param chunkSize Size (in bytes) of chunks used to divide staging area
    ///                  into. 4-8MiB is a good default choice. Unused if device
    ///                  is VK_NULL_HANDLE.
    /// \param allocator VmaAllocator to use for any device allocations. Unused
    ///                  if device is VK_NULL_HANDLE.
    void create(U32 maxParseThreads,
                fs::path basePath,
                VkDevice device,
                QueueInformation queueInfo,
                VkDeviceSize stagingAreaSize,
                VkDeviceSize chunkSize,
                VmaAllocator allocator);

    /// Destroys the class.
    ///
    /// This destroys the current instance, but waits for all operations in all
    /// threads spawned by this instance to finish before returning. This may
    /// thus be an extremely slow operation, and according care should be taken,
    /// such as not having any unnecessary user interface waiting for this to
    /// finish.
    void destroy();

    /// Adds support for a new resource.
    ///
    /// This registers a class factory whose resulting object is a subclass of
    /// ResourceParser and can be used for parsing resources.
    ///
    /// \param mediaType Media type to load, as described in RFC 2046.
    /// \param parser Factory returning an instance of a resource parser.
    void addResourceSupport(std::string mediaType, ResourceParserCreator parser);

    /// Schedules asynchronous loading of a resource in memory.
    ///
    /// \param mediaType Media type to load, as described in RFC 2046. Used to
    ///                  select an appropriate parser.
    /// \param opts Options describing source data, and options to parser.
    ///
    /// \returns An ID which can be used to query the status of the resource and
    ///          to subsequently acquire it.
    ResourceID load(std::string mediaType, Options opts);

    /// Schedules asynchronous loading of a resource from a file.
    ///
    /// \param mediaType Media type to load, as described in RFC 2046. Used to
    ///                  select an appropriate parser.
    /// \param path Path giving the location of the resource relative to the
    ///             base path given on construction of this instance of
    ///             ResourceLoader.
    /// \param opts Options describing source data, and options to parser.
    ///
    /// \returns An ID which can be used to query the status of the resource and
    ///          to subsequently acquire it.
    ResourceID load(std::string mediaType, fs::path path, Options opts);

    /// Queries current status of a resource.
    ///
    /// \param id Id of the resource.
    ///
    /// \returns The current status of the resource.
    Status getStatus(ResourceID id);

    /// Queries result of resource loading.
    ///
    /// \param id Id of the resource.
    ///
    /// \returns The result of loading the resource.
    VkResult getResult(ResourceID id);

    /// Queries the available resource types.
    ///
    /// \param id Id of the resource.
    ///
    /// \returns Which of the resource's types that can be acquired safely.
    ResourceType getAvailableResourceTypes(ResourceID id);

    /// Obtains the information used to load a particular resource.
    ///
    /// \param id Id of the resource.
    /// \param options If not null, a const pointer to the load options will
    ///                be written to the given pointer.
    /// \param mediaType If not null, a const pointer to the media type will
    ///                  be written to the given pointer.
    /// \param path If not null, a const pointer to the path will be written to
    ///             the given pointer. Note that the path may be empty.
    void getResourceInformation(ResourceID id,
                                const Options** options,
                                const std::string** mediaType,
                                const fs::path** path);


    /// Allows this to use Vulkan commands requiring external synchronisation.
    ///
    /// When called this may submit Vulkan commands requiring externally
    /// synchronised access such as vkQueueSubmit, which will otherwise not be
    /// called. It is required to periodically call this if any resources are to
    /// be transferred to a Vulkan device; failure to do so leaves this soft-
    /// locked as it is unable to transfer anything.
    ///
    /// \param waitTimeout describes (in nanoseconds) how long to wait for
    ///                    Vulkan commands to finish. If 0, this will not wait.
    ///
    /// \returns The result of submission if applicable, otherwise VK_SUCCESS.
    VkResult executeVulkanCommands(U64 waitTimeout = 0);

    /// Tries to acquire a raw file.
    ///
    /// \param id Resource identifier.
    /// \param out Pointer to object to move raw file data to.
    ///
    /// \returns True if the object is acquired sucesssfully (and written to
    ///          out), and false otherwise.
    bool acquireRawFile(ResourceID id, RawData* out);

    /// Tries to acquire a host resource.
    ///
    /// \param id Resource identifier.
    /// \param out Pointer to object to move resource to.
    ///
    /// \returns True if the object is acquired sucesssfully (and written to
    ///          out), and false otherwise.
    bool acquireHostResource(ResourceID id, HostResource* out);

    /// Tries to acquire a Vulkan resource.
    ///
    /// \param id Resource identifier.
    /// \param out Pointer to object to move resource to.
    ///
    /// \returns True if the object is acquired sucesssfully (and written to
    ///          out), and false otherwise.
    bool acquireVulkanResource(ResourceID id, VulkanResource* out);

    /// Gets a list of subresources for the given resource.
    ///
    /// \remark This function may only be called once the resource has finished
    ///         loading (and either has status WaitingForSubresources or Ready).
    ///
    /// \param id Resource identifier.
    ///
    /// \returns A (reference to a) list of subresources.
    const SubresourceList& getSubresourceList(ResourceID id);

    /// Destroys a resource and all of its remaining components.
    ///
    /// This can be used to destroy any resource, however it is the caller's
    /// responsibility to ensure that it is not currently being processed by any
    /// thread. Note that this can (and should) be used to destroy resources if
    /// not all loaded parts are acquired, e.g. if a resource is scheduled to be
    /// loaded both to host and device memory, but is only acquired for one of
    /// them. Failure to either acquire all available parts of a resource or
    /// call this function will result in a memory leak.
    ///
    /// \param id ID identifying the resource.
    void destroyResource(ResourceID id);

    /// Determines whether a given resource id exists.
    ///
    /// \param id Resource identifier.
    ///
    /// \returns True if a resource with such an id exists, otherwise false.
    bool exists(ResourceID id);

    /// Allows resource to load the given types.
    ///
    /// When the loading options has blockTypes set to something different from
    /// ResourceType::None, those types cannot load until this function has been
    /// called with those types here. Note that blocking everything, and then
    /// calling this once with e.g. RawFile | HostResource | VulkanResource will
    /// remove all these blockings, even if the resource is not currently
    /// waiting to be transferred to the Vulkan device.
    ///
    /// \param id Resource identifier.
    /// \param types Types to unblock from loading.
    void unblockResource(ResourceID id, ResourceLoader::ResourceType types);

    /// Fetches number of currently active threads.
    ///
    /// A thread is considered to be active if is currently working on some
    /// task, and any inactive (but existing) thread can be used to immediately
    /// (modulo OS scheduling) perform some work.
    /// This function is also useful for debugging to determine whether a dead-
    /// lock has occured. E.g. if getActiveThreadCounts(...) = {0, 0, 0}, but
    /// a resource is still in a non-final (i.e. not ready, not waiting for sub-
    /// resources, not failed) stage. Note, however, that a common cause of this
    /// is to set a block flag, but forgetting to unblock the relevant resource
    /// or its parents.
    ///
    /// \param diskThreads If not null, the number of currently active disk
    ///                    threads will be written here.
    /// \param parseThreads If not null, the number of currently active parsing
    ///                     threads will be written here.
    /// \param transferThreads If not null, the number of currently active
    ///                        transfer threads will be written here.
    void getActiveThreadCounts(U32* diskThreads,
                               U32* parseThreads,
                               U32* transferThreads) const;

    /// Determines whether the current instance is idle.
    ///
    /// Note that even though no resources are being loaded, this may still
    /// return false due to the possiblity of threads being woken spuriously.
    /// However, this never falsely reports an active ResourceLoader as idle.
    ///
    /// \returns True if all owned threads are sleeping, false otherwise.
    bool isIdle() const;

    /// Helps debug multi-threading problems.
    ///
    /// This can be called if the instance is repeatedly reported idle -- then
    /// this will first write out some information to aid debugging, then it may
    /// try to start the various threads
    void panicIfIdle(bool writeDebugOutput);

    template <typename ResourceParserCreator>
    void addResourceSupport(std::string mediaType)
    {
        ResourceParserCreator::addResourceSupport(*this, mediaType);
    }

private:
    friend class ResourceParser;

    enum class Stage : U32
    {
        DiskReadQueued = 0,
        DiskReading,
        ParsingQueued,
        Parsing,
        TransferQueued,
        Transferring,
        ReadyInCallback, // For when the resource is ready but notifying that
                         // it has finished.
        Ready,
    };
    struct Resource
    {
        std::unique_ptr<ResourceParser> parser;

        RawData rawData;
        HostResource hostResource;
        VulkanResource vulkanResource;
        SubresourceList subresources;

        ResourceType acquirable = ResourceType::None;
        ResourceType available = ResourceType::None;
    };
    struct ResourceInfo
    {
        Options options;
        std::string mediaType;
        fs::path path;

        U32 childCount = 0;
        U32 finishedChildCount = 0;
        U32 destroyedChildCount = 0;

        Status status;
        Stage currentStage;
        ResourceID id;
        VkResult result;
        VkEvent transferInitialised = VK_NULL_HANDLE;

        U32 blocksTransferred;
        U32 totalBlockCount;

        ResourceType resourcesToAcquire;
        ResourceType resourcesToLoad;
        ResourceType resourcesUsedByChildren;

        ResourceType blockTypes;
    };
    struct DiskTask
    {
        fs::path path;
        decltype(Options::priority) priority;
        ResourceID id;
        bool relativeToLoaderPath = true;

        // As the other ___Task structs, this operator is inverted such that a
        // lower number results in higher priority -- the idea is that one can
        // assign a timepoint to each task indicating its desired completion
        // time, and then the queues will order them according to this.
        bool operator<(const DiskTask& other) const
        {
            if (priority == other.priority) return id > other.id;
            return priority > other.priority;
        }
    };
    struct ParseTask
    {
        decltype(Options::priority) priority;
        ResourceID id;

        bool operator<(const ParseTask& other) const
        {
            if (priority == other.priority) return id > other.id;
            return priority > other.priority;
        }
    };
    struct HostTransferTask
    {
        ResourceParser* parser;
        DataSource source;
        U32 blockCount;

        decltype(Options::priority) priority;
        ResourceID id;

        bool operator<(const HostTransferTask& other) const
        {
            if (priority == other.priority) return id > other.id;
            return priority > other.priority;
        }
    };
    struct DeviceTransferTask
    {
        ResourceID id;
        VkDeviceSize bufferOffset;
        VkDeviceSize bufferSize;
        ResourceParser* parser;
        VulkanResource* vulkanResource;
        VkEvent waitFor;
        U32* blocksTransferred;
        U32* totalBlockCount;
        const void* tag;
        U32 extraTag;
    };
    struct TransferFinalisationInfo
    {
        ResourceID id;
        ResourceParser* parser;
        VulkanResource* vulkanResource;
    };

    struct TransferSubmitBatch
    {
        VkFence fence;
        std::vector<DeviceTransferTask> transfers;
        std::vector<ResourceID> finishes;
        std::vector<VkCommandBuffer> commandBuffers;
    };

    // Returns true if task was blocked (and therefore moved), false if not.
    bool atomicMoveBlockedTask(ResourceID id, ResourceType type,
                               DiskTask* = nullptr,
                               ParseTask* = nullptr,
                               HostTransferTask* = nullptr);

    void appendChild(ResourceID parent, ResourceID child);

    // Gets resource info for the resource that the current thread is currently
    // parsing.
    void getCurrentResourceInfo(ResourceID* id,
                                const Options** options,
                                const std::string** mediaType,
                                const fs::path** path);


    VkResult submitTransfers(U64 timeout);

    ResourceID newResourceID()
    {
        ResourceID id;
        while ((id = m_nextResourceID.fetch_add(1, std::memory_order_relaxed))
               == invalidResource)
            ;
        return id;
    }

    VkResult createTransferrer(VkDevice device,
                               QueueInformation queueInfo,
                               VkDeviceSize stagingAreaSize,
                               VkDeviceSize blockSize,
                               VmaAllocator allocator);

    ResourceID createResource(std::string mediaType, fs::path, Options, Stage);

    ResourceType compatibleTypes(std::string mediaType);
    void setStatus(ResourceID, Status, VkResult error = VK_SUCCESS);
    void setStage(ResourceID, Stage);

    void notifyChildFinished(ResourceID parent, ResourceID child);
    void notifyChildDestroyed(ResourceID parent, ResourceID child);
    void endResource(ResourceID id);

    bool readFile(fs::path, ResourceID, Bytedata& dataOut, bool optional = false);
    bool parseResource(ResourceID);
    void transferResource(ResourceID);

    void scheduleNextStage(ResourceID, Stage current);

    void diskThread(U32 threadID);
    void parserThread(U32 threadID);
    void hostTransferThread(U32 threadID);

    size_t allocateTransferBlock();
    void deallocateTransferBlock(size_t block);
    void scheduleDeviceTransfer(ResourceID id,
                                VkDeviceSize bufferOffset,
                                VkDeviceSize bufferSize,
                                const void* tag,
                                U32 extraTag);

    bool shouldDestroyThread(U32 threadID) const;
    void notifyThreadFinished(U32 threadID);
    void destroyThreads();

    void destroyResources();

    void addDefaultResourceSupport();

    DataSource toDataSource(const RawData& data,
                            ResourceType fromType = ResourceType::RawFile) const;


    /// The following functions are special in that they require external
    /// synchronisation, e.g. by mutex.


    void prelockedRecursiveUnblockResource(ResourceID id,
                                           ResourceType types,
                                           U32* diskUnblockCount = nullptr,
                                           U32* parseUnblockCount = nullptr,
                                           U32* transferUnblockCount = nullptr);

    Resource* getResource(ResourceID id);
    bool isAcquirable(const Resource& resource, ResourceType type) const;
    bool isAvailable(const Resource& resource, ResourceType type) const;

    // This requires m_resourceMutex to be held.
    // On return, dataLeft indicates whether the resource has any data left, or
    // if it can be safely destroyed.
    Resource* tryAcquire(ResourceID id, ResourceType type, bool& dataLeft);

    // This removes available resources even if they are not acquirable (i.e.
    // this is suitable for actually destroying resources).
    void trimResource(Resource& resource, ResourceType toRemove) const;

    ResourceType requiredResourcesToLoad(const Options& opts) const;

    std::condition_variable m_diskReadCond;
    std::condition_variable m_parseCond;
    std::condition_variable m_hostTransferCond;
    std::priority_queue<DiskTask> m_diskTaskQueue;
    std::priority_queue<ParseTask> m_parseTaskQueue;
    std::priority_queue<HostTransferTask> m_hostTransferTaskQueue;

    std::unordered_map<ResourceID, DiskTask> m_blockedDiskTasks;
    std::unordered_map<ResourceID, ParseTask> m_blockedParseTasks;
    std::unordered_map<ResourceID, HostTransferTask> m_blockedHostTransferTasks;

    std::vector<DeviceTransferTask> m_deviceTransferTasks;
    std::vector<ResourceID> m_toInitTransfers; // This is also protected by
                                               // m_deviceTransferTaskMutex.

    std::vector<TransferSubmitBatch> m_submittedBatches;
    std::vector<TransferFinalisationInfo> m_transferFinalisations;

    // While these containers may be replaced by similar containers, such
    // replacements MUST satisfy the guarantee that any pointer or reference to
    // an element is never invalidated, even though other elements are inserted
    // or erased.
    std::unordered_map<ResourceID, ResourceInfo> m_resourceInfo;
    std::unordered_map<ResourceID, Resource> m_resources;

    std::map<std::string, ResourceParserCreator> m_parserCreators;
    std::unordered_map<std::thread::id, ResourceID> m_currParsing;

    fs::path m_basePath;
    std::thread m_diskThread;
    std::vector<std::thread> m_parseThreads;
    std::thread m_hostTransferThread;

    std::atomic<bool> m_destroyObjects;
    std::atomic<ResourceID> m_nextResourceID;
    std::atomic<U32> m_liveThreadCount;
    std::atomic<U32> m_activeDiskThreads;
    std::atomic<U32> m_activeParseThreads;
    std::atomic<U32> m_activeTransferThreads;

    /// ATTENTION: If several mutexes are to be acquired they MUST be acquired
    /// in the order defined here, to prevent deadlock.
    std::mutex m_resourceMutex;
    std::mutex m_resourceInfoMutex;

    std::mutex m_parserCreatorMutex;

    std::mutex m_diskTaskMutex;
    std::mutex m_parseTaskMutex;
    std::mutex m_hostTransferTaskMutex;
    std::mutex m_deviceTransferTaskMutex;

    std::mutex m_transferTaskMutex;

    std::mutex m_totalBlockUpdateMutex; // Protects both total block count AND m_transferFinalisations.

    std::mutex m_blockMutex;

    VmaAllocator m_allocator = VK_NULL_HANDLE;
    VkDevice m_device = VK_NULL_HANDLE;

    std::vector<U16> m_blocks;

    VkCommandPool m_commandPool = VK_NULL_HANDLE;
    VkQueue m_queue = VK_NULL_HANDLE;
    BufferAllocation m_stagingBuffer = {VK_NULL_HANDLE, VK_NULL_HANDLE};
    size_t m_blockCount;
    size_t m_blockSize;
    size_t m_nextFreeBlock;
};


class ResourceParser
{
public:
    virtual ~ResourceParser() {}

    /// Determines what additional files to load, if any.
    ///
    /// If the given resource is loaded from a file, then this function should
    /// return a list consisting of paths to additional files to load. This, by
    /// its very nature, may perform some parsing of the already-loaded file,
    /// but as this function call is not parallelised, it should NOT perform any
    /// kind of complicated parsing; only what is strictly necessary to
    /// determine which additional files to load.
    ///
    /// @remark If this function outputs a non-empty list, then it will be
    ///         called again after loading these files.
    ///
    /// @remark The restriction that this can only be called if the appropriate
    ///         resource is loaded from a file using the same ResourceLoader
    ///         instance may be lifted at some point.
    ///
    /// @example If a file A depends upon B, which in turn depends upon C, then
    ///          the path of A could be given to load; when A is loaded, this
    ///          can then parse a tiny bit of A to determine the dependency upon
    ///          B, and output that. When B has then been read, this will be
    ///          called again, and this can now parse a tiny bit of A and B,
    ///          determine C needs to be read, and return that. Finally, when C
    ///          is read, this can then determine no more files needs to be
    ///          read, and return an empty list.
    ///
    /// @param fileInfoOut If non-null, *fileInfoCount gives the maximal number
    ///                    of paths to write here. They should be witten here.
    ///                    If null, the number of paths that would be written
    ///                    here should be written to *fileInfoCount.
    /// @param fileInfoCount *fileInfoCount denotes how many paths can be
    ///                      written to fileInfoOut; this function sets
    ///                      *fileInfoCount to however many paths were written
    ///                      to fileInfoOut, except if the latter is a null
    ///                      pointer; then *fileInfoCount is set to the number
    ///                      of paths that this will write in a subsequent call.
    /// @param resourceLoader Resource loader which created this parser.
    /// @param files The files which have been loaded so far.
    virtual void getAdditionalRequiredFiles(ResourceLoader::FileInfo* fileInfoOut,
                                            U32* fileInfoCount,
                                            ResourceLoader& resourceLoader,
                                            const ResourceLoader::RawData& files);

    /// Decodes a resource encoded in a byte stream.
    ///
    /// Given a byte stream describing a resource (e.g. a PNG file in memory),
    /// this function decodes it to a usable format (e.g. a PNG file will be
    /// uncompressed).
    ///
    /// @param data Pointer to the data to decode.
    /// @param size Size (in bytes) of the given data. Note that if this is 0,
    ///             then if data is not nullptr, it means that data is instead
    ///             of type const ResourceLoader::RawFile*; observe furthermore
    ///             that while any auxiliary files have been given their own
    ///             names by this ResourceParser, the original file loaded by
    ///             the ResourceLoader is called "main".
    /// @param options Options specific to this parser as a JSON value.
    /// @param resourceLoader Resource loader which created this parser.
    /// @param resourceOut Pointer to object storing the decoded resource.
    ///
    /// @returns True if parsing succeeded, false otherwise.
    virtual bool parse(const U8* data, size_t size,
                       const kraken::JSONValue& options,
                       ResourceLoader& resourceLoader,
                       ResourceLoader::HostResource* resourceOut);

    /// Performs initial preparation before transferring to device.
    ///
    /// This function will be called exactly once by ResourceLoader for a given
    /// resource before any transfer operations from host to device. This makes
    /// it ideal to allocate device buffers, images, etc. It is not required
    /// to be implemented and defaults to doing nothing.
    ///
    /// @remark This may be called synchronously and therefore the function
    ///         implementation should have low complexity.
    ///
    /// @param commandBuf Vulkan buffer to record commands into. Note that the
    ///                   queue this will be submitted to is only guaranteed to
    ///                   have VK_QUEUE_TRANSFER_BIT set, so no graphics
    ///                   operations are permitted.
    /// @param device Vulkan device to use in Vulkan calls.
    /// @param allocator Vulkan allocator to use in creation of Vulkan objects.
    /// @param options Transfer options for the given resource. May differ from
    ///                those given to parse(...).
    /// @param source Source of data to transfer from.
    /// @param resourceOut Pointer to object storing host information of
    ///                    resource. This may not be stored as the actual
    ///                    resource might be relocated in memory between
    ///                    different function calls.
    ///
    /// @returns VK_SUCCESS if successful, otherwise an appropriate error code.
    virtual VkResult initialiseTransfer(VkCommandBuffer commandBuf,
                                        VkDevice device,
                                        VmaAllocator allocator,
                                        const kraken::JSONValue& options,
                                        ResourceLoader::DataSource source,
                                        ResourceLoader::VulkanResource* resourceOut);

    /// Obtains a pointer to the next block of transfer data.
    ///
    /// This function is called sequentially (i.e. externally synchronised) to
    /// get a block of transfer data which fits within maxSize. The returned
    /// pointer must be valid until the next call to this function. Furthermore,
    /// some custom data can be written to tagOut and idOut, which will be given
    /// back to transfer() when that is called for this particular block. The
    /// return value indicates whether there is more data left -- true means
    /// that this should be called again, while false signifies the end of data.
    ///
    /// @remark To indicate a fatal error, sizeOut should be set to zero.
    ///
    /// @remark This may be called both before and after initialiseTransfer.
    ///
    /// @remark This is guaranteed to be called asynchronously and therefore
    ///         medium-weight work (like allocating and copying multi-megabyte
    ///         buffers) is acceptable.
    ///
    /// @param source Source of data to transfer from.
    /// @param maxSize Maximal size of output data.
    /// @param dataOut Pointer to output data.
    /// @param sizeOut Size of output data. Zero indicates a fatal error.
    /// @param tagOut Some pointer which will be passed back when the
    ///               corresponding block is transferred using transfer().
    ///               It will not be dereferenced externally.
    /// @param extraTagOut An extra integer tag which will be passed back to
    ///                    transfer() when the corresponding block is
    ///                    transferred.
    ///
    /// @returns True if there is more data left, otherwise false.
    virtual bool getNextTransferData(ResourceLoader::DataSource source,
                                     VkDeviceSize maxSize,
                                     const U8** dataOut,
                                     size_t* sizeOut,
                                     const void** tagOut,
                                     U32* extraTagOut);

    /// Transfers resource from host-visible buffer.
    ///
    /// This function transfers a resource from a host-visible staging buffer to
    /// the actual Vulkan resource. Note that this may be called several times
    /// to transfer part of the data. However, no ordering constraint is imposed
    /// on the data so this may give data in any order, with only 'dataOffset'
    /// determining which part of the resource that is to be transferred.
    /// This is guaranteed to be called only after initialiseTransfer() is
    /// called and all commands recorded there are finished, but before
    /// finaliseTransfer() is called. Note that this function only may be called
    /// simultaneously from different threads for the same object and therefore
    /// must be made thread-safe.
    ///
    /// @param commandBuf Command buffer to record commands into.
    /// @param buffer The buffer containing the data to be copied from.
    /// @param bufferOffset Offset into source buffer where the data starts.
    /// @param bufferSize Size (in bytes) of data stored in source buffer.
    /// @param dataOffset Offset into resource determining which part of the
    ///                   resource data is currently being transferred.
    /// @param resourceOut Pointer to object storing host information of
    ///                    resource. This may not be stored as the actual
    ///                    resource might be relocated in memory between
    ///                    different function calls.
    ///
    /// @returns VK_SUCCESS on success, otherwise an appropriate error code.
    virtual VkResult transfer(VkCommandBuffer commandBuf,
                              VkBuffer buffer,
                              VkDeviceSize bufferOffset,
                              VkDeviceSize bufferSize,
                              const void* tag,
                              U32 extraTag,
                              ResourceLoader::VulkanResource* resourceOut);

    /// Records final commands to be executed after transfer.
    ///
    /// This function may only be called after any calls to transfer() have
    /// finished, along with any commands they have recorded into command
    /// buffers. This can be used to e.g. perform a memory barrier, etc. It is
    /// not required to be implemented and defaults to doing nothing.
    ///
    /// @remark This may be called synchronously and therefore the function
    ///         implementation should have low complexity.
    ///
    /// @param commandBuf Command buffer to record commands into.
    /// @param device Vulkan device to use in Vulkan calls.
    /// @param allocator Vulkan allocator to use in creation of Vulkan objects.
    /// @param resourceOut Pointer to object storing host information of
    ///                    resource. This may not be stored as the actual
    ///                    resource might be relocated in memory between
    ///                    different function calls.
    ///
    /// @returns VK_SUCCESS on success, otherwise an appropriate error code.
    virtual VkResult finaliseTransfer(VkCommandBuffer commandBuf,
                                      VkDevice device,
                                      VmaAllocator allocator,
                                      ResourceLoader::VulkanResource* resourceOut);

protected:
    /// Gets information about the current resource.
    ///
    /// This can be used to obtain creation info for this resource. If any of
    /// the pointers are nullptr, nothing will be written to them.
    ///
    /// \param loader ResourceLoader which is the only thing that should call
    ///               any of the functions defined in this interface in the
    ///               first place.
    /// \param id ID of the current resource.
    /// \param options Current resource's options.
    /// \param mediaType Current resource's media type.
    /// \param path Current resource's path (if any).
    void getCurrentResourceInfo(ResourceLoader& loader,
                                ResourceLoader::ResourceID* id = nullptr,
                                const ResourceLoader::Options** options = nullptr,
                                const std::string** mediaType = nullptr,
                                const fs::path** path = nullptr) const;
};

inline bool operator!(ResourceLoader::ResourceType tp)
{
    return tp == ResourceLoader::ResourceType::None;
}

inline ResourceLoader::ResourceType operator|(ResourceLoader::ResourceType a, ResourceLoader::ResourceType b)
{
    return static_cast<ResourceLoader::ResourceType>(static_cast<U32>(a)|static_cast<U32>(b));
}
inline ResourceLoader::ResourceType operator&(ResourceLoader::ResourceType a, ResourceLoader::ResourceType b)
{
    return static_cast<ResourceLoader::ResourceType>(static_cast<U32>(a)&static_cast<U32>(b));
}

inline ResourceLoader::ResourceType& operator|=(ResourceLoader::ResourceType& a, ResourceLoader::ResourceType b)
{
    return a = (a|b);
}
inline ResourceLoader::ResourceType& operator&=(ResourceLoader::ResourceType& a, ResourceLoader::ResourceType b)
{
    return a = (a&b);
}

inline ResourceLoader::ResourceType operator~(ResourceLoader::ResourceType tp)
{
    return static_cast<ResourceLoader::ResourceType>(~static_cast<U32>(tp));
}


inline ResourceLoader::VulkanResource::Type operator|(ResourceLoader::VulkanResource::Type a, ResourceLoader::VulkanResource::Type b)
{
    return static_cast<ResourceLoader::VulkanResource::Type>(static_cast<U32>(a)|static_cast<U32>(b));
}
inline ResourceLoader::VulkanResource::Type operator&(ResourceLoader::VulkanResource::Type a, ResourceLoader::VulkanResource::Type b)
{
    return static_cast<ResourceLoader::VulkanResource::Type>(static_cast<U32>(a)&static_cast<U32>(b));
}

inline ResourceLoader::VulkanResource::Type& operator|=(ResourceLoader::VulkanResource::Type& a, ResourceLoader::VulkanResource::Type b)
{
    return a = (a|b);
}
inline ResourceLoader::VulkanResource::Type& operator&=(ResourceLoader::VulkanResource::Type& a, ResourceLoader::VulkanResource::Type b)
{
    return a = (a&b);
}

inline ResourceLoader::VulkanResource::Type operator~(ResourceLoader::VulkanResource::Type tp)
{
    return static_cast<ResourceLoader::VulkanResource::Type>(~static_cast<U32>(tp));
}
#endif // RESOURCELOADER_HPP_INCLUDED
