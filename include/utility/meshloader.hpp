#ifndef UTILITY_MESHLOADER_HPP_INCLUDED
#define UTILITY_MESHLOADER_HPP_INCLUDED

#include <kraken/io/datasource.hpp>
#include <kraken/globaltypes.hpp>

class Mesh;

bool loadPLY(kraken::io::DataSource& source, Mesh& meshOut);
bool loadSTL(kraken::io::DataSource& source, Mesh& meshOut);

#endif // UTILITY_MESHLOADER_HPP_INCLUDED
