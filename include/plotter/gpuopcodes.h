#ifndef PLOTTER_OPCODES_H_INCLUDED
#define PLOTTER_OPCODES_H_INCLUDED

#define PLOT_OP_RETURN 0u
#define PLOT_OP_PUSH_X 1u
#define PLOT_OP_PUSH_Y 2u
#define PLOT_OP_PUSH_CONSTANT 3u
#define PLOT_OP_UNARY_FUNCTION 4u
// Arguments to diadic operations are assumed to be stored left-to-right, i.e.
// A + B will be stored on the stack as [base] A B
#define PLOT_OP_ADD 5u
#define PLOT_OP_SUB 6u
#define PLOT_OP_MUL 7u
#define PLOT_OP_DIV 8u
#define PLOT_OP_MOD 9u
#define PLOT_OP_NEGATE 10u

#define PLOT_FUNCTION_ABS 0u
#define PLOT_FUNCTION_SIN 1u
#define PLOT_FUNCTION_COS 2u
#define PLOT_FUNCTION_TAN 3u
#define PLOT_FUNCTION_SIGN 4u

#endif // PLOTTER_OPCODES_H_INCLUDED
