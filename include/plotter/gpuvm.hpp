#ifndef PLOTTER_GPUPROGRAMVM_HPP_INCLUDED
#define PLOTTER_GPUPROGRAMVM_HPP_INCLUDED

#include <optional>

namespace plotter
{

class GPUProgram;

// Returns nullopt on runtime error.
std::optional<float> run(const GPUProgram& program, float x, float y);

} // namespace plotter

#endif // PLOTTER_GPUPROGRAMVM_HPP_INCLUDED
