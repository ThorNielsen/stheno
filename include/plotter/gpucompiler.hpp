#ifndef PLOTTER_GPUCOMPILER_HPP_INCLUDED
#define PLOTTER_GPUCOMPILER_HPP_INCLUDED

#include <kraken/globaltypes.hpp>

#include <optional>
#include <string>
#include <vector>

namespace plotter
{

// This is specifically crafted to be run on a GPU -- and only covers GPU-
// evaluable expressions.
struct GPUProgram
{
    std::vector<U32> code;
    std::vector<F32> constants;
};

std::optional<GPUProgram> compileForGPU(const std::string& source);

} // namespace plotter

#endif // PLOTTER_GPUCOMPILER_HPP_INCLUDED
