#ifndef STHENO_INTERPRETER_SOURCELOCATION_HPP_INCLUDED
#define STHENO_INTERPRETER_SOURCELOCATION_HPP_INCLUDED

#include <kraken/globaltypes.hpp>

#include <compare>

namespace interpreter
{

struct SourceCodeLocation
{
    U64 line; // Note: 0-indexed by default, be mindful when outputting.
    U64 character;

    [[nodiscard]] constexpr auto operator<=>(const SourceCodeLocation&) const noexcept = default;
};

// Describes a range of characters in source code. Observe that rangeEnd points
// to one-past-the-end of the range, and is thus not part of the code.
// Thus, if rangeBegin == rangeEnd, this can be interpreted either as a location
// or an empty range, depending on the context.
struct SourceCodeRange
{
    SourceCodeLocation rangeBegin;
    SourceCodeLocation rangeEnd;

    [[nodiscard]] SourceCodeRange normalise() const noexcept
    {
        if (rangeEnd.line && !rangeEnd.character)
        {
            return {rangeBegin, {rangeEnd.line-1, 0}};
        }
        return *this;
    }

    [[nodiscard]] constexpr auto operator<=>(const SourceCodeRange&) const noexcept = default;
};

} // namespace interpreter

#endif // STHENO_INTERPRETER_SOURCELOCATION_HPP_INCLUDED
