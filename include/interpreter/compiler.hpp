#ifndef INTERPRETER_COMPILER_HPP_INCLUDED
#define INTERPRETER_COMPILER_HPP_INCLUDED

#include "interpreter/error.hpp"
#include "interpreter/program.hpp"

#include <string>

namespace interpreter
{

class ASTContext;
class Expression;

struct CompiledProgram
{
    Program program;
    // Variable names, same size and indexing as program.inputVariables.
    std::vector<std::string> inputNames;
};

// We compile expressions until statements are implemented.
std::expected
<
    CompiledProgram,
    std::unique_ptr<CompilationError>
>
compile(const Expression& expression, const ASTContext& context);

} // namespace interpreter

#endif // INTERPRETER_COMPILER_HPP_INCLUDED
