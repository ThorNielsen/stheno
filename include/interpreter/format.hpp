#ifndef STHENO_INTERPRETER_FORMAT_HPP_INCLUDED
#define STHENO_INTERPRETER_FORMAT_HPP_INCLUDED

#include "interpreter/ast.hpp"
#include "interpreter/token.hpp"

#include <format>
#include <string>

namespace interpreter
{

std::string to_string(TokenType type);
std::string to_string(UnaryOperationType op);
std::string to_string(BinaryOperationType op);

} // namespace interpreter

template <>
struct std::formatter<interpreter::TokenType> : std::formatter<std::string>
{
    auto format(interpreter::TokenType tokenType, format_context& context) const
    {
        return formatter<string>::format(to_string(tokenType), context);
    }
};

template <>
struct std::formatter<interpreter::UnaryOperationType> : std::formatter<std::string>
{
    auto format(interpreter::UnaryOperationType op, format_context& context) const
    {
        return formatter<string>::format(to_string(op), context);
    }
};

template <>
struct std::formatter<interpreter::BinaryOperationType> : std::formatter<std::string>
{
    auto format(interpreter::BinaryOperationType op, format_context& context) const
    {
        return formatter<string>::format(to_string(op), context);
    }
};

#endif // STHENO_INTERPRETER_FORMAT_HPP_INCLUDED
