#ifndef INTERPRETER_VM_HPP_INCLUDED
#define INTERPRETER_VM_HPP_INCLUDED

#include "interpreter/error.hpp"
#include "interpreter/program.hpp"

namespace interpreter
{

// Until we have a better error system, we just return "compilation" errors on
// runtime errors.
std::expected
<
    PrimitiveVariable,
    std::unique_ptr<CompilationError>
>
evaluate(const Program& program);

} // namespace interpreter

#endif // INTERPRETER_VM_HPP_INCLUDED
