#ifndef STHENO_INTERPRETER_PARSER_HPP_INCLUDED
#define STHENO_INTERPRETER_PARSER_HPP_INCLUDED

#include "interpreter/error.hpp"
#include "interpreter/programsyntaxtree.hpp"
#include "interpreter/token.hpp"

#include <kraken/globaltypes.hpp>
#include <expected>
#include <vector>

namespace interpreter
{

std::expected
<
    ProgramSyntaxTree,
    std::unique_ptr<CompilationError>
>
parse(const std::vector<Token>& tokens);

} // namespace interpreter

#endif // STHENO_INTERPRETER_PARSER_HPP_INCLUDED
