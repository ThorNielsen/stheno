#ifndef STHENO_INTERPRETER_TYPES_HPP_INCLUDED
#define STHENO_INTERPRETER_TYPES_HPP_INCLUDED

#include <cstdint>

namespace interpreter
{

enum class PrimitiveVariableType : uint8_t
{
    Null,
    Boolean,
    Float,
    Double,
    Uint,
    Ulong,
};

union PrimitiveVariableElement
{
    bool asBool;
    float asFloat;
    double asDouble;
    uint32_t asUint;
    uint64_t asUlong;
};

struct PrimitiveVariable
{
    PrimitiveVariableElement element;
    PrimitiveVariableType type;

    [[nodiscard]] static PrimitiveVariable create(double value) noexcept
    {
        return {.element{.asDouble{value}}, .type{PrimitiveVariableType::Double}};
    }
};

} // namespace interpreter

#endif // STHENO_INTERPRETER_TYPES_HPP_INCLUDED
