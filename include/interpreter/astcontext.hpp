#ifndef STHENO_INTERPRETER_ASTCONTEXT_HPP_INCLUDED
#define STHENO_INTERPRETER_ASTCONTEXT_HPP_INCLUDED

#include <compare>
#include <cstdint>
#include <optional>
#include <stack>
#include <string>
#include <unordered_map>

namespace interpreter
{

class ASTContext
{
public:
    struct StringID
    {
        uint32_t id;

        [[nodiscard]] operator uint32_t() const noexcept { return id; }
        [[nodiscard]] auto operator<=>(const StringID&) const noexcept = default;
    };

    [[nodiscard]] std::string_view operator[](StringID id) const { return getString(id); }

    [[nodiscard]] std::string_view getString(StringID id) const;
    StringID getID(std::string_view input);
    [[nodiscard]] std::optional<StringID> getID(std::string_view input) const;

    void erase(StringID id);

private:
    StringID nextID() noexcept;

    // We might use a plain map instead to trade O(1) "average"-case, O(n)
    // worst-case performance for O(log(n)) guaranteed performance, since
    // rehashing can cause pretty long-running operations.
    std::unordered_map<std::string_view, StringID> m_viewToID;
    // Could be a vector; references to strings should remain valid since the
    // string's move constructor *should* keep elements in the same memory.
    std::unordered_map<uint32_t, std::string> m_strings;
    std::stack<uint32_t> m_freeIDs;
};

} // namespace interpreter

#endif // STHENO_INTERPRETER_ASTCONTEXT_HPP_INCLUDED
