#ifndef INTERPRETER_OPCODES_HPP_INCLUDED
#define INTERPRETER_OPCODES_HPP_INCLUDED

#include <cstdint>

namespace interpreter
{

enum class Opcode : uint8_t
{
    Add = 0,
    Sub,
    Mul,
    Div,
    Mod,
    Exp,
    PushConstant,
    PushVariable
};

} // namespace interpreter

#endif // INTERPRETER_OPCODES_HPP_INCLUDED
