#ifndef STHENO_INTERPRETER_ASTMANIP_SIMPLIFICATION_HPP_INCLUDED
#define STHENO_INTERPRETER_ASTMANIP_SIMPLIFICATION_HPP_INCLUDED

#include "interpreter/programsyntaxtree.hpp"

namespace interpreter
{

// Simplification *can* change the context since it might need ot introduce
// auxiliary elements and/or delete others.
std::unique_ptr<Expression>
simplify(std::unique_ptr<Expression> expr,
         ASTContext& context);

ProgramSyntaxTree simplify(ProgramSyntaxTree&& pst);

inline ProgramSyntaxTree simplifyProgram(ProgramSyntaxTree&& pst)
{
    return simplify(std::move(pst));
}

} // namespace interpreter

#endif // STHENO_INTERPRETER_ASTMANIP_SIMPLIFICATION_HPP_INCLUDED
