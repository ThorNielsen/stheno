#ifndef STHENO_INTERPRETER_ASTMANIP_COPYING_HPP_INCLUDED
#define STHENO_INTERPRETER_ASTMANIP_COPYING_HPP_INCLUDED

#include "interpreter/ast.hpp"
#include "interpreter/astcontext.hpp"

#include <set>

namespace interpreter
{

std::set<ASTContext::StringID> findIdentifiers(const Expression& expr);

// Applies a metafunction (like diff and similar) if that is encoded, otherwise
// it just returns the original expression.
std::unique_ptr<Expression>
applyMetafunction(std::unique_ptr<CallExpression> expr,
                  ASTContext& context);

template <typename Deriv>
std::unique_ptr<Deriv> downcastNode(std::unique_ptr<Expression>&& ptr) noexcept
{
    return std::unique_ptr<Deriv>(static_cast<Deriv*>(ptr.release()));
}

} // namespace interpreter

#endif // STHENO_INTERPRETER_ASTMANIP_COPYING_HPP_INCLUDED
