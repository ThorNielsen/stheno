#ifndef STHENO_INTERPRETER_ASTMANIP_DIFFERENTIATION_HPP_INCLUDED
#define STHENO_INTERPRETER_ASTMANIP_DIFFERENTIATION_HPP_INCLUDED

#include "interpreter/ast.hpp"
#include "interpreter/astcontext.hpp"
#include "interpreter/error.hpp"

namespace interpreter
{

ExpectedPointer<Expression>
computeDerivative(std::unique_ptr<Expression> expr,
                  ASTContext& context,
                  ASTContext::StringID diffWRT);

} // namespace interpreter

#endif // STHENO_INTERPRETER_ASTMANIP_DIFFERENTIATION_HPP_INCLUDED
