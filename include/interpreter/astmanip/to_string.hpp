#ifndef STHENO_INTERPRETER_ASTMANIP_TO_STRING_HPP_INCLUDED
#define STHENO_INTERPRETER_ASTMANIP_TO_STRING_HPP_INCLUDED

#include <string>

namespace interpreter
{

class Expression;
class ASTContext;
class ProgramSyntaxTree;

std::string to_string(const Expression& expr,
                      const ASTContext& context);

std::string to_string(const ProgramSyntaxTree& pst);

} // namespace interpreter

#endif // STHENO_INTERPRETER_ASTMANIP_TO_STRING_HPP_INCLUDED
