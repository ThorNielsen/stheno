#ifndef STHENO_INTERPRETER_ASTOPERATORS_HPP_INCLUDED
#define STHENO_INTERPRETER_ASTOPERATORS_HPP_INCLUDED

#include "interpreter/ast.hpp"

namespace interpreter
{

[[nodiscard]] std::unique_ptr<Expression>
operator+(std::unique_ptr<Expression> left,
          std::unique_ptr<Expression> right);

[[nodiscard]] std::unique_ptr<Expression>
operator-(std::unique_ptr<Expression> left,
          std::unique_ptr<Expression> right);

[[nodiscard]] std::unique_ptr<Expression>
operator*(std::unique_ptr<Expression> left,
          std::unique_ptr<Expression> right);

[[nodiscard]] std::unique_ptr<Expression>
operator/(std::unique_ptr<Expression> left,
          std::unique_ptr<Expression> right);

[[nodiscard]] std::unique_ptr<Expression>
operator%(std::unique_ptr<Expression> left,
          std::unique_ptr<Expression> right);

[[nodiscard]] std::unique_ptr<Expression>
operator*(double left,
          std::unique_ptr<Expression> right);

[[nodiscard]] std::unique_ptr<Expression>
operator/(double left,
          std::unique_ptr<Expression> right);

[[nodiscard]] std::unique_ptr<Expression>
operator-(std::unique_ptr<Expression> right);

} // namespace interpreter

#endif // STHENO_INTERPRETER_ASTOPERATORS_HPP_INCLUDED
