#ifndef STHENO_INTERPRETER_EVALUATOR_HPP_INCLUDED
#define STHENO_INTERPRETER_EVALUATOR_HPP_INCLUDED

#include "interpreter/ast.hpp"
#include "interpreter/astcontext.hpp"
#include "interpreter/types.hpp"

#include <expected>
#include <map>
#include <string>

namespace interpreter
{

struct EvaluationContext
{
    std::map<std::string, PrimitiveVariable> bindings;
    const ASTContext* astContext;
};

struct RuntimeError
{
    std::string message;
};

using EvaulationResultOrRuntimeError = std::expected<PrimitiveVariable,
                                                     RuntimeError>;

EvaulationResultOrRuntimeError
evaluateExpression(const Expression& expression, const EvaluationContext& context);

} // namespace interpreter

#endif // STHENO_INTERPRETER_EVALUATOR_HPP_INCLUDED
