#ifndef STHENO_INTERPRETER_LEXER_HPP_INCLUDED
#define STHENO_INTERPRETER_LEXER_HPP_INCLUDED

#include "interpreter/error.hpp"
#include "interpreter/token.hpp"

#include <kraken/globaltypes.hpp>

#include <expected>
#include <memory>
#include <string>
#include <vector>

namespace interpreter
{

std::expected
<
    std::vector<Token>,
    std::unique_ptr<CompilationError>
>
tokenise(const std::string& expression);

} // namespace interpreter

#endif // STHENO_INTERPRETER_LEXER_HPP_INCLUDED
