#ifndef INTERPRETER_PROGRAM_HPP_INCLUDED
#define INTERPRETER_PROGRAM_HPP_INCLUDED

#include "interpreter/opcodes.hpp"
#include "interpreter/types.hpp"
#include <vector>

namespace interpreter
{

struct Program
{
    std::vector<Opcode> codes;

    // All input variables are assigned IDs, and can be mapped through this
    // interface:
    std::vector<PrimitiveVariable> inputVariables;
    std::vector<PrimitiveVariable> constants;
};

} // namespace interpreter

#endif // INTERPRETER_PROGRAM_HPP_INCLUDED
