#ifndef STHENO_INTERPRETER_ERROR_HPP_INCLUDED
#define STHENO_INTERPRETER_ERROR_HPP_INCLUDED

#include "interpreter/sourcelocation.hpp"

#include <exception>
#include <expected>
#include <memory>
#include <optional>
#include <string>
#include <string_view>

namespace interpreter
{

class CompilationError
{
public:
    virtual ~CompilationError() {}

    // The location in the source code, if available.
    [[nodiscard]] virtual std::optional<SourceCodeRange> location() const noexcept = 0;

    // Yields a human-readable description of the error.
    [[nodiscard]] virtual std::string_view description() const noexcept = 0;

    // Yields a description of the exact compilation pass that contained the
    // error.
    [[nodiscard]] virtual std::string_view compilerPass() const noexcept = 0;
};

template <typename ExpectedType>
using Expected = std::expected<ExpectedType, std::unique_ptr<CompilationError>>;

template <typename ContainedType>
using ExpectedPointer =
std::expected
<
    std::unique_ptr<ContainedType>,
    std::unique_ptr<CompilationError>
>;

class UncaughtExceptionError : public CompilationError
{
public:
    // Note: “pass” MUST be initialised with something that remains valid for
    // the lifetime of this exception, i.e. a constant string view.
    UncaughtExceptionError(const std::exception& exception, std::string_view pass) noexcept;

    [[nodiscard]] std::optional<SourceCodeRange> location() const noexcept override;

    [[nodiscard]] std::string_view description() const noexcept override;

    [[nodiscard]] std::string_view compilerPass() const noexcept override;

private:
    std::unique_ptr<std::string> m_description;
    std::string_view m_pass;
};

namespace detail
{

// Adds non-noexcept specified versions of each function, which can optionally
// be overridden instead; if one of those throw (or are not implemented), a
// fallback value is used instead.
class NoexceptCompilationError : public CompilationError
{
public:
    [[nodiscard]] std::optional<SourceCodeRange> location() const noexcept override;

    [[nodiscard]] std::string_view description() const noexcept override;

    [[nodiscard]] std::string_view compilerPass() const noexcept override;

protected:
    [[nodiscard]] virtual std::optional<SourceCodeRange> getLocation() const;

    [[nodiscard]] virtual std::string_view getDescription() const;

    [[nodiscard]] virtual std::string_view getCompilerPass() const;
};

} // namespace detail

class InternalCompilerError : public CompilationError
{
public:
    InternalCompilerError(std::string_view pass,
                          std::string&& description,
                          std::optional<SourceCodeRange> location = std::nullopt) noexcept;

    [[nodiscard]] std::optional<SourceCodeRange> location() const noexcept override;

    [[nodiscard]] std::string_view description() const noexcept override;

    [[nodiscard]] std::string_view compilerPass() const noexcept override;

private:

    std::optional<SourceCodeRange> m_location;
    std::string m_description;
    std::string_view m_pass;
};

} // namespace interpreter

#endif // STHENO_INTERPRETER_ERROR_HPP_INCLUDED
