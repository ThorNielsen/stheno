#ifndef STHENO_INTERPRETER_PROGRAMSYNTAXTREE_HPP_INCLUDED
#define STHENO_INTERPRETER_PROGRAMSYNTAXTREE_HPP_INCLUDED

#include "interpreter/ast.hpp"
#include "interpreter/astcontext.hpp"

#include <memory>

namespace interpreter
{

struct ProgramSyntaxTree
{
    std::unique_ptr<Expression> expression;
    ASTContext context;
};

} // namespace interpreter

#endif // STHENO_INTERPRETER_PROGRAMSYNTAXTREE_HPP_INCLUDED
