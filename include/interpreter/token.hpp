#ifndef STHENO_INTERPRETER_TOKEN_HPP_INCLUDED
#define STHENO_INTERPRETER_TOKEN_HPP_INCLUDED

#include "interpreter/sourcelocation.hpp"

#include <string_view>

namespace interpreter
{

enum class TokenType
{
    LeftParenthesis, RightParenthesis,
    LeftSquareBracket, RightSquareBracket,
    Plus, Dash, Star, Slash, Percent, Caret, Comma,
    Identifier, Number,

    Eof
};

struct Token
{
    std::string_view value;
    TokenType type;
    SourceCodeLocation location; // We already have the length from the value.

    [[nodiscard]] auto operator<=>(const Token&) const = default;
};

} // namespace interpreter

#endif // STHENO_INTERPRETER_TOKEN_HPP_INCLUDED
