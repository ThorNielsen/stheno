#ifndef STHENO_INTERPRETER_AST_HPP_INCLUDED
#define STHENO_INTERPRETER_AST_HPP_INCLUDED

#include "interpreter/astcontext.hpp"
#include <cstdint>
#include <memory>
#include <vector>

namespace interpreter
{

namespace ExpressionTypeBits
{
    enum Type : uint32_t
    {
        Array = 0,
        Binary,
        Call,
        Identifier,
        Number,
        Unary,
    };

    constexpr std::underlying_type_t<Type> extendedTypeOffset = 0x100000;
}

using ExpressionType = ExpressionTypeBits::Type;

enum class UnaryOperationType
{
    UnaryPlus,
    UnaryMinus,
};

enum class BinaryOperationType
{
    Addition,
    Subtraction,
    Multiplication,
    Division,
    Modulo,
    Exponentiation,
};

class Expression
{
public:
    virtual ~Expression() {}

    [[nodiscard]] virtual ExpressionType type() const noexcept = 0;

    [[nodiscard]] virtual std::unique_ptr<Expression> copy() const = 0;

    template <typename Derived>
    [[nodiscard]] Derived& as() noexcept { return static_cast<Derived&>(*this); }
    template <typename Derived>
    [[nodiscard]] const Derived& as() const noexcept { return static_cast<const Derived&>(*this); }

    [[nodiscard]] virtual bool isEqual(const Expression& other) const noexcept = 0;
};

class ArrayExpression : public Expression
{
public:
    ArrayExpression(std::vector<std::unique_ptr<Expression>>&& elems)
        : elements{std::move(elems)}
    {}

    ExpressionType type() const noexcept override
    {
        return ExpressionType::Array;
    }

    std::unique_ptr<Expression> copy() const override
    {
        std::vector<std::unique_ptr<Expression>> newElems;
        for (const auto& elem : elements)
        {
            newElems.emplace_back(elem->copy());
        }
        return std::make_unique<ArrayExpression>(std::move(newElems));
    }

    [[nodiscard]] bool isEqual(const Expression& other) const noexcept override
    {
        if (type() != other.type()) return false;
        const auto& otherAsThis = other.as<ArrayExpression>();
        if (elements.size() != otherAsThis.elements.size()) return false;
        for (std::size_t i = 0; i < elements.size(); ++i)
        {
            if (!elements[i]->isEqual(*otherAsThis.elements[i])) return false;
        }
        return true;
    }

    std::vector<std::unique_ptr<Expression>> elements;
};

class BinaryExpression : public Expression
{
public:
    BinaryExpression(BinaryOperationType operation,
                     std::unique_ptr<Expression>&& leftExpr,
                     std::unique_ptr<Expression>&& rightExpr)
        : op{operation}
        , left{std::move(leftExpr)}
        , right{std::move(rightExpr)}
    {}

    ExpressionType type() const noexcept override
    {
        return ExpressionType::Binary;
    }

    std::unique_ptr<Expression> copy() const override
    {
        return std::make_unique<BinaryExpression>(op, left->copy(), right->copy());
    }

    [[nodiscard]] bool isEqual(const Expression& other) const noexcept override
    {
        if (type() != other.type()) return false;
        const auto& otherAsThis = other.as<BinaryExpression>();
        return op == otherAsThis.op
            && left->isEqual(*otherAsThis.left)
            && right->isEqual(*otherAsThis.right);
    }

    BinaryOperationType op;
    std::unique_ptr<Expression> left;
    std::unique_ptr<Expression> right;
};

class CallExpression : public Expression
{
public:
    CallExpression(ASTContext::StringID lhs,
                   std::vector<std::unique_ptr<Expression>>&& args)
        : toCall{std::move(lhs)}
        , arguments{std::move(args)}
    {}

    ExpressionType type() const noexcept override
    {
        return ExpressionType::Call;
    }

    std::unique_ptr<Expression> copy() const override
    {
        std::vector<std::unique_ptr<Expression>> newArgs;
        for (const auto& arg : arguments)
        {
            newArgs.emplace_back(arg->copy());
        }
        return std::make_unique<CallExpression>(toCall, std::move(newArgs));
    }

    [[nodiscard]] bool isEqual(const Expression& other) const noexcept override
    {
        if (type() != other.type()) return false;
        const auto& otherAsThis = other.as<CallExpression>();
        if (toCall != otherAsThis.toCall) return false;
        if (arguments.size() != otherAsThis.arguments.size()) return false;
        for (std::size_t i = 0; i < arguments.size(); ++i)
        {
            if (!arguments[i]->isEqual(*otherAsThis.arguments[i])) return false;
        }
        return true;
    }

    ASTContext::StringID toCall; // Should be replaced with an expression if we should support custom functions.
    std::vector<std::unique_ptr<Expression>> arguments;
};

class IdentifierExpression : public Expression
{
public:
    IdentifierExpression(ASTContext::StringID identifierName)
        : name{identifierName}
    {}

    ExpressionType type() const noexcept override
    {
        return ExpressionType::Identifier;
    }

    std::unique_ptr<Expression> copy() const override
    {
        return std::make_unique<IdentifierExpression>(name);
    }

    [[nodiscard]] bool isEqual(const Expression& other) const noexcept override
    {
        if (type() != other.type()) return false;
        return name == other.as<IdentifierExpression>().name;
    }

    ASTContext::StringID name;
};

// Could be renamed to LiteralExpression in case we want to support multiple
// literal types (and maybe even with type checking).
class NumberExpression : public Expression
{
public:
    NumberExpression(double v)
        : value{v}
    {}

    ExpressionType type() const noexcept override
    {
        return ExpressionType::Number;
    }

    std::unique_ptr<Expression> copy() const override
    {
        return std::make_unique<NumberExpression>(value);
    }

    [[nodiscard]] bool isEqual(const Expression& other) const noexcept override
    {
        if (type() != other.type()) return false;
        return value == other.as<NumberExpression>().value;
    }

    double value;
};

class UnaryExpression : public Expression
{
public:
    UnaryExpression(UnaryOperationType operation, std::unique_ptr<Expression>&& rightExpr)
        : op{operation}
        , right{std::move(rightExpr)}
    {}

    ExpressionType type() const noexcept override
    {
        return ExpressionType::Unary;
    }

    std::unique_ptr<Expression> copy() const override
    {
        return std::make_unique<UnaryExpression>(op, right->copy());
    }

    [[nodiscard]] bool isEqual(const Expression& other) const noexcept override
    {
        if (type() != other.type()) return false;
        const auto& otherAsThis = other.as<UnaryExpression>();
        return op == otherAsThis.op
            && right->isEqual(*otherAsThis.right);
    }

    UnaryOperationType op;
    std::unique_ptr<Expression> right;
};

} // namespace interpreter

#endif // STHENO_INTERPRETER_AST_HPP_INCLUDED
