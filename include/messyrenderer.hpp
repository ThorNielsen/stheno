#ifndef MESSYRENDERER_HPP_INCLUDED
#define MESSYRENDERER_HPP_INCLUDED

#include "renderer/dataproviders.hpp"
#include "renderer/graphicsshaderinstance.hpp"

#include <cmath>
#include <filesystem>
#include <future>
#include <mutex>
#include <optional>

#include "renderer/inputlayouts.hpp"
#include "vulkan/allocation_utils.hpp"
#include "vulkan/allocator.hpp"
#include "vulkan/descriptor.hpp"
#include "vulkan/framegraph.hpp"
#include "vulkan/image.hpp"
#include "vulkan/mesh.hpp"
#include "vulkan/pipeline.hpp"
#include "vulkan/shader.hpp"
#include "vulkan/swapchain.hpp"

#include <kraken/window/window.hpp>

#include <kraken/math/transform.hpp>

#include "resourceloader.hpp"
#include "renderer/camera.hpp"
#include "renderer/controls.hpp"
#include "renderer/device.hpp"
#include "renderer/instance.hpp"
#include "renderer/perspective.hpp"
#include "renderer/scene.hpp"
#include "renderer/windowopener.hpp"

namespace fs = std::filesystem;

// To implement:
// [ ] Alignment of an arbitrary number of meshes so that multiple scans can be
//     automatically merged to a single one.
//     Subtasks to implement:
//     [ ] Manually connecting two points of two meshes (select triangles or
//         vertices and mark identical).
//     [x] Display line between two selected points between two meshes.
//     [ ] (Debug) Show distance between two points of two meshes and optionally
//         just their normal distance (i.e. mesh-mesh distance).
//     [ ] Implement realignment matrix computation for the simplest case where
//         there are exactly enough constraints (selected point pairs) to derive
//         the appropriate transformation.
//         Cases:
//         [ ] Pure translation. (Mostly for testing).
//         [ ] Pure rotation.
//         [ ] Combined translation and rotation.
//         [ ] Translation, rotation and reflection.
//         [ ] Translation, rotation, reflection and scale.
//     [ ] Detect when enough point pairs have been selected, change line
//         colouring to mark this fact. Also show resulting match, maybe with
//         transparency.
//     [ ] Add statistical matching so that many point pairs can be selected and
//         then together matched to a transformation matrix.
//     [ ] For each point pair, compute distance loss, write that to terminal,
//         and colour lines according to their distance loss (here distance loss
//         is the distance between the points when the models are transformed;
//         note that they will all be zero if there are as many constraints as
//         degrees of freedom in the realignment matrix).
// [ ] General import of models / meshes by drag-and-drop, with PBR materials if
//     at all possible.
//     [ ] Autodetection of file format.
//     [ ] Autogeneration of normals if not already existing
//     [ ] Finding a matching shader interface for the specific model.
//     [ ] For mesh-only models: Support drag-and-drop of images onto model to
//         apply that as a texture (hold C for colour / N for normal, and maybe
//         default to just applying to colour).

// This contains a single line meant to be drawn in 3d.
struct DrawableLine
{
    struct Vertex
    {
        vec4 positionAndWidth; // Packed as .xyz = position, .w = width. Quite _fitting_.
        vec4 colour; // Alpha can be set for transparency. Line draw order not
                     // guaranteed.
    };

    std::vector<Vertex> vertices;
};

struct Model
{
    const Mesh* mesh = nullptr;
    const Material* material = nullptr;
    std::unique_ptr<DefaultMaterialParameterProvider> materialProvider;
    std::array<VkDescriptorSet, 8> materialSets; // All that are not VK_NULL_HANDLE are valid.
    mat4 transform = mat4(1.f);
};

using EnqueueableFunction = std::function<void(VkCommandBuffer)>;

class MessyRenderer
{
public:
    MessyRenderer(fs::path dataDirectory);

    bool run();

    ~MessyRenderer();

private:
    struct PerFrameData
    {
        // Made per-frame to support resetting without any kind of sync issues.
        VkCommandPool pool = VK_NULL_HANDLE;
        VkCommandBuffer commandBuffer = VK_NULL_HANDLE;
        VkFence completionFence = VK_NULL_HANDLE;
        VkSemaphore imageAcquire = VK_NULL_HANDLE;
        VkSemaphore renderFinish = VK_NULL_HANDLE;
        dvec2 cursorPos;
        Mesh lineMesh; // Ugly, but we need to update the buffers at some point.
                       // There *really* needs to be a better way of handling
                       // destruction, e.g. a buffer destruction queue.
    };

    VkResult immediateExecute(const std::function<void(VkCommandBuffer)>&);

    VkResult createAllocator();
    void createSwapchain();
    void recreateSwapchain();

    // All of this is scene-specific ...
    // Both of these internally synchronises access; m_modelMutex MUST NOT be
    // held by a caller. The sole exception is if one passes in true as
    // hasExternalSync.
    void invalidateDescriptors(size_t modelIndex, bool hasExternalSync = false);
    void notifyLoadStatusUpdated(size_t modelIndex);

    void constructFrameGraph();

    Pipeline createSwapchainCopyPipeline(U32 width, U32 height,
                                         VkRenderPass renderpass,
                                         U32 subpass) const;
    void createSwapchainCopyStuff();

    void updateDescriptors(U32 imageIndex, Model& model);

    void updateHoveredModel(std::optional<size_t> newHover);
    void updateSelectedModel(std::optional<size_t> newSelected);

    void recordCommands();

    void updateUniforms(U32 imageIndex);
    void updateLines(U32 imageIndex);

    Image createPlaceholderTexture(VkExtent3D extent,
                                   VkFormat format = VK_FORMAT_R8G8B8A8_SRGB);

    void updateLoadedResources();

    void destroySwapchain();
    void performDestruction();

    void setupWindowCallbacks();

    PerFrameData& currFrame() { return m_frameData[m_currFrame]; }
    const PerFrameData& currFrame() const { return m_frameData[m_currFrame]; }
    void advanceToNextFrame()
    {
        m_currFrame = (m_currFrame + 1) % maxCachedFrames;
    }

    std::optional<DrawableLine::Vertex> getCurrentVertex(std::optional<vec3> replacementPosition = std::nullopt);
    void endCurrentLine();

    constexpr static size_t swapchainGroup = 1;
    constexpr static U32 maxCachedFrames = 2;

    static_assert(maxCachedFrames <= 32, "Must fit within a 32-bit integer.");

    constexpr static U32 maxCachedFrameMask = (U32(1) << (maxCachedFrames & 31))
                                              - 1;

    // Required for Vulkan to function (and ResourceLoader).
    fs::path m_datadir;
    kraken::window::Window m_window;
    VulkanInstance m_instance;
    WindowOpener m_windowOpener;
    VulkanDevice m_device;
    VmaAllocator m_allocator;
    VkCommandPool m_transientCommandPool;
    ResourceLoader m_resourceLoader;
    std::vector<std::string> m_wantedLayers;
    std::vector<const char*> m_enabledLayers;

    // Required for presentation (swap chain, etc.)
    std::array<PerFrameData, maxCachedFrames> m_frameData;
    U32 m_currFrame;

    FrameGraph m_framegraph;
    Swapchain m_swapchain;

    // Swapchain copy pass stuff
    struct
    {
        GraphicsShader shader;
        Pipeline pipeline;
        DescriptorSetLayout descSetLayout;
        DescriptorSet descSet;
        FrameGraph::RenderPass* pass;
        VkEvent prevPassesFinished;
    } m_scCopy;

    // Semi-specific for this scene (caches, allocators, etc.).
    SamplerCache m_samplerCache;
    DescriptorAllocator m_descAllocator;
    std::deque<EnqueueableFunction> m_enqueuedFunctions;
    std::mutex m_functionEnqueueMutex;
    MultiAllocatedBuffer m_multiAllocatedBuffer;

    SceneObjectLoader m_sceneObjectLoader;

    // Required for specific scene (pipeline+shader+mesh+...)
    PerspectiveTransform m_perspective;
    std::unique_ptr<ScreenspaceMovableCamera> m_camera;
    MovementState m_currMovement;
    dvec2 m_lastCursorPosition;

    bool m_showNormals;
    bool m_drawToon;

    std::mutex m_sharedResourceMutex;

    struct InvalidatedDescriptor
    {
        size_t modelIndex;
        U32 invalidated;
    };

    // Protectected by m_modelMutex...
    std::vector<InvalidatedDescriptor> m_invalidatedDescriptors;

    std::map<SceneObjectID, size_t> m_sceneIDToModelIndex;
    std::vector<Model> m_models;
    std::mutex m_modelMutex;

    std::optional<size_t> m_selectedModel;
    std::optional<size_t> m_hoveringModel;
    std::optional<dvec3> m_currHoverPoint;
    std::optional<double> m_currHoverDepth;

    std::optional<DragMovementInfo> m_currDrag;

    DrawableLine m_currentDrawingLine;
    std::optional<double> m_currLineLastDepth;

    std::vector<DrawableLine> m_lines;

    ShaderInputSetLayout m_transformAndPickSetLayout;
    ShaderInputSetLayout m_lightSetLayout;
    ShaderInputSetLayout m_materialParameterSetLayout;
    ShaderInputSetLayout m_plotFunctionLayout;
    std::array<VkDescriptorSet[2], maxCachedFrames> m_transformLightSets;

    std::array<VkDescriptorSet, maxCachedFrames> m_plotFunctionSets;

    std::optional<std::future<std::string>> m_nextFunctionInput;

    DescriptorSet m_ddnDescriptorSet;

    GraphicsShader m_ddnShader;

    VertexInputSpecification m_defaultSpec;
    VertexInputSpecification m_flatSpec;
    VertexInputSpecification m_lineSpec;

    Material m_defaultMaterial;
    Material m_flatMaterial;
    Material m_lineMaterial;

    Material m_plotMaterial;

    std::unique_ptr<GlobalTransformProvider> m_transformProvider;
    std::unique_ptr<ReadbackArrayProvider> m_readbackProvider;
    std::unique_ptr<PickDataProvider> m_pickProvider;
    std::unique_ptr<LightDataProvider> m_lightDataProvider;
    std::unique_ptr<PlotFunctionBufferProvider> m_plotFunctionProvider;
    size_t m_lineElemDrawCount;

    std::atomic<bool> m_resized;
    Image m_placeholderTexture;
};

#endif // MESSYRENDERER_HPP_INCLUDED
