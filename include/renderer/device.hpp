#ifndef RENDERER_DEVICE_HPP_INCLUDED
#define RENDERER_DEVICE_HPP_INCLUDED

#include "vulkan/common.hpp"

#include <vulkan/vulkan.h>

#include <set>
#include <string>

// The instance is not used UNLESS no physical device is given in which case it
// will be used for enumeration of physical devices.
// If the given physical device is a null handle, this will select an
// appropriate one.
// If surface is a null handle, presentation support of queue is not checked,
// otherwise a device can be created only if it supports present to the given
// surface with a queue.
struct VulkanDeviceParameters
{
    VkInstance instance = VK_NULL_HANDLE;
    VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
    VkSurfaceKHR surface = VK_NULL_HANDLE;

    QueueSelectionInfo queue;
    VkPhysicalDeviceFeatures features = {};

    std::set<std::string> requiredExtensions;
    std::set<std::string> optionalExtensions;
};

// Not ideal now as only one unprioritised queue will be added, but that can be
// changed later, when appropriate.
class VulkanDevice
{
public:
    VulkanDevice(const VulkanDeviceParameters& params = {});
    ~VulkanDevice();

    VulkanDevice(const VulkanDevice&) = delete;
    VulkanDevice& operator=(const VulkanDevice&) = delete;

    operator VkDevice() const noexcept { return m_device; }
    VkDevice device() const noexcept { return m_device; }

    operator VkPhysicalDevice() const noexcept { return m_physicalDevice; }
    VkPhysicalDevice physicalDevice() const noexcept { return m_physicalDevice; }

    // This has explicitly not been made into a conversion operator so when this
    // in the future supports multiple queues, a parameter for selecting the
    // appropriate queue can simply be added to the calls instead of having to
    // track down auto-conversions.
    VkQueue queue() const noexcept { return m_queue; }
    QueueInformation queueInfo() const noexcept { return m_queueInfo; }
    size_t queueCount() const noexcept { return 1; }

private:
    VkDevice m_device;
    VkPhysicalDevice m_physicalDevice;
    VkQueue m_queue;
    QueueInformation m_queueInfo;
};


#endif // RENDERER_DEVICE_HPP_INCLUDED
