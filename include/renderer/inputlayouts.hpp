#ifndef RENDERER_INPUTLAYOUTS_HPP_INCLUDED
#define RENDERER_INPUTLAYOUTS_HPP_INCLUDED

#include <array>
#include <iosfwd>
#include <optional>
#include <string>
#include <tuple>
#include <variant>
#include <vector>

#include <kraken/globaltypes.hpp>
#include <vulkan/vulkan.h>

struct ShaderInputSetLayout
{
    std::vector<VkDescriptorSetLayoutBinding> bindings;
    VkDescriptorSetLayout layout = VK_NULL_HANDLE;

    operator VkDescriptorSetLayout() const noexcept { return layout; }

    const VkDescriptorSetLayoutBinding* pBindings() const noexcept
    {
        return bindings.data();
    }

    size_t bindingCount() const noexcept { return bindings.size(); }
};

VkResult compileLayout(ShaderInputSetLayout& layout, VkDevice device);

struct PushConstantLayout
{
    std::vector<VkPushConstantRange> ranges;
};

enum class InputLayoutElementTag : U32
{
    FirstTag = 0,

    // VkVertexInputAttributeDescription
    Location = FirstTag,
    Binding,
    Format,
    Offset,

    // VkVertexInputBindingDescription
    Stride,
    InputRate,

    // VkDescriptorSetLayoutBinding
    DescriptorType,
    DescriptorCount,
    StageFlags,
    ImmutableSamplers,

    // VkPushConstantRange
    Size,

    TagCount
};

std::string to_string(InputLayoutElementTag);

inline constexpr U32 asBitMask(InputLayoutElementTag tag) noexcept
{
    return U32(1) << U32(tag);
}

// This must be kept in sync with the input layout tag.
using SingleInputLayoutElement = std::variant
<
    // VkVertexInputAttributeDescription
    U32,
    U32,
    VkFormat,
    U32,

    // VkVertexInputBindingDescription
    U32,
    VkVertexInputRate,

    // VkDescriptorSetLayoutBinding
    VkDescriptorType,
    U32,
    VkShaderStageFlags,
    const VkSampler*,

    // VkPushConstantRange
    U32
>;

using ElementLayoutCreateInfo = std::variant
<
    VkVertexInputAttributeDescription,
    VkVertexInputBindingDescription,
    VkDescriptorSetLayoutBinding,
    VkPushConstantRange
>;

template <typename Element>
struct VariantTupleConverter {};

template <typename... ContainedTypes>
struct VariantTupleConverter<typename std::variant<ContainedTypes...>>
{
    using result = std::tuple<ContainedTypes...>;
};

template <typename... ContainedTypes>
struct VariantTupleConverter<typename std::tuple<ContainedTypes...>>
{
    using result = std::variant<ContainedTypes...>;
};

template <size_t Index, typename Head, typename... Rest>
struct GetTypeByIndex
{
    using result = GetTypeByIndex<Index-1, Rest...>::result;
};

template <typename Head, typename... Rest>
struct GetTypeByIndex<0, Head, Rest...>
{
    using result = Head;
};

template <size_t Index, typename Element>
struct GetContainedTypeByIndex {};

template <size_t Index, typename... ContainedTypes>
struct GetContainedTypeByIndex<Index, typename std::variant<ContainedTypes...>>
{
    using result = GetTypeByIndex<Index, ContainedTypes...>::result;
};

using InputLayoutElements = VariantTupleConverter<SingleInputLayoutElement>::result;

static_assert(size_t(InputLayoutElementTag::TagCount) == std::tuple_size_v<InputLayoutElements>);

struct InputLayoutSpecificationElements
{
    InputLayoutElements value;
    U32 validElements = 0;

    ElementLayoutCreateInfo getCreateInfo() const;

    InputLayoutSpecificationElements& operator*=(const InputLayoutSpecificationElements&);
};

std::ostream& operator<<(std::ostream&, const InputLayoutSpecificationElements&);

inline InputLayoutSpecificationElements
operator*(InputLayoutSpecificationElements first,
          const InputLayoutSpecificationElements second)
{
    return first *= second;
}

struct InputLayoutSpecificationElementSet
{
    std::vector<InputLayoutSpecificationElements> elements;

    // Todo support setting offset by +.
    InputLayoutSpecificationElementSet& operator|=(const InputLayoutSpecificationElements&);
    InputLayoutSpecificationElementSet& operator|=(const InputLayoutSpecificationElementSet&);
    InputLayoutSpecificationElementSet& operator*=(const InputLayoutSpecificationElements&);
    InputLayoutSpecificationElementSet& operator*=(const InputLayoutSpecificationElementSet&);
};

inline InputLayoutSpecificationElementSet
operator|(InputLayoutSpecificationElementSet first,
          const InputLayoutSpecificationElementSet& second)
{
    return first |= second;
}

inline InputLayoutSpecificationElementSet
operator*(InputLayoutSpecificationElementSet first,
          const InputLayoutSpecificationElementSet& second)
{
    return first *= second;
}

inline InputLayoutSpecificationElementSet
operator*(InputLayoutSpecificationElements first,
          InputLayoutSpecificationElementSet second)
{
    return second *= first;
}

inline InputLayoutSpecificationElementSet
operator*(InputLayoutSpecificationElementSet first,
          const InputLayoutSpecificationElements& second)
{
    return first *= second;
}

inline InputLayoutSpecificationElementSet
operator|(InputLayoutSpecificationElementSet first,
          const InputLayoutSpecificationElements& second)
{
    return first |= InputLayoutSpecificationElementSet{{second}};
}

inline InputLayoutSpecificationElementSet
operator|(const InputLayoutSpecificationElements& first,
          InputLayoutSpecificationElementSet second)
{
    return second | first;
}

inline InputLayoutSpecificationElementSet
operator|(const InputLayoutSpecificationElements& first,
          const InputLayoutSpecificationElements& second)
{
    return InputLayoutSpecificationElementSet{{first}} |= second;
}

std::vector<ElementLayoutCreateInfo>
getAllCreateInfo(const InputLayoutSpecificationElementSet&);

template <typename CreateInfo>
std::vector<CreateInfo>
getCreateInfo(const InputLayoutSpecificationElementSet& elemSet)
{
    std::vector<CreateInfo> results;
    for (const auto& elem : elemSet.elements)
    {
        auto info = elem.getCreateInfo();
        if (const auto* infoType = std::get_if<CreateInfo>(&info))
        {
            results.push_back(*infoType);
        }
    }
    return results;
}

template <typename CreateInfo>
std::vector<CreateInfo>
getCreateInfo(const InputLayoutSpecificationElements& elem)
{
    return getCreateInfo<CreateInfo>(InputLayoutSpecificationElementSet{{elem}});
}

template <InputLayoutElementTag Tag>
struct InputLayoutSpecificationElementContainer
{
    using type = GetContainedTypeByIndex<size_t(Tag), SingleInputLayoutElement>::result;

    type value;

    operator InputLayoutSpecificationElements() const noexcept
    {
        InputLayoutSpecificationElements elem;
        elem.validElements = asBitMask(Tag);
        std::get<size_t(Tag)>(elem.value) = value;
        return elem;
    }

    operator InputLayoutSpecificationElementSet() const noexcept
    {
        return {{(InputLayoutSpecificationElements)*this}};
    }
};

template <InputLayoutElementTag FirstTag, InputLayoutElementTag SecondTag>
inline InputLayoutSpecificationElements
operator*(InputLayoutSpecificationElementContainer<FirstTag> first,
          InputLayoutSpecificationElementContainer<SecondTag> second)
{
    return (InputLayoutSpecificationElements)first
         * (InputLayoutSpecificationElements)second;
}

template <InputLayoutElementTag Tag>
inline InputLayoutSpecificationElementSet
operator*(InputLayoutSpecificationElementContainer<Tag> first,
          const InputLayoutSpecificationElementSet& second)
{
    return (InputLayoutSpecificationElements)first * second;
}

template <InputLayoutElementTag Tag>
inline InputLayoutSpecificationElementSet
operator*(const InputLayoutSpecificationElementSet& first,
          InputLayoutSpecificationElementContainer<Tag> second)
{
    return first * (InputLayoutSpecificationElements)second;
}

template <InputLayoutElementTag Tag>
inline InputLayoutSpecificationElements
operator*(InputLayoutSpecificationElementContainer<Tag> first,
          const InputLayoutSpecificationElements& second)
{
    return (InputLayoutSpecificationElements)first * second;
}

template <InputLayoutElementTag Tag>
inline InputLayoutSpecificationElements
operator*(const InputLayoutSpecificationElements& first,
          InputLayoutSpecificationElementContainer<Tag> second)
{
    return first * (InputLayoutSpecificationElements)second;
}

template <InputLayoutElementTag FirstTag, InputLayoutElementTag SecondTag>
inline InputLayoutSpecificationElementSet
operator|(InputLayoutSpecificationElementContainer<FirstTag> first,
          InputLayoutSpecificationElementContainer<SecondTag> second)
{
    return (InputLayoutSpecificationElements)first
           | (InputLayoutSpecificationElements)second;
}

template <InputLayoutElementTag Tag>
inline InputLayoutSpecificationElementSet
operator|(InputLayoutSpecificationElementContainer<Tag> first,
          InputLayoutSpecificationElements second)
{
    return (InputLayoutSpecificationElements)first
           | second;
}

template <InputLayoutElementTag Tag>
inline InputLayoutSpecificationElementSet
operator|(InputLayoutSpecificationElementContainer<Tag> first,
          const InputLayoutSpecificationElementSet& second)
{
    return (InputLayoutSpecificationElements)first
           | second;
}


template <InputLayoutElementTag Tag>
inline InputLayoutSpecificationElementSet
operator|(InputLayoutSpecificationElements first,
          InputLayoutSpecificationElementContainer<Tag> second)
{
    return first | (InputLayoutSpecificationElements)second;
}

template <InputLayoutElementTag Tag>
inline InputLayoutSpecificationElementSet
operator|(const InputLayoutSpecificationElementSet& first,
          InputLayoutSpecificationElementContainer<Tag> second)
{
    return first | (InputLayoutSpecificationElements)second;
}

namespace inputlayout
{

using Location = InputLayoutSpecificationElementContainer<InputLayoutElementTag::Location>;
using Binding = InputLayoutSpecificationElementContainer<InputLayoutElementTag::Binding>;
using Format = InputLayoutSpecificationElementContainer<InputLayoutElementTag::Format>;
using Offset = InputLayoutSpecificationElementContainer<InputLayoutElementTag::Offset>;
using Stride = InputLayoutSpecificationElementContainer<InputLayoutElementTag::Stride>;
using InputRate = InputLayoutSpecificationElementContainer<InputLayoutElementTag::InputRate>;
using DescriptorType = InputLayoutSpecificationElementContainer<InputLayoutElementTag::DescriptorType>;
using DescriptorCount = InputLayoutSpecificationElementContainer<InputLayoutElementTag::DescriptorCount>;
using StageFlags = InputLayoutSpecificationElementContainer<InputLayoutElementTag::StageFlags>;
using ImmutableSamplers = InputLayoutSpecificationElementContainer<InputLayoutElementTag::ImmutableSamplers>;
using Size = InputLayoutSpecificationElementContainer<InputLayoutElementTag::Size>;

} // namespace inputlayout

struct ShaderInputSetLayoutReference
{
    const ShaderInputSetLayout* pLayout;
    U32 layoutIndex;
};

inline
ShaderInputSetLayoutReference
operator*(U32 index, const ShaderInputSetLayout& layout) noexcept
{
    return {&layout, index};
}

inline
ShaderInputSetLayoutReference
operator*(const ShaderInputSetLayout& layout, U32 index) noexcept
{
    return {&layout, index};
}

struct ShaderInputSetLayoutSpecification
{
    // Max four supported for now.
    // Nullptr == unused.
    std::array<const ShaderInputSetLayout*, 4> layouts;

    ShaderInputSetLayoutSpecification() = default;
    ShaderInputSetLayoutSpecification(const ShaderInputSetLayoutSpecification&) = default;
    ShaderInputSetLayoutSpecification(ShaderInputSetLayoutSpecification&&) = default;

    ShaderInputSetLayoutSpecification(ShaderInputSetLayoutReference ref)
    {
        layouts.at(ref.layoutIndex) = ref.pLayout;
    }

    std::optional<VkPipelineLayout>
    createPipelineLayout(VkDevice device, U32 maxSetCount = ~(U32)0);
};

inline
ShaderInputSetLayoutSpecification
operator+(const ShaderInputSetLayoutReference& first,
          ShaderInputSetLayoutSpecification second)
{
    second.layouts.at(first.layoutIndex) = first.pLayout;
    return second;
}

inline
ShaderInputSetLayoutSpecification
operator+(ShaderInputSetLayoutSpecification first,
          const ShaderInputSetLayoutReference& second)
{
    first.layouts.at(second.layoutIndex) = second.pLayout;
    return first;
}

inline
ShaderInputSetLayoutSpecification
operator+(const ShaderInputSetLayoutReference& first,
          const ShaderInputSetLayoutReference& second)
{
    ShaderInputSetLayoutSpecification result(first);
    return result + second;
}

#endif // RENDERER_INPUTLAYOUTS_HPP_INCLUDED
