#ifndef RENDERER_CONTROLS_HPP_INCLUDED
#define RENDERER_CONTROLS_HPP_INCLUDED

#include "renderer/perspective.hpp"

#include <kraken/math/matrix.hpp>
#include <kraken/globaltypes.hpp>

enum class MovementState
{
    None,
    Translating,
    Rotating,
    Scaling
};

// Computes the inverse of a rigid transformation, i.e. an affine distance-
// preserving transform (meaning that the linear part has to be isometric).
template <typename Prec>
inline kraken::math::Matrix<4, 4, Prec>
rigidInverse(const kraken::math::Matrix<4, 4, Prec>& input)
{
    using namespace kraken::math;
    auto invRot = transpose(Matrix<3, 3, Prec>(input));

    auto lastCol = Vector<4, Prec>(invRot * (-Vector<3, Prec>(input.col(3))));
    lastCol[3] = 1.;

    kraken::math::Matrix<4, 4, Prec> result(invRot);

    result.setCol(3, lastCol);

    return result;
}

// A better inversion is something like:
enum class TransformType : U32
{
    Identity = 0,
    Translation = 1,
    Rotation = 2,
    Scale = 4,

    Rigid = 3,

    General = 0xffffffff
};
/*
template <typename TransformType, typename Prec>
Matrix<4, 4, Prec> inverse(Matrix<4, 4, Prec> orig)
{
    // Use special information from TransformType if possible.
}*/

struct DragMovementInfo
{
    kraken::math::dvec2 startPos;
    size_t objectIndex;
    kraken::math::dmat4 origTransform;
    kraken::math::dvec3 movementOrigPos;

    MovementState state = MovementState::None;

    kraken::math::dmat4 transformAtPosition(kraken::math::dmat4 worldToCamera,
                                            const PerspectiveTransform& pt,
                                            kraken::math::dvec2 currMousePos,
                                            kraken::math::uvec2 viewportDimensions);

    kraken::math::dmat4 rotateAtPosition(kraken::math::dmat4 worldToCamera,
                                         kraken::math::dvec2 currMousePos);

    kraken::math::dmat4 scaleAtPosition(kraken::math::dvec2 currMousePos);


    kraken::math::dmat4 continueDrag(kraken::math::dmat4 worldToCamera,
                                     const PerspectiveTransform& pt,
                                     kraken::math::dvec2 currMousePos,
                                     kraken::math::uvec2 viewportDimensions);
};

#endif // RENDERER_CONTROLS_HPP_INCLUDED
