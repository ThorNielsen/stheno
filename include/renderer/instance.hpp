#ifndef RENDERER_INSTANCE_HPP_INCLUDED
#define RENDERER_INSTANCE_HPP_INCLUDED

#include <kraken/globaltypes.hpp>

#include <vulkan/vulkan.h>

#include <set>
#include <string>
#include <vector>

// More parameters will be added when appropriate.
struct VulkanInstanceParameters
{
    std::string name = "Unnamed Vulkan program";
    bool swapchainSupport = false;
    bool enableValidation = true;
    U32 desiredVulkanVersion = VK_MAKE_VERSION(1, 1, 0);
};

class VulkanInstance
{
public:
    VulkanInstance(const VulkanInstanceParameters& params = {});
    ~VulkanInstance();

    VulkanInstance(const VulkanInstance&) = delete;
    VulkanInstance& operator=(const VulkanInstance&) = delete;

    operator VkInstance() const noexcept { return m_instance; }
    VkInstance instance() const noexcept { return m_instance; }

    const char* const* ppEnabledLayerNames() const noexcept
    {
        return m_layerPointers.data();
    }
    size_t enabledLayerCount() const noexcept
    {
        return m_layerPointers.size();
    }

private:
    std::set<std::string> m_enabledLayers;
    std::vector<const char*> m_layerPointers;
    VkInstance m_instance;
    VkDebugUtilsMessengerEXT m_debugUtils;
};

#endif // RENDERER_INSTANCE_HPP_INCLUDED
