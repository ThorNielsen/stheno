#ifndef SCENE_HPP_INCLUDED
#define SCENE_HPP_INCLUDED

#include <filesystem>
#include "vulkan/allocation_utils.hpp"
#include "vulkan/allocator.hpp"
#include "vulkan/descriptor.hpp"
#include "vulkan/device.hpp"
#include "vulkan/framegraph.hpp"
#include "vulkan/image.hpp"
#include "vulkan/mesh.hpp"
#include "vulkan/pipeline.hpp"
#include "vulkan/shader.hpp"
#include "vulkan/swapchain.hpp"
#include "vulkan/types.hpp"
#include <kraken/window/window.hpp>

#include "resourceloader.hpp"

using namespace kraken::math;
namespace fs = std::filesystem;

struct MaterialIndex
{
    std::string name;
    const NamedMeshAttributeElement* pAttrs;
    VkDeviceSize attrCount;
    const VkVertexInputBindingDescription* pBindings;
    VkDeviceSize bindingCount;
};

template<> struct std::hash<MaterialIndex>
{
    std::size_t operator()(const MaterialIndex& mi) const noexcept
    {
        std::size_t h = std::hash<std::string>{}(mi.name);
        for (VkDeviceSize i = 0; i < mi.attrCount; ++i)
        {
            const auto& ad = mi.pAttrs[i];
            h ^= std::hash<uint32_t>{}(ad.attribute.format);
            h ^= std::hash<uint32_t>{}(ad.attribute.offset);
        }
        h ^= mi.bindingCount;
        return h;
    }
};

bool operator==(const MaterialIndex& a, const MaterialIndex& b);

struct Material
{
    VkPipeline pipeline = VK_NULL_HANDLE;
    VkPipelineLayout layout = VK_NULL_HANDLE;
    std::shared_ptr<GraphicsShader> shader;

    void destroy(VkDevice device)
    {
        vkDestroyPipeline(device, pipeline, nullptr);
        vkDestroyPipelineLayout(device, layout, nullptr);
    }
};

// We need to support at least the following materials:
// * Simple (Blinn-)Phong shading.
// * Some kind of PBR material.
// * Transparent non-reflective material.
// * Transparent and reflective material.
// * Purely reflective material (mirroring?)

// But also we'll need to support different combinations of shaders/materials:
// Both in terms of combining vertex/geometry/tesselation/fragment shaders
// (and cross-combining those), but also dynamic state in terms of polygon mode,
// display size, etc.

class Scene;
class SceneMaterial;

struct SceneMaterialInstance
{
    SceneMaterial* material;
    VkDescriptorBufferInfo descriptorBufferInfo;
    VkDescriptorSet* pDescriptorSets;
    size_t setCount;
};

// Material hierarchy:
// * Shader -- contains code + layout information + reflection info
// * "Layouted shader" -- no. of desc set *layouts* and push constant layouts
//                        so this determines how many
// * Material - all render-pass specific stuff needed to make a pipeline, so at
//              least colour blend states, vertex input descriptions, topology,
//              cull modes, viewport, etc. Basically, all the state.
// * Concrete instance with all parameters, descriptor sets, etc.

// Implementation:
// * Shader -> GraphicsShader.
// * "Layouted shader" -> ShaderTemplate; do add descriptor buffers here.
// * Material -> Much more lightweight; essentially just a struct of:
//      ShaderTemplate* shaderTemplate;
//      VkPipeline pipeline;
//      std::vector<VkVertexInputAttributeDescription> attributes;
//      std::vector<VkVertexInputBindingDescription> bindings;
//      GraphicsPipelineCreateInfo createInfo;
//  The idea is that this would allow one to deduce exactly if a given material
//  can be reused, a new one is to be created, or if that can be modified to use
//  a new pipeline -- e.g. if we resize, then we can just recompile these.
// * Concrete instance -> MaterialInstance, should be lightweight enough to copy
//   around all the time.

// For later, add storage buffers, along with support for dynamic uniform and
// storage buffers.

struct MaterialInstanceExample
{
    Material* pMaterial;
    VkDescriptorBufferInfo* pUniformBuffers;
    VkDeviceSize uniformBufferCount;
    VkDescriptorSet* pDescriptorSets;
    // Count is implicit from SceneMaterialInstance;
};

// Or maybe: pUniformBuffers and pDescriptorSets should not be pointers, but
// instead indices into the ShaderTemplate's buffer, forcing someone rendering
// to actually sort by materials.
// And also, either ShaderTemplate or SceneMaterialInstance are the ones knowing
// exactly how many of the buffers/descriptor sets are used.
// ShaderTemplate is possibly best -- this cuts down on the number of different
// buffers we have to use (if ShaderTemplate: 1 per shader, if Material: 1 per
// pipeline config, which can be MANY).
// In the initial version, all resources from all descriptor sets are just
// stored in the same buffers, because that way we don't need to concern
// ourselves about some sets being bound earlier.
// Though to do that would maybe just be to (in the hierarchy) add a dummy node
// of type "Group" or something similar, with that being able to provide a
// descriptor set for binding.
// So maybe SceneMaterialInstance should have the number of uniform buffers/
// descriptor sets provided (along with a bitmask of the sets, maybe?), and then
// that can be modified.

struct ShaderTemplate
{
    ShaderTemplate()
        : shader{nullptr}, layout{VK_NULL_HANDLE}, device{VK_NULL_HANDLE}
    {}
    ShaderTemplate(const ShaderTemplate&) = delete;
    ShaderTemplate(ShaderTemplate&& other)
    {
        *this = std::move(other);
    }

    ShaderTemplate& operator=(ShaderTemplate&& other)
    {
        shader = other.shader;
        setLayouts = std::move(other.setLayouts);
        layout = other.layout;
        device = other.device;
        other.shader = nullptr;
        other.setLayouts.clear();
        other.layout = VK_NULL_HANDLE;
        other.device = VK_NULL_HANDLE;
        return *this;
    }
    ShaderTemplate& operator=(const ShaderTemplate&) = delete;

    ~ShaderTemplate()
    {
        if (device != VK_NULL_HANDLE && layout != VK_NULL_HANDLE)
        {
            vkDestroyPipelineLayout(device, layout, nullptr);
        }
    }

    std::shared_ptr<GraphicsShader> shader;
    std::vector<DescriptorSetLayout> setLayouts; // Store or forget?
    VkPipelineLayout layout;
    VkDevice device;
};

class SceneMaterial
{
public:
    SceneMaterial()
        : m_template{nullptr},
          m_pipeline{VK_NULL_HANDLE},
          m_device{VK_NULL_HANDLE}
    {}
    SceneMaterial(const SceneMaterial&) = delete;
    SceneMaterial(SceneMaterial&& other)
    {
        this->~SceneMaterial();
        *this = std::move(other);
    }

    SceneMaterial& operator=(SceneMaterial&& other);

    ~SceneMaterial();

    GraphicsShader& shader() const { return *m_shader.get(); }

    SceneMaterialInstance allocateInstance();
    void freeInstance(SceneMaterialInstance smi);

    VkDescriptorSet* pDescriptorSets(const SceneMaterialInstance&);
    const VkDescriptorSet* pDescriptorSets(const SceneMaterialInstance&) const;
    VkDeviceSize descriptorSetCount() const;
    const VkDescriptorBufferInfo*
    pDescriptorBuffers(const SceneMaterialInstance&) const;
    VkDeviceSize descriptorBufferCount() const;

private:
    friend class Scene;

    void destroy();

    MultiAllocatedBuffer m_mab;
    MultiAllocatedBuffer::AllocationType m_allocType;
    MultiAllocatedBuffer::AllocationTypeIndex m_allocTypeIndex;
    std::vector<VkDescriptorSet> m_descSets;
    std::shared_ptr<GraphicsShader> m_shader;
    const void* m_template; // Scene::ShaderTemplate
    VkPipeline m_pipeline;
    VkDevice m_device;
    size_t m_setsPerInstance;
};

struct MaterialInstance
{
    const Material* material = nullptr;
    // Currently we only support a single descriptor set per-object..
    DescriptorSet descriptors;
    std::vector<VkDescriptorBufferInfo> materialParameters;
};

using SceneObjectID = U64;

// Right now, this is a scene model, but that can be changed later.
struct SceneObject
{
    SceneObjectID parent = 0;
    SceneObjectID sibling = 0;
    SceneObjectID child = 0;

    std::string name; // Temporary and may be killed.
    std::shared_ptr<Mesh> mesh = nullptr;
    // Maybe this should have a MaterialInstance which also contains a pointer
    // or the like into the VkDescriptorSet array (so that the descriptor sets
    // are externally allocated and thus doesn't need an explicit count here).
    // MaterialInstance matInstance;
    std::shared_ptr<Image> colour = nullptr; // Ugly ref-count, but will be replaced soon(TM) anyway.
    std::shared_ptr<Image> normal = nullptr; // Ugly ref-count, but will be replaced soon(TM) anyway.

    mat4 transform = mat4(1.f);
};

// This should really be factored into two separate classes: One simple async
// loader (which might be a little easier to use or more specialised than
// ResourceLoader), and a scene parser, acquirer, etc.
// Currently this will just extract transforms and break any parent-child
// relationships, but that will be fixed later.
struct SceneObjectLoader
{
public:
    // The resource loader need not be fully constructed when this is created;
    // however, the resource loader must be ready as soon as any loading
    // function is called.
    SceneObjectLoader(ResourceLoader& loader, VkDevice device);
    ~SceneObjectLoader();

    // Note: This WILL fail if there are still elements that are currently
    // loading.
    void clear();

    void loadScene(const kraken::JSONValue& description,
                   fs::path searchDir = fs::current_path());

    void loadMesh(std::string uri);

    // When this is called the caller implicitly guarantees that it is safe to
    // both record commands into the buffer, and that the sampler cache is not
    // currently being used in any other thread.
    void recordVulkanCommands(VkCommandBuffer buffer,
                              SamplerCache& samplerCache);

    // Modified means either that it has been fully loaded or some detail of it
    // has been changed.
    std::optional<SceneObjectID> nextModifiedObject();

    SceneObject get(SceneObjectID id) const;

private:
    struct ImageTransitionBatch
    {
        VkEvent event;
        std::vector<std::pair<std::string, std::shared_ptr<Image>>> imgs;
    };

    // Temporary destination while scene objects are extremely simplistic.
    enum class ImageDestinationah
    {
        Colour,
        Normal,
    };

    static std::shared_ptr<Image>& getDst(SceneObject&, ImageDestinationah);

    void onImageLoadFinish(std::string uri, std::shared_ptr<Image>);
    void onMeshLoadFinish(std::string uri, std::shared_ptr<Mesh>);

    void enqueueModelLoad(const kraken::JSONValue& description, mat4 transform);

    // This signature WILL change...
    void loadImage(std::string uri,
                   ImageDestinationah imgDst,
                   SceneObjectID objDst);

    void loadMesh(std::string uri, SceneObjectID dst);

    void markModified(SceneObjectID id, bool externalSynced = false);

    SceneObjectID nextID() noexcept;

    // Ugly as fuck, but until I figure out a good way to know when old commands
    // have finished, we have to keep old events around indefinitely.
    // Somehow there should be a "global" structure that can tell when something
    // has finished executing.
    VkEvent newEvent(bool externalSynced);
    void deleteEvent(VkEvent, bool externalSynced);

    std::vector<VkEvent> m_eventCache;

    // The below could just as well be IDs instead of strings, with a potential
    // string-to-id mapper instead...
    std::unordered_map<std::string, std::shared_ptr<Mesh>> m_meshes;
    std::unordered_map<std::string, std::shared_ptr<Image>> m_images;

    std::multimap<std::string, std::pair<SceneObjectID, ImageDestinationah>> m_imgPendingLoads;
    std::multimap<std::string, SceneObjectID> m_meshPendingLoads;
    std::map<SceneObjectID, SceneObject> m_objects;

    std::vector<ImageTransitionBatch> m_currTransitions;

    std::set<SceneObjectID> m_modified;

    // Made mutable to support locking within const functions.
    mutable std::mutex m_mutex;

    ResourceLoader* m_resourceLoader;
    SceneObjectID m_nextObjectID;

    VkDevice m_device;
};

#endif // SCENE_HPP_INCLUDED
