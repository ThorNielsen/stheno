#ifndef RENDERER_CAMERA_HPP_INCLUDED
#define RENDERER_CAMERA_HPP_INCLUDED

#include <kraken/math/matrix.hpp>

#include <optional>

class Camera
{
public:
    virtual ~Camera() {}

    /// Computes the world-to-camera transform matrix.
    ///
    /// The implementation of this function is not guaranteed to be particularly
    /// efficient, and as such, the result should be cached on the calling side.
    ///
    /// \returns The world-to-camera matrix.
    virtual kraken::math::dmat4 worldToCamera() const noexcept = 0;

    /// Access the position of the camera in world space.
    ///
    /// \returns The position.
    virtual kraken::math::dvec3 position() const noexcept = 0;

    /// Access the forward direction of the camera in world space.
    ///
    /// \returns The direction the camera points in.
    virtual kraken::math::dvec3 forward() const noexcept = 0;

    /// Access the up direction of the camera in world space.
    ///
    /// \returns A natural "up" direction orthogonal to the direction in which
    ///          the camera points.
    virtual kraken::math::dvec3 up() const noexcept = 0;
};

// This represents a camera which can be controlled through proxy movement in
// screen space. Scaling only has a single movement as it is always supposed to
// be uniform to not stretch something weirdly.
class ScreenspaceMovableCamera : public Camera
{
public:
    virtual void translate(kraken::math::dvec2 movement) noexcept = 0;
    virtual void rotate(kraken::math::dvec2 movement) noexcept = 0;
    virtual void scale(double movement) noexcept = 0;

    /// This function must be called after all the apply*-functions are called.
    ///
    /// This will indicate that the current transform operation has ended and
    /// thus that the next inputs are disconnected. For example, this can be
    /// used to ensure that rotation in a turntable camera still follows the
    /// same directions until one releases the button that activated the
    /// rotation.
    virtual void endTransform() noexcept = 0;
};

// This type of camera works by rotation around a fixed point and axis in 3d,
// and the transformation simply moves one closetransformation
class TurntableCamera : public ScreenspaceMovableCamera
{
public:
    TurntableCamera(kraken::math::dvec3 initialLookat,
                    std::optional<double> initialDistance = std::nullopt,
                    std::optional<kraken::math::dvec3> initialUp = std::nullopt,
                    std::optional<kraken::math::dvec3> initialRight = std::nullopt,
                    std::optional<kraken::math::dvec3> initialForward = std::nullopt);

    kraken::math::dmat4 worldToCamera() const noexcept override;

    kraken::math::dvec3 position() const noexcept override;

    kraken::math::dvec3 forward() const noexcept override;
    kraken::math::dvec3 up() const noexcept override;

    void translate(kraken::math::dvec2 mouseMove) noexcept override;
    void rotate(kraken::math::dvec2 mouseMove) noexcept override;
    void scale(double movement) noexcept override;

    void endTransform() noexcept override;

private:
    kraken::math::dmat3 getPendingTransform() const noexcept;
    void reorthonormalise();

    kraken::math::dvec3 m_direction;
    kraken::math::dvec3 m_rotationCenter; // Center to look at.
    kraken::math::dvec3 m_up;
    kraken::math::dvec3 m_right;
    kraken::math::dvec2 m_pendingOffset;
    double m_distance; // Must be non-negative.
};

#endif // RENDERER_CAMERA_HPP_INCLUDED
