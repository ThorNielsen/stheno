#ifndef RENDERER_WINDOWOPENER_HPP_INCLUDED
#define RENDERER_WINDOWOPENER_HPP_INCLUDED

#include <vulkan/vulkan.h>

namespace kraken::window
{

class Window;
class WindowCreationSettings;

} // namespace kraken::window

// This merely opens (and optionally closes) a window on creation (resp.
// destruction).
class WindowOpener
{
public:
    WindowOpener(kraken::window::Window& window,
                 VkInstance instance,
                 const kraken::window::WindowCreationSettings& settings,
                 bool closeOnDestruction = true);
    ~WindowOpener();

    WindowOpener(WindowOpener&) = delete;
    WindowOpener& operator=(const WindowOpener&) = delete;

    VkSurfaceKHR surface() const noexcept { return m_surface; }

    VkSurfaceKHR acquireSurface() noexcept;

private:
    kraken::window::Window* m_window;
    VkSurfaceKHR m_surface;
    VkInstance m_instance;
};

#endif // RENDERER_WINDOWOPENER_HPP_INCLUDED
