#ifndef RENDERER_GRAPHICSSHADERINSTANCE_HPP_INCLUDED
#define RENDERER_GRAPHICSSHADERINSTANCE_HPP_INCLUDED

#include "renderer/scene.hpp"
#include "vulkan/framegraph.hpp"

#include <kraken/globaltypes.hpp>
#include <vulkan/vulkan.h>

#include <optional>
#include <vector>

// These specifies the usage of each attribute to be able to more easily match
// shaders and meshes together.
enum class VertexAttributeUsage : U32
{
    Position = 0,
    Normal = 1,
    Texcoords = 2,
    Tangent = 3,
    Colour = 4,
};

// Subject to change, also needs usage information etc.
// Potentially should just have static storage instead of vectors.
// Or alternatively pointers into a pool/paged allocator.
struct VertexInputSpecification
{
    std::vector<VkVertexInputAttributeDescription> attributes;
    std::vector<VkVertexInputBindingDescription> bindings;
};

// End to specify
/*
class ManagedShader
{
public:
    virtual ~ManagedShader();
    virtual const VertexInputSpecification& vertexInputSpec() = 0;
    virtual void refresh(const FrameGraph::RenderPass& pass) = 0;

    virtual Material& getMaterial() noexcept = 0;
};*/

// Requires much better naming, but the general idea is that this represents a
// single shader and supports querying required sizes for different kind of
// buffers in a uniform way, while one has to downcast to a specific derived
// type in case one wants to know something more specific about that shader.
// Corresponds to a material.
class GraphicsShaderInstance
{
public:
    virtual ~GraphicsShaderInstance();

    virtual const VertexInputSpecification& vertexInputSpec() = 0;
    virtual void refresh(const FrameGraph::RenderPass& pass) = 0;

    virtual std::optional<MaterialInstance> createInstance(size_t descCopyCount, DescriptorAllocator&) noexcept = 0;
};

class FlatDrawShader : public GraphicsShaderInstance
{
public:
    ~FlatDrawShader();

    void create(VkDevice device,
                GraphicsShader&& shader,
                const VertexInputSpecification& spec,
                const FrameGraph::RenderPass* pass = nullptr);

    void destroy();

    const VertexInputSpecification& vertexInputSpec() override;
    void refresh(const FrameGraph::RenderPass& pass) override;
    std::optional<MaterialInstance> createInstance(size_t descCopyCount, DescriptorAllocator&) noexcept override;

private:
    void createLayouts(GraphicsShader& shader);
    void createPipeline(GraphicsShader& shader,
                        const FrameGraph::RenderPass& pass);
    void destroyPipeline();

    VertexInputSpecification m_spec;
    VkDevice m_device = VK_NULL_HANDLE;
    DescriptorSetLayout m_layout;
    std::unique_ptr<Material> m_material; // To keep a stable address.
};

#endif // RENDERER_GRAPHICSSHADERINSTANCE_HPP_INCLUDED
