#ifndef RENDERER_DATAPROVIDERS_HPP_INCLUDED
#define RENDERER_DATAPROVIDERS_HPP_INCLUDED

#include "vulkan/allocation_utils.hpp"
#include "vulkan/allocator.hpp"
#include "vulkan/image.hpp"
#include "vulkan/vma.hpp"

#include <kraken/math/matrix.hpp>

#include <cstddef>
#include <vector>

using namespace kraken::math;

struct PointLight
{
    vec4 position;
    vec4 colour;
};

struct DirectionalLight
{
    vec4 direction;
    vec4 colour;
};

// MUST be kept in sync with the shader's version.
struct LightSources
{
    constexpr static size_t maxDirectionalLights = 3;
    constexpr static size_t maxPointLights = 5;

    DirectionalLight dirLights[maxDirectionalLights];
    PointLight pointLights[maxPointLights];
};

struct ReadbackArrayElement
{
    vec4 position; // In camera space, last element gives gl_FragCoord.z
    uvec4 ids; // object index, vertex index, gl_FragCoord.x, gl_FragCoord.y
};

// Currently provides a *specific* readback array, but can trivially be modified
// to do it for any.
class ReadbackArrayProvider
{
public:
    static constexpr size_t cachedFrameCount = 2;

    ReadbackArrayProvider(VkDevice device, VmaAllocator allocator, size_t maxElementCount);
    ~ReadbackArrayProvider();

    void write(VkDescriptorSet descSet, size_t descriptorIndex);

    void update(size_t) {} // For identical interface with other providers.

    std::vector<ReadbackArrayElement> readback(size_t frameIndex, bool reset = true);

private:
    static constexpr size_t headerSize = 16;

    size_t totalBufferByteSize() const;

    std::array<BufferAllocation, cachedFrameCount> m_allocations;
    VkDevice m_device;
    VmaAllocator m_allocator;
    size_t m_maxElementCount;
};

// Currently supports a single buffer allocation only, but ideally it should
// support multiple going forwards, or alternatively one should just use
// multiple of these if lightweight enough. But bundling is probably a good
// reason to support multiple.
class BufferProvider
{
public:
    static constexpr size_t cachedFrameCount = 2;

    BufferProvider(MultiAllocatedBuffer& allocFrom,
                   VkDeviceSize requiredSize,
                   VkDevice device);
    virtual ~BufferProvider();

    BufferProvider(BufferProvider&& other) = delete;

    // Has default implementation, but can optionally be overridden.
    virtual void write(VkDescriptorSet descSet, size_t descriptorIndex);

    // Same.
    virtual void update(size_t frameIndex);

protected:
    // This will be called FOR EACH CACHED FRAME instance, so it contains as
    // many elements as there are buffers.
    virtual void updateDescriptorSets(VkWriteDescriptorSet* pToUpdate) = 0;

    // This is for refreshing written elements.`
    virtual void performUpdate(void* allocBegin) = 0;

    const VkDescriptorBufferInfo* pBufferInfos(size_t cachedFrameIndex)
    {
        return &m_bufferInfos.at(cachedFrameIndex);
    }
    // Counts buffers PER cached frame index, and NOT total.
    size_t bufferCount() const noexcept { return 1; }

    VkDevice getDevice() const { return m_device; }

private:
    MultiAllocatedBuffer* m_bufAllocator;
    std::array<VkDescriptorBufferInfo, cachedFrameCount> m_bufferInfos;
    VkDevice m_device;
};

class GlobalTransformProvider : public BufferProvider
{
public:
    GlobalTransformProvider(MultiAllocatedBuffer& allocFrom, VkDevice device);

    mat4 worldToCamera = mat4(1.);
    mat4 cameraToClip = mat4(1.);

protected:
    void updateDescriptorSets(VkWriteDescriptorSet* pDescriptors) override;
    void performUpdate(void* allocBegin) override;
};

// Todo maybe merge with another buffer provider to provide hybrid uniform
// / storage buffer?
class PickDataProvider : public BufferProvider
{
public:
    PickDataProvider(MultiAllocatedBuffer& allocFrom, VkDevice device);

    uvec2 center;
    U32 radius;

protected:
    void updateDescriptorSets(VkWriteDescriptorSet* pDescriptors) override;
    void performUpdate(void* allocBegin) override;
};

class LightDataProvider : public BufferProvider
{
public:
    LightDataProvider(MultiAllocatedBuffer& allocFrom,
                      VkDevice device);

    LightSources lights;

protected:
    void updateDescriptorSets(VkWriteDescriptorSet* pDescriptors) override;
    void performUpdate(void* allocBegin) override;
};

class DefaultMaterialParameterProvider : public BufferProvider
{
public:
    DefaultMaterialParameterProvider(MultiAllocatedBuffer& allocFrom,
                                     VkDevice device,
                                     const Image& placeholderTexture);

    ~DefaultMaterialParameterProvider();

    void write(VkDescriptorSet descSet, size_t frameIndex) override;

    // Note: Currently it is not valid to set these after they have been set the
    // first time since there is nothing to sync, even if using frameIndex.
    std::shared_ptr<Image> colourTexture;
    std::shared_ptr<Image> normalTexture;

    vec4 tint;

protected:
    void updateDescriptorSets(VkWriteDescriptorSet* pDescriptors) override;
    void performUpdate(void* allocBegin) override;

private:
    void moveFromOther(DefaultMaterialParameterProvider& other) noexcept;

    const Image* m_placeholderTexture;
};

class PlotFunctionBufferProvider
{
public:
    static constexpr size_t cachedFrameCount = 2;

    PlotFunctionBufferProvider(VkDevice device,
                               VmaAllocator allocator);
    ~PlotFunctionBufferProvider();

    PlotFunctionBufferProvider(PlotFunctionBufferProvider&&) = delete;

    void setFunction(std::vector<U32>&& code,
                     std::vector<F32>&& constants);

    void write(VkDescriptorSet descSet, size_t descriptorIndex);

    void update(size_t frameIndex);

    std::vector<ReadbackArrayElement> readback(size_t frameIndex, bool reset = true);

private:
    // We create a buffer with space for 2048/4 = 512 opcodes, nad 256/4 = 64
    // constants, which should be enough to cover reasonable functions,
    // otherwise we can simply reallocate (or throw until realloc implemented).
    const std::size_t m_opcodeBufSize = 2048;
    const std::size_t m_constantsBufSize = 512;

    std::array<BufferAllocation, cachedFrameCount> m_allocations;
    std::array<bool, cachedFrameCount> m_dirty;
    std::array<std::array<VkDescriptorBufferInfo, 2>, cachedFrameCount> m_bufferInfos;
    std::vector<U32> m_code;
    std::vector<F32> m_constants;
    VkDevice m_device;
    VmaAllocator m_allocator;
};

#endif // RENDERER_DATAPROVIDERS_HPP_INCLUDED
