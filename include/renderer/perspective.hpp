#ifndef RENDERER_PERSPECTIVE_HPP_INCLUDED
#define RENDERER_PERSPECTIVE_HPP_INCLUDED

#include <kraken/math/constants.hpp>
#include <kraken/math/matrix.hpp>

#include <utility>

/// This describes a perspective transform from camera space to normalised
/// device coordinate space, which should then subsequently be drawn into a
/// framebuffer, which we will refer to as a “screen” or “viewport” (though note
/// that no monitor output is technically necessary).
///
/// There are multiple standing assumptions on the coordinate systems, which
/// are as follows:
///
/// * The camera space is oriented such that if one stands at the camera
///   position and looks the same way the camera points:
///     * X points to the left (towards the left edge of the screen)
///     * Y points downwards (towards the bottom of the screen).
///     * Z points *inwards* (towards the viewer/opposite of the camera's
///       direction).
///
/// * The normalised device coordinate space has the following axes:
///     * x∈[-1, 1] corresponds to the horizontal viewport position, with
///       increasing NDC x-coordinates going towards the right of the screen.
///     * y∈[-1, 1] corresponds to the vertical viewport position, with
///       increasing NDC y-coordinates going towards the bottom of the screen.
///     * z∈[0, 1] gives the depth of the point in the view direction;
///       z=0 corresponds to a point on the near plane and z=1 is a point on the
///       far plane.
///     * w∈ℝ is used solely for the perspective divide.
///
/// While this class is intended to be as easy-to-use as possible, it has also
/// been designed with extensibility in mind, and so can be derived from. This
/// could for example allow one to create a subclass supporting infinite far
/// planes, etc.
///
/// \remark Observe that y points DOWNWARDS on the screen, which corresponds to
///         Vulkan's convention but is different from e.g. OpenGL.
///
/// \remark The interface uses double-precision types (as does the default
///         implementation) for maximal precision; as most scenes have only few
///         cameras (or even just a single one), this does not significantly
///         affect performance.
class PerspectiveTransform
{
public:
    virtual ~PerspectiveTransform();

    /// Constructor.
    ///
    /// \param aspectRatio Aspect ratio of the screen (width/height).
    /// \param verticalFOV Field of view (in radians) specified relative to the
    ///                    height of the viewport.
    /// \param nearPlane Distance to the near plane.
    /// \param farPlane Distance to the far plane.
    ///
    /// \throws An exception if one attempts to construct this with invalid
    ///         arguments.
    PerspectiveTransform(double aspectRatio = 1.,
                         double verticalFOV = 0.3 * kraken::math::Constant<double>::pi,
                         double nearPlane = 0.1,
                         double farPlane = 1000.);

    PerspectiveTransform(const PerspectiveTransform&) = default;
    PerspectiveTransform(PerspectiveTransform&&) = default;
    PerspectiveTransform& operator=(const PerspectiveTransform&) = default;
    PerspectiveTransform& operator=(PerspectiveTransform&&) = default;

    operator kraken::math::dmat4() const
    {
        return transform();
    }

    kraken::math::dmat4 transform() const;

    virtual double aspect() const noexcept;
    virtual double fov() const noexcept;
    virtual double near() const noexcept;
    virtual double far() const noexcept;

    // These always set the desired values, but return false in case the input
    // was adjusted.
    virtual bool setAspect(double aspectRatio) noexcept;
    virtual bool setAspect(double width, double height) noexcept;
    virtual bool setFov(double verticalFOV) noexcept;
    virtual bool setNear(double nearPlane) noexcept;
    virtual bool setFar(double farPlane) noexcept;

    /// This translates viewport space to camera space at a particular depth.
    ///
    /// \remark The returned matrix transforms from *homogeneous 2d-space* (the
    ///         viewport space / screen space) into normal 3d space (i.e. ℝ³;
    ///         the camera space).
    ///
    /// \param depth The depth for which to construct this matrix.
    /// \param viewportHeight The height of the viewport.
    ///
    /// \returns A matrix transforming from viewport/screen space at the given
    ///          depth into camera space. Observe that the screen space is
    ///          homogeneous, and thus this can both transform directions and
    ///          points.
    virtual kraken::math::dmat3
    viewportToCameraSpace(double depth, double viewportHeight) const noexcept;

    /// Translates NDC depth (as written to the depth buffer) into camera depth.
    ///
    /// \param ndcDepth The depth (in the interval [0, 1]) as it were written to
    ///                 the depth buffer.
    ///
    /// \returns The depth in camera space (essentially the lenght of the
    ///          projection of the camera space point onto the camera space's Z-
    ///          axis.
    virtual double ndcToCameraDepth(double ndcDepth) const noexcept;

private:
    double m_aspect;
    double m_vertFOV;
    double m_nearDist;
    double m_farDist;
};

#endif // RENDERER_PERSPECTIVE_HPP_INCLUDED
