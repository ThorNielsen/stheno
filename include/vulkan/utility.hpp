#ifndef VULKAN_UTILITY_HPP_INCLUDED
#define VULKAN_UTILITY_HPP_INCLUDED

#include "vulkan/common.hpp"

#include <string>

// The values are chosen such that the least significant 8 bits give the size
// (in bytes) of the type, the next 8 bits give the alignment (in bytes), the
// next 4 bits is the number of components - 1, then the next 4 bits determines
// the underlying type and finally the remaining upper 8 bits is just some
// unique id.
// A better representation of this could be to consider the number 0xGHIJKLMN.
// Here:
// 0xMN -- total size in bytes of the type.
// 0xKL -- alignment requirement in bytes.
// 0xJ  -- number of components - 1 (e.g. a mat3 will have 0xJ = 8).
// 0xI  -- underlying type, 0=Float, 1=Double, 2=Int, 3=UInt.
// 0xGH -- Unique id to be able to distinguish types (not strictly necessary).
// WARNING: The above conventions may change without any notice whatsoever, and
// therefore the provided get(Size|Alignment|ArrayComponents|UnderlyingType)
// functions should be used.
enum class ShaderDataFormat : U32
{
    Float = 0x000404, Double = 0x100808, Int = 0x200404, UInt = 0x300404,
    Vec2 = 0x010808, DVec2 = 0x111010, IVec2 = 0x210808, UVec2 = 0x310808,
    Vec3 = 0x02100c, DVec3 = 0x122018, IVec3 = 0x22100c, UVec3 = 0x32100c,
    Vec4 = 0x031010, DVec4 = 0x132020, IVec4 = 0x231010, UVec4 = 0x331010,
    Mat2 = 0x030810, DMat2 = 0x131020, IMat2 = 0x230810, UMat2 = 0x330810,
    Mat3 = 0x081024, DMat3 = 0x182048, IMat3 = 0x281024, UMat3 = 0x381024,
    Mat4 = 0x0f1040, DMat4 = 0x1f2080, IMat4 = 0x2f1040, UMat4 = 0x3f1040,
};

inline U32 getSize(ShaderDataFormat fmt) { return static_cast<U32>(fmt) & 0xff; }
inline U32 getAlignment(ShaderDataFormat fmt) { return (static_cast<U32>(fmt) >> 8) & 0xff; }

inline U32 getArrayComponents(ShaderDataFormat fmt)
{
    return ((static_cast<U32>(fmt) >> 16) & 0xf)+1;
}

inline ShaderDataFormat getUnderlyingType(ShaderDataFormat fmt)
{
    switch ((static_cast<U32>(fmt) >> 20) & 0xf)
    {
    default:
    case 0: return ShaderDataFormat::Float;
    case 1: return ShaderDataFormat::Double;
    case 2: return ShaderDataFormat::Int;
    case 3: return ShaderDataFormat::UInt;
    }
}

U32 getRequiredPadding(U32 currAlignment, ShaderDataFormat fmt);

bool parseFormat(const std::string& fmt, VkFormat& formatOut);

bool parseFormat(const std::string& fmt, ShaderDataFormat& formatOut);



#endif // VULKAN_UTILITY_HPP_INCLUDED
