#ifndef VULKAN_PIPELINE_HPP_INCLUDED
#define VULKAN_PIPELINE_HPP_INCLUDED

#include <kraken/globaltypes.hpp>
#include <vulkan/vulkan.h>
#include <vector>

struct Pipeline
{
    VkPipeline pipeline = VK_NULL_HANDLE;
    VkPipelineLayout layout = VK_NULL_HANDLE;
    VkDevice device = VK_NULL_HANDLE;

    void destroy()
    {
        if (device == VK_NULL_HANDLE) return;
        if (pipeline != VK_NULL_HANDLE) vkDestroyPipeline(device, pipeline, nullptr);
        if (layout != VK_NULL_HANDLE) vkDestroyPipelineLayout(device, layout, nullptr);
        device = VK_NULL_HANDLE;
        pipeline = VK_NULL_HANDLE;
        layout = VK_NULL_HANDLE;
    }
};

struct GraphicsPipelineInfo
{
    VkPipelineVertexInputStateCreateInfo vertexInputCreate = {};
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyCreate = {};
    VkPipelineTessellationStateCreateInfo tesselationCreate = {};
    std::vector<VkRect2D> scissors;
    std::vector<VkViewport> viewports;
    mutable VkPipelineViewportStateCreateInfo viewportCreate = {};
    VkPipelineRasterizationStateCreateInfo rasterizerCreate = {};
    VkPipelineMultisampleStateCreateInfo multisampleCreate = {};
    VkPipelineDepthStencilStateCreateInfo depthStencilCreate = {};
    std::vector<VkPipelineColorBlendAttachmentState> colourBlendStates;
    mutable VkPipelineColorBlendStateCreateInfo colourBlendCreate = {};
    VkPipelineLayoutCreateInfo pipelineCreate = {};
    VkPipelineDynamicStateCreateInfo dynamicState = {};

    void initialise() { clear(); }
    void clear();

    void fillInfo(U32 width,
                  U32 height,
                  VkPrimitiveTopology topology,
                  VkPolygonMode polygonMode,
                  VkCullModeFlags cullMode,
                  VkFrontFace frontFace,
                  VkSampleCountFlagBits samples);

    VkGraphicsPipelineCreateInfo getGraphicsPipelineCreateInfo() const;
};

#endif // VULKAN_PIPELINE_HPP_INCLUDED
