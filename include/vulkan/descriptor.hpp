#ifndef VULKAN_DESCRIPTOR_HPP_INCLUDED
#define VULKAN_DESCRIPTOR_HPP_INCLUDED

#include <kraken/globaltypes.hpp>
#include "vulkan/common.hpp"
#include <vector>

class DescriptorAllocator
{
public:
    std::vector<VkDescriptorPoolSize> poolSizes =
    {
        {VK_DESCRIPTOR_TYPE_SAMPLER, 128},
        {VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 128},
        {VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 128},
        {VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 128},
        {VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 128},
        {VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 128},
        {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 128},
        {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 128},
        {VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 128},
        {VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 128},
        {VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 128},
    };

    DescriptorAllocator();
    DescriptorAllocator(VkDevice device);
    ~DescriptorAllocator();

    void construct(VkDevice device);
    void destroy();

    bool allocate(VkDescriptorSet* setsOut,
                  const VkDescriptorSetLayout* layouts,
                  U32 layoutCount);
    void resetPools();


private:
    void createNewDescriptorPool();

    std::vector<VkDescriptorPool> m_pools;
    std::vector<bool> m_poolFull;
    std::vector<U32> m_nextFreePool;
    VkDevice m_device;
};

inline bool operator<(VkDescriptorSetLayoutBinding a,
                      VkDescriptorSetLayoutBinding b)
{
    if (a.binding < b.binding) return true;
    if (a.descriptorType < b.descriptorType) return true;
    if (a.descriptorCount < b.descriptorCount) return true;
    if (a.stageFlags < b.stageFlags) return true;
    return a.pImmutableSamplers < b.pImmutableSamplers;
}

inline bool operator==(VkDescriptorSetLayoutBinding a,
                       VkDescriptorSetLayoutBinding b)
{
    if (a.binding != b.binding) return false;
    if (a.descriptorType != b.descriptorType) return false;
    if (a.descriptorCount != b.descriptorCount) return false;
    if (a.stageFlags != b.stageFlags) return false;
    return a.pImmutableSamplers == b.pImmutableSamplers;
}

struct DescriptorSetLayoutInfo
{
    std::vector<VkDescriptorSetLayoutBinding> bindings;

    void addBinding(U32 binding, VkDescriptorType type,
                    VkShaderStageFlags stages, U32 count = 1);

    VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo() const;
    VkWriteDescriptorSet writeDescriptorSetInfo(size_t binding, VkDescriptorSet set = VK_NULL_HANDLE) const;
};

inline bool operator<(const DescriptorSetLayoutInfo& a,
                      const DescriptorSetLayoutInfo& b)
{
    auto upto = std::min(a.bindings.size(), b.bindings.size());
    for (size_t i = 0; i < upto; ++i)
    {
        if (a.bindings[i] < b.bindings[i]) return true;
    }
    if (a.bindings.size() < b.bindings.size()) return true;
    return false;
}

inline bool operator==(const DescriptorSetLayoutInfo& a,
                       const DescriptorSetLayoutInfo& b)
{
    if (a.bindings.size() != b.bindings.size()) return false;
    for (size_t i = 0; i < a.bindings.size(); ++i)
    {
        if (!(a.bindings[i] == b.bindings[i])) return false;
    }
    return true;
}

class DescriptorSetLayout
{
public:
    DescriptorSetLayout();
    DescriptorSetLayout(VkDevice device, VkAllocationCallbacks* alloc = nullptr);

    DescriptorSetLayout(DescriptorSetLayout&&) noexcept;
    DescriptorSetLayout& operator=(DescriptorSetLayout&&) noexcept;

    ~DescriptorSetLayout();

    void construct(VkDevice device, VkAllocationCallbacks* alloc);
    void destroy();

    VkResult compileLayout();
    void destroyLayout();

    VkDescriptorSetLayout layout() const { return m_layout; }
    VkDescriptorSetLayout& layout() { return m_layout; } // To help transitioning away from this.

    DescriptorSetLayoutInfo& layoutInfo() { return m_layoutInfo; }
    const DescriptorSetLayoutInfo& layoutInfo() const { return m_layoutInfo; }

    /*
    VkResult createDescriptorSets(DescriptorSet* setsOut, VkDeviceSize count,
                                  DescriptorAllocator& descAllocator);*/

private:
    DescriptorSetLayoutInfo m_layoutInfo;
    VkDevice m_device;
    VkAllocationCallbacks* m_alloc;
    VkDescriptorSetLayout m_layout;
};

struct DescriptorSet
{
    const DescriptorSetLayout* pLayout;
    std::vector<VkDescriptorSet> descriptorSets;

    void allocateCopies(size_t count)
    {
        descriptorSets.resize(count, VK_NULL_HANDLE);
    }

    VkDescriptorSet* sets() { return descriptorSets.data(); }
    const VkDescriptorSet* sets() const { return descriptorSets.data(); }
    U32 count() const { return descriptorSets.size(); }
};

class DescriptorSetLayoutCache
{
public:
    using SetLayoutID = size_t;

    DescriptorSetLayoutCache();
    ~DescriptorSetLayoutCache();

    void construct();
    void destroy();

    SetLayoutID addUser(DescriptorSetLayout&& setLayout);
    void addUser(SetLayoutID);
    void removeUser(SetLayoutID);

    const DescriptorSetLayout* layout(SetLayoutID) const;

private:
    // Returns m_layouts.size() when no suitable id is found.
    SetLayoutID findID(const DescriptorSetLayout&) const;

    std::vector<DescriptorSetLayout> m_layouts;
    std::vector<size_t> m_userCount;
};

bool createDescriptorSets(VkDescriptorSet* setsOut, VkDeviceSize count,
                          const DescriptorSetLayout* pLayout,
                          DescriptorAllocator& descAllocator);

#endif // VULKAN_DESCRIPTOR_HPP_INCLUDED
