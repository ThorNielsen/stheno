#ifndef VULKAN_MESH_HPP_INCLUDED
#define VULKAN_MESH_HPP_INCLUDED

#include "vulkan/common.hpp"
#include "vulkan/allocator.hpp"

#include <vector>

// Vk...BindingDesc... associates a buffer binding to the current objects to be
// drawn (buffer is bound by a command). This just tells whether the buffer is
// to be read per-vertex or per-instance (and also the stride within that buffer).

// Vk...AttributeDesc... tells which format a single attribute
// [layout (location = N) in FMT NAME] is, and also which location it is.
// Furthermore, it tells which bound buffer it should be fetched from, and the
// offset within an element of that buffer.

class ShaderInfo;

using MeshAttributeNames = std::vector<std::string>;

struct MeshBuffer
{
    MeshBuffer() = default;
    MeshBuffer(const MeshBuffer&) = delete;
    MeshBuffer(MeshBuffer&&) noexcept;
    MeshBuffer& operator=(MeshBuffer&&) noexcept;

    ~MeshBuffer();
    void reserveHostMemory(U64 bytes);
    void destroy() noexcept;

    // TODO: Change this so it can point into another (non-owned) buffer, thus
    // enabling sharing of many smaller buffers, e.g. if a mesh consists of a
    // vertex buffer, an index buffer, and an instance one, then they can be
    // stored in the same buffer.
    BufferAllocation deviceBuffer; // “Pointer” to device buffer. May be null;
                                   // then .buffer == VK_NULL_HANDLE.
    VmaAllocator allocator = VK_NULL_HANDLE;
    void* buffer = nullptr; // Pointer to host memory buffer, may be null.
                            // Owned by this buffer; automatically freed.
    VkDeviceSize deviceAllocationSize = 0; // Size of device allocation (in bytes).
    U64 hostAllocationSize = 0; // Size of host allocation (in bytes).
    U64 stride = 0; // Distance between elements in the buffer.
    U64 count = 0; // Number of elements currently stored in buffer(s). Note that it
                   // is an external responsibility to either keep them in sync, or
                   // know if this isn't the case.
};

struct MeshAttributeElement
{
    VkFormat format;
    uint32_t offset;
};

struct NamedMeshAttributeElement
{
    MeshAttributeElement attribute;
    std::string name;
};

// Note that this class ONLY manages the memory of several mesh buffers but does
// nothing more than that. In particular, it does not ensure that the
// descriptions are up to date, nor even that any of the memory is initialised.
class Mesh
{
public:
    Mesh();
    Mesh(const Mesh&) = delete;
    Mesh(Mesh&& other) noexcept { moveIntoThis(std::move(other)); }
    Mesh& operator=(Mesh&&) noexcept;
    ~Mesh();
    void destroy() noexcept;

    MeshBuffer& vertexBuffer(U32 index) { return m_vertexBuffers[index]; }
    const MeshBuffer& vertexBuffer(U32 index) const { return m_vertexBuffers[index]; }
    U32 vertexBufferCount() const { return m_vertexBufferCount; }

    MeshBuffer& indexBuffer(U32 index) { return m_indexBuffers[index]; }
    const MeshBuffer& indexBuffer(U32 index) const { return m_indexBuffers[index]; }
    U32 indexBufferCount() const { return m_indexBufferCount; }

    // There is one binding description for each vertex buffer.
    VkVertexInputBindingDescription* bindings() { return m_bindings; }
    const VkVertexInputBindingDescription* bindings() const { return m_bindings; }
    U32 bindingCount() const { return vertexBufferCount(); }

    void resizeVertexBuffers(U32 count);
    void resizeIndexBuffers(U32 count);

    size_t attributeCount() const noexcept { return m_attributes.size(); }
    const NamedMeshAttributeElement* attributes() const noexcept { return m_attributes.data(); }

    template <typename Iter>
    void setAttributes(Iter attrBegin, Iter attrEnd)
    {
        m_attributes.clear();
        for (auto it = attrBegin; it != attrEnd; ++it)
        {
            m_attributes.push_back(*it);
        }
    }
    template <typename Generator>
    void setAttributes(const Generator& generator)
    {
        setAttributes(begin(generator), end(generator));
    }

private:
    void moveIntoThis(Mesh&& other) noexcept;

    constexpr static U32 preallocatedVertexBufs = 1;
    constexpr static U32 preallocatedIndexBufs = 1;
    MeshBuffer m_vbufStorage[preallocatedVertexBufs];
    MeshBuffer m_ibufStorage[preallocatedIndexBufs];
    VkVertexInputBindingDescription m_bindingStorage[preallocatedVertexBufs];

    MeshBuffer* m_vertexBuffers;
    MeshBuffer* m_indexBuffers;
    VkVertexInputBindingDescription* m_bindings;

    std::vector<NamedMeshAttributeElement> m_attributes;

    U32 m_vertexBufferCount;
    U32 m_indexBufferCount;
};

std::vector<VkVertexInputAttributeDescription>
getAttributeDescriptions(const NamedMeshAttributeElement* pAttributes,
                         VkDeviceSize attrCounts,
                         const ShaderInfo& shaderInfo);

#endif // VULKAN_MESH_HPP_INCLUDED
