#ifndef VULKAN_FRAMEBUFFER_HPP_INCLUDED
#define VULKAN_FRAMEBUFFER_HPP_INCLUDED

#include "vulkan/image.hpp"

struct FramebufferAttachment
{
    ImageAllocation alloc = {VK_NULL_HANDLE, VK_NULL_HANDLE};
    VkImageView view = VK_NULL_HANDLE;
    VkFormat format = VK_FORMAT_UNDEFINED;
    VkAttachmentDescription description;

    bool hasDepthComponent() const;
    bool hasStencilComponent() const;
};

struct AttachmentInfo
{
    VkImageAspectFlags aspectMask;
    VkImageUsageFlagBits usage;
    VkFormat format;
    VkSampleCountFlagBits samples = VK_SAMPLE_COUNT_1_BIT;
};

class Framebuffer
{
public:
    // Note: In each attachment, the member FramebufferAttachment::alloc::alloc
    // determines whether a given attachment's memory should be managed by this
    // class or if it is managed externally. For external management, set the
    // aforementioned member to VK_NULL_HANDLE; then it will not be touched on
    // destruction.
    // Conversely, one can transfer ownership by setting the same member to the
    // actual allocation, but for this to be well-defined, it MUST have been
    // allocated using the same device and allocator.
    std::vector<FramebufferAttachment> attachments;

    Framebuffer()
    {
        resetVariables();
    }
    Framebuffer(VkDevice device, VmaAllocator allocator, U32 width, U32 height)
    {
        resetVariables();
        create(device, allocator, width, height);
    }
    Framebuffer(const Framebuffer&) = delete;
    Framebuffer(Framebuffer&& other) noexcept
    {
        resetVariables();
        (*this) = std::move(other);
    }

    ~Framebuffer() { destroy(); }

    Framebuffer& operator=(const Framebuffer&) = delete;
    Framebuffer& operator=(Framebuffer&& other) noexcept;

    VkResult create(VkDevice device, VmaAllocator allocator, U32 width, U32 height);
    void destroy() noexcept;

    U32 width() const { return m_width; }
    U32 height() const { return m_height; }

    VkResult createAttachment(AttachmentInfo info);
    VkResult createImage(AttachmentInfo info, ImageAllocation* alloc);
    VkResult createView(AttachmentInfo info, VkImage image, VkImageView* view);
    VkResult createFramebuffer(VkRenderPass renderpass, VkFramebuffer* framebuffer);
    VkResult createRenderpass(VkRenderPass* renderpass);

private:
    void destroyImage(ImageAllocation& alloc) noexcept;
    void destroyView(VkImageView& view) noexcept;
    void resetVariables()
    {
        m_device = VK_NULL_HANDLE;
        m_allocator = VK_NULL_HANDLE;
        m_width = 0;
        m_height = 0;
        attachments.clear();
    }

    VkDevice m_device = VK_NULL_HANDLE;
    VmaAllocator m_allocator = VK_NULL_HANDLE;
    U32 m_width = 0;
    U32 m_height = 0;
};
/*
class Subpass
{
public:
    Subpass();
    Subpass(VkDevice device) { create(device); }
    ~Subpass() { destroy(); }

    void create(VkDevice device);
    void destroy();
};*/

class RenderPass
{
public:
    RenderPass();
    RenderPass(VkDevice device) { create(device); }
    ~RenderPass() { destroy(); }

    void create(VkDevice device);
    void destroy();



    Framebuffer framebuffer;

private:
    VkDevice m_device;
    //std::vector<Subpass> subpasses;
};

#endif // VULKAN_FRAMEBUFFER_HPP_INCLUDED
