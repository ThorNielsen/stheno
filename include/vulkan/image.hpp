#ifndef VULKAN_IMAGE_HPP_INCLUDED
#define VULKAN_IMAGE_HPP_INCLUDED

#include "vulkan/allocator.hpp"
#include "vulkan/common.hpp"

#include <unordered_map>

struct ImageAllocation
{
    VkImage image = VK_NULL_HANDLE;
    VmaAllocation alloc = VK_NULL_HANDLE;
};

struct Image
{
    VkImage image = VK_NULL_HANDLE;
    VmaAllocation alloc = VK_NULL_HANDLE;
    VkImageView view = VK_NULL_HANDLE;
    VkSampler sampler = VK_NULL_HANDLE;
    VmaAllocator allocator = VK_NULL_HANDLE;
    VkDevice device = VK_NULL_HANDLE;
    VkImageLayout layout;
    VkAccessFlags accessMask;
    VkDescriptorImageInfo descriptor;
    VkExtent3D extent;
    U32 mipmapCount;
    U32 layerCount;
    U32 dimensions;
    bool destroyOnDestruction = false;

    ~Image()
    {
        if (destroyOnDestruction) destroy();
    }

    VkImageViewCreateInfo viewCreateInfo(VkFormat format) const;

    VkResult allocate(const VkImageCreateInfo& createInfo,
                      U32 dimensionCount,
                      VmaAllocator allocator,
                      VkDevice device,
                      bool createView = true);

    void destroy(VmaAllocator allocator, VkDevice device, bool destroyView = true);

    void destroy();

    VkImageSubresourceRange defaultSubresourceRange() const;

    void updateDescriptor()
    {
        descriptor.imageLayout = layout;
        descriptor.sampler = sampler;
        descriptor.imageView = view;
    }
};

bool hasDepthComponent(VkFormat fmt);
bool hasStencilComponent(VkFormat fmt);

bool operator==(const VkSamplerCreateInfo&, const VkSamplerCreateInfo&);

namespace std
{
    template<>
    struct hash<VkSamplerCreateInfo>
    {
        size_t operator()(const VkSamplerCreateInfo& info) const
        {
            return (hash<const void*>()(info.pNext)
                    ^ ((hash<U32>()(info.flags) << 1) >> 1)
                    ^ ((hash<U32>()(info.magFilter) << 1))
                    ^ ((hash<U32>()(info.minFilter) << 2) >> 1)
                    ^ ((hash<U32>()(info.mipmapMode) << 1) >> 2)
                    ^ ((hash<U32>()(info.addressModeU) << 3) >> 2)
                    ^ ((hash<U32>()(info.addressModeV) << 5) >> 4)
                    ^ ((hash<U32>()(info.addressModeW)) >> 1)
                    ^ ((hash<float>()(info.mipLodBias)) >> 2)
                    ^ ((hash<bool>()(info.anisotropyEnable) << 1) >> 1)
                    ^ ((hash<float>()(info.maxAnisotropy)))
                    ^ ((hash<bool>()(info.compareEnable) << 1) >> 1)
                    ^ ((hash<U32>()(info.compareOp) << 1) >> 1)
                    ^ ((hash<float>()(info.minLod) << 1) >> 1)
                    ^ ((hash<float>()(info.maxLod) << 1) >> 1)
                    ^ ((hash<U32>()(info.borderColor) << 1) >> 1)
                    ^ ((hash<bool>()(info.unnormalizedCoordinates) << 1) >> 1));
        }
    };
} // namespace std

class SamplerCache
{
public:
    SamplerCache();
    SamplerCache(VkDevice device);
    SamplerCache(const SamplerCache&) = delete;
    SamplerCache(SamplerCache&&);
    ~SamplerCache();

    SamplerCache& operator=(const SamplerCache&) = delete;
    SamplerCache& operator=(SamplerCache&&);

    VkResult create(VkDevice device);
    void destroy();

    VkResult getSampler(VkSampler* sampler,
                        VkFilter magFilter,
                        VkFilter minFilter,
                        VkSamplerMipmapMode mipmapMode,
                        VkSamplerAddressMode addressMode,
                        float maxAnisotropy);

    VkResult getSampler(VkSampler* sampler,
                        const VkSamplerCreateInfo& createInfo);

private:
    void resetVariables();

    std::unordered_map<VkSamplerCreateInfo, VkSampler> m_samplers;
    VkDevice m_device;
};

#endif // VULKAN_IMAGE_HPP_INCLUDED
