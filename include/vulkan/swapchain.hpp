#ifndef VULKAN_SWAPCHAIN_HPP_INCLUDED
#define VULKAN_SWAPCHAIN_HPP_INCLUDED

#include "vulkan/common.hpp"
#include "vulkan/image.hpp"

#include "vulkan/framebuffer.hpp"

struct SwapchainCreateInfo
{
    struct Swapchain
    {
        VkSurfaceFormatKHR desiredFormat;
        VkPresentModeKHR desiredPresentMode;
        U32 minImageCount;
        VkExtent2D extent;
        bool clipped;
    } swapchain;

    VkSurfaceKHR surface;
    VkPhysicalDevice physicalDevice;
    VkDevice device;
    U32 maxCachedFrames;
};

class Swapchain
{
public:
    VkResult create(const SwapchainCreateInfo& createInfo, VmaAllocator allocator,
                    bool createSyncObjects = true);

    void destroy();

    //const Attachment& attachment(U32 idx) const { return m_attachments[idx]; }
    VkExtent2D extent() const { return m_extent; }
    VkSurfaceFormatKHR format() const { return m_format; }
    VkPresentModeKHR presentMode() const { return m_presentMode; }

    U32 imageCount() const { return m_swapImages.size(); }

    U32 maxCachedFrames() const { return m_maxCachedFrames; }

    VkImageView view(U32 idx) const { return m_swapViews[idx]; }

    VkResult acquireNextImage(U32& imageIndex, VkSemaphore imageAcquireSemaphore = VK_NULL_HANDLE);

    void getCurrentSemaphores(VkSemaphore& imageAcquire,
                              VkSemaphore& renderFinished);

    VkFence getFrameFence() { return m_frameFences[m_currFrame]; }

    VkResult present(VkQueue queue, U32 waitSemaphoreCount = 0,
                     const VkSemaphore* pWaitSemaphores = nullptr);

private:
    // If either of the create...() functions fail, they clean up such that no
    // resources are leaked.
    VkResult createSwapchain(const SwapchainCreateInfo& createInfo);
    VkResult createSynchronisationObjects();
    bool hasSyncObjects() const;

    void destroySwapchain();

    std::vector<VkSemaphore> m_imageAcquireSemaphores;
    std::vector<VkSemaphore> m_renderFinishedSemaphores;
    std::vector<VkFence> m_frameFences;
    std::vector<VkFence> m_cachedFrameFences;

    std::vector<VkImage> m_swapImages;
    std::vector<VkImageView> m_swapViews;
    VkSwapchainKHR m_swapchain;

    VmaAllocator m_allocator = VK_NULL_HANDLE;
    VkDevice m_device = VK_NULL_HANDLE;
    VkSurfaceKHR m_surface = VK_NULL_HANDLE;

    VkSurfaceFormatKHR m_format;
    VkPresentModeKHR m_presentMode;
    VkExtent2D m_extent;

    U32 m_currImageIndex;
    U32 m_currFrame;
    U32 m_maxCachedFrames;
};

#endif // VULKAN_SWAPCHAIN_HPP_INCLUDED
