#ifndef VULKAN_ALLOCATION_UTILS_HPP_INCLUDED
#define VULKAN_ALLOCATION_UTILS_HPP_INCLUDED

#include "vulkan/allocator.hpp"
#include "vulkan/shader.hpp"

#include <set>
#include <vector>
#include <utility>

/// This gives the required size needed to store the relevant descriptors.
///
/// \remark The buffer infos written to bufInfoOut will not have the .buffer
///         field filled;
///
/// \param type The type of descriptor to get the buffer size of. Note that some
///             descriptor types cannot be stored in buffers; such as a
///             VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, and the like. In such
///             cases, 0 is returned.
/// \param info The shader info to compute buffer sizes from.
/// \param bufInfoOut A pointer to a vector of "VkDescriptorBufferInfo"s to
///                   which all buffer infos will be written. If a null pointer,
///                   nothing will be written (obviously); nullptr is a valid
///                   argument.
/// \param setBits A bitfield indicating which descriptor sets from 'info' that
///                should be included in this computation.
///
/// \returns The required buffer size (including enough space for padding
///          between different uniform blocks, if applicable) if the type makes
///          sense (e.g. VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER); and 0 otherwise.
VkDeviceSize
getRequiredBufferSize(VkDescriptorType type,
                      const ShaderInfo& info,
                      std::vector<VkDescriptorBufferInfo>* bufInfoOut = nullptr,
                      U64 setBits = ~(size_t)0);

class MemoryMapGuard
{
public:
    MemoryMapGuard(VmaAllocator allocator,
                   VmaAllocation allocation)
        : m_allocator{allocator}, m_allocation{allocation}
    {
        vmaMapMemory(m_allocator, m_allocation, &m_memory);
    }
    MemoryMapGuard(const MemoryMapGuard&) = delete;
    MemoryMapGuard(MemoryMapGuard&& other)
    {
        *this = std::move(other);
    }

    MemoryMapGuard& operator=(const MemoryMapGuard&) = delete;
    MemoryMapGuard& operator=(MemoryMapGuard&& other)
    {
        m_allocator = other.m_allocator;
        m_allocation = other.m_allocation;
        m_memory = other.m_memory;
        other.m_memory = nullptr;
        return *this;
    }

    ~MemoryMapGuard()
    {
        vmaUnmapMemory(m_allocator, m_allocation);
    }

    void* data()
    {
        return m_memory;
    }

    void* data(size_t offset)
    {
        return static_cast<void*>(static_cast<U8*>(m_memory)+offset);
    }

    template <typename Type>
    Type* getAt(size_t offset)
    {
        return static_cast<Type*>(data(offset));
    }

    operator void*()
    {
        return data();
    }

    template <typename CastType>
    operator typename std::enable_if<sizeof(CastType) == 1, CastType*>()
    {
        return static_cast<CastType*>(data());
    }

private:
    VmaAllocator m_allocator;
    VmaAllocation m_allocation;
    void* m_memory;
};

class MultiAllocatedBuffer
{
public:
    struct AllocationType
    {
        VkDeviceSize size;
        VkDeviceSize alignment;
    };
    using AllocationTypeIndex = size_t;

    MultiAllocatedBuffer()
        : m_allocator{VK_NULL_HANDLE}, m_allocatedBytes{0}
    {
        m_allocation.allocation = VK_NULL_HANDLE;
    }

    MultiAllocatedBuffer(VmaAllocator allocator,
                         VkDescriptorType type,
                         size_t byteSize)
        : m_allocator{allocator}, m_usedBytes{0}, m_descriptorType{type}
    {
        if (construct(allocator, type, byteSize) != VK_SUCCESS)
        {
            throw std::runtime_error("Failed to construct multi-alloc buffer.");
        }
    }
    MultiAllocatedBuffer(const MultiAllocatedBuffer&) = delete;
    MultiAllocatedBuffer(MultiAllocatedBuffer&& other) noexcept
    {
        (*this) = std::move(other);
    }

    MultiAllocatedBuffer& operator=(MultiAllocatedBuffer&& other) noexcept;

    ~MultiAllocatedBuffer()
    {
        destroy();
    }

    VkResult construct(VmaAllocator allocator,
                       VkDescriptorType type,
                       size_t byteSize);

    void destroy()
    {
        destroyAllocation(m_allocation);
    }

    VkResult allocateBuffer(VkDeviceSize size,
                            VkDeviceSize alignment,
                            VkDescriptorBufferInfo& bufInfoOut);

    // At some point this should be made nothrow or something similar, but for
    // now, throwing on bad buffer infos is a good debugging aid.
    void freeBuffer(VkDescriptorBufferInfo bufInfo);

    MemoryMapGuard mapBuffer()
    {
        return MemoryMapGuard(m_allocator, m_allocation.allocation);
    }

private:
    struct AllocatedRange
    {
        VkDeviceSize from; ///< Byte offset into buffer where this allocation
                           ///  starts.
        VkDeviceSize to; ///< Byte offset one-past-the-end where this allocation
                         ///  ends; note that this may not satisfy any
                         ///  particular alignment requirements.
        bool operator<(const AllocatedRange& o) const { return from < o.from; }
    };

    /// Suballocates into the buffer this class contains.
    ///
    /// \param size Size to suballocate.
    /// \param alignment Alignment requirement within buffer.
    ///
    /// \returns Offset into buffer where the data is allocated from; if the
    ///          allocation fails, this returns some offset larger than the
    ///          allocated buffer.
    VkDeviceSize suballocate(VkDeviceSize size, VkDeviceSize alignment);

    VkDeviceSize align(VkDeviceSize offset,
                       VkDeviceSize alignment,
                       VkDeviceSize alignmentMask) const
    {
        auto straddle = offset & alignmentMask;
        if (!straddle) return offset;
        return offset + (alignment - straddle);
    }

    // Todo: Decide how to handle reallocations, because it requires the buffer
    // to not be in use, and for the descriptors depending upon this, if any, to
    // be updated.
    void reallocateBuffer()
    {
        throw std::logic_error("Buffer reallocation not implemented.");
    }

    VkResult allocate(VkDeviceSize size, BufferAllocation& allocOut);

    VkResult allocate(VkDeviceSize size,
                      VkBufferUsageFlags flags,
                      VmaMemoryUsage usage,
                      BufferAllocation& allocOut);

    void destroyAllocation(BufferAllocation& alloc);

    std::vector<AllocationType> m_allocTypes;

    std::set<AllocatedRange> m_allocatedRanges;
    BufferAllocation m_allocation;
    VmaAllocator m_allocator;
    VkDeviceSize m_allocatedBytes;
    VkDeviceSize m_usedBytes;

    VkDescriptorType m_descriptorType;
};

#endif // VULKAN_ALLOCATION_UTILS_HPP_INCLUDED
