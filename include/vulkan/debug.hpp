#ifndef VULKAN_DEBUG_HPP_INCLUDED
#define VULKAN_DEBUG_HPP_INCLUDED

#include "vulkan/common.hpp"
#include <ostream>

void printFormatBufferProperties(VkPhysicalDevice device, std::ostream& output);

#endif // VULKAN_DEBUG_HPP_INCLUDED
