#ifndef VULKAN_SHADER_HPP_INCLUDED
#define VULKAN_SHADER_HPP_INCLUDED

#include "vulkan/common.hpp"
#include "vulkan/utility.hpp"

#include "vulkan/descriptor.hpp"

#include <kraken/utility/json.hpp>

struct ShaderInfo
{
    struct Attribute
    {
        std::string name;
        VkFormat format;
        U32 location;
    };

    struct Descriptor
    {
        std::string name;
        U32 set;
        U32 size;
        U32 binding;
        VkShaderStageFlagBits stages;
        VkDescriptorType type;
    };

    void appendLayoutInfo(DescriptorSetLayoutInfo& layoutOut, U32 set) const;

    std::vector<Attribute> attributes;
    std::vector<Descriptor> descriptors;
    std::vector<U32> usedSets; // Used descriptor sets; guaranteed to be listed
                               // in ascending order.
};

class GraphicsShader
{
public:
    GraphicsShader();
    GraphicsShader(GraphicsShader&&) noexcept;
    GraphicsShader(const GraphicsShader&) = delete;

    GraphicsShader& operator=(GraphicsShader&&) noexcept;

    GraphicsShader(const kraken::JSONValue& description, VkDevice device);
    ~GraphicsShader();

    bool create(const kraken::JSONValue& description, VkDevice device);
    bool createFromFile(fs::path filePath, VkDevice device);

    void setDevice(VkDevice device) { m_device = device; }

    void enableStages(int which);
    void disableStages(int which);
    void destroyStages(int which);

    void destroy();

    const ShaderInfo& info() const { return m_shaderInfo; }
    const VkPipelineShaderStageCreateInfo* pStages() const { return m_infos; }
    U32 stageCount() const { return m_enabledStages; }

    VkShaderModule& vertex() { return m_modules[0]; }
    VkShaderModule& tessellationContol() { return m_modules[1]; }
    VkShaderModule& tessellationEvaluation() { return m_modules[2]; }
    VkShaderModule& geometry() { return m_modules[3]; }
    VkShaderModule& fragment() { return m_modules[4]; }
    VkPipelineShaderStageCreateInfo& vertexInfo() { return m_infos[m_mapping[0]]; }
    VkPipelineShaderStageCreateInfo& tessellationContolInfo() { return m_infos[m_mapping[1]]; }
    VkPipelineShaderStageCreateInfo& tessellationEvaluationInfo() { return m_infos[m_mapping[2]]; }
    VkPipelineShaderStageCreateInfo& geometryInfo() { return m_infos[m_mapping[3]]; }
    VkPipelineShaderStageCreateInfo& fragmentInfo() { return m_infos[m_mapping[4]]; }

private:
    void reset(int stagesToEnable) noexcept;
    U32 getSlotOwner(U32 index) const;

    ShaderInfo m_shaderInfo;
    VkShaderModule m_modules[5];
    VkPipelineShaderStageCreateInfo m_infos[5];
    std::string m_entrypoints[5];
    U32 m_mapping[5];
    U32 m_enabledStages;

    VkDevice m_device = VK_NULL_HANDLE;
};

#endif // VULKAN_SHADER_HPP_INCLUDED
