#ifndef VULKAN_FRAMEGRAPH_HPP_INCLUDED
#define VULKAN_FRAMEGRAPH_HPP_INCLUDED

#include <functional>
#include <map>

#include "vulkan/descriptor.hpp"
#include "vulkan/image.hpp"
#include "vulkan/swapchain.hpp"

/**
struct FramebufferAttachment
{
    ImageAllocation alloc = {VK_NULL_HANDLE, VK_NULL_HANDLE};
    VkImageView view = VK_NULL_HANDLE;
    VkFormat format = VK_FORMAT_UNDEFINED;
    VkAttachmentDescription description;

    bool hasDepthComponent() const;
    bool hasStencilComponent() const;
};*/

struct PassLayout
{
    U32 width = 0;
    U32 height = 0;
    VkSampleCountFlagBits samples = VK_SAMPLE_COUNT_1_BIT;
    U32 layers = 1;

    [[nodiscard]] auto operator<=>(const PassLayout& other) const = default;
};

struct ExternalImageInfo
{
    // If there is more than one view, then it is critical to remember to
    // specify that when executing the frame graph.
    VkImageView view;
    // Technically, it is not necessary to fill in the images, UNLESS one wants
    // to make use of automatic blitting -- then it is necessary.
    VkImage image;

    // The layouts describing this image outside of rendering, meaning:
    // initialLayout is what layout the image will be in right before rendering
    // starts.
    // finalLayout is the layout the image is expected to be transitioned into
    // right before ending the rendering.
    VkImageLayout initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    VkImageLayout finalLayout = VK_IMAGE_LAYOUT_UNDEFINED;
};

struct RenderPassInfo
{
    VkRenderPass pass;
    VkFramebuffer framebuffer;
    U32 subpass;
};

class FrameGraph
{
public:
    constexpr static U32 invalidID = 0xffffffff;

    struct PassAttachmentReference
    {
        U32 passID;
        U32 inputID;
        U32 outputID;

        PassAttachmentReference inputReference() const
        {
            return {passID, inputID, 0xffffffff};
        }
        PassAttachmentReference asInput() const { return inputReference(); }

        PassAttachmentReference outputReference() const
        {
            return {passID, 0xffffffff, outputID};
        }
        PassAttachmentReference asOutput() const { return outputReference(); }
    };

    struct AttachmentInfo
    {
        VkImage image;
        VkImageView view;
        VkImageLayout finalLayout;
        VkImageUsageFlags usageFlags;
    };

    class RenderPass
    {
    public:
        friend class FrameGraph;

        RenderPass()
            : m_layout{},
              m_renderPassInfo{VK_NULL_HANDLE, VK_NULL_HANDLE, 0},
              m_passID{invalidID},
              m_swapchainImageID{0xffffffff},
              m_subpassCompatibleInputs{true}
            {}

        // addInputAttachment specifies that it is an attachment, and thus will
        // only be read from the particular (x,y,layer,sample)-position.
        RenderPass& addInputAttachment(std::string name, VkFormat format)
        {
            m_inputs.push_back({name, format, VK_NULL_HANDLE, {}, true, nullptr, invalidID, {}});
            auto layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            if (hasDepthComponent(format) || hasStencilComponent(format))
            {
                layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
            }
            m_inputs.back().imgInfo.usedLayout = layout;
            return *this;
        }

        // addInputImage specifies that the given input can be read from
        // anywhere.
        RenderPass& addInputImage(std::string name, VkFormat format, VkSampler sampler)
        {
            m_inputs.push_back({name, format, sampler, {}, false, nullptr, invalidID, {}});
            m_subpassCompatibleInputs = false;
            auto layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            if (hasDepthComponent(format) || hasStencilComponent(format))
            {
                layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
            }
            m_inputs.back().imgInfo.usedLayout = layout;
            return *this;
        }

        // If clearValue is set, the attachment will automatically be cleared to
        // the desired value.
        // Note that unsetting it will have the effect that the attachment WILL
        // NOT be cleared, even if one calls setClearValue(...) afterwards!
        RenderPass& addOutput(std::string name, VkFormat format,
                              std::optional<VkClearValue> clearValue = {},
                              VkImageUsageFlags extraUsageFlags = 0);

        RenderPass& addOutput(std::string name, const Swapchain& swapchain,
                              std::optional<VkClearValue> clearValue = {});

        void setClearValue(PassAttachmentReference ref,
                           VkClearValue cv)
        {
            if (ref.outputID >= m_outputs.size())
            {
                throw std::logic_error("Invalid output reference.");
            }
            m_outputs[ref.outputID].clearValue = cv;
        }

        void markExternallyCleared(PassAttachmentReference outputRef,
                                   VkImageLayout initialLayout)
        {
            if (outputRef.outputID >= m_outputs.size())
            {
                throw std::logic_error("Invalid output reference.");
            }
            m_outputs[outputRef.outputID].loadOrClear = true;
            m_outputs[outputRef.outputID].isClearedExternally = true;
            m_outputs[outputRef.outputID].imgInfo.initialLayout = initialLayout;
        }

        // Gets VkWriteDescriptorSet structures which describes this pass's
        // attachments/sampled input images.
        // As is customary for Vulkan functions of this type, pass nullptr to
        // 'writeOut' to get the number of structures this will write. If
        // 'writeOut' is a valid pointer, then the value pointed to by 'count'
        // will be set to the number of structures written to 'writeOut'.
        // Note that FrameGraph::compile() must have been called first, as this
        // will otherwise not know anything about the actual resources.
        void writeDescriptorSet(VkDescriptorSet dstSet,
                                U32* count,
                                VkWriteDescriptorSet* writeOut) const;

        PassAttachmentReference operator[](std::string_view name);

        RenderPass& operator=(RenderPass&& o)
        {
            m_inputs = std::move(o.m_inputs);
            m_outputs = std::move(o.m_outputs);
            m_layout = o.m_layout;
            m_renderPassInfo = o.m_renderPassInfo;
            m_stages = o.m_stages;
            m_passID = o.m_passID;
            m_subpassCompatibleInputs = o.m_subpassCompatibleInputs;
            return *this;
        }
        RenderPass(RenderPass&& o)
        {
            *this = std::move(o);
        }

        void setCallback(std::function<void(VkCommandBuffer)> cb)
        {
            m_callback = cb;
        }

        RenderPassInfo vulkanPass() const
        {
            return m_renderPassInfo;
        }

        U32 passID() const { return m_passID; }

        PassLayout layout() const { return m_layout; }

    private:
        void dump();

        struct ConnectionType
        {
            // Whether the destination of the connection uses the attachment as
            // an input attachment or as a sampled image.
            bool attachment = true;
            // Whether the connection is simple or not, i.e. whether any
            // conversion needs to take place between the source pass and the
            // destination pass.
            bool simple = true;
            // Whether the connection is to an input attachment, or as an input
            // to an output attachment.
            bool toInput = true;
        };

        struct AttachmentReference
        {
            RenderPass* pass = nullptr;
            U32 attachmentID = 0xffffffff;
            bool isInput = true;
        };

/*
        enum class ConnectionType : U32
        {
            // Indicates that the connection is simple, i.e. that no conversion
            // needs to take place from the source pass to the destination pass,
            // except for a single layout transition.
            SimpleInputAttachment = 0,
            // Indicates that some conversion needs to take place, but the
            // output attachment is still used as an input attachment. This is
            // e.g. the case if rescaling and/or changing format.
            ConvertedInputAttachment,
            // Indicates that the output attachment is used as a shader input
            // and may be sampled anywhere
            SimpleSampled,
        };*/

        struct AttachmentImageInfo
        {
            void dump(std::string_view prefix) const;
            // Where is this image used (within the SORTED pass chain)?
            // These references refer to the previous and next uses of the
            // If previous.passID == 0xffffffff, then this is the owner of the
            AttachmentReference previous;
            AttachmentReference next;
            // View "pointing" to image.
            VkImageView view = VK_NULL_HANDLE;
            // Actual image.
            VkImage image = VK_NULL_HANDLE;

            // Usage flags.
            VkImageUsageFlags usage = 0;

            // Layout that this expects the attachment to be in initially.
            VkImageLayout initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

            // Layout that the attachment is used in during the pass.
            VkImageLayout usedLayout = VK_IMAGE_LAYOUT_UNDEFINED;

            // Layout that this image will end up in.
            VkImageLayout finalLayout = VK_IMAGE_LAYOUT_UNDEFINED;

            // Last pass # in order where this is used.
            // E.g. if we schedule passes 0-4 as 1 2 0 4 3, and this is used
            // in pass #0 and #1, then lastPass == 2, since pass #1 appears in
            // position 0 and pass #0 appears in position 2, which is the final
            // place of usage.
            U32 lastUsage = 0xffffffff;
        };

        struct InputAttachmentInfo
        {
            std::string name;
            VkFormat format;
            VkSampler sampler = VK_NULL_HANDLE;
            AttachmentImageInfo imgInfo;
            bool attachment; // Whether this is used as an attachment, or merely
                             // as an image.
            RenderPass* connectedPass;
            U32 connectedAttachment;

            ExternalImageInfo externalInfo;
        };

        struct OutputAttachmentInfo
        {
            struct OutgoingConnection
            {
                RenderPass* pass;
                U32 attachment;
                ConnectionType type;
            };

            std::string name;
            VkFormat format;
            std::vector<OutgoingConnection> connections;

            AttachmentImageInfo imgInfo;

            RenderPass* sourcePass;
            U32 sourceAttachment;

            VkClearValue clearValue;
            bool loadOrClear;
            bool isClearedExternally = false;

            ExternalImageInfo externalInfo = {};

            VkImageUsageFlags extraCreationFlags = 0;
            bool hasExternalUsage = false;
        };

        struct DescriptorInfo
        {
            std::vector<VkDescriptorImageInfo> attachmentInfo;
            std::vector<VkDescriptorImageInfo> sampledInfo;
            std::vector<VkWriteDescriptorSet> descWrites;
        };

        static VkImageUsageFlags getRequiredImageUsageFlags(const OutputAttachmentInfo& attachment);
        VkImageLayout getFinalLayout(const OutputAttachmentInfo&);
        static AttachmentImageInfo* getImageInfo(AttachmentReference ref);


        VkAttachmentDescription* updateAttachmentInfo(U32& attachmentCount);

        RenderPass(U32 passID, PassLayout layout, VkPipelineStageFlags stages)
            : m_layout{layout}, m_renderPassInfo{VK_NULL_HANDLE, VK_NULL_HANDLE, 0}, m_stages{stages}, m_passID{passID}, m_subpassCompatibleInputs{true} {}

        //static bool areSubpassCompatible(const RenderPass* pass, U32 outputAttachmentID);

        static bool subpassCompatible(const RenderPass* dstPass, U32 inputID);

        void createIncomingConnection(RenderPass* otherPass,
                                      U32 otherAttachment,
                                      U32 thisAttachment,
                                      bool isInput);

        void createOutgoingConnection(RenderPass* otherPass,
                                      U32 otherAttachment,
                                      U32 thisAttachment,
                                      bool isToInput);

        std::function<void(VkCommandBuffer)> m_callback = [](VkCommandBuffer){};

        std::vector<InputAttachmentInfo> m_inputs;
        std::vector<OutputAttachmentInfo> m_outputs;
        std::vector<VkAttachmentDescription> m_attachDescCache;

        DescriptorInfo m_descInfo;

        PassLayout m_layout;
        RenderPassInfo m_renderPassInfo;
        VkPipelineStageFlags m_stages;
        U32 m_passID;
        U32 m_swapchainImageID = 0xffffffff;
        bool m_subpassCompatibleInputs;
    };

    FrameGraph()
    {
        resetVariables();
    }
    FrameGraph(VkDevice device, VmaAllocator allocator)
    {
        resetVariables();
        create(device, allocator);
    }
    ~FrameGraph() { destroy(); }

    VkResult create(VkDevice device, VmaAllocator allocator);
    void destroy();

    RenderPass& addPass(std::string_view name, PassLayout layout, VkPipelineStageFlags stages);
    RenderPass* pass(const std::string& name);

    void connect(PassAttachmentReference output, PassAttachmentReference input);
    void connectToInput(PassAttachmentReference output,
                        PassAttachmentReference input);
    void connectToOutput(PassAttachmentReference outputToReadFrom,
                         PassAttachmentReference outputToUse);

    void markOutputUsed(PassAttachmentReference output,
                        VkImageLayout desiredFinalLayout,
                        VkImageUsageFlags extraUsageFlags = 0);

    void markExternal(PassAttachmentReference ref,
                      ExternalImageInfo info);

    ExternalImageInfo& externalImageInfo(PassAttachmentReference ref);
    const ExternalImageInfo& externalImageInfo(PassAttachmentReference ref) const;

    AttachmentInfo inputAttachmentInfo(PassAttachmentReference inputRef) const;
    AttachmentInfo outputAttachmentInfo(PassAttachmentReference outputRef) const;

    void compile(const VkImageView* swapchainViews = nullptr,
                 U32 swapchainViewCount = 0,
                 bool printDebugInfo = false);

    // This executes all of the render passes. It is the responsibility of each
    // pass's callback to bind the appropriate descriptor set describing the
    // attachments; the relevant VkWriteDescriptorSet structures can be
    // retrieved using RenderPass::writeDescriptorSet(...) from each pass.
    void execute(VkCommandBuffer commandBuf,
                 U32 swapchainIndex = 0,
                 bool printDebugInfo = false);

private:
    struct Connection
    {
        U32 srcPass;
        U32 srcAttachment;
        U32 dstPass;
        U32 dstAttachment;
    };

    struct ScheduledRenderPassDescription
    {
        struct SubpassInfoStorage
        {
            std::vector<VkAttachmentReference> inputRefs;
            std::vector<VkAttachmentReference> colourRefs;
            VkAttachmentReference depthStencil;
        };

        std::vector<VkSubpassDependency> dependencies;
        std::vector<RenderPass*> passes;
        std::vector<VkAttachmentDescription> attachmentDescriptions;
        std::vector<SubpassInfoStorage> subpassStorage;
        std::vector<VkSubpassDescription> subpassDescriptions;
        std::vector<VkImageView> imageViews;
    };


    void ensurePassReferenceValid(PassAttachmentReference ref) const;
    void ensureInputReferenceValid(PassAttachmentReference ref) const;
    void ensureOutputReferenceValid(PassAttachmentReference ref) const;
    void ensureExactlyOnePassReferenceValid(PassAttachmentReference ref) const;

    void createVulkanRenderPass(U32 passID,
                                const ScheduledRenderPassDescription& rpdesc);

    VkFramebuffer createFramebuffer(U32 passID,
                                    ScheduledRenderPassDescription& rpdesc,
                                    VkImageView swapchainReplacement = VK_NULL_HANDLE);

    ScheduledRenderPassDescription getScheduledPassDescription(U32 passID) const;

    AttachmentInfo getAttachmentInfo(const RenderPass::AttachmentImageInfo* ref) const;

    // Assumes m_visited is cleared for all connected passes; appends relevant
    // passes onto m_order.
    void scheduleConnectedPasses(U32 startAt);

    void assignImagesToPasses(bool printDebugInfo);

    U32 getLastUsage(const RenderPass::OutputAttachmentInfo& output,
                     U32 outputPassID) const;

    void resetVisited();
    void resetVariables()
    {
        m_swapchainFramebuffers.clear();
        m_renderPasses.clear();
        m_passReferences.clear();
        m_passNumbers.clear();
        m_images.clear();
        m_visited.clear();
        m_order.clear();

        m_device = VK_NULL_HANDLE;
        m_allocator = VK_NULL_HANDLE;
        m_currPassID = 0;
        m_swapchainPassID = 0xffffffff;
    }

    const Image& getFramebufferAttachmentImage(PassLayout layout, VkFormat fmt,
                                               VkImageUsageFlags requiredUsages,
                                               U32 firstPass, U32 lastPassUsed);

    void assignResources();

    U32 maxPassID() const { return static_cast<U32>(m_passReferences.size()); }

    DescriptorAllocator m_descAllocator;
    std::map<std::string, RenderPass> m_renderPasses;
    std::vector<std::optional<decltype(m_renderPasses.begin())>> m_passReferences;
    std::vector<U32> m_passNumbers;
    std::vector<Image> m_images;
    std::vector<VkFramebuffer> m_swapchainFramebuffers;

    std::vector<bool> m_visited;

    // List of passes in their rendering order.
    std::vector<U32> m_order;

    // m_inverseOrder[X] -- gives the index into m_order where the pass is located
    std::vector<U32> m_inverseOrder;

    VkDevice m_device;
    VmaAllocator m_allocator;

    U32 m_currPassID = 0;
    U32 m_swapchainPassID = 0xffffffff;
};

#endif // VULKAN_FRAMEGRAPH_HPP_INCLUDED
