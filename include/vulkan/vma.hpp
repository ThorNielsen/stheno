#ifndef VULKAN_VMA_HPP_INCLUDED
#define VULKAN_VMA_HPP_INCLUDED

// The below lines can be commented/uncommented at will to enable or disable
// debugging options.
#define ADD_VMA_DEBUG_INSTRUMENTATION

#ifdef ADD_VMA_DEBUG_INSTRUMENTATION

#include <cstdio>

#define VMA_LEAK_LOG_FORMAT(format, ...)\
    do\
    {\
        std::fprintf(stderr, (format "\n"), __VA_ARGS__);\
    } while (false)

#endif // ADD_VMA_DEBUG_INSTRUMENTATION

#include "vk_mem_alloc.h"

#endif // VULKAN_VMA_HPP_INCLUDED
