#ifndef VULKAN_ALLOCATOR_HPP_INCLUDED
#define VULKAN_ALLOCATOR_HPP_INCLUDED

#include "vulkan/common.hpp"
#include "vulkan/vma.hpp"

struct BufferAllocation
{
    VmaAllocation allocation;
    VkBuffer buffer = VK_NULL_HANDLE;

    void destroy(VmaAllocator allocator);
};

#endif // VULKAN_ALLOCATOR_HPP_INCLUDED
