// Note: Autogenerated file -- this should not be modified by hand.
#ifndef VULKAN_TYPES_HPP_INCLUDED
#define VULKAN_TYPES_HPP_INCLUDED

#include <vulkan/vulkan.h>
#include <string>

const char* toString(VkPipelineCacheHeaderVersion value);
const char* toString(VkResult value);
const char* toString(VkStructureType value);
const char* toString(VkSystemAllocationScope value);
const char* toString(VkInternalAllocationType value);
const char* toString(VkFormat value);
const char* toString(VkImageType value);
const char* toString(VkImageTiling value);
const char* toString(VkPhysicalDeviceType value);
const char* toString(VkQueryType value);
const char* toString(VkSharingMode value);
const char* toString(VkImageLayout value);
const char* toString(VkImageViewType value);
const char* toString(VkComponentSwizzle value);
const char* toString(VkVertexInputRate value);
const char* toString(VkPrimitiveTopology value);
const char* toString(VkPolygonMode value);
const char* toString(VkFrontFace value);
const char* toString(VkCompareOp value);
const char* toString(VkStencilOp value);
const char* toString(VkLogicOp value);
const char* toString(VkBlendFactor value);
const char* toString(VkBlendOp value);
const char* toString(VkDynamicState value);
const char* toString(VkFilter value);
const char* toString(VkSamplerMipmapMode value);
const char* toString(VkSamplerAddressMode value);
const char* toString(VkBorderColor value);
const char* toString(VkDescriptorType value);
const char* toString(VkAttachmentLoadOp value);
const char* toString(VkAttachmentStoreOp value);
const char* toString(VkPipelineBindPoint value);
const char* toString(VkCommandBufferLevel value);
const char* toString(VkIndexType value);
const char* toString(VkSubpassContents value);
const char* toString(VkObjectType value);
const char* toString(VkVendorId value);
const char* toString(VkFormatFeatureFlagBits value);
const char* toString(VkImageUsageFlagBits value);
const char* toString(VkImageCreateFlagBits value);
const char* toString(VkSampleCountFlagBits value);
const char* toString(VkQueueFlagBits value);
const char* toString(VkMemoryPropertyFlagBits value);
const char* toString(VkMemoryHeapFlagBits value);
const char* toString(VkDeviceQueueCreateFlagBits value);
const char* toString(VkPipelineStageFlagBits value);
const char* toString(VkImageAspectFlagBits value);
const char* toString(VkSparseImageFormatFlagBits value);
const char* toString(VkSparseMemoryBindFlagBits value);
const char* toString(VkFenceCreateFlagBits value);
const char* toString(VkQueryPipelineStatisticFlagBits value);
const char* toString(VkQueryResultFlagBits value);
const char* toString(VkBufferCreateFlagBits value);
const char* toString(VkBufferUsageFlagBits value);
const char* toString(VkImageViewCreateFlagBits value);
const char* toString(VkPipelineCreateFlagBits value);
const char* toString(VkPipelineShaderStageCreateFlagBits value);
const char* toString(VkShaderStageFlagBits value);
const char* toString(VkCullModeFlagBits value);
const char* toString(VkColorComponentFlagBits value);
const char* toString(VkSamplerCreateFlagBits value);
const char* toString(VkDescriptorSetLayoutCreateFlagBits value);
const char* toString(VkDescriptorPoolCreateFlagBits value);
const char* toString(VkFramebufferCreateFlagBits value);
const char* toString(VkRenderPassCreateFlagBits value);
const char* toString(VkAttachmentDescriptionFlagBits value);
const char* toString(VkSubpassDescriptionFlagBits value);
const char* toString(VkAccessFlagBits value);
const char* toString(VkDependencyFlagBits value);
const char* toString(VkCommandPoolCreateFlagBits value);
const char* toString(VkCommandPoolResetFlagBits value);
const char* toString(VkCommandBufferUsageFlagBits value);
const char* toString(VkQueryControlFlagBits value);
const char* toString(VkCommandBufferResetFlagBits value);
const char* toString(VkStencilFaceFlagBits value);
const char* toString(VkPointClippingBehavior value);
const char* toString(VkTessellationDomainOrigin value);
const char* toString(VkSamplerYcbcrModelConversion value);
const char* toString(VkSamplerYcbcrRange value);
const char* toString(VkChromaLocation value);
const char* toString(VkDescriptorUpdateTemplateType value);
const char* toString(VkSubgroupFeatureFlagBits value);
const char* toString(VkPeerMemoryFeatureFlagBits value);
const char* toString(VkMemoryAllocateFlagBits value);
const char* toString(VkExternalMemoryHandleTypeFlagBits value);
const char* toString(VkExternalMemoryFeatureFlagBits value);
const char* toString(VkExternalFenceHandleTypeFlagBits value);
const char* toString(VkExternalFenceFeatureFlagBits value);
const char* toString(VkFenceImportFlagBits value);
const char* toString(VkSemaphoreImportFlagBits value);
const char* toString(VkExternalSemaphoreHandleTypeFlagBits value);
const char* toString(VkExternalSemaphoreFeatureFlagBits value);
const char* toString(VkDriverId value);
const char* toString(VkShaderFloatControlsIndependence value);
const char* toString(VkSamplerReductionMode value);
const char* toString(VkSemaphoreType value);
const char* toString(VkResolveModeFlagBits value);
const char* toString(VkDescriptorBindingFlagBits value);
const char* toString(VkSemaphoreWaitFlagBits value);
const char* toString(VkColorSpaceKHR value);
const char* toString(VkPresentModeKHR value);
const char* toString(VkSurfaceTransformFlagBitsKHR value);
const char* toString(VkCompositeAlphaFlagBitsKHR value);
const char* toString(VkSwapchainCreateFlagBitsKHR value);
const char* toString(VkDeviceGroupPresentModeFlagBitsKHR value);
const char* toString(VkDisplayPlaneAlphaFlagBitsKHR value);
const char* toString(VkPerformanceCounterUnitKHR value);
const char* toString(VkPerformanceCounterScopeKHR value);
const char* toString(VkPerformanceCounterStorageKHR value);
const char* toString(VkPerformanceCounterDescriptionFlagBitsKHR value);
const char* toString(VkAcquireProfilingLockFlagBitsKHR value);
const char* toString(VkPipelineExecutableStatisticFormatKHR value);
const char* toString(VkDebugReportObjectTypeEXT value);
const char* toString(VkDebugReportFlagBitsEXT value);
const char* toString(VkRasterizationOrderAMD value);
const char* toString(VkShaderInfoTypeAMD value);
const char* toString(VkValidationCheckEXT value);
const char* toString(VkConditionalRenderingFlagBitsEXT value);
const char* toString(VkSurfaceCounterFlagBitsEXT value);
const char* toString(VkDisplayPowerStateEXT value);
const char* toString(VkDeviceEventTypeEXT value);
const char* toString(VkDisplayEventTypeEXT value);
const char* toString(VkDiscardRectangleModeEXT value);
const char* toString(VkConservativeRasterizationModeEXT value);
const char* toString(VkDebugUtilsMessageSeverityFlagBitsEXT value);
const char* toString(VkDebugUtilsMessageTypeFlagBitsEXT value);
const char* toString(VkBlendOverlapEXT value);
const char* toString(VkValidationCacheHeaderVersionEXT value);
const char* toString(VkQueueGlobalPriorityEXT value);
const char* toString(VkPipelineCompilerControlFlagBitsAMD value);
const char* toString(VkTimeDomainEXT value);
const char* toString(VkMemoryOverallocationBehaviorAMD value);
const char* toString(VkPipelineCreationFeedbackFlagBitsEXT value);
const char* toString(VkPerformanceConfigurationTypeINTEL value);
const char* toString(VkQueryPoolSamplingModeINTEL value);
const char* toString(VkPerformanceOverrideTypeINTEL value);
const char* toString(VkPerformanceParameterTypeINTEL value);
const char* toString(VkPerformanceValueTypeINTEL value);
const char* toString(VkShaderCorePropertiesFlagBitsAMD value);
const char* toString(VkToolPurposeFlagBitsEXT value);
const char* toString(VkValidationFeatureEnableEXT value);
const char* toString(VkValidationFeatureDisableEXT value);
const char* toString(VkLineRasterizationModeEXT value);

bool toVkPipelineCacheHeaderVersion(const std::string& value, VkPipelineCacheHeaderVersion* out);
bool toVkResult(const std::string& value, VkResult* out);
bool toVkStructureType(const std::string& value, VkStructureType* out);
bool toVkSystemAllocationScope(const std::string& value, VkSystemAllocationScope* out);
bool toVkInternalAllocationType(const std::string& value, VkInternalAllocationType* out);
bool toVkFormat(const std::string& value, VkFormat* out);
bool toVkImageType(const std::string& value, VkImageType* out);
bool toVkImageTiling(const std::string& value, VkImageTiling* out);
bool toVkPhysicalDeviceType(const std::string& value, VkPhysicalDeviceType* out);
bool toVkQueryType(const std::string& value, VkQueryType* out);
bool toVkSharingMode(const std::string& value, VkSharingMode* out);
bool toVkImageLayout(const std::string& value, VkImageLayout* out);
bool toVkImageViewType(const std::string& value, VkImageViewType* out);
bool toVkComponentSwizzle(const std::string& value, VkComponentSwizzle* out);
bool toVkVertexInputRate(const std::string& value, VkVertexInputRate* out);
bool toVkPrimitiveTopology(const std::string& value, VkPrimitiveTopology* out);
bool toVkPolygonMode(const std::string& value, VkPolygonMode* out);
bool toVkFrontFace(const std::string& value, VkFrontFace* out);
bool toVkCompareOp(const std::string& value, VkCompareOp* out);
bool toVkStencilOp(const std::string& value, VkStencilOp* out);
bool toVkLogicOp(const std::string& value, VkLogicOp* out);
bool toVkBlendFactor(const std::string& value, VkBlendFactor* out);
bool toVkBlendOp(const std::string& value, VkBlendOp* out);
bool toVkDynamicState(const std::string& value, VkDynamicState* out);
bool toVkFilter(const std::string& value, VkFilter* out);
bool toVkSamplerMipmapMode(const std::string& value, VkSamplerMipmapMode* out);
bool toVkSamplerAddressMode(const std::string& value, VkSamplerAddressMode* out);
bool toVkBorderColor(const std::string& value, VkBorderColor* out);
bool toVkDescriptorType(const std::string& value, VkDescriptorType* out);
bool toVkAttachmentLoadOp(const std::string& value, VkAttachmentLoadOp* out);
bool toVkAttachmentStoreOp(const std::string& value, VkAttachmentStoreOp* out);
bool toVkPipelineBindPoint(const std::string& value, VkPipelineBindPoint* out);
bool toVkCommandBufferLevel(const std::string& value, VkCommandBufferLevel* out);
bool toVkIndexType(const std::string& value, VkIndexType* out);
bool toVkSubpassContents(const std::string& value, VkSubpassContents* out);
bool toVkObjectType(const std::string& value, VkObjectType* out);
bool toVkVendorId(const std::string& value, VkVendorId* out);
bool toVkFormatFeatureFlagBits(const std::string& value, VkFormatFeatureFlagBits* out);
bool toVkFormatFeatureFlags(const std::string& value, VkFormatFeatureFlags* out);
bool toVkImageUsageFlagBits(const std::string& value, VkImageUsageFlagBits* out);
bool toVkImageUsageFlags(const std::string& value, VkImageUsageFlags* out);
bool toVkImageCreateFlagBits(const std::string& value, VkImageCreateFlagBits* out);
bool toVkImageCreateFlags(const std::string& value, VkImageCreateFlags* out);
bool toVkSampleCountFlagBits(const std::string& value, VkSampleCountFlagBits* out);
bool toVkSampleCountFlags(const std::string& value, VkSampleCountFlags* out);
bool toVkQueueFlagBits(const std::string& value, VkQueueFlagBits* out);
bool toVkQueueFlags(const std::string& value, VkQueueFlags* out);
bool toVkMemoryPropertyFlagBits(const std::string& value, VkMemoryPropertyFlagBits* out);
bool toVkMemoryPropertyFlags(const std::string& value, VkMemoryPropertyFlags* out);
bool toVkMemoryHeapFlagBits(const std::string& value, VkMemoryHeapFlagBits* out);
bool toVkMemoryHeapFlags(const std::string& value, VkMemoryHeapFlags* out);
bool toVkDeviceQueueCreateFlagBits(const std::string& value, VkDeviceQueueCreateFlagBits* out);
bool toVkDeviceQueueCreateFlags(const std::string& value, VkDeviceQueueCreateFlags* out);
bool toVkPipelineStageFlagBits(const std::string& value, VkPipelineStageFlagBits* out);
bool toVkPipelineStageFlags(const std::string& value, VkPipelineStageFlags* out);
bool toVkImageAspectFlagBits(const std::string& value, VkImageAspectFlagBits* out);
bool toVkImageAspectFlags(const std::string& value, VkImageAspectFlags* out);
bool toVkSparseImageFormatFlagBits(const std::string& value, VkSparseImageFormatFlagBits* out);
bool toVkSparseImageFormatFlags(const std::string& value, VkSparseImageFormatFlags* out);
bool toVkSparseMemoryBindFlagBits(const std::string& value, VkSparseMemoryBindFlagBits* out);
bool toVkSparseMemoryBindFlags(const std::string& value, VkSparseMemoryBindFlags* out);
bool toVkFenceCreateFlagBits(const std::string& value, VkFenceCreateFlagBits* out);
bool toVkFenceCreateFlags(const std::string& value, VkFenceCreateFlags* out);
bool toVkQueryPipelineStatisticFlagBits(const std::string& value, VkQueryPipelineStatisticFlagBits* out);
bool toVkQueryPipelineStatisticFlags(const std::string& value, VkQueryPipelineStatisticFlags* out);
bool toVkQueryResultFlagBits(const std::string& value, VkQueryResultFlagBits* out);
bool toVkQueryResultFlags(const std::string& value, VkQueryResultFlags* out);
bool toVkBufferCreateFlagBits(const std::string& value, VkBufferCreateFlagBits* out);
bool toVkBufferCreateFlags(const std::string& value, VkBufferCreateFlags* out);
bool toVkBufferUsageFlagBits(const std::string& value, VkBufferUsageFlagBits* out);
bool toVkBufferUsageFlags(const std::string& value, VkBufferUsageFlags* out);
bool toVkImageViewCreateFlagBits(const std::string& value, VkImageViewCreateFlagBits* out);
bool toVkImageViewCreateFlags(const std::string& value, VkImageViewCreateFlags* out);
bool toVkShaderModuleCreateFlags(const std::string& value, VkShaderModuleCreateFlags* out);
bool toVkPipelineCreateFlagBits(const std::string& value, VkPipelineCreateFlagBits* out);
bool toVkPipelineCreateFlags(const std::string& value, VkPipelineCreateFlags* out);
bool toVkPipelineShaderStageCreateFlagBits(const std::string& value, VkPipelineShaderStageCreateFlagBits* out);
bool toVkPipelineShaderStageCreateFlags(const std::string& value, VkPipelineShaderStageCreateFlags* out);
bool toVkShaderStageFlagBits(const std::string& value, VkShaderStageFlagBits* out);
bool toVkShaderStageFlags(const std::string& value, VkShaderStageFlags* out);
bool toVkCullModeFlagBits(const std::string& value, VkCullModeFlagBits* out);
bool toVkCullModeFlags(const std::string& value, VkCullModeFlags* out);
bool toVkColorComponentFlagBits(const std::string& value, VkColorComponentFlagBits* out);
bool toVkColorComponentFlags(const std::string& value, VkColorComponentFlags* out);
bool toVkSamplerCreateFlagBits(const std::string& value, VkSamplerCreateFlagBits* out);
bool toVkSamplerCreateFlags(const std::string& value, VkSamplerCreateFlags* out);
bool toVkDescriptorSetLayoutCreateFlagBits(const std::string& value, VkDescriptorSetLayoutCreateFlagBits* out);
bool toVkDescriptorSetLayoutCreateFlags(const std::string& value, VkDescriptorSetLayoutCreateFlags* out);
bool toVkDescriptorPoolCreateFlagBits(const std::string& value, VkDescriptorPoolCreateFlagBits* out);
bool toVkDescriptorPoolCreateFlags(const std::string& value, VkDescriptorPoolCreateFlags* out);
bool toVkFramebufferCreateFlagBits(const std::string& value, VkFramebufferCreateFlagBits* out);
bool toVkFramebufferCreateFlags(const std::string& value, VkFramebufferCreateFlags* out);
bool toVkRenderPassCreateFlagBits(const std::string& value, VkRenderPassCreateFlagBits* out);
bool toVkRenderPassCreateFlags(const std::string& value, VkRenderPassCreateFlags* out);
bool toVkAttachmentDescriptionFlagBits(const std::string& value, VkAttachmentDescriptionFlagBits* out);
bool toVkAttachmentDescriptionFlags(const std::string& value, VkAttachmentDescriptionFlags* out);
bool toVkSubpassDescriptionFlagBits(const std::string& value, VkSubpassDescriptionFlagBits* out);
bool toVkSubpassDescriptionFlags(const std::string& value, VkSubpassDescriptionFlags* out);
bool toVkAccessFlagBits(const std::string& value, VkAccessFlagBits* out);
bool toVkAccessFlags(const std::string& value, VkAccessFlags* out);
bool toVkDependencyFlagBits(const std::string& value, VkDependencyFlagBits* out);
bool toVkDependencyFlags(const std::string& value, VkDependencyFlags* out);
bool toVkCommandPoolCreateFlagBits(const std::string& value, VkCommandPoolCreateFlagBits* out);
bool toVkCommandPoolCreateFlags(const std::string& value, VkCommandPoolCreateFlags* out);
bool toVkCommandPoolResetFlagBits(const std::string& value, VkCommandPoolResetFlagBits* out);
bool toVkCommandPoolResetFlags(const std::string& value, VkCommandPoolResetFlags* out);
bool toVkCommandBufferUsageFlagBits(const std::string& value, VkCommandBufferUsageFlagBits* out);
bool toVkCommandBufferUsageFlags(const std::string& value, VkCommandBufferUsageFlags* out);
bool toVkQueryControlFlagBits(const std::string& value, VkQueryControlFlagBits* out);
bool toVkQueryControlFlags(const std::string& value, VkQueryControlFlags* out);
bool toVkCommandBufferResetFlagBits(const std::string& value, VkCommandBufferResetFlagBits* out);
bool toVkCommandBufferResetFlags(const std::string& value, VkCommandBufferResetFlags* out);
bool toVkStencilFaceFlagBits(const std::string& value, VkStencilFaceFlagBits* out);
bool toVkStencilFaceFlags(const std::string& value, VkStencilFaceFlags* out);
bool toVkPointClippingBehavior(const std::string& value, VkPointClippingBehavior* out);
bool toVkTessellationDomainOrigin(const std::string& value, VkTessellationDomainOrigin* out);
bool toVkSamplerYcbcrModelConversion(const std::string& value, VkSamplerYcbcrModelConversion* out);
bool toVkSamplerYcbcrRange(const std::string& value, VkSamplerYcbcrRange* out);
bool toVkChromaLocation(const std::string& value, VkChromaLocation* out);
bool toVkDescriptorUpdateTemplateType(const std::string& value, VkDescriptorUpdateTemplateType* out);
bool toVkSubgroupFeatureFlagBits(const std::string& value, VkSubgroupFeatureFlagBits* out);
bool toVkSubgroupFeatureFlags(const std::string& value, VkSubgroupFeatureFlags* out);
bool toVkPeerMemoryFeatureFlagBits(const std::string& value, VkPeerMemoryFeatureFlagBits* out);
bool toVkPeerMemoryFeatureFlags(const std::string& value, VkPeerMemoryFeatureFlags* out);
bool toVkMemoryAllocateFlagBits(const std::string& value, VkMemoryAllocateFlagBits* out);
bool toVkMemoryAllocateFlags(const std::string& value, VkMemoryAllocateFlags* out);
bool toVkExternalMemoryHandleTypeFlagBits(const std::string& value, VkExternalMemoryHandleTypeFlagBits* out);
bool toVkExternalMemoryHandleTypeFlags(const std::string& value, VkExternalMemoryHandleTypeFlags* out);
bool toVkExternalMemoryFeatureFlagBits(const std::string& value, VkExternalMemoryFeatureFlagBits* out);
bool toVkExternalMemoryFeatureFlags(const std::string& value, VkExternalMemoryFeatureFlags* out);
bool toVkExternalFenceHandleTypeFlagBits(const std::string& value, VkExternalFenceHandleTypeFlagBits* out);
bool toVkExternalFenceHandleTypeFlags(const std::string& value, VkExternalFenceHandleTypeFlags* out);
bool toVkExternalFenceFeatureFlagBits(const std::string& value, VkExternalFenceFeatureFlagBits* out);
bool toVkExternalFenceFeatureFlags(const std::string& value, VkExternalFenceFeatureFlags* out);
bool toVkFenceImportFlagBits(const std::string& value, VkFenceImportFlagBits* out);
bool toVkFenceImportFlags(const std::string& value, VkFenceImportFlags* out);
bool toVkSemaphoreImportFlagBits(const std::string& value, VkSemaphoreImportFlagBits* out);
bool toVkSemaphoreImportFlags(const std::string& value, VkSemaphoreImportFlags* out);
bool toVkExternalSemaphoreHandleTypeFlagBits(const std::string& value, VkExternalSemaphoreHandleTypeFlagBits* out);
bool toVkExternalSemaphoreHandleTypeFlags(const std::string& value, VkExternalSemaphoreHandleTypeFlags* out);
bool toVkExternalSemaphoreFeatureFlagBits(const std::string& value, VkExternalSemaphoreFeatureFlagBits* out);
bool toVkExternalSemaphoreFeatureFlags(const std::string& value, VkExternalSemaphoreFeatureFlags* out);
bool toVkDriverId(const std::string& value, VkDriverId* out);
bool toVkShaderFloatControlsIndependence(const std::string& value, VkShaderFloatControlsIndependence* out);
bool toVkSamplerReductionMode(const std::string& value, VkSamplerReductionMode* out);
bool toVkSemaphoreType(const std::string& value, VkSemaphoreType* out);
bool toVkResolveModeFlagBits(const std::string& value, VkResolveModeFlagBits* out);
bool toVkResolveModeFlags(const std::string& value, VkResolveModeFlags* out);
bool toVkDescriptorBindingFlagBits(const std::string& value, VkDescriptorBindingFlagBits* out);
bool toVkDescriptorBindingFlags(const std::string& value, VkDescriptorBindingFlags* out);
bool toVkSemaphoreWaitFlagBits(const std::string& value, VkSemaphoreWaitFlagBits* out);
bool toVkSemaphoreWaitFlags(const std::string& value, VkSemaphoreWaitFlags* out);
bool toVkColorSpaceKHR(const std::string& value, VkColorSpaceKHR* out);
bool toVkPresentModeKHR(const std::string& value, VkPresentModeKHR* out);
bool toVkSurfaceTransformFlagBitsKHR(const std::string& value, VkSurfaceTransformFlagBitsKHR* out);
bool toVkCompositeAlphaFlagBitsKHR(const std::string& value, VkCompositeAlphaFlagBitsKHR* out);
bool toVkSwapchainCreateFlagBitsKHR(const std::string& value, VkSwapchainCreateFlagBitsKHR* out);
bool toVkDeviceGroupPresentModeFlagBitsKHR(const std::string& value, VkDeviceGroupPresentModeFlagBitsKHR* out);
bool toVkDisplayPlaneAlphaFlagBitsKHR(const std::string& value, VkDisplayPlaneAlphaFlagBitsKHR* out);
bool toVkPerformanceCounterUnitKHR(const std::string& value, VkPerformanceCounterUnitKHR* out);
bool toVkPerformanceCounterScopeKHR(const std::string& value, VkPerformanceCounterScopeKHR* out);
bool toVkPerformanceCounterStorageKHR(const std::string& value, VkPerformanceCounterStorageKHR* out);
bool toVkPerformanceCounterDescriptionFlagBitsKHR(const std::string& value, VkPerformanceCounterDescriptionFlagBitsKHR* out);
bool toVkAcquireProfilingLockFlagBitsKHR(const std::string& value, VkAcquireProfilingLockFlagBitsKHR* out);
bool toVkPipelineExecutableStatisticFormatKHR(const std::string& value, VkPipelineExecutableStatisticFormatKHR* out);
bool toVkDebugReportObjectTypeEXT(const std::string& value, VkDebugReportObjectTypeEXT* out);
bool toVkDebugReportFlagBitsEXT(const std::string& value, VkDebugReportFlagBitsEXT* out);
bool toVkRasterizationOrderAMD(const std::string& value, VkRasterizationOrderAMD* out);
bool toVkShaderInfoTypeAMD(const std::string& value, VkShaderInfoTypeAMD* out);
bool toVkValidationCheckEXT(const std::string& value, VkValidationCheckEXT* out);
bool toVkConditionalRenderingFlagBitsEXT(const std::string& value, VkConditionalRenderingFlagBitsEXT* out);
bool toVkSurfaceCounterFlagBitsEXT(const std::string& value, VkSurfaceCounterFlagBitsEXT* out);
bool toVkDisplayPowerStateEXT(const std::string& value, VkDisplayPowerStateEXT* out);
bool toVkDeviceEventTypeEXT(const std::string& value, VkDeviceEventTypeEXT* out);
bool toVkDisplayEventTypeEXT(const std::string& value, VkDisplayEventTypeEXT* out);
bool toVkDiscardRectangleModeEXT(const std::string& value, VkDiscardRectangleModeEXT* out);
bool toVkConservativeRasterizationModeEXT(const std::string& value, VkConservativeRasterizationModeEXT* out);
bool toVkDebugUtilsMessageSeverityFlagBitsEXT(const std::string& value, VkDebugUtilsMessageSeverityFlagBitsEXT* out);
bool toVkDebugUtilsMessageTypeFlagBitsEXT(const std::string& value, VkDebugUtilsMessageTypeFlagBitsEXT* out);
bool toVkBlendOverlapEXT(const std::string& value, VkBlendOverlapEXT* out);
bool toVkValidationCacheHeaderVersionEXT(const std::string& value, VkValidationCacheHeaderVersionEXT* out);
bool toVkQueueGlobalPriorityEXT(const std::string& value, VkQueueGlobalPriorityEXT* out);
bool toVkPipelineCompilerControlFlagBitsAMD(const std::string& value, VkPipelineCompilerControlFlagBitsAMD* out);
bool toVkTimeDomainEXT(const std::string& value, VkTimeDomainEXT* out);
bool toVkMemoryOverallocationBehaviorAMD(const std::string& value, VkMemoryOverallocationBehaviorAMD* out);
bool toVkPipelineCreationFeedbackFlagBitsEXT(const std::string& value, VkPipelineCreationFeedbackFlagBitsEXT* out);
bool toVkPerformanceConfigurationTypeINTEL(const std::string& value, VkPerformanceConfigurationTypeINTEL* out);
bool toVkQueryPoolSamplingModeINTEL(const std::string& value, VkQueryPoolSamplingModeINTEL* out);
bool toVkPerformanceOverrideTypeINTEL(const std::string& value, VkPerformanceOverrideTypeINTEL* out);
bool toVkPerformanceParameterTypeINTEL(const std::string& value, VkPerformanceParameterTypeINTEL* out);
bool toVkPerformanceValueTypeINTEL(const std::string& value, VkPerformanceValueTypeINTEL* out);
bool toVkShaderCorePropertiesFlagBitsAMD(const std::string& value, VkShaderCorePropertiesFlagBitsAMD* out);
bool toVkToolPurposeFlagBitsEXT(const std::string& value, VkToolPurposeFlagBitsEXT* out);
bool toVkValidationFeatureEnableEXT(const std::string& value, VkValidationFeatureEnableEXT* out);
bool toVkValidationFeatureDisableEXT(const std::string& value, VkValidationFeatureDisableEXT* out);
bool toVkLineRasterizationModeEXT(const std::string& value, VkLineRasterizationModeEXT* out);

#endif // VULKAN_TYPES_HPP_INCLUDED

