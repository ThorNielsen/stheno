#ifndef VULKAN_DEVICE_HPP_INCLUDED
#define VULKAN_DEVICE_HPP_INCLUDED

#include "vulkan/common.hpp"

#include <string>
#include <vector>

std::vector<const char*>
getSupportedVulkanLayers(const std::vector<std::string>& layers);

std::vector<const char*>
getSupportedVulkanInstanceExtensions(const std::vector<std::string>& required,
                                     const std::vector<std::string>& optional);

std::vector<const char*>
getSupportedVulkanDeviceExtensions(VkPhysicalDevice device,
                                   const std::vector<std::string>& required,
                                   const std::vector<std::string>& optional);

std::vector<const char*>
getSupportedVulkanExtensions(const std::vector<VkExtensionProperties>& extensionProps,
                             const std::vector<std::string>& required,
                             const std::vector<std::string>& optional,
                             std::string_view extensionType);

#endif // VULKAN_DEVICE_HPP_INCLUDED
