#ifndef VULKAN_COMMON_HPP_INCLUDED
#define VULKAN_COMMON_HPP_INCLUDED

#include <kraken/globaltypes.hpp>
#include <vulkan/vulkan.h>

#include <filesystem>
#include <optional>
#include <vector>

namespace fs = std::filesystem;

struct QueueSelectionInfo
{
    VkQueueFlagBits requiredFlags;
    VkQueueFlagBits optionalFlags;
    float priority;
    VkBool32 requirePresentSupport;
};

struct QueueInformation
{
    U32 familyIndex;
    U32 queueIndex;
};

template <typename Return, typename VulkanFunction, typename... Args>
std::vector<Return> getArrayOfStructs(VulkanFunction func, Args&&... args)
{
    U32 count;
    func(std::forward<Args>(args)..., &count, nullptr);
    std::vector<Return> data(count);
    func(std::forward<Args>(args)..., &count, data.data());
    if (count != data.size())
    {
        data.resize(count);
    }
    return data;
}

std::optional<VkPhysicalDevice>
selectBestPhysicalDevice(VkInstance instance,
                         bool requireGPU = false);

std::pair<std::vector<VkDeviceQueueCreateInfo>, std::unique_ptr<std::vector<float>[]>>
selectDeviceQueues(VkPhysicalDevice device,
                   VkSurfaceKHR surface,
                   U32 infoCount,
                   const QueueSelectionInfo* selectionInfo,
                   QueueInformation* queueInformation);

U32 getSuitableMemoryType(VkPhysicalDevice device,
                          VkMemoryPropertyFlags requiredProperties,
                          U32 allowedTypes);

U32 getSuitableMemoryType(VkPhysicalDeviceMemoryProperties memprops,
                          VkMemoryPropertyFlags requiredProperties,
                          U32 allowedTypes);

fs::path findDirectory(std::string_view name,
                       std::vector<fs::path> searchDirectories);

VkImageCreateInfo simpleImageCreateInfo(VkFormat format,
                                        VkExtent3D extents,
                                        VkImageUsageFlags usageFlags);

using ShaderInputAttributeDescription = std::vector<VkVertexInputAttributeDescription>;
ShaderInputAttributeDescription canonicalForm(ShaderInputAttributeDescription desc);
void toCanonicalForm(ShaderInputAttributeDescription& desc);

bool operator<(const VkVertexInputAttributeDescription&,
               const VkVertexInputAttributeDescription&);
bool operator<(const ShaderInputAttributeDescription&,
               const ShaderInputAttributeDescription&);

#endif // VULKAN_COMMON_HPP_INCLUDED
