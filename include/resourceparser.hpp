#ifndef RESOURCEPARSER_HPP_INCLUDED
#define RESOURCEPARSER_HPP_INCLUDED

#include "resourceloader.hpp"

#include <streambuf>
#include <kraken/image/image.hpp>
#include <kraken/graphics/import.hpp>

class ImageTransferrer : public ResourceParser
{
public:
    VkResult initialiseTransfer(VkCommandBuffer commandBuf,
                                VkDevice device,
                                VmaAllocator allocator,
                                const kraken::JSONValue& options,
                                ResourceLoader::DataSource source,
                                ResourceLoader::VulkanResource* resourceOut) override;
    bool getNextTransferData(ResourceLoader::DataSource,
                             VkDeviceSize maxSize,
                             const U8** dataOut,
                             size_t* sizeOut,
                             const void** tagOut,
                             U32*) override;

    VkResult transfer(VkCommandBuffer commandBuf,
                      VkBuffer buffer,
                      VkDeviceSize bufferOffset,
                      VkDeviceSize bufferSize,
                      const void* tag,
                      U32,
                      ResourceLoader::VulkanResource* resourceOut) override;

protected:
    virtual void readImageData(ResourceLoader::DataSource source) = 0;

    U32 mp_width = 0;
    U32 mp_height = 0;
    const U8* mp_data = nullptr;
    size_t mp_rowLength = 0;

    size_t m_currLine = 0;
};

class ImageParser : public ImageTransferrer
{
public:
    static void addResourceSupport(ResourceLoader& loader, std::string mediaType);

    bool parse(const U8* data, size_t size,
               const kraken::JSONValue& options,
               ResourceLoader&,
               ResourceLoader::HostResource* resourceOut) override;

protected:
    void readImageData(ResourceLoader::DataSource source) override;

private:
    kraken::image::Image* m_image = nullptr;
    size_t m_currLine = 0;
};

class MeshTransferrer : public ResourceParser
{
public:
    VkResult initialiseTransfer(VkCommandBuffer commandBuf,
                                VkDevice device,
                                VmaAllocator allocator,
                                const kraken::JSONValue& options,
                                ResourceLoader::DataSource,
                                ResourceLoader::VulkanResource* resourceOut) override;

    bool getNextTransferData(ResourceLoader::DataSource source,
                             VkDeviceSize maxSize,
                             const U8** dataOut,
                             size_t* sizeOut,
                             const void** tagOut,
                             U32* extraTagOut) override;

    VkResult transfer(VkCommandBuffer commandBuf,
                      VkBuffer buffer,
                      VkDeviceSize bufferOffset,
                      VkDeviceSize bufferSize,
                      const void* tag,
                      U32 extraTag,
                      ResourceLoader::VulkanResource* resourceOut) override;

protected:
    Mesh* mp_mesh = nullptr;

private:
    const U8* m_currPtr = nullptr;
    U32 m_currId = 0;
};

class PLYParser : public MeshTransferrer
{
public:
    bool parse(const U8* data, size_t size,
               const kraken::JSONValue& options,
               ResourceLoader&,
               ResourceLoader::HostResource* resourceOut) override;
};

class STLParser : public MeshTransferrer
{
public:
    bool parse(const U8* data, size_t size,
               const kraken::JSONValue& options,
               ResourceLoader&,
               ResourceLoader::HostResource* resourceOut) override;
};

/// Temporary utility memory stream, to convert between data in memory and
/// std::istreams. @todo move to separate utility file.

struct MemoryBuffer : public std::streambuf
{
    MemoryBuffer(const U8* data, size_t size)
    {
        auto* ptr = reinterpret_cast<char*>(const_cast<U8*>(data));
        this->setg(ptr, ptr, ptr + size);
    }
};

struct MemoryIStream : public virtual MemoryBuffer, public virtual std::istream
{
    MemoryIStream(const U8* data, size_t size)
        : MemoryBuffer(data, size),
          std::istream(static_cast<std::streambuf*>(this)) {}
};

#endif // RESOURCEPARSER_HPP_INCLUDED
