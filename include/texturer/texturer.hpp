#ifndef TEXTURER_TEXTURER_HPP_INCLUDED
#define TEXTURER_TEXTURER_HPP_INCLUDED

/// Remaining stuff to do:
/// [x] Implement option parsing. Also help/documentation.
/// [x] Clean up output.
/// [.] Clean up code.
/// [x] Implement restricted texturing (e.g. restrict to [0,0.5]x[0,0.5] so only
///     one fourth of the final texture is constructed); this can be used to
///     create much larger textures than whatever the hardware currently
///     constrains us to. E.g. there will be no theoretical max texture
///     dimensions, memory usage for all framebuffer attachments can be
///     constrained, etc.
/// [x] Implement sharpness-based quality evaluation (to include in parameters);
///     on CPU (so for qualitymap when parsing).
/// [.] Implement depth difference computation (height map for bump mapping).
/// [x] Add tangent space computation option (mikktspace seems promising). This
///     should be able to be turned both on/off, so it is possible to choose
///     whether the maps should be generated in object space or tangent space.
/// [x] Implement texture post-processing options like expanding to fill blank
///     areas (so that e.g. a colour texture outside of the used region will be
///     filled with colours from the closest part of the used region so
///     mipmapping by simple scaling works).
/// [x] Implement different export options -- RGBA?(8|16)-png as minimum.
/// [x] Write out metadata file for e.g. height map, containing parameters, etc.
///     just so it is easier to reproduce and identify runs. For example, just
///     the used options file in some log (add a --log-file parameter), and add
///     option to load configuration files (--conf [...]); then the "log" can
///     simply be a dump of the current configuration.
/// [x] Add option to write alpha map to a separate file.
/// [ ] Allow fine-grained control of ResourceLoader staging area size and
///     chunk size.
/// [ ] Allow configuration of progress bar -- whether to colour it, and the
///     length of it.
/// [.] Write out focus parameters/info to files to allow for quicker re-runs.
///     Thus this also needs ResourceLoader to support optional loads -- along
///     the line of: Also load "00025.png.focus" if it exists, otherwise just
///     compute the focus parameters and write them to this file. Perhaps
///     ResourceLoader should also have some way of writing data TO disk, such
///     that both are not done at the same time.
/// [x] Don't unload images if they fit in main memory. That is, add an option
///     to define a number of images to keep in memory. A computer with 128GiB
///     of memory should easily be able to hold hundreds of images, which means
///     we do not need to re-read and re-compute focus/quality stuff all the
///     time.
/// [ ] Implement depth discontinuity filter -- remove all quality from any area
///     with large depth discontinuities (missing/0 depth no (big) problem).

#include <filesystem>
#include <unordered_set>
#include <kraken/globaltypes.hpp>
#include <kraken/image/image.hpp>


#include "vulkan/common.hpp"
#include "vulkan/framegraph.hpp"
#include "vulkan/image.hpp"
#include "vulkan/pipeline.hpp"
#include "vulkan/shader.hpp"

#include "resourceloader.hpp"
#include "texturer/dmapparser.hpp"

using namespace kraken::math;
namespace fs = std::filesystem;

struct TexturerOptions
{
    // General texturer options
    fs::path meshFile;
    fs::path dmapDirectory;
    fs::path gltfDest;
    fs::path colourTextureDest;
    fs::path normalTextureDest;
    fs::path displacementTextureDest;
    fs::path alphaTextureDest;
    fs::path metadataDest;
    U32 maxThreads;
    U32 maxLoadCapacity;

    // Hidden general options (i.e. often debug options)
    fs::path internalDataDirectory;
    U32 debugLevel;

    bool dumpImages;

    // General texture options
    U32 width;
    U32 height;
    vec2 bottomLeft;
    vec2 topRight;
    bool expandTextures;
    std::string colourExportFileType;
    std::string normalExportFileType;
    std::string displacementExportFileType;
    std::string alphaExportFileType;
    kraken::image::ColourType colourFormat;
    kraken::image::ColourType normalFormat;
    kraken::image::ColourType displacementFormat;
    kraken::image::ColourType alphaFormat;

    // Quality estimator options
    F32 focusHighpassSigma;
    F32 focusLowerCutoff;
    U32 focusCutoffMeanIterations;
    F32 maxAngle;
    F32 minConfidence;
    F32 blurSigma;
    F32 expandRadius;
    F32 expandSigma;
    F32 expandMinConfidence;

    // Shader options
    F32 qualityWeight;
    F32 normalWeight;
    F32 distanceWeight;
    F32 squaredDistanceWeight;

    F32 focusWeight;

    F32 cosineGoodCutoff;
    F32 cosineBadCutoff;
    F32 cosineDiscard;

    F32 focusSDNearCutoff;
    F32 focusSDFarCutoff;
    F32 focusSDFarDiscard;

    U32 qualityDiscardParameter;
    U32 qualityTransformFunction;
    F32 qualityTransformPar0;
    F32 qualityTransformPar1;

    // Postprocessing options
    U32 relaxIterations;
    F32 cosineGoodRelaxFactor;
    F32 cosineBadRelaxFactor;
    F32 cosineDiscardRelaxFactor;
    F32 normalWeightRelaxFactor;
};

struct TexturerOutput
{
    kraken::image::Image colourImage;
    kraken::image::Image normalImage;
    kraken::image::Image displacementImage;
    kraken::image::Image alphaImage;
    kraken::JSONValue metadata;
};

/// Below: Put in .cpp instead.

struct TileOutput
{
    kraken::image::Image colourImage;
    kraken::image::Image normalImage;
    FloatImage displacementImage;
    kraken::image::Image alphaImage;
};

struct QualityMapperInputs
{
    mat4 camRot;
    mat4 invCamRot;
    mat4 rotation;
    mat4 inverseRotation;
    vec4 position;
    float qualityWeight;
    float normalWeight;
    float distanceWeight;
    float squaredDistanceWeight;

    float cosAngleGoodCutoff;
    float cosAngleBadCutoff;
    float cosAngleDiscard;
    float unused0 = 0.f;

    float focusMean = 0.f;
    float focusSD = 0.f;
    float unused1 = 0.f;
    float unused2 = 0.f;

    float focusSDNearCutoff;
    float focusSDFarCutoff;
    float focusSDFarDiscard;
    float focusWeight;

    U32 discardLessThanMean = 1;
    U32 qualityTransform = 0;
    float qualityTransformParameter0 = 0.f;
    float qualityTransformParameter1 = 0.f;

    U32 inputDepthTextureWidth = 0;
    U32 inputDepthTextureHeight = 0;
};

struct VulkanDMAP
{
    std::string colourImageFilename;
    Image normalDepthMap;
    Image confidenceMap;
    Image qualityMap;
    Image colourImage;
    FocusInfo focusInfo;
    dmat3 camera;
    dmat3 rotation;
    dvec3 position;
    U32 width;
    U32 height;
};


class Texturer
{
public:
    Texturer();

    // Decimate values along an axis determines how many tiles/chunks will be
    // rendered along that axis; the number is 2^d, where d is the decimate
    // value (a way to think of this value is the number of times to
    // succesively halve the resolution of the final output for the rendered
    // tiles).
    TexturerOutput run(TexturerOptions opts,
                       U32 xTiles = 1,
                       U32 yTiles = 1);

    ~Texturer();

    const Mesh& getMesh() { return m_mesh; }

private:
    struct DMAPLoadInfo
    {
        fs::path path;
        std::shared_ptr<DMAPContent> content;
        ///bool isCurrentlyParsing = false;
    };

    void expandTexturesByAlpha(TexturerOutput& output) const;
    TileOutput renderTile(TexturerOptions opts, bool isOnlyTile);

    // Set up instance, device, allocator, resource loader, etc.
    // This also enqueues mesh loading, sets up DMAP paths, enqueues the
    // relevant number of DMAPS, etc.
    void init(const TexturerOptions& opts);

    void drawMeshToTexture(const TexturerOptions& opts,
                           Image& positionOut,
                           Image& normalOut,
                           Image& tangentOut);

    void runQualityMapper(const TexturerOptions& opts,
                          const Image& position,
                          const Image& normal,
                          Image& statsOut);

    void runColourRenderer(const TexturerOptions& opts,
                           const Image& position,
                           const Image& normal,
                           const Image& tangent,
                           const Image& stats,
                           Image& colourOut,
                           Image& normalOut);

    // Set debugWritePath, debugWidth, debugHeight AND debugChannels to
    // appropriate values in order to write (an approximation of) the attachment
    // with size debugWidth × debugHeight and debugChannels number of floating-
    // point channels to the image at debugWritePath.
    void transitionAttachment(VkImage image,
                              VkImageLayout currentLayout,
                              VkImageLayout finalLayout,
                              VkPipelineStageFlags srcStage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                              VkImageAspectFlags aspect = VK_IMAGE_ASPECT_COLOR_BIT,
                              fs::path debugWritePath = "",
                              U32 debugWidth = 0,
                              U32 debugHeight = 0,
                              U32 debugChannels = 0);

    // The input image MUST be in VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL.
    Image copyImage(VkImage sourceImage,
                    U32 width, U32 height,
                    VkImageAspectFlags aspect,
                    VkAccessFlags access,
                    VkImageLayout newLayout,
                    VkImageUsageFlags extraFlags,
                    bool addToDestructionQueue);

    QualityMapperInputs extractQualityMapperInputs(const TexturerOptions& opts) const;

    void ensureMeshLoaded();

    Pipeline createMeshPipeline(U32 width,
                                U32 height,
                                const Mesh* mesh,
                                const GraphicsShader& shader,
                                VkRenderPass pass,
                                U32 subpass,
                                const VkDescriptorSetLayout* setLayouts,
                                U32 setLayoutCount,
                                bool blend);

    Pipeline createQualityMapperPipeline(U32 width,
                                         U32 height,
                                         const GraphicsShader& shader,
                                         VkRenderPass pass,
                                         U32 subpass,
                                         const VkDescriptorSetLayout* setLayouts,
                                         U32 setLayoutCount,
                                         U32 passCount = 1,
                                         VkBlendOp alphaOp = VK_BLEND_OP_MAX);

    bool acquireReadyVulkanDMAP(std::vector<ResourceLoader::ResourceID>& loadIDs,
                                VulkanDMAP* dmapOut);

    VulkanDMAP acquireVulkanDMAP(std::vector<ResourceLoader::ResourceID>& loadIDs);

    std::vector<DMAPLoadInfo> getDepthmapPaths(fs::path dmapDirectory);

    void enqueueDMAPLoads();

    void clearImage(VkImage image,
                    VkImageLayout initialLayout,
                    VkImageLayout finalLayout,
                    VkAccessFlags srcAccess,
                    VkAccessFlags dstAccess,
                    VkClearColorValue ccv);

    void updateQualityMapperInputs(QualityMapperInputs& qmInputs,
                                   const VulkanDMAP& dmap,
                                   VmaAllocation alloc);

    U32 writeBarriers(VkImageMemoryBarrier* barriers,
                      U32 maxBarrierCount,
                      const VulkanDMAP& dmap) const;


    VkPhysicalDeviceLimits getDeviceLimits(VkPhysicalDevice device);
    VkPhysicalDevice selectPhysicalDevice(VkInstance instance);

    VkResult createInstance(std::string appname,
                            bool enableValidation = true);
    VkResult createDevice();
    VkResult createAllocator();
    VkResult createResourceloader(fs::path dataDirectory,
                                  QueueInformation queueInfo);

    VkResult immediateExecute(const std::function<void(VkCommandBuffer)>&);

    BufferAllocation createBuffer(VkDeviceSize size,
                                  VkBufferUsageFlags bufFlags,
                                  VmaMemoryUsage memUsage);

    void pushAllDMAPIndices();
    bool isDMAPCacheFull() const;
    bool canLoadNextDMAPIndex(U32 index) const;

    // Purges *one* inactive DMAP if that is possible to do.
    bool tryPurgeInactiveDMAP();


    // This checks that no more than the max load capacity of DMAPs is contained
    // in the cache.
    bool ensureCacheNotFull(bool throwOnFull);

    void copyBuffer(VkBuffer source, VkBuffer dest, VkDeviceSize size,
                    VkDeviceSize srcOffset = 0, VkDeviceSize dstOffset = 0);


    // Destruction happens in groups, to allow for partial destruction.
    // E.g. group 0 is the core elements, which are to be destroyed when this
    // class is destroyed, while swapchainGroup (defined below) is for stuff
    // that should be destroyed when the swapchain is to be destroyed.
    void addDestructor(size_t group, std::function<void(void)> fun)
    {
        while (m_destructors.size() <= group)
        {
            m_destructors.push_back({});
        }
        m_destructors[group].push_back(fun);
    }
    void performDestruction(size_t group = 0)
    {
        while (m_destructors.size() > group)
        {
            while (!m_destructors.back().empty())
            {
                m_destructors.back().back()();
                m_destructors.back().pop_back();
            }
            m_destructors.pop_back();
        }
    }

    std::vector<std::vector<std::function<void()>>> m_destructors;
    ResourceLoader m_resourceLoader;
    SamplerCache m_samplerCache;
    DescriptorAllocator m_descAllocator;

    // Stuff specific to texturing algorithm.

    // The next two describes the DMAPs which are currently in memory in some
    // form or another. Inactive means that they are already loaded and can
    // safely be deleted, while active means it is currently being used as a
    // parse source or something and CANNOT safely be deleted.
    std::unordered_set<U32> m_cachedInactiveDMAPs;
    std::unordered_set<U32> m_cachedActiveDMAPs;

    std::vector<ResourceLoader::ResourceID> m_dmapLoadIDs;
    std::map<ResourceLoader::ResourceID, U32> m_loadIDsToIndices;
    std::vector<DMAPLoadInfo> m_dmapLoadInfo;

    std::vector<U32> m_dmapIndicesToLoad;

    ResourceLoader::Options m_dmapLoadOpts;
    U32 m_maxLoadCapacity = 0;
    ResourceLoader::ResourceID m_currentTransferID = ResourceLoader::invalidResource;
    ResourceLoader::ResourceID m_meshLoadID = ResourceLoader::invalidResource;
    Mesh m_mesh;

    // Vulkan loading stuff only
    std::vector<std::string> m_wantedLayers;
    std::vector<const char*> m_enabledLayers;
    std::vector<QueueInformation> m_queueInfo;
    VkPhysicalDeviceLimits m_deviceLimits;
    // End vulkan loading stuff only

    // Required Vulkan variables
    VkDebugUtilsMessengerEXT m_debugUtils;
    VmaAllocator m_allocator;
    VkQueue m_graphicsQueue;
    VkDevice m_device;
    VkPhysicalDevice m_physicalDevice;
    VkInstance m_instance;
    VkCommandPool m_transientCommandPool;
};

int texturerMain(int argc, char* argv[]);

#endif // TEXTURER_TEXTURER_HPP_INCLUDED
