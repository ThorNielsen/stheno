#ifndef TEXTURER_DMAPPARSER_HPP_INCLUDED
#define TEXTURER_DMAPPARSER_HPP_INCLUDED

#include "resourceparser.hpp"
#include <kraken/math/matrix.hpp>
#include <kraken/utility/memory.hpp>

using namespace kraken::math;

class FloatImage
{
public:
    FloatImage()
        : m_width{0}, m_height{0}, m_channels{0} {}
    FloatImage(U32 width_, U32 height_, U32 channels_)
        : m_width{0}, m_height{0}, m_channels{0}
    {
        resize(width_, height_, channels_);
    }

    void setPixel(U32 x, U32 y, vec4 value)
    {
        size_t idx = (y*m_width+x)*m_channels;
        for (U32 i = 0; i < m_channels; ++i)
        {
            m_data[idx+i] = value[i];
        }
    }
    void setPixel(U32 x, U32 y, float value)
    {
        size_t idx = (y*m_width+x)*m_channels;
        for (U32 i = 0; i < m_channels; ++i)
        {
            m_data[idx+i] = value;
        }
    }
    vec4 getPixel(U32 x, U32 y) const
    {
        size_t idx = (y*m_width+x)*m_channels;
        vec4 val{0, 0, 0, 0};
        for (U32 i = 0; i < m_channels; ++i)
        {
            val[i] = m_data[idx+i];
        }
        return val;
    }

    void resize(U32 width_, U32 height_, U32 channels_)
    {
        m_data.resize(width_*height_*channels_);
        m_width = width_;
        m_height = height_;
        m_channels = channels_;
    }

    U32 width() const { return m_width; }
    U32 height() const { return m_height; }
    U32 channels() const { return m_channels; }

    F32* data() { return m_data.data(); }
    const F32* data() const { return m_data.data(); }

    size_t floatCount() const { return m_data.size(); }
    size_t byteSize() const { return floatCount()*sizeof(decltype(m_data[0])); }

    bool empty() const { return m_data.empty(); }

private:
    std::vector<F32> m_data;
    U32 m_width;
    U32 m_height;
    U32 m_channels;
};


FloatImage blurNormals(const FloatImage& image);

kraken::image::Image toKrakenImage(const FloatImage& floatImg,
                                   vec4* minValuesOut = nullptr,
                                   vec4* maxValuesOut = nullptr);

struct DMAPHeader
{
    constexpr static U32 depthMask = 0x1;
    constexpr static U32 normalMask = 0x2;
    constexpr static U32 confidenceMask = 0x4;
    U16 magic;
    U8 type;
    U8 padding;
    U32 imageWidth;
    U32 imageHeight;
    U32 depthWidth;
    U32 depthHeight;
    float minDepth;
    float maxDepth;
};

static_assert(sizeof(DMAPHeader) == 7*4, "DMAPHeader not tightly packed.");


struct FocusInfo
{
    float depthMean;
    float depthSD;
};

struct DMAPContent
{
    std::string filename;
    std::vector<U32> neighbours;
    dmat3 camera;
    dmat3 rotation;
    dvec3 position;

    FocusInfo focusInfo;

    FloatImage normalDepthMap; // Combined normal-depth map where normals are
                               // stored in .rgb, and depth in .w.
    FloatImage confidenceMap;
    FloatImage qualityMap; // Generated quality map.
    kraken::image::Image colourImage;

    dvec3 worldPosToCameraSpace(dvec3 worldPos) const
    {
        return camera * rotation * (worldPos - position);
    }

    dvec2 worldPosToImagePos(dvec3 worldPos) const
    {
        auto csPos = worldPosToCameraSpace(worldPos);
        return dvec2{csPos.x, csPos.y} / csPos.z;
    }

    vec3 imagePosToWorldPos(ivec2 imagePos) const
    {
        if (imagePos.x < 0
            || static_cast<U32>(imagePos.x) >= normalDepthMap.width()
            || imagePos.y < 0
            || static_cast<U32>(imagePos.y) >= normalDepthMap.height())
        {
            return vec3{0, 0, 0};
        }
        vec3 camPos{static_cast<float>(imagePos.x),
                    static_cast<float>(imagePos.y),
                    normalDepthMap.getPixel(imagePos.x, imagePos.y).w};
        camPos.x *= camPos.z;
        camPos.y *= camPos.z;
        auto invRot = inverse(prec_cast<F32>(rotation));
        auto invCam = inverse(prec_cast<F32>(camera));
        return invRot * invCam * camPos + prec_cast<F32>(position);
    }
};

// This class implements a DMAP parser and transferrer.
// Parser returns DMAPContent.
// The Vulkan resource is a subresource list, with three or four images:
// [0] normalDepthMap  (VK_FORMAT_R32G32B32A32_SFLOAT)
// [1] confidenceMap   (VK_FORMAT_R32_SFLOAT)
// [2] qualityMap      (VK_FORMAT_R32_SFLOAT)
// [3] colour          (usually VK_FORMAT_R8G8B8A8_SRGB) (optional)
// In this order.
// TODO (search for [3fbe992a] in .cpp for implementation): support not loading/
// transferring e.g. confidenceMap.
class DMAPParser : public ImageTransferrer
{
public:
    struct DMAPRetransferDataSource
    {
        ResourceLoader::DataSource dataSource;
        std::shared_ptr<DMAPContent> content;
    };

    static const std::function<void(void*)>* getDeleter();
    static void addResourceSupport(ResourceLoader& loader, std::string mediaType);

    void getAdditionalRequiredFiles(ResourceLoader::FileInfo* fileInfoOut,
                                    U32* fileInfoCount,
                                    ResourceLoader& resourceLoader,
                                    const ResourceLoader::RawData& files) override;

    bool parse(const U8* data, size_t size,
               const kraken::JSONValue& options,
               ResourceLoader& loader,
               ResourceLoader::HostResource* resourceOut) override;

    static DMAPRetransferDataSource
    createDataSourceFromPreparsedDMAP(std::shared_ptr<DMAPContent> content)
    {
        DMAPRetransferDataSource dmapSrc;
        dmapSrc.content = content;
        auto& source = dmapSrc.dataSource;
        source.fromType = ResourceLoader::ResourceType::HostResource;
        source.data = dmapSrc.content.get();
        source.size = 0;
        return dmapSrc;
    }
    // This makes a new data source from a pre-parsed DMAP, enabling this to
    // transfer to Vulkan again.
    static DMAPRetransferDataSource
    createDataSourceFromPreparsedDMAP(DMAPContent&& content)
    {
        auto ptr = std::make_shared<DMAPContent>();
        *ptr.get() = std::move(content);
        return createDataSourceFromPreparsedDMAP(ptr);
    }

protected:
    void readImageData(ResourceLoader::DataSource source) override
    {
        if (source.size) throw std::logic_error("Expected data structure (not byte data).");
        if (!source.data) throw std::logic_error("Expected data pointer.");
        auto src = *reinterpret_cast<const std::pair<U32, const void*>*>(source.data);
        if (src.first == 1)
        {
            const auto& image = *static_cast<const kraken::image::Image*>(src.second);
            mp_width = image.width();
            mp_height = image.height();
            mp_data = image.data();
            mp_rowLength = image.rowBytes();
        }
        else
        {
            const auto& image = *static_cast<const FloatImage*>(src.second);
            mp_width = image.width();
            mp_height = image.height();
            mp_data = reinterpret_cast<const U8*>(image.data());
            mp_rowLength = image.width() * 4 * image.channels();
        }
    }

private:
    std::pair<U32, const void*> m_sources[4];

    fs::path m_infoCachePath;
    fs::path m_dmapDirectory;
    std::string m_imageID;
}; //*/

// Return value determines whether the image was successfully written or not.
bool writeImage(fs::path out, float* floatImg, U32 width, U32 height);
void write1CImage(fs::path out, float* floatImg, U32 width, U32 height);

void dumpInfo(const DMAPContent& content);
void dumpInfo(const DMAPHeader& header);

#endif // TEXTURER_DMAPPARSER_HPP_INCLUDED
