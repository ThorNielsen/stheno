#ifndef MATH_TRANSFORM_HPP_INCLUDED
#define MATH_TRANSFORM_HPP_INCLUDED

#include <kraken/math/matrix.hpp>
#include <kraken/math/transform.hpp>
#include <cmath>

// Creates a transformation matrix from some three-dimensional space described
// by the three vectors, into a right-handed three-dimensional space where x is
// to the left, y is DOWN and z is pointing inwards.
// Note that the fourth dimension merely stores the translation.
template <typename Prec>
inline kraken::math::Matrix<4, 4, Prec>
vulkanLookat(const kraken::math::Vector<3, Prec>& eye,
             const kraken::math::Vector<3, Prec>& point,
             const kraken::math::Vector<3, Prec>& up)
{
    auto d = kraken::math::normalise(point - eye);
    auto s = kraken::math::normalise(cross(d, up));
    auto u = kraken::math::cross(s, d);
    kraken::math::Matrix<4, 4, Prec> m(Prec(1));
    m(0, 0) = s.x;
    m(0, 1) = s.y;
    m(0, 2) = s.z;
    m(1, 0) = -u.x;
    m(1, 1) = -u.y;
    m(1, 2) = -u.z;
    m(2, 0) = d.x;
    m(2, 1) = d.y;
    m(2, 2) = d.z;
    m(0, 3) = -dot(s, eye);
    m(1, 3) =  dot(u, eye); // There is no forgotten '-' on this line.
    m(2, 3) = -dot(d, eye);
    m(3, 3) = 1;
    return m;
}

template <typename Prec>
inline kraken::math::Matrix<4, 4, Prec>
vulkanPerspective(Prec fovy, Prec aspect, Prec near, Prec far)
{
    auto scale = std::tan(fovy/Prec(2));
    kraken::math::Matrix<4, 4, Prec> m(Prec(0));
    m(0, 0) = Prec(1) / (scale * aspect);
    m(1, 1) = Prec(1) / scale;
    m(2, 2) = far / (far - near);
    m(3, 2) = Prec(1);
    m(2, 3) = -far * near / (far - near);
    return m;
}

#endif // MATH_TRANSFORM_HPP_INCLUDED
