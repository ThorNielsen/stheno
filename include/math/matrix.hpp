#ifndef MATH_MATRIX_HPP_INCLUDED
#define MATH_MATRIX_HPP_INCLUDED

#include <kraken/math/matrix.hpp>

using kraken::math::vec2;
using kraken::math::dvec2;
using kraken::math::ivec2;
using kraken::math::uvec2;
using kraken::math::vec3;
using kraken::math::dvec3;
using kraken::math::ivec3;
using kraken::math::uvec3;
using kraken::math::vec4;
using kraken::math::dvec4;
using kraken::math::ivec4;
using kraken::math::uvec4;
using kraken::math::mat2;
using kraken::math::dmat2;
using kraken::math::imat2;
using kraken::math::umat2;
using kraken::math::mat3;
using kraken::math::dmat3;
using kraken::math::imat3;
using kraken::math::umat3;
using kraken::math::mat4;
using kraken::math::dmat4;
using kraken::math::imat4;
using kraken::math::umat4;
using kraken::math::quat;
using kraken::math::dquat;

#endif // MATH_MATRIX_HPP_INCLUDED
