Texturer for 3d-reconstructions
===============================

Introduction
------------
This is intended to function as a replacement for the built-in texturer in [OpenMVS](https://github.com/cdcseacave/openMVS), in order to support custom UV maps, configurable texture sizes, normal maps, and various other improvements in a much more efficient manner and with higher-quality output. A major design goal was to eliminate the need for all images to be stored in RAM, enabling this to run efficiently on virtually any hardware supporting Vulkan in any form (including software rendering). Furthermore, the runtime is virtually unaffected by the complexity of the mesh due to it only needing to be rendered a single time in practically all cases.

Building
--------
This uses CMake, and the only dependencies are Vulkan and [kraken](https://gitlab.com/ThorNielsen/kraken). To build, use

```bash
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make -j$(nproc)
sudo make install # Optional for system-wide installation.
```

To run this, simply use `stheno texturer --help` to list options and usage instructions.
